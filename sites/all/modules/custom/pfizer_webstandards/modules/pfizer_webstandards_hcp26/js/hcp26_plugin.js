jQuery.fn.bindFirst = function (name, fn) {
  this.on(name, fn);
  this.each(function () {
    var handlers = jQuery._data(this, 'events')[name.split('.')[0]];
    var handler = handlers.pop();
    handlers.splice(0, 0, handler);
  });
};

(function ($) {

  Drupal.behaviors.pfizerWebstandardsHcp26 = {
    attach: function (context, settings) {
      // Material ordering.
      var page = $('body', context);
      page.once('pfizerWebstandardsHcp26', function () {
        if (typeof build_s === 'undefined') {
          return;
        }
        // Add to basket links tracking.
        var s = build_s();
        var addToBasketButtons = $('.wrapper-basket-links .form-submit', context);
        addToBasketButtons.bindFirst('mouseup', function () {
          var wrapper = $(this).closest('div[id^=edit-entity]');
          var quantity = wrapper.find('.form-select').val();
          var productTitle = wrapper.find('.item-title').html();

          s.events = 'scOpen,scAdd';
          s.products = ';' + productTitle + ';' + quantity;

          s.linkTrackVars = 'products';
          s.linkTrackEvents = 'scOpen,scAdd';

          s.tl(true, 'o', 'Add to basket clicked');
          s.pfClearVars();
        });

        // Add to basket for Ajax processed buttons.
        $(document).ajaxComplete(function (event, xhr, settings) {
          if (typeof settings.extraData !== 'undefined' &&
              typeof settings.extraData._triggering_element_value !== 'undefined' &&
              settings.extraData._triggering_element_value == Drupal.t('Add to basket')) {
            var wrapper = $("[name=" + settings.extraData._triggering_element_name + "]").closest('div[id^=edit-entity]');
            var quantity = wrapper.find('.form-select').val();
            var productTitle = wrapper.find('.item-title').html();

            s.events=s.apl(s.events,'scOpen',',',1);
            s.events=s.apl(s.events,'scAdd',',',1);
            s.products=s.apl(s.products,';' + productTitle + ';' + quantity,',',1);

            s.linkTrackVars = 'products';
            s.linkTrackEvents = s.apl(s.linkTrackEvents,'scOpen',',',1);
            s.linkTrackEvents = s.apl(s.linkTrackEvents,'scAdd',',',1);

            s.tl(true, 'o', 'Add to basket clicked');
            s.pfClearVars();
          }
        });

        // Order complete.
        if (typeof settings.pfizerWebstandardsHcp26 != 'undefined' &&
            typeof settings.pfizerWebstandardsHcp26.products != 'undefined') {
          var orderedProducts = settings.pfizerWebstandardsHcp26.products;
          for (var i in orderedProducts) {
            s.products = s.apl(s.products,';' + orderedProducts[i].productTitle + ';' + orderedProducts[i].quantity + ';0',',',1);
          }
          s.events = 'purchase';
          s.linkTrackVars = 'products';
          s.linkTrackEvents = 'purchase';
          s.tl(true, 'o', 'Order complete');
          s.pfClearVars();
        }

        // Material download.
        page.find('.wrapper-basket-links a').removeAttr('target').click(function () {
          var wrapper = $(this).closest('div[id^=edit-entity]');
          var productTitle = wrapper.find('.item-title').html();

          s.events = 'event44';
          s.products = ';' + productTitle + ';1';

          s.linkTrackVars = 'products';
          s.linkTrackEvents = 'event44';

          s.tl(true, 'o', 'Material download clicked');
          s.pfClearVars();
        });

        // Contact widget.
        var contactLinks = page.find('.cw-number a');
        contactLinks.click(function () {
          s.events = 'event43';
          s.linkTrackEvents = 'event43';
          s.linkTrackVars = 'eVar63';

          s.tl(true, 'o', 'Telephone number clicked');
          s.pfClearVars();
        });
      });

      // Complete order button tracking.
      var basketSubmit = $('.basket-submit a', context);
      basketSubmit.once('pfizerWebstandardsHcp26', function () {
        basketSubmit.click(function () {
          var basket = $(this).closest('#pfizer-hcp2-order-basket');
          basket.find('.row-item').each(function (i, el) {
            var productTitle = $(el).find('.title').html().trim();
            var quantity = $(el).find('.quantity').html().replace(/^\D+/g, '').trim();
            s.products = s.apl(s.products,';' + productTitle + ';' + quantity + ';0',',',1);
          });

          s.events=s.apl(s.events,'scCheckout',',',1);
          s.linkTrackVars = s.apl(s.linkTrackVars,'products',',',1);
          s.linkTrackEvents = s.apl(s.linkTrackEvents,'scCheckout',',',1);

          s.tl(true, 'o', 'Complete order clicked');
          s.pfClearVars();
        });
      });

      function pfizerWebstandardsHcp26DownloadLink(fileName) {
        s.events = 'event4';
        s.prop7 = 'md:' + fileName;
        s.eVar7 = 'md:' + fileName;
        s.prop8 = $('title').html();

        s.linkTrackVars = s.apl(s.linkTrackVars,'prop7,eVar7,prop8',',',1);
        s.linkTrackEvents = s.apl(s.linkTrackEvents,'event4',',',1);

        s.tl(true, 'o', 'Material download clicked');
        s.pfClearVars();
      }
    }
  }

})(jQuery);

/**
 * HCP2 specific JS function.
 *
 * @param s
 *   Report Suite Object
 */
function pfizerWebstandardsHcp26(s) {
  var $ = jQuery;
  var page = $('body');

  // Content types.
  if (typeof Drupal.settings.pfizerWebstandardsHcp26 != 'undefined' &&
      typeof Drupal.settings.pfizerWebstandardsHcp26.contentType != 'undefined') {
    var data = Drupal.settings.pfizerWebstandardsHcp26.contentType;

    for (prop in data) {
      if (typeof data[prop] != 'undefined') {
        s[prop] = data[prop];
      }
    }
  }

  // Stock availability.
  var stockAvailabilityPane = page.find('.pane-stock-availability');
  if (stockAvailabilityPane.length) {
    var searchTerm = stockAvailabilityPane.find('.form-item-sap-search input').val();
    if (stockAvailabilityPane.find('.stock-available-list tr.items').length && searchTerm !== '') {
      s.events = s.apl(s.events,'event5',',',1);
      s.prop5 = 'stock:' + searchTerm;
      s.eVar5 = 'stock:' + searchTerm;
      s.prop20 = 'stock availability';
      s.eVar20 = 'stock availability';
      s.eVar3 = 'stock:' + $('title').html();

      s.linkTrackVars = 'prop5,eVar5,prop20,eVar20,eVar3';
      s.linkTrackEvents = s.apl(s.linkTrackEvents,'event5',',',1);
    }
  }

  // Contact pane.
  var contactPane = page.find('.pane-contact');
  contactPane.each(function (i, el) {
    s.eVar63 = 'Default Pfizer contact';
  });
}
