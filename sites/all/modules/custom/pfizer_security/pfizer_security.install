<?php
/**
 * @file
 * Install, update and uninstall functions
 */

/**
 * Implements hook_install().
 */
function pfizer_security_install() {
  $seckit_csrf = array(
    'origin' => 1,
    'origin_whitelist' => 'https://dm-federate.pfizer.com',
  );
  variable_set('seckit_csrf', $seckit_csrf);

  // Remove Strongarmed variables that need to be overridden from time to time.
  variable_set('session_limit_logged_out_message', 'You have been automatically logged out, since it appears you have logged in from another computer.');
  variable_set('autologout_inactivity_message', 'You have been logged out due to inactivity.');
  variable_set('autologout_message', 'Your session is about to expire. Do you want to reset it?');
  variable_set('autologout_redirect_url', 'user/login');

  // Automatically enable Auto Logout module.
  module_enable(array('autologout'));
}

/**
 * Implements hook_requirements().
 */
function pfizer_security_requirements($phase) {
  $t = get_t();
  $requirements = array();

  switch ($phase) {
    case 'runtime':
      // Get Pfizer Security module information.
      $info = system_get_info('module', 'pfizer_security');

      // Set common information, most platforms are outdated.
      $requirements['pfizer_security'] = array(
        'title' => $t('Pfizer Security'),
        'value' => "Pfizer Security {$info['version']} is enabled and hidden from the Modules administration page.",
        'severity' => REQUIREMENT_OK,
      );

      break;
  }

  return $requirements;
}

/**
 * Disable Auto Logout Module.
 */
function pfizer_security_update_7001(&$sandbox) {
  if (module_exists('autologout')) {
    module_disable(array('autologout'), TRUE);
    drupal_uninstall_modules(array('autologout'));
  }
}

/**
 * Set or update CSRF Whitelist variable with SSO URL.
 */
function pfizer_security_update_7002(&$sandbox) {
  $default['seckit_csrf'] = array(
    'origin'           => 1,
    'origin_whitelist' => '',
  );
  $seckit_csrf = variable_get('seckit_csrf', $default['seckit_csrf']);
  if (is_array($seckit_csrf) && !empty($seckit_csrf['origin_whitelist'])) {
    $origin_whitelist = array_map('trim', explode(',', $seckit_csrf['origin_whitelist']));
    if (!in_array('https://dm-federate.pfizer.com', $origin_whitelist)) {
      $seckit_csrf['origin_whitelist'] .= ', https://dm-federate.pfizer.com';
    }
  }
  else {
    $seckit_csrf['origin_whitelist'] = 'https://dm-federate.pfizer.com';
  }
  variable_set('seckit_csrf', $seckit_csrf);
}

/**
 * Move clickjacking variable to new name.
 */
function pfizer_security_update_7003(&$sandbox) {
  $var = variable_get('seckit_clickjacking');

  if (empty($var['js_css_noscript'])) {
    $var['x_frame_bust'] = 0;
  }
  else {
    $var['x_frame_bust'] = $var['js_css_noscript'];
  }

  // Remove old variable item and save.
  unset($var['js_css_noscript']);
  variable_set('seckit_clickjacking', $var);
}
