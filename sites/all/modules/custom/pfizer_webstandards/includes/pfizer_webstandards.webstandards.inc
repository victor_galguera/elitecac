<?php
/**
 * @file
 * Include file to manage the Web Standards configuration.
 */

/**
 * This function provides the pfConfig admin form.
 */
function pfizer_webstandards_configuration_form($form, &$form_state) {
  $form['pfizer_webstandards_mode'] = array(
    '#required' => '1',
    '#key_type_toggled' => '0',
    '#description' => t('Select whether the s_code file is in Production (prod) or Development (dev) mode.'),
    '#default_value' => variable_get('pfizer_webstandards_mode', 'dev'),
    '#weight' => '0',
    '#type' => 'radios',
    '#options' => array(
      'dev' => t('Development'),
      'prod' => t('Production'),
    ),
    '#title' => t('Pfizer Configuration Site Catalyst Mode:'),
  );

  $form['pfizer_webstandards_rebuild_cron'] = array(
    '#type' => 'checkbox',
    '#title' => 'Rebuild the JavaScript files each time Cron runs:',
    '#default_value' => variable_get('pfizer_webstandards_rebuild_cron', '1'),
  );
  $form['pfizer_webstandards_include_files'] = array(
    '#type' => 'checkbox',
    '#title' => 'Automatically include JS files on each page:',
    '#default_value' => variable_get('pfizer_webstandards_include_files', '1'),
  );
  $form['report_suites'] = array(
    '#weight' => '1',
    '#description' => t('Enter the Site Catalyst Report Suites to use for each mode:'),
    '#type' => 'fieldset',
    '#title' => t('Report Suites'),
    '#collapsible' => '1',
    '#collapsed' => '0',
  );
  $form['report_suites']['pfizer_webstandards_development_report_suites'] = array(
    '#required' => '0',
    '#description' => t('A comma delimited list of all the development report suites'),
    '#weight' => '0',
    '#type' => 'textfield',
    '#title' => t('Development:'),
    '#default_value' => variable_get('pfizer_webstandards_development_report_suites', ''),
  );
  $form['report_suites']['pfizer_webstandards_production_report_suites'] = array(
    '#required' => '0',
    '#description' => t('A comma delimited list of all the production report suites.'),
    '#weight' => '1',
    '#type' => 'textfield',
    '#title' => t('Production'),
    '#default_value' => variable_get('pfizer_webstandards_production_report_suites', ''),
  );
  $form['domains'] = array(
    '#weight' => '2',
    '#description' => t('Enter the domains used by each mode:'),
    '#type' => 'fieldset',
    '#title' => t('Domains'),
    '#collapsible' => '1',
    '#collapsed' => '0',
  );
  $form['domains']['pfizer_webstandards_development_domains'] = array(
    '#required' => '0',
    '#description' => t('A comma delimited list of all the development domains this s_code is used for.'),
    '#weight' => '0',
    '#type' => 'textfield',
    '#title' => t('Development:'),
    '#default_value' => variable_get('pfizer_webstandards_development_domains', ''),
  );
  $form['domains']['pfizer_webstandards_production_domains'] = array(
    '#required' => '0',
    '#description' => t('A comma delimited list of all the production domains this s_code is used for.'),
    '#weight' => '1',
    '#type' => 'textfield',
    '#title' => t('Production:'),
    '#default_value' => variable_get('pfizer_webstandards_production_domains', ''),
  );
  $form['site_section'] = array(
    '#weight' => '3',
    '#description' => t('Enter the Site Section values:'),
    '#type' => 'fieldset',
    '#title' => t('Site Section'),
    '#collapsible' => '1',
    '#collapsed' => '0',
  );
  $form['site_section']['pfizer_webstandards_site_section_prefix'] = array(
    '#required' => '0',
    '#description' => t("A prefix for all the returned values. All the returned values for s.channel and s.prop1 – 4 will have this prefix in the string. Example: if 'prefix' is set to 'LIPITOR' s.channel value would be 'LIPITOR>folder1'."),
    '#weight' => '0',
    '#type' => 'textfield',
    '#title' => t('Prefix:'),
    '#default_value' => variable_get('pfizer_webstandards_site_section_prefix', ''),
  );
  $form['site_section']['pfizer_webstandards_site_section_delimiter'] = array(
    '#required' => '0',
    '#description' => t('The delimiter to use to delimit the values returned.'),
    '#weight' => '1',
    '#type' => 'textfield',
    '#title' => t('Delimiter:'),
    '#default_value' => variable_get('pfizer_webstandards_site_section_delimiter', '>'),
  );
  $form['page_name'] = array(
    '#weight' => '4',
    '#description' => t('Enter the Page Name values:'),
    '#title' => t('Page Name'),
    '#type' => 'fieldset',
    '#collapsible' => '1',
    '#collapsed' => '0',
  );
  $form['page_name']['pfizer_webstandards_page_name_base'] = array(
    '#required' => '0',
    '#description' => t("['path'|'title'] If this configuration setting is set to 'title', the returned value of s.pageName will be based on the DOM document.title object. If it is set to 'path', the returned value of s.pageName will be based on the page URL excluding the domain-name (location.pathname)."),
    '#weight' => '0',
    '#type' => 'textfield',
    '#title' => t('Base:'),
    '#default_value' => variable_get('pfizer_webstandards_page_name_base', ''),
  );
  $form['page_name']['pfizer_webstandards_page_name_prefix'] = array(
    '#required' => '0',
    '#description' => t('A prefix to prepend to all pageNames.'),
    '#weight' => '1',
    '#type' => 'textfield',
    '#title' => t('Prefix:'),
    '#default_value' => variable_get('pfizer_webstandards_page_name_prefix', ''),
  );
  $form['page_name']['pfizer_webstandards_page_name_homepage'] = array(
    '#required' => '0',
    '#description' => t('The pageName value to use for the home page of the site.'),
    '#weight' => '2',
    '#type' => 'textfield',
    '#title' => t('Homepage:'),
    '#default_value' => variable_get('pfizer_webstandards_page_name_homepage', ''),
  );
  $form['page_name']['pfizer_webstandards_page_name_delimiter'] = array(
    '#required' => '0',
    '#description' => t('The delimiter to use in the pageName string.'),
    '#weight' => '3',
    '#type' => 'textfield',
    '#title' => t('Delimiter:'),
    '#default_value' => variable_get('pfizer_webstandards_page_name_delimiter', '>'),
  );

  // Use System Settings Form.
  $return_form = system_settings_form($form);
  $return_form['#submit'][] = 'pfizer_webstandards_configuration_submit';

  return $return_form;
}

/**
 * Submit handler for form - adds a rebuild command after the system_setting.
 */
function pfizer_webstandards_configuration_submit($form, &$form_state) {
  // Re-export the config file.
  pfizer_webstandards_generate_js_build_pfconfig();
}
