Pfizer Performance 7.x-2.0.0, 2016-07-04
-------------------------------------
- Updated CDN to 2.8.
- Removed CF Purge.
- Added Cloudflare 1.0-beta4.

Pfizer Performance 7.x-1.0.0, 2015-11-18
-------------------------------------
- Updated to use semantic versioning 1.0 -> 1.0.0.

Pfizer Performance 7.x-1.0, 2015-11-05
-------------------------------------
- Official release.
- Updated Entity Cache to 1.5.

Pfizer Performance 7.x-1.0-beta7, 2015-10-14
-------------------------------------
- Added Fast 404 module as a dependency and added hook_update implementation.
- Updated Acquia Purge to 1.3.

Pfizer Performance 7.x-1.0-beta6, 2015-06-16
-------------------------------------
- Minor enhancements to Pfizer Performance Purge.

Pfizer Performance 7.x-1.0-beta5, 2015-06-16
-------------------------------------
- Fixed typo in make file.

Pfizer Performance 7.x-1.0-beta4, 2015-06-04
-------------------------------------
- Added Cloudflare Purge 1.2.
- Changed make subdir from performance to contrib.
- Updated readme.

Pfizer Performance 7.x-1.0-beta3, 2015-06-02
-------------------------------------
- Updated Pfizer Performance Purge to automatically detect subscription name.
- Updated Pfizer Performance Purge to use new Deploy publish hook.

Pfizer Performance 7.x-1.0-beta2, 2015-05-27
-------------------------------------
- Added Entity Cache module version 1.2.
- Updated readme with some documentation on each module's usage.

Pfizer Performance 7.x-1.0-beta1, 2015-05-27
-------------------------------------
- Initial release.
