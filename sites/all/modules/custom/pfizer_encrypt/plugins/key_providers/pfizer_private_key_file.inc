<?php

/**
 * @file
 * Plugin definition for the Pfizer Private Key file provider.
 */

define('PFIZER_ENCRYPT_FILE_NAME', 'encrypt_key.key');

$plugin = pfizer_encrypt_pfizer_private_key_file_encrypt_key_providers();

/**
 * Implements MODULE_FILENAME_encrypt_key_providers().
 */
function pfizer_encrypt_pfizer_private_key_file_encrypt_key_providers() {
  return array(
    'title' => t('Pfizer Private Key File'),
    'description' => t('Use Pfizer Encrypt private key file.'),
    'key callback' => 'pfizer_encrypt_private_key_file',
    'static key' => TRUE,
  );
}

/**
 * Callback method to return the encryption key from a file.
 */
function pfizer_encrypt_private_key_file() {
  // This encryption method only works on Acquia environments.
  if (!empty($_ENV['AH_SITE_GROUP']) && !empty($_ENV['AH_SITE_ENVIRONMENT'])) {

    // Grab the Acquia environment variables.
    $sub = $_ENV['AH_SITE_GROUP'];
    $env = $_ENV['AH_SITE_ENVIRONMENT'];

    $file_name = '/var/www/html/' . $sub . '.' . $env . '/pfizer/' . PFIZER_ENCRYPT_FILE_NAME;

    // If the file doesn't exist, just abort.
    if (!file_exists($file_name)) {
      drupal_set_message(t('The file %file does not exist! Pfizer Encrypt cannot retrieve the encryption key.', array('%file' => $file_name)), 'error');
      return;
    }

    return file_get_contents($file_name);
  }
  elseif (!empty($_ENV['PFE_DT_SERVER'])) {
    $file_name = realpath(DRUPAL_ROOT . '/..') . '/encrypt.key';

    // If the file doesn't exist, just abort.
    if (!file_exists($file_name)) {
      drupal_set_message(t('The file %file does not exist! Pfizer Encrypt cannot retrieve the encryption key.', array('%file' => $file_name)), 'error');
      return;
    }

    return file_get_contents($file_name);
  }
  else {
    // Return insecure encryption key only for development and testing purposes.
    return '1234567890123456';
  }
}
