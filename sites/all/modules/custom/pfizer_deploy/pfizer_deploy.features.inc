<?php
/**
 * @file
 * pfizer_deploy.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pfizer_deploy_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
}
