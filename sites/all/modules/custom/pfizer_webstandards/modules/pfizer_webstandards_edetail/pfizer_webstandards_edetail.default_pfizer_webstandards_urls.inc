<?php
/**
 * @file
 * pfizer_webstandards_edetail.default_pfizer_webstandards_urls.inc
 */

/**
 * Implements hook_default_pfizer_webstandards_url().
 */
function pfizer_webstandards_edetail_default_pfizer_webstandards_url() {
  $export = array();

  $url = new stdClass();
  $url->disabled = FALSE; /* Edit this to true to make a default url disabled initially */
  $url->api_version = 1;
  $url->machine_name = 'edetail_fallback';
  $url->weight = 0;
  $url->url = '*';
  $url->variables = 'a:9:{s:6:"eVar27";s:7:"1038774";s:6:"eVar28";s:7:"EDETAIL";s:6:"eVar54";s:9:"PBTTest10";s:6:"prop28";s:7:"1038774";s:6:"prop29";s:7:"EDETAIL";s:6:"prop41";s:3:"PBT";s:6:"prop42";s:23:"1-230522026:1-230522130";s:6:"prop43";s:5:"VISIT";s:6:"prop54";s:9:"PBTTest10";}';
  $export['edetail_fallback'] = $url;

  return $export;
}
