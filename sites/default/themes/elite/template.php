<?php
/**
 * @file
 * The primary PHP file for this theme.
 */

function elite_preprocess_page(&$variables) {
    // check if the user is logged in.
    global $user;
    if ($user->uid) {
        // Logged in user.
    }
    else {
        // Not logged in.
        // if not logged in then give generic custom titles.
        //allow customization of user / login / password page titles.

        if (arg(0) == 'user' && arg(1) == 'login') {
            drupal_set_title(t('Login'));
        }

        if (arg(0) == 'user') {
            drupal_set_title(t('Login'));
        }

        if (arg(0) == 'user' && arg(1) == 'password') {
            drupal_set_title(t('Request password'));
        }

        if (arg(0) == 'user' && arg(1) == 'register') {
            drupal_set_title(t('Create new account'));
        }
    }
}

/**
 * @file
 * The primary PHP file for this theme.
 */
function elite_theme() {
    $items = array();
    /*$items['user_profile_form'] = array(
        'render element' => 'form',
        'path' => drupal_get_path('theme', 'hv') . '/templates',
        'template' => 'user-profile-edit',
        'preprocess functions' => array(
            'hv_preprocess_user_profile_form'
        ),
    );*/

    // create custom user-login.tpl.php
    $items['user_login'] = array(
        'render element' => 'form',
        'path' => drupal_get_path('theme', 'elite') . '/templates/user',
        'template' => 'user-login',
        'preprocess functions' => array(
            'hv_preprocess_user_login'
        ),
    );

    /*$items['user_profile_form'] = array(
      'render element' => 'form',
      'path' => drupal_get_path('theme', 'hv') . '/templates/user',
      'template' => 'user-profile'
      );*/

    /*$items['user_pass'] = array(
        'render element' => 'form',
        'path' => drupal_get_path('theme', 'hv') . '\templates\user',
        'template' => 'user-pass',
        'preprocess functions' => array(
            'hv_preprocess_user_pass'
        ),
    );*/
    return $items;
}

function elite_preprocess_views_view_field(&$vars){
    global $user;
    if ($vars['view']->name == 'user_profile') {
        if (isset($vars['view']->query->pager->display->handler->handlers['field']['rid'])) {
            $rids = $vars['view']->query->pager->display->handler->handlers['field']['rid']->items[$user->uid];
            foreach ($rids as $index => $rid) {
                $vars['view']->query->pager->display->handler->handlers['field']['fieldset_1']->options['fieldset']['classes'] = 'role-' . $index;
            }
        }
    }
}

function elite_preprocess_user_login(&$vars) {
    $vars['content_attributes_array']['class'][] = 'content';
    $vars['title_attributes_array']['class'][] = 'content';
    $vars['attributes_array']['class'][] = 'content';
    $vars['classes_array'] = array('content');
    $vars['forgot_link'] = l(t('Forgot password?'),'user/password');
}