/**
 * Pfizer Web Standards and Interaction Management (IM) Reporting
 * Based on original s_code.js provided by Adobe for Pfizer Web Standards.
 * @author Greg Clausen, Yuriy Babenko, Eric Silva, Abhijit Rajurkar (Analytics Team)
 * @version 1.41
 *
 * The Pitcher player uses its own copy of the s_code.js file. It is important
 * to send Pitcher the updated s_code.js whenever changes are made to that file.
 */

/**
 * Functions for supporting device specific plugins
 */
var pfizerWebstandardsRegisterdWebStandardPlugins = [];

/**
 * Register a function.
 *
 * @param functionName
 */
function pfizerWebstandardsRegisterWebStandardPlugin(functionName) {

  "use strict";

  pfizerWebstandardsRegisterdWebStandardPlugins.push(functionName);
}

/**
 * Invoke the registered plugins.
 *
 * @param s
 *   Report suite object.
 */
function pfizerWebstandardsInvokeWebStandardPlugins(s) {

  "use strict";

  var functionName, loop;

  for (loop = 0; loop < pfizerWebstandardsRegisterdWebStandardPlugins.length; loop++) {
    functionName = pfizerWebstandardsRegisterdWebStandardPlugins[loop];

    // Invoke the function.
    window[functionName](s);
  }
}

// Function to check the url matches.
// Takes an array of URLs
function pfizerWebstandardsUrlMatch(urlsToCheck) {
  "use strict";

  var currentUrl  = window.location.pathname.toLowerCase();

  // remove initial / from the url
  if (currentUrl.substring(0, 1) === '/') {
    currentUrl = currentUrl.substring(1);
  }

  for (var loop = 0; loop < urlsToCheck.length; loop++) {
    var urlToCheck = urlsToCheck[loop].toLowerCase();

    if (urlToCheck.substring(0, 1) === '/') {
      urlToCheck = urlToCheck.substring(1);
    }

    if (urlToCheck.indexOf('*') === -1) {
      if (currentUrl === urlToCheck) {
        return true;
      }
    }
    else {
      // we need to replace asterisk (*) wildcards with regex compatible versions
      var regexUrl = urlToCheck.replace(/(\/{0,1})\*(\/{0,1})/g, function(match, p1, p2) {
        /*
          p1 - either a leading forward slash, or nothing
          p2 - either a trailing forward slash, or nothing

          to account for cases of wildcards being in the middle (rather than front or end of)
          a URL, we must ensure the wildcard only matches up to (but not including) a forward
          slash
         */

        return p1 + '([^\/]*)' + p2;
      });

      regexUrl = '^' + regexUrl + '$';

      if (currentUrl.search(regexUrl) >= 0) {
        return true;
      }
    }
  }

  // no matches
  return false;
}

/**
 * End of Web Standard changes.
 */
/*
  ============== DO NOT DELETE BELOW CODE ! ===============
  Fix for error "Uncaught TypeError: this.Z.apply is not a function"
  This change is for Media Module.
  */
  var i = null;
  /*
  =========================================================
  */
/* joins an array into a string but doesn't include empty array items */
/*
Array.prototype.joinNoNull = function(d){
  var i, r = [];
  for (i=0; i<this.length; i++) if (this[i].length>0) r.push(this[i]);
  return r.join(d);
}
*/

/*
 * SiteCatalyst code version: AppMeasurement for JavaScript version: 1.0.2
 * Copyright 1996-2013 Adobe, Inc. All Rights Reserved
 * More info available at http://www.omniture.com
 */
/**
 * This function should be used to create a new instance of the s_code
 * object whenever it's necessary. A simple instantiation of AppMeasurement()
 * will not contain the object extensions defined below, causing all kinds
 * of problems.
 *
 * @return {[type]} New s_code object 's'.
 */
function build_s() {
  // sanity check fallback to prevent the plugin functions below from erroring out
  // if the pfConfig object is missing.
  if (typeof pfConfig == 'undefined') {
    pfConfig = {
      mode: 'dev',
      reportSuites: {
        dev: '',
        prod: ''
      },
      domains: {
        dev: '',
        prod: ''
      },
      siteSection: {
        prefix: '',
        delimiter: '>'
      },
      pageName: {
        base: 'title',
        prefix: '',
        delimiter: '>',
        homePage: ''
      },
      customLinks: []
    };
  }

  var s_custom = {
    // use the pfConfig in its current state; if the object changes between requests,
    // each request will be using the latest version at the time of the request
    pfc: pfConfig,

    /**
    * Returns a clean (lowercase) version of the specified mode. In mode is not defined,
    * returns boolean false.
    *
    * @return {[type]} [description]
    */
    getMode: function() {
      if (typeof this.pfc.mode == 'undefined') {
        return false;
      }

      return this.pfc.mode.toLowerCase();
    },

    /**
     * Returns the 'Report Suites' value (string) for current enviornment/mode.
     *
     * @return {[type]} [description]
     */
    getReportSuite: function() {
      var mode;
      if (mode = this.getMode()) {
        if (typeof this.pfc.reportSuites[mode] !== 'undefined') {
          return this.pfc.reportSuites[mode];
        }
      }

      return this.pfc.reportSuites.dev;
    },

    /**
     * Returns the 'Domains' value (string) for current enviornment/mode.
     *
     * @return {[type]} [description]
     */
    getDomains: function() {
      var mode, d;
      if (mode = this.getMode()) {
        if (typeof this.pfc.domains[mode] !== 'undefined') {
          d = this.pfc.domains[mode];
        }
      }
      d = d || this.pfc.domains.dev;
      return d.indexOf(location.hostname)>-1?d:d +","+location.hostname;
    },

    /**
     * Removes special characters from given value and returns result.
     *
     * @param  {[type]} val [description]
     * @return {[type]}     [description]
     */
    scStrip: function(val) {
      var chars = "',|,$,^,\\,>,;,&,#,%,*,!,<, ,~".split(',');

      for (i in chars) {
        val = val.split(chars[i]).join('');
      }

      return val;
    },

    /**
     * Returns current date & time as YYYYMMDDTHHMM.
     *
     * @return {[type]} [description]
     */
    getDateTime: function() {
      var d       = new Date(),
          year    = d.getFullYear(),
          month   = d.getMonth() + 1,
          day     = d.getDate(),
          hour    = d.getHours(),
          minute  = d.getMinutes();

      month   = (month < 10)  ? '0' + month : month;
      day     = (day < 10)    ? '0' + day : day;
      hour    = (hour < 10)   ? '0' + hour : hour;
      minute  = (minute < 10) ? '0' + minute : minute;

      return [year, month, day, 'T', hour, minute].join('');
    },

    /**
     * Returns current date as YYYYMMDD
     * @return {[type]} [description]
     */
    getDate: function() {
      return this.getDateTime().split('T')[0];
    },

    /**
     * Returns a delimited string of sections based on URL parts
     * @param  {[type]} num_sections [description]
     * @return {[type]}              [description]
     */
    getSection: function(num_sections) {
      var sections = location.pathname.split('?')[0].split('/'); // url parts
      if (sections.length < 2 || !num_sections || isNaN(num_sections) || num_sections < 1 ) {
        return;
      }

      var delimiter = '>';
      if (this.pfc.siteSection.delimiter && this.pfc.siteSection.delimiter.length) {
        delimiter = this.pfc.siteSection.delimiter;
      }

      // remove the filename extension (if there is one)
      if (sections[sections.length - 1].indexOf('.') > -1) {
        sections.splice(sections.length - 1, 1);
      }

      // collect the sections, up to specified max in num_sections
      var result = [];
      for (var i = 1; i <= num_sections, i < sections.length; i++) {
        result.push(sections[i]);
      }

      var prefix = location.hostname + delimiter;
      if (this.pfc.siteSection.prefix) {
        prefix = this.pfc.siteSection.prefix;
      }

      return prefix + result.join(delimiter);
    },

    /**
     * Returns page name for current page.
     *
     * @return {[type]} [description]
     */
    getPageName: function() {
      // check for configured base
      var base = 'path';
      if (this.pfc.pageName.base.toLowerCase()) {
        base = this.pfc.pageName.base;
      }

      // set delimiter
      var delimiter = '>';
      if (this.pfc.pageName.delimiter) {
        delimiter = this.pfc.pageName.delimiter;
      }

      // figure out the page name
      if (base === 'title') {
        var page_name = document.title;
      }
      else {
        // grab everything after the domain, not including leading slash, and
        // escape the delimiter
        var page_name = location.pathname.substr(1).replace(/\//g, delimiter);

        // handle empty URL / homepage title
        if (page_name.length < 1) {
          page_name = 'homePage';
          if (this.pfc.pageName.homePage) {
            page_name = this.pfc.pageName.homePage;
          }
        }
      }

      // check for configured prefix
      var prefix = location.hostname + delimiter;
      if (this.pfc.pageName.prefix) {
        prefix = this.pfc.pageName.prefix;
      }

      page_name = prefix + page_name;

      return page_name.toLowerCase();
    },
    
    /**
     * Returns meta value by meta property name.
     *
     * @return {[type]} [description]
     */
    getMetaPropertyValue: function(meta) {
      var v ="", m;
      try{
        m = document.querySelectorAll("META[property=" + meta.replace(":","\\:") + "]");
        v = m && m.length > 0 ? m[0].getAttribute("content") : "";
      } catch(err){
        m = document.getElementsByTagName('meta');
        for (i = 0; i < m.length; i++) { 
          if (m[i].getAttribute("property") == meta) {
            v = m[i].getAttribute("content");
          }
        }
      }
      return v;
    },
    
    /**
     * Returns meta value by meta property name.
     *
     * @return {[type]} [description]
     */
    isLinkDownloadType: function(linkurl) {
      var t;
      try{
        if (s.trackDownloadLinks && s.linkDownloadFileTypes) {
            var j = linkurl.toLowerCase();
            var f = j.indexOf("?");
            var g = j.indexOf("#");
            f >= 0 ? g >= 0 && g < f && (f = g) : f = g;
            f >= 0 && (j = j.substring(0, f));
            f = s.linkDownloadFileTypes.toLowerCase().split(",");
            for (g = 0; g < f.length; g++){
                m = f[g];
                if(j.substring(j.length - (m.length + 1)) == "." + m){
                  t = "d";
                  break;
                }
            }
        }
      } catch(err){
      }
      return t;
    }    
  };

  // var s = new AppMeasurement();
  var s = s_gi(s_custom.getReportSuite());

  s.custom = s_custom;
  s.account = s.custom.getReportSuite();
  /************************** CONFIG SECTION **************************/
  /* You may add or alter any code config here. */
  s.charSet = 'UTF-8'
  /* Conversion Config */
  s.currencyCode = 'USD';
  /* Link Tracking Config */
  s.trackDownloadLinks = true;
  s.trackExternalLinks = true;
  s.trackInlineStats = false;
  s.linkDownloadFileTypes = 'exe,zip,wav,mp3,mov,mpg,avi,wmv,pdf,doc,docx,xls,xlsx,ppt,pptx';
  s.linkInternalFilters = 'javascript:,' + s.custom.getDomains();
  s.linkLeaveQueryString = false;
  s.linkTrackVars = 'prop15,eVar15';
  s.linkTrackEvents = 'None';
  s.usePlugins = true;
  s.enableVideoTracking = true; // set to false if not tracking video

  /* 3rd party cookie tracking server */
  s.trackingServer = 'pfizer.122.2o7.net';

  
  /* BrightCove Video Variable Mapping config
   * disable video tracking by setting s.enableVideoTracking to false */
  if (s.enableVideoTracking){
    s.loadModule("Media")
    s.Media.autoTrack=false;
    s.Media.trackWhilePlaying=true;
    /* TrackVars and TrackEvents are needed to properly track additional video data points. */
    s.Media.trackVars="events,prop19,eVar19,eVar44,eVar12,contextData.bc_tags,contextData.bc_refid,contextData.bc_player,contextData.bc_playertype,contextData.bc_playlist";
    s.Media.trackEvents="event12,event13,event32,event33,event19,event20,event21";
    s.Media.trackMilestones="25,50,75";
    s.Media.segmentByMilestones = true;
    s.Media.trackUsingContextData = true;
    s.Media.contextDataMapping = {
      "a.media.name":"eVar19,prop19",
      "a.media.segment":"eVar44",
      "a.contentType":"eVar12",
      "a.media.timePlayed":"event32",
      "a.media.view":"event12",
      "a.media.segmentView":"event33",
      "a.media.complete":"event13",
      "a.media.milestones":{
        25:"event19",
        50:"event20",
        75:"event21"
      }
    }
  }

  /* TimeParting Config. US DST dates */
  s._tpDST = {
    2012:'3/11,11/4',
    2013:'3/10,11/3',
    2014:'3/9,11/2',
    2015:'3/8,11/1',
    2016:'3/13,11/6',
    2017:'3/12,11/5',
    2018:'3/11,11/4',
    2019:'3/10,11/3'
  }

  /* Add calls to plugins here */
  function s_doPlugins(s) {
    // Call the web_standards js functions,
    // Defined in urlconfig.js.
    pfizerWebstandardsCheckUrls(s);
    pfizerWebstandardsInvokeWebStandardPlugins(s);

    // 404 Page
    if (typeof pfizerWebstandardsPageIs404 !== 'undefined' && pfizerWebstandardsPageIs404) {
      s.pfError404(window.location);
    }

    /* PageNaming */
    if (!s.pageName) {
      s.pageName = s.custom.getPageName();
    }

    if (!s.hier1) {
      s.hier1  = 'D=pageName';
    }

    /* Site Sections */
    if (!s.prop1) {
      s.prop1  = s.custom.getSection(1);
    }

    if (!s.prop2) {
      s.prop2  = s.custom.getSection(2);
    }

    if (!s.prop3) {
      s.prop3  = s.custom.getSection(3);
    }

    if (!s.prop4) {
      s.prop4  = s.custom.getSection(4);
    }

    if (!s.server) {
      s.server = location.hostname;
    }

    // Page URL without protocol; there are scenarios in which this value is
    // pre-populated
    if (!s.hasOwnProperty('prop23') || !s.prop23) {
      s.prop23 = s.custom.scStrip(location.href.split(':')[1]);
    }

    /* External Campaign Tracking using "cmp" URL query parameter */
    if (!s.campaign) {
      s.campaign = s.Util.getQueryParam('cmp');
      s.campaign = s.getValOnce(s.campaign, 's_cmp', 0);

      if (s.campaign.length<1) {
        s.campaign = s.Util.getQueryParam('cbn');
      }
    }

    /* Internal Campaign Tracking using "intcmp" URL query parameter  */
    if (!s.eVar4) {
      s.eVar4 = s.Util.getQueryParam('intcmp');
      s.eVar4 = s.getValOnce(s.eVar4, 's_v4', 0);

      if (s.eVar4) s.events = s.apl(s.events, 'event31', ',', 2);
    }
    /*---------------SMILE 2.0 Variables---------------------*/
    /* Platform (HCP Version) */
    if(!s.prop69){
      s.prop69 = s.custom.getMetaPropertyValue('hcp:version');
    }                                                      

    /* Content Build Number*/
    if(!s.prop32){
      s.prop32 = s.custom.getMetaPropertyValue('hcp:build-no');
    }   
    
    /* Featured Content Flag*/
    if(!s.prop53){
      s.prop53 = s.custom.getMetaPropertyValue('hcp:featured');
    }
    
    /* Primary Message Category*/
    if(!s.prop71){
      s.prop71 = s.custom.getMetaPropertyValue('hcp:primary-message:category');
    }
    
    /* Primary Message*/
    if(!s.prop72){
      s.prop72 = s.custom.getMetaPropertyValue('hcp:primary-message');
    }
    
    /* Web Asset Type*/
    if(!s.prop12){
      s.prop12 = s.custom.getMetaPropertyValue('hcp:asset-type');
      s.eVar12 = "D=c12";
    }              
    /*----------------------------------------------------------*/
    /* Visitor Profession Tracking using "profession" URL query parameter  */
    if (!s.eVar10) {
      s.eVar10 = s.Util.getQueryParam('profession');
      s.eVar10 = s.getValOnce(s.eVar10, 's_v10', 0);
    }

    /* Visitor specialty Tracking using "specialty" URL query parameter  */
    if (!s.eVar11) {
      s.eVar11 = s.Util.getQueryParam('specialty');
      s.eVar11 = s.getValOnce(s.eVar11, 's_v11', 0);
    }

    /* Custom link tracking (download, exit and other link tracking) */
    s.customLinkTracker();

    /* TimeParting, US eastern time */
    s.prop15 = s.getTimeParting('n', '-5');
    s.prop36 = s.custom.getDateTime();  //YYYYMMDDTHHNN

    /* New/Repeat visitor */
    s.prop18 = s.getNewRepeat();

    /* Login Pathing */
    if (s.prop21 && s.pageName) {
      s.prop22 = s.prop21 + ':' + s.pageName;
    }
	
	/* Registration Interaction Status */
    if (s.events && s.events.indexOf("event9")>-1) {
		s.prop43 = "REGISTRATION";
        s.linkTrackVars = s.apl(s.linkTrackVars, 'prop43', ',', 2);
    }

    /* custom pageView event */
    s.events = s.apl(s.events, 'event7', ',', 2);

    /* Error Page tracking */
    s.prevPage = s.getPreviousValue(s.pageName,'prevPage');

    /* Copy props to eVars */
    if (s.prop1) {
      s.channel = 'D=c1';
    }

    if (s.prop6 ) {
      s.eVar6  = 'D=c6';
    }

    if (s.prop12) {
      s.eVar12 = 'D=c12';
    }

    if (s.prop14) {
      s.eVar14 = 'D=c14';
    }

    if (s.prop15) {
      s.eVar15 = 'D=c15';
    }

    if (s.prop18) {
      s.eVar18 = s.prop18;
    }

    if (s.prop21) {
      s.eVar21 = 'D=c21';
    }

    if (s.prop36) {
      s.eVar34 = 'D=c36';
    }

    if (s.prop37) {
      s.eVar35 = 'D=c37';
    }

    if (s.prop68) {
      s.eVar73 = 'D=c68';
    }
    
    /* Copy eVars to props */
    if (s.eVar34) {
      s.prop35 = 'D=v34';
    }

    if (s.eVar32) {
      s.prop33 = 'D=v32';
    }
    
    
    /* Fallback values */
    if (typeof s.eVar45 == 'undefined' || s.eVar45 == '' || s.eVar45 == -1) {
      s.eVar45 = -1;

      if (typeof localStorage.janrainCaptureProfileData !== 'undefined') {
        var profile = JSON.parse(localStorage.janrainCaptureProfileData);

        // customer ID
        if (typeof profile.uuid !== 'undefined') {
          s.eVar45 = profile.uuid;
          s.linkTrackVars = s.apl(s.linkTrackVars, 'eVar45', ',', 2);
        }
      }
    }

    /* Fallback values */
    if ((typeof s.eVar46 == 'undefined' || s.eVar46 == '') && typeof localStorage.janrainCaptureProfileData !== 'undefined') {
      s.eVar46 = 'grv';
      s.linkTrackVars = s.apl(s.linkTrackVars, 'eVar46', ',', 2);
    }

    if (typeof s.prop54 == 'undefined' || s.prop54 == '' || typeof s.eVar54 == 'undefined' || s.eVar54 == '') {
      s.prop54 = 'NA';
      s.eVar54 = 'NA';
    }

    s.plugins = '';
  }

  s.doPlugins = s_doPlugins

  /************************** PLUGINS SECTION *************************/
  /* You may insert any plugins you wish to use here.                 */

  /*
   * Plugin: generates a new UUID
   */
  s.genUUID = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
  };

  /*
   * Plugin: getAndPersistValue
   */
  s.getAndPersistValue=new Function("v","c","e",""
    +"var s=this,a=new Date;e=e?e:0;a.setTime(a.getTime()+e*86400000);if("
    +"v)s.Util.cookieWrite(c,v,e?a:0);return s.Util.cookieRead(c);");
  /*
   * Plugin: getNewRepeat
   */
  s.getNewRepeat=new Function("d","cn",""
    +"var s=this,e=new Date(),cval,sval,ct=e.getTime();d=d?d:30;cn=cn?cn:"
    +"'s_nr';e.setTime(ct+d*24*60*60*1000);cval=s.Util.cookieRead(cn);if("
    +"cval.length==0){s.Util.cookieWrite(cn,ct+'-New',e);return'New';}sva"
    +"l=cval.split('-');if(ct-sval[0]<30*60*1000&&sval[1]=='New'){s.Util."
    +"cookieWrite(cn,ct+'-New',e);return'New';}else{s.Util.cookieWrite(cn"
    +",ct+'-Repeat',e);return'Repeat';}");
  /*
   * Plugin: getTimeParting 3.4
   */
  s.getTimeParting=new Function("h","z",""
    +"var s=this,od;od=new Date('1/1/2000');if(od.getDay()!=6||od.getMont"
    +"h()!=0){return'Data Not Available';}else{var H,M,D,U,ds,de,tm,da=['"
    +"Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturda"
    +"y'],d=new Date();z=z?z:0;z=parseFloat(z);if(s._tpDST){var dso=s._tp"
    +"DST[d.getFullYear()].split(/,/);ds=new Date(dso[0]+'/'+d.getFullYea"
    +"r());de=new Date(dso[1]+'/'+d.getFullYear());if(h=='n'&&d>ds&&d<de)"
    +"{z=z+1;}else if(h=='s'&&(d>de||d<ds)){z=z+1;}}d=d.getTime()+(d.getT"
    +"imezoneOffset()*60000);d=new Date(d+(3600000*z));H=d.getHours();M=d"
    +".getMinutes();M=(M<10)?'0'+M:M;D=d.getDay();U=' AM';if(H>=12){U=' P"
    +"M';H=H-12;}if(H==0){H=12;}D=da[D];tm=H+':'+M+U;return(tm+'|'+D);}");
  /*
   * Plugin: getValOnce
   */
  s.getValOnce=new Function("v","c","e","t",""
    +"if(!v)return'';var s=this,a=new Date,v=v?v:'',c=c?c:'s_gvo',e=e?e:0"
    +",i=t=='m'?60000:86400000,k=s.Util.cookieRead(c);a.setTime(a.getTime"
    +"()+e*i);s.Util.cookieWrite(c,v,e==0?0:a);return v==k?'':v");
  /*
   * Plugin Utility: apl
   */
  s.apl=new Function("l","v","d","u",""
    +"var s=this,m=0;if(!l)l='';if(u){var i,n,a=l.split(d);for(i=0;i<a.le"
    +"ngth;i++){n=a[i];m=m||(u==1?(n==v):(n.toLowerCase()==v.toLowerCase("
    +")));}}if(!m)l=l?l+d+v:v;return l;");
  /*
   * Plugin: getPreviousValue
   */
  s.getPreviousValue=new Function("v","c","el",""
    +"var s=this,t=new Date,i,j,r='';t.setTime(t.getTime()+1800000);if(el"
    +"){if(s.events){i=el.split(',');j=s.events.split(',');for(x in i){fo"
    +"r(y in j){if(i[x]==j[y]){if(s.Util.cookieRead(c))r=s.Util.cookieRea"
    +"d(c);v?s.Util.cookieWrite(c,v,t):s.Util.cookieWrite(c,'no value',t)"
    +";return r}}}}}else{if(s.Util.cookieRead(c))r=s.Util.cookieRead(c);v"
    +"?s.Util.cookieWrite(c,v,t):s.Util.cookieWrite(c,'no value',t);retur"
    +"n r;}");
  /*
   * Plugin: Custom Link Tracker
   */
  /* -- Original Adobe function. Left commented out for reference.
  s.customLinkTracker=new Function("",""
    +"var s=this,c=pfConfig.customLinks,k,i,o,getCleanURL=function(u){u=u"
    +".indexOf('://')>-1?u.split('://')[1]:u;u=u.indexOf('#')>-1?u.split("
    +"'#')[0]:u;u=u.indexOf('?')>-1?u.split('?')[0]:u;while(u.lastIndexOf"
    +"('/')==u.length-1)u=u.substr(0,u.length-1);return u.toLowerCase();}"
    +",getIndex=function(ls,t,l){for(i=0;i<ls.length;i++)if(ls[i].href)if"
    +"(ls[i].type&&ls[i].funcName)if(ls[i].type.toLowerCase()==t){if(t=='"
    +"e'){if(l.indexOf(ls[i].href.toLowerCase())>-1)return i;}else{k=document.createEle"
    +"ment('a');k.href=ls[i].href.toLowerCase();if(l.indexOf(k.href.split"
    +"('://')[1])>-1)return i;}}for(i=0;i<ls.length;i++)if(!ls[i].href)if"
    +"(ls[i].type&&ls[i].funcName)if(ls[i].type.toLowerCase()==t)return i"
    +";return-1;},getLinkObject=function(){if(s.version.toUpperCase().ind"
    +"exOf('H.')>-1){o=s.linkHandler('.',null,true);if(o)s.linkURL=o.href"
    +".toLowerCase();}else if(!isNaN(s.version.split('.')[0])){o=s.linkOb"
    +"ject;if(o&&s.linkType!='d'&&s.linkType!='e')s.linkType='o';}else o="
    +"null;return o;};s.linkObject=getLinkObject();if(!s.linkObject)retur"
    +"n;i=getIndex(c,s.linkType,getCleanURL(o.href));if(i>-1)this[c[i].fu"
    +"ncName](o,c[i]);");
  */
  s.customLinkTracker = function() {
    var s = this;
    var c = pfConfig.customLinks;
    var k;
    var i;
    var o;

    var getCleanURL = function (u) {
      if (u.length > 0) {
          if (u.indexOf('://') > -1) {
            u = u.split('://')[1];
          }

          if (u.indexOf('#') > -1) {
            u = u.split('#')[0];
          }

          if (u.indexOf('?') > -1) {
            u = u.split('?')[0];
          }

          while ((u.lastIndexOf('/') == u.length - 1) && u.length > 0) {
            u = u.substr(0, u.length - 1);
          }
      }
      return u.toLowerCase();
    };

    var getIndex = function (ls, t, l) {
      // check config items with a specified HREF
      for (i = 0; i < ls.length; i++) {
        if (ls[i].href) {
          if (ls[i].type && ls[i].funcName) {
            if (ls[i].type.toLowerCase() == t) {
              // exit links
              if (t == 'e') {
                if (l.indexOf(ls[i].href.toLowerCase()) > -1) {
                  return i;
                }
              }
              // download and 'other' links
              else {
                k = getCleanURL(ls[i].href.toLowerCase());
                if (l.indexOf(k) > -1) {
                  return i;
                }
              }
            }
          }
        }
      }

      // check config items WITHOUT an HREF
      for (i = 0; i < ls.length; i++) {
        if (!ls[i].href) {
          if (ls[i].type && ls[i].funcName) {
            if (ls[i].type.toLowerCase() == t) {
              return i;
            }
          }
        }
      }

      // no match found
      return -1;
    };

    var getLinkObject = function () {
      if (s.version.toUpperCase().indexOf('H.') > -1) {
        o = s.linkHandler('.', null, true);

        if (o) {
          s.linkURL = o.href.toLowerCase();
        }
      }
      else if (!isNaN(s.version.split('.')[0])) {
        o = s.linkObject;

        if (o && s.linkType != 'd' && s.linkType != 'e') {
          s.linkType = 'o';
        }
      }
      else {
        o = null;
      }

      // A fix for external links treated by pfizer_exit_popup (Pfizer Pro US).
      if (s.linkObject && s.linkType == 'o' && s.linkURL.indexOf(location.href.split('/')[2] + '/exit') > -1 && s.linkObject.getAttribute('data-href')) {
        s.linkURL = s.linkObject.getAttribute('data-href').toLowerCase();
      }

      return o;
    };

    s.linkObject = getLinkObject();

    if (!s.linkObject) {
      return;
    }
    var u = o.href;
    if (s.linkType == 'o' && u.indexOf(location.href.split('/')[2] + '/exit') > -1) {
      s.linkType = s.custom.isLinkDownloadType(s.linkURL)||'e';
      u = s.linkURL;
    }
    i = getIndex(c, s.linkType, getCleanURL(u));
    /* FIX - for tracking other and exit link as download link*/
    if (s.linkType == 'o' || s.linkType == 'e') {
        s.prop7 = s.eVar7 = s.prop8 = '';
        if (s.events && s.events == 'event4') {
          s.events = '';
        }
    }
    if (i > -1) {
      this[c[i].funcName](o, c[i]);
    } else {
      if (s.linkType == 'd') {
        s.pfDownloadLink(o, {funcName:"pfDownloadLink", href:"", interaction_status:"", obj:"", type:"d"});
      }
    }
    /* Custom name tracking */
    if (s.linkName == false && s.linkObject.getAttribute("sc:linkname")) {
      s.linkName = s.linkObject.getAttribute("sc:linkname");
    }
  };

  /*
   * Lead link tracking
   */
  s.pfLeadLink = function(lto, lco){
    var e = lco.obj.l? lco.obj.l.toLowerCase(): null,
      v = e=='initiated'? 'event1': (e=='completed'? 'event2': null);
    if(!lco.obj.t || !v || !e) return;
    var s = this;
    s.eVar1 = lco.obj.t;
    s.linkName = 'lead ' + e;
    s.events = s.apl(s.events, v, ',', 2);
    s.linkTrackEvents = s.apl(s.linkTrackEvents, v, ',', 2);
    s.linkTrackVars = s.apl(s.linkTrackVars, 'eVar1,events', ',', 2);
  }
  /*
   * Lead tracking
   */
  s.pfLead = function(t, e){
    e = e? e.toLowerCase(): null;
    var v = e=='initiated'? 'event1': (e=='completed'? 'event2': null);
    if(!t || !v) return;
    var s = this;
    s.eVar1 = t;
    s.linkName = 'lead ' + e;
    s.events = s.apl(s.events, v, ',', 2);
  }

  /*
   * Custom download link tracking
   */
  s.pfDownloadLink = function(obj, config) {
    var s = this;
    s.linkName = s.prop7 = s.linkURL.substring(s.linkURL.lastIndexOf('/')+1).toLowerCase();
    s.eVar7 = 'D=c7';
    s.prop8 = 'D=pageName';
    s.events = s.apl(s.events, 'event4', ',', 2);
    s.linkTrackEvents = s.apl(s.linkTrackEvents, 'event4', ',', 2);
    s.linkTrackVars = s.apl(s.linkTrackVars, 'prop7,eVar7,prop8,events', ',', 2);

    s.pfTrackInteractionStatus(obj, config);
  }

  /**
   * Custom interaction status tracking for non-download (ie. exit) links
   * @param  {[type]} obj    [description]
   * @param  {[type]} config [description]
   * @return {[type]}        [description]
   */
  var original_prop43 = null;
  s.pfTrackInteractionStatus = function(obj, config) {
    // save the original "fallback" value
    if (original_prop43 == null && typeof this.prop43 != 'undefined') {
      original_prop43 = this.prop43;
    }

    var s = this;
    // set the pre-configured value
    if (typeof config.interaction_status != 'undefined' && config.interaction_status != '') {
      s.prop43 = config.interaction_status.toUpperCase();
    }
    // set the fallback value back in place
    else if (original_prop43 != null) {
      s.prop43 = original_prop43;
    }

    if (typeof s.prop54 == 'undefined' || s.prop54 == '' || typeof s.eVar54 == 'undefined' || s.eVar54 == '') {
      s.prop54 = 'NA';
      s.eVar54 = 'NA';
    }
    s.prop55 = s.genUUID();
    s.linkTrackVars = s.apl(s.linkTrackVars, 'prop43,prop55,prop54,eVar54', ',', 2);
  }

  /*
   * Custom download tracking
   */
  s.pfDownload = function(f){
    if(!f) return;
    var s = this;
    s.prop7 = f.toLowerCase();
    s.eVar7 = 'D=c7';
    s.prop8 = 'D=pageName';
    s.events = s.apl(s.events, 'event4', ',', 2);
  }
  /*
   * Webinar tracking
   */
  s.pfWebinar = function(action, webinar_name) {
    action = action ? action.toLowerCase(): null;
    webinar_name = webinar_name ? webinar_name.toLowerCase(): null;

    var events = '';

    switch (action) {
      case 'start':
        events = 'event17';
        break;

      case 'abandon':
        events = 'event22';
        break;

      case 'complete':
        events = 'event25';
        break;

      case 'registration':
        events = 'event28';
        break;

      default:
        events = null;
    }

    if (!action || !webinar_name || !events) {
      return;
    }

    var s = this;
    s.prop29 = s.eVar28 = 'WEBINAR';
    s.prop30 = webinar_name;
    s.eVar29 = 'D=c30';
    s.events = s.apl(s.events, events, ',', 2);
  }
  /*
   * Webinar link tracking
   */
  s.pfWebinarLink = function(lto, lco){
    var e = lco.obj.l? lco.obj.l.toLowerCase(): null,
      v = e=='start'? 'event17':(e=='abandon'? 'event22':(e=='complete'? 'event25':(e=='registration'? 'event28':null)));
    if(!e || !lco.obj.n || !v) return;
    var s = this;
    s.events = s.apl(s.events, v, ',', 2);
    s.prop29 = s.eVar28 = 'WEBINAR';
    s.prop30 = lco.obj.n.toLowerCase();
    s.eVar29 = 'D=c30';
    s.linkName = 'WEBINAR ' + e;
    s.linkTrackVars = s.apl(s.linkTrackVars, 'events,prop29,prop30,eVar28,eVar29', ',', 2);
    s.linkTrackEvents = s.apl(s.linkTrackEvents, v, ',', 2);
  }
  /*
   * E-Detail tracking
   */
  s.pfEdetail = function(e, n){
    e = e? e.toLowerCase(): null;
    n = n? n.toLowerCase(): null;
    var v = e=='start'? 'event16': (e=='abandon'? 'event23': (e=='complete'? 'event24': null));
    if(!e || !n || !v) return;
    var s = this;
    s.prop29 = s.eVar28 = 'EDETAIL';
    s.prop31 = n;
    s.eVar30 = 'D=c31';
    s.events = s.apl(s.events, v, ',', 2);
  }
  /*
   * E-Detail link tracking
   */
  s.pfEdetailLink = function(lto, lco){
    var e = lco.obj.l? lco.obj.l.toLowerCase(): null,
      v = e=='start'? 'event16': (e=='abandon'? 'event23': (e=='complete'? 'event24': null));
    if(!e || !lco.obj.n || !v) return;
    var s = this;
    s.events = s.apl(s.events, v, ',', 2);
    s.prop29 = s.eVar28 = 'EDETAIL';
    s.prop31 = n;
    s.eVar30 = 'D=c31';
    s.linkName = 'EDETAIL ' + e;
    s.linkTrackVars = s.apl(s.linkTrackVars, 'events,prop29,prop30,eVar28,eVar29', ',', 2);
    s.linkTrackEvents = s.apl(s.linkTrackEvents, v, ',', 2);
  }
  /*
   * self-Detail tracking
   */
  s.pfSelfDetail = function(){
    var s = this;
    s.events = s.apl(s.events, 'event15', ',', 2);
  }
  /*
   * Site tool tracking
   */
  s.pfSiteTool = function(n){
    n = n? n.toLowerCase(): null;
    if(!n) return;
    var s = this;
    s.eVar23 = n;
    s.events = s.apl(s.events, 'event14', ',', 2);
  }
  /*
   * Site tool link tracking
   */
  s.pfSiteToolLink = function(lto, lco){
    var n = lco.obj.n? lco.obj.n.toLowerCase(): null;
    if(!n) return;
    var s = this;
    s.eVar23 = n;
    s.linkName = 'site tool usage';
    s.events = s.apl(s.events, 'event14', ',', 2);
    s.linkTrackVars = s.apl(s.linkTrackVars, 'events,eVar23', ',', 2);
    s.linkTrackEvents = s.apl(s.linkTrackEvents, 'event14', ',', 2);
  }
  /*
   * Support request tracking
   */
  s.pfSupportReq = function(t){
    if(!t) return;
    var s = this;
    s.events = s.apl(s.events, 'event3', ',', 2);
    s.eVar2 = t.toLowerCase();
    s.linkName = 'Support Request';
  }
  /*
   * Support request link tracking
   */
  s.pfSupportReqLink = function(lto, lco){
    if(!lco.obj.t) return;
    var s = this;
    s.events = s.apl(s.events, 'event3', ',', 2);
    s.eVar2 = lco.obj.t.toLowerCase();
    s.linkName = 'support request';
    s.linkTrackVars = s.apl(s.linkTrackVars, 'events,eVar2', ',', 2);
    s.linkTrackEvents = s.apl(s.linkTrackEvents, 'event3', ',', 2);
  }
  /*
   * Registration tracking
   */
  s.pfRegistration = function(source, action) {
    if (!source || !action) {
      return;
    }

    var event = null;
    switch (action.toLowerCase()) {
      case 'initiated':
        event = 'event8';
        break;

      case 'completed':
        event = 'event9';
        break;

      default:
        return;
    }

    var s = this;

    s.events = s.apl(s.events, event, ',', 2);
    s.eVar9 = source.toLowerCase(); // registration source (GRV, PAC, etc.)

    s.linkTrackVars = s.apl(s.linkTrackVars, 'events,eVar9', ',', 2);
    s.linkTrackEvents = s.apl(s.linkTrackEvents, event, ',', 2);
  }
  /*
   * Registration link tracking
   */
  s.pfRegistrationLink = function(lto, lco){
    var e = lco.obj.l? lco.obj.l.toLowerCase(): null,
      v = e=='initiated'? 'event8': (e=='completed'? 'event9': null);
    if(!e || !lco.obj.t || !v) return;
    var s = this;
    s.events = s.apl(s.events, v, ',', 2);
    s.eVar9 = lco.obj.t.toLowerCase();
    s.linkName = 'registration ' + e;
    s.linkTrackVars = s.apl(s.linkTrackVars, 'events,eVar9', ',', 2);
    s.linkTrackEvents = s.apl(s.linkTrackEvents, v, ',', 2);
  }
  /*
   * Login tracking
   */
  s.pfLogin = function(source, action, login_type) {
    if (!source || !action) {
      return;
    }

    var event = null;
    switch (action.toLowerCase()) {
      case 'initiated':
        event = 'event36';
        break;

      case 'completed':
        event = 'event37';
        break;

      default:
        return;
    }

    var s = this;

    s.events = s.apl(s.events, event, ',', 2);

    // customer ID type (GRV, PAC, etc.)
    s.eVar46 = source.toLowerCase();

    // login type (Traditional or SSO)
    if (!login_type) {
      login_type = 'Traditional';
    }

    s.prop61 = login_type.toLowerCase();

    s.linkTrackVars = s.apl(s.linkTrackVars, 'events,eVar45,eVar46,prop61', ',', 2);
    s.linkTrackEvents = s.apl(s.linkTrackEvents, event, ',', 2);
  }
  /*
   * Copay tracking
   */
  s.pfCopay = function(action, type, quantity) {
    // action is always required
    if (!action) {
      return;
    }

    var s = this;

    var event = null;
    switch (action.toLowerCase()) {
      case 'initiated':
        event = 'event38';
        break;

      case 'completed':
        // type and quantity are both required for completed action
        if (!type || !quantity) {
          return;
        }

        event = 'event39';

        s.eVar37 = type.toUpperCase(); // EMAIL or PRINT
        s.eVar38 = quantity;

        s.linkTrackVars = s.apl(s.linkTrackVars, 'eVar37,eVar38', ',', 2);
        break;

      default:
        return;
    }

    s.events = s.apl(s.events, event, ',', 2);
    s.linkTrackVars = s.apl(s.linkTrackVars, 'events', ',', 2);
    s.linkTrackEvents = s.apl(s.linkTrackEvents, event, ',', 2);
  }
  /*
   * Video Open tracking
   */
  s.pfVideoOpen = function(){
    s = this;
    s.events = s.apl(s.events, 'event18', ',', 2);
  }
  /*
   * Banner Ad Impression tracking
   */
  s.pfBannerAdImpression = function(id){
    var s = this;
    s.events = s.apl(s.events, 'event30 ', ', ', 2);
    if(id) s.eVar4 = id;
  }
  /*
   * Survey tracking
   */
  s.pfSurvey = function(n, e, qa){
    e = e ? e.toLowerCase(): null;

    if (!e || !n) {
      return;
    }

    var s = this;
    s.eVar8 = n;
    if (e=='offered') {
      s.events = s.apl(s.events, 'event10', ',', 2);
      s.prop37 = s.custom.getDate();
      s.eVar35 = 'D=c37';
    }
    else if(e=='completed') {
      s.events = s.apl(s.events, 'event11', ',', 2);
      s.prop38 = s.custom.getDate();
      s.eVar36 = 'D=c38';

      if (qa) {
        s.eVar22 = n + ' | ' + qa;
      }
    }
  }
  /*
   * 404 error page tracking
   */
  s.pfError404 = function(u){
    if(!u) return;
    s.pageName = '404 ErrorPage';
    s.pageType= 'ErrorPage';
    s.prop16 = s.prevPage;
    s.prop17 = u;
  }
  /*
   * Internal keyword search tracking
   */
  s.pfSearch = function(k, t, r){
    k = k? k.toLowerCase(): null;
    t = t? t.toLowerCase(): null;
    if(!k || !t || r<0) return;
    if(r<1) s.prop6 = k;
    else {
      s.prop5 = k;
      s.eVar5 = 'D=c5';
    }
    s.prop20 = t;
    s.eVar20 = 'D=c20';
    s.events = s.apl(s.events, 'event5', ',', 2);
    s.prop11 = 'D=pageName';
  }
  /**
   * Clears common vars to allow for population of new content.
   * @return {[type]} [description]
   */
  s.pfClearVars = function () {
    var s = this;

    for (var i = 1; i <= 75; i++) {
      s['prop' + i] = '';
      s['eVar' + i] = '';
    }

    var extras = [
      'pageName',
      'channel',
      'products',
      'events',
      'campaign',
      'purchaseID',
      'state',
      'zip',
      'server',
      'linkName',
      'linkTrackEvents',
      'linkTrackVars'
    ];

    for (var i = 0; i < extras.length ; i++) {
      s[extras[i]] = '';
    }
  }

  /*
   * Function - read combined cookies v 0.41
   * LAST UPDATED: 06-05-2013
   * APP MEASUREMENT JS COMPATIBLE
   */
  if(!s.__ccucr){
    s.c_rr = s.c_r;
    s.__ccucr = true;
    function c_r(k){
      var s = this,d = new Date,v = s.c_rr(k),c = s.c_rspers(),i, m, e;
      if(v)return v;k = s.Util.urlDecode(k);i = c.indexOf(' ' + k + '=');c = i < 0 ? s.c_rr('s_sess') : c;
      i = c.indexOf(' ' + k + '=');m = i < 0 ? i : c.indexOf('|', i);
      e = i < 0 ? i : c.indexOf(';', i);m = m > 0 ? m : e;
      v = i < 0 ? '' : s.Util.urlDecode(c.substring(i + 2 + k.length, m < 0 ? c.length : m));
      return v;
    }
    function c_rspers(){
      var cv = s.c_rr("s_pers");var date = new Date().getTime();var expd = null;var cvarr = [];var vcv = "";
      if(!cv)return vcv; cvarr = cv.split(";");for(var i = 0, l = cvarr.length; i < l; i++){
        expd = cvarr[i].match(/\|([0-9]+)$/);if(expd && parseInt(expd[1]) >= date){vcv += cvarr[i] + ";";}}
      return vcv;
    }
    s.c_rspers = c_rspers;
    s.c_r = c_r;
  }
  /*
   * Function - write combined cookies v 0.41
   */
  if(!s.__ccucw){
    s.c_wr = s.c_w;
    s.__ccucw = true;
    function c_w(k, v, e){
      var s = this,d = new Date,ht = 0,pn = 's_pers',sn = 's_sess',pc = 0,sc = 0,pv, sv, c, i, t;
      d.setTime(d.getTime() - 60000);if(s.c_rr(k))s.c_wr(k, '', d);k = s.Util.urlEncode(k);
      pv = s.c_rspers();i = pv.indexOf(' ' + k + '=');if(i > -1){
        pv = pv.substring(0, i) + pv.substring(pv.indexOf(';', i) + 1);pc = 1;}
      sv = s.c_rr(sn);i = sv.indexOf(' ' + k + '=');if(i > -1){
        sv = sv.substring(0, i) + sv.substring(sv.indexOf(';', i) + 1);sc = 1;}
      d = new Date;if(e){if(e.getTime() > d.getTime()){
        pv += ' ' + k + '=' + s.Util.urlEncode(v) + '|' + e.getTime() + ';';pc = 1;}}
      else{sv += ' ' + k + '=' + s.Util.urlEncode(v) + ';';sc = 1;}sv = sv.replace(/%00/g, '');
      pv = pv.replace(/%00/g, '');if(sc)s.c_wr(sn, sv, 0);if(pc){t = pv;
        while(t && t.indexOf(';') != -1){var t1 = parseInt(t.substring(t.indexOf('|') + 1, t.indexOf(';')));
          t = t.substring(t.indexOf(';') + 1);ht = ht < t1 ? t1 : ht;}d.setTime(ht);
        s.c_wr(pn, pv, d);}return v == s.c_r(s.Util.urlEncode(k));}
    s.c_w = c_w;
  }

  /************************** ADDITIONAL DEFAULTS *************************/

  // regenerate new UUID for every interaction/request
  s.prop55 = s.genUUID();
  s.linkTrackVars = s.apl(s.linkTrackVars, 'prop55', ',', 2);

  return s;
}

var s = build_s();

/*
 * Custom Code: Brightcove Smart Analytics v2.1
 */

// saves a copy of all brightcove event handlers for use in Lift's code
var legacy_brightcove = {
  myTemplateLoaded: myTemplateLoaded,
  onTemplateReady: onTemplateReady,
  onPlay: onPlay,
  onStop: onStop,
  onProgress: onProgress
};

var player, modVP, modExp, modCon, mediaFriendly, mediaName, mediaID = 0;
var mediaLength, mediaOffset = 0, mediaTagsArray = [], mediaTagsArray2 = [];
var mediaRefID, mediaPlayerType;
var mediaPlayerName = "vga omniture testing player"; //Required hard code player name here.

function myTemplateLoaded(experienceID) {
  player = brightcove.api.getExperience(experienceID);
  modVP = player.getModule(brightcove.api.modules.APIModules.VIDEO_PLAYER);
  modExp = player.getModule(brightcove.api.modules.APIModules.EXPERIENCE);
  modCon = player.getModule(brightcove.api.modules.APIModules.CONTENT);
  modExp.addEventListener(brightcove.api.events.ExperienceEvent.TEMPLATE_READY, function(event) { onTemplateReady(event); });
}

function onTemplateReady(evt) {
  modVP.addEventListener(brightcove.api.events.MediaEvent.PLAY, function(event) { onPlay(event); });
  modVP.addEventListener(brightcove.api.events.MediaEvent.STOP, function(event) { onStop(event); });
  modVP.addEventListener(brightcove.api.events.MediaEvent.PROGRESS, function(event) { onProgress(event); });
}

function onPlay(evt) {
  if (mediaID != (evt.media.id).toString()) {
    mediaLength = evt.duration; //Required video duration
    mediaOffset = Math.floor(evt.position); //Required video position
    mediaID = (evt.media.id).toString(); //Required video id
    mediaFriendly = evt.media.displayName; //Required video title
    mediaName = mediaID + ":" + mediaFriendly; //Required Format video name
    mediaRefID = evt.media.referenceId; //Optional reference id
    mediaPlayerType = player.type; //Optional player type
    mediaTagsArray = evt.media.tags; //Optional tags
    mediaPlaylist = ""; //Optional playlist id
    for (i = 0; i < mediaTagsArray.length; i++) {
      mediaTagsArray2[i] = mediaTagsArray[i]['name'];
    }
    if (mediaPlayerType == "flash") { //Optional playlist id
      mediaPlaylist = (evt.media.lineupId).toString();
    } else {
      mediaPlaylist = (evt.media.playlistID).toString();
    }
  }
  
  /* Check for start of video */
  if (mediaOffset == 0) {
    /* These data points are optional. If using SC14, change context data variables to hard coded variable names and change trackVars above. */
    s.contextData['bc_tags'] = mediaTagsArray2.toString(); //Optional returns list of tags for current video.  Flash only.
    s.contextData['bc_refid'] = mediaRefID; //Optional returns reference id
    s.contextData['bc_player'] = mediaPlayerName; //Optional player name is currently hard coded.  Will be dynamic in later releases.
    s.contextData['bc_playertype'] = mediaPlayerType; //Optional returns flash or html
    s.contextData['bc_playlist'] = mediaPlaylist; //Optional returns playlist number for current video.
    s.Media.open(mediaName, mediaLength, mediaPlayerName);
    s.Media.play(mediaName, mediaOffset);
  } else {
    s.Media.play(mediaName, mediaOffset);
  }
}

function onStop(evt) {
  mediaOffset = Math.floor(evt.position);
  if (mediaOffset == Math.floor(evt.duration)) {
    s.Media.stop(mediaName, mediaOffset);
    s.Media.close(mediaName);
    // Reset mediaID after video was finished.
    mediaID = 0;
  } else {
    s.Media.stop(mediaName, mediaOffset);
  }
}

function onProgress(evt) {
  s.Media.monitor = function (s, media) {
    if (media.event == "MILESTONE") {
      /* Use to set additional data points during milestone calls */
      s.Media.track(media.name);
    }
  }
}

/*
 ============== DO NOT ALTER ANYTHING BELOW THIS LINE ! ===============

*/
var i=null;function F(){return function(){}}
function AppMeasurement_Module_Media(s){var m=this;m.s=s;var w=window;if(!w.s_c_in)w.s_c_il=[],w.s_c_in=0;m._il=w.s_c_il;m._in=w.s_c_in;m._il[m._in]=m;w.s_c_in++;m._c="s_m";m.list=[];m.open=function(d,e,g,j){var c={},a=new Date,b="",f;e||(e=-1);if(d&&g){if(!m.list)m.list={};m.list[d]&&m.close(d);if(j&&j.id)b=j.id;if(b)for(f in m.list)!Object.prototype[f]&&m.list[f]&&m.list[f].Bf==b&&m.close(m.list[f].name);c.name=d;c.length=e;c.$b=0;c.Q=0;c.playerName=m.playerName?m.playerName:g;c.Bf=b;c.Qd=0;c.V=
0;c.timestamp=Math.floor(a.getTime()/1E3);c.ua=0;c.Zb=c.timestamp;c.P=-1;c.gc="";c.ca=-1;c.kc=0;c.Bd={};c.nc=0;c.Ea=0;c.R="";c.Bb=0;c.Jd=0;c.fc=0;c.lc=0;c.ra=!1;c.wb="";c.ac="";c.bc=0;c.Wb=!1;c.xa="";c.complete=0;c.nf=0;c.ub=0;c.vb=0;m.list[d]=c;c.Fd=!1}};m.openAd=function(d,e,g,j,c,a,b,f){var h={};m.open(d,e,g,f);if(h=m.list[d])h.ra=!0,h.wb=j,h.ac=c,h.bc=a,h.xa=b};m.De=function(d){var e=m.list[d];m.list[d]=0;e&&e.monitor&&clearTimeout(e.monitor.J)};m.close=function(d){m.fa(d,0,-1)};m.play=function(d,
e,g,j){var c=m.fa(d,1,e,g,j);if(c&&!c.monitor)c.monitor={},c.monitor.update=function(){c.ua==1&&m.fa(c.name,3,-1);c.monitor.J=setTimeout(c.monitor.update,1E3)},c.monitor.update()};m.click=function(d,e){m.fa(d,7,e)};m.complete=function(d,e){m.fa(d,5,e)};m.stop=function(d,e){m.fa(d,2,e)};m.track=function(d){m.fa(d,4,-1)};m.lf=function(d,e){var g="a.media.",j=d.linkTrackVars,c=d.linkTrackEvents,a="m_i",b,f=d.contextData,h;if(e.ra){g+="ad.";if(e.wb)f["a.media.name"]=e.wb,f[g+"pod"]=e.ac,f[g+"podPosition"]=
e.bc;if(!e.nc)f[g+"CPM"]=e.xa}if(e.Wb)f[g+"clicked"]=!0,e.Wb=!1;f["a.contentType"]="video"+(e.ra?"Ad":"");f["a.media.channel"]=m.channel;f[g+"name"]=e.name;f[g+"playerName"]=e.playerName;if(e.length>0)f[g+"length"]=e.length;f[g+"timePlayed"]=Math.floor(e.V);Math.floor(e.V)>0&&(f[g+"timePlayed"]=Math.floor(e.V));if(!e.nc)f[g+"view"]=!0,a="m_s",m.Heartbeat&&m.Heartbeat.enabled&&(a=e.ra?m.__primetime?"mspa_s":"msa_s":m.__primetime?"msp_s":"ms_s"),e.nc=1;if(e.R){f[g+"segmentNum"]=e.Ea;f[g+"segment"]=
e.R;if(e.Bb>0)f[g+"segmentLength"]=e.Bb;e.fc&&e.V>0&&(f[g+"segmentView"]=!0)}if(!e.nf&&e.complete)f[g+"complete"]=!0,e.Vf=1;if(e.ub>0)f[g+"milestone"]=e.ub;if(e.vb>0)f[g+"offsetMilestone"]=e.vb;if(j)for(h in f)Object.prototype[h]||(j+=",contextData."+h);b=f["a.contentType"];d.pe=a;d.pev3=b;var D,C;if(m.contextDataMapping){if(!d.events2)d.events2="";j&&(j+=",events");for(h in m.contextDataMapping)if(!Object.prototype[h]){a=h.length>g.length&&h.substring(0,g.length)==g?h.substring(g.length):"";b=m.contextDataMapping[h];
if(typeof b=="string"){D=b.split(",");for(C=0;C<D.length;C++)b=D[C],h=="a.contentType"?(j&&(j+=","+b),d[b]=f[h]):a=="view"||a=="segmentView"||a=="clicked"||a=="complete"||a=="timePlayed"||a=="CPM"?(c&&(c+=","+b),a=="timePlayed"||a=="CPM"?f[h]&&(d.events2+=(d.events2?",":"")+b+"="+f[h]):f[h]&&(d.events2+=(d.events2?",":"")+b)):a=="segment"&&f[h+"Num"]?(j&&(j+=","+b),d[b]=f[h+"Num"]+":"+f[h]):(j&&(j+=","+b),d[b]=f[h])}else if(a=="milestones"||a=="offsetMilestones")h=h.substring(0,h.length-1),f[h]&&
m.contextDataMapping[h+"s"][f[h]]&&(c&&(c+=","+m.contextDataMapping[h+"s"][f[h]]),d.events2+=(d.events2?",":"")+m.contextDataMapping[h+"s"][f[h]]);f[h]&&(f[h]=0);a=="segment"&&f[h+"Num"]&&(f[h+"Num"]=0)}}d.linkTrackVars=j;d.linkTrackEvents=c};m.fa=function(d,e,g,j,c){var a={},b=(new Date).getTime()/1E3,f,h,D=m.trackVars,C=m.trackEvents,G=m.trackSeconds,t=m.trackMilestones,u=m.trackOffsetMilestones,A=m.segmentByMilestones,x=m.segmentByOffsetMilestones,o,p,y=1,l={},H;if(!m.channel)m.channel=m.s.w.location.hostname;
if(a=d&&m.list&&m.list[d]?m.list[d]:0){if(a.ra)G=m.adTrackSeconds,t=m.adTrackMilestones,u=m.adTrackOffsetMilestones,A=m.adSegmentByMilestones,x=m.adSegmentByOffsetMilestones;g<0&&(g=a.ua==1&&a.Zb>0?b-a.Zb+a.P:a.P);a.length>0&&(g=g<a.length?g:a.length);g<0&&(g=0);a.$b=g;if(a.length>0)a.Q=a.$b/a.length*100,a.Q=a.Q>100?100:a.Q;if(a.P<0)a.P=g;H=a.kc;l.name=d;l.ad=a.ra;l.length=a.length;l.openTime=new Date;l.openTime.setTime(a.timestamp*1E3);l.offset=a.$b;l.percent=a.Q;l.playerName=a.playerName;l.mediaEvent=
a.ca<0?"OPEN":e==1?"PLAY":e==2?"STOP":e==3?"MONITOR":e==4?"TRACK":e==5?"COMPLETE":e==7?"CLICK":"CLOSE";if(e>2||e!=a.ua&&(e!=2||a.ua==1)){if(!c)j=a.Ea,c=a.R;if(e){if(e==1)a.P=g;if((e<=3||e>=5)&&a.ca>=0)if(y=!1,D=C="None",a.ca!=g){h=a.ca;if(h>g)h=a.P,h>g&&(h=g);o=t?t.split(","):0;if(a.length>0&&o&&g>=h)for(p=0;p<o.length;p++)if((f=o[p]?parseFloat(""+o[p]):0)&&h/a.length*100<f&&a.Q>=f)y=!0,p=o.length,l.mediaEvent="MILESTONE",a.ub=l.milestone=f;if((o=u?u.split(","):0)&&g>=h)for(p=0;p<o.length;p++)if((f=
o[p]?parseFloat(""+o[p]):0)&&h<f&&g>=f)y=!0,p=o.length,l.mediaEvent="OFFSET_MILESTONE",a.vb=l.offsetMilestone=f}if(a.Jd||!c){if(A&&t&&a.length>0){if(o=t.split(",")){o.push("100");for(p=h=0;p<o.length;p++)if(f=o[p]?parseFloat(""+o[p]):0){if(a.Q<f)j=p+1,c="M:"+h+"-"+f,p=o.length;h=f}}}else if(x&&u&&(o=u.split(","))){o.push(""+(a.length>0?a.length:"E"));for(p=h=0;p<o.length;p++)if((f=o[p]?parseFloat(""+o[p]):0)||o[p]=="E"){if(g<f||o[p]=="E")j=p+1,c="O:"+h+"-"+f,p=o.length;h=f}}if(c)a.Jd=!0}if((c||a.R)&&
c!=a.R){a.lc=!0;if(!a.R)a.Ea=j,a.R=c;a.ca>=0&&(y=!0)}if((e>=2||a.Q>=100)&&a.P<g)a.Qd+=g-a.P,a.V+=g-a.P;if(e<=2||e==3&&!a.ua)a.gc+=(e==1||e==3?"S":"E")+Math.floor(g),a.ua=e==3?1:e;if(!y&&a.ca>=0&&e<=3&&(G=G?G:0)&&a.V>=G)y=!0,l.mediaEvent="SECONDS";a.Zb=b;a.P=g}if(!e||e<=3&&a.Q>=100)a.ua!=2&&(a.gc+="E"+Math.floor(g)),e=0,D=C="None",l.mediaEvent="CLOSE";if(e==7)y=l.clicked=a.Wb=!0;if(e==5||m.completeByCloseOffset&&(!e||a.Q>=100)&&a.length>0&&g>=a.length-m.completeCloseOffsetThreshold)y=l.complete=a.complete=
!0;b=l.mediaEvent;b=="MILESTONE"?b+="_"+l.milestone:b=="OFFSET_MILESTONE"&&(b+="_"+l.offsetMilestone);a.Bd[b]?l.eventFirstTime=!1:(l.eventFirstTime=!0,a.Bd[b]=1);l.event=l.mediaEvent;l.timePlayed=a.Qd;l.segmentNum=a.Ea;l.segment=a.R;l.segmentLength=a.Bb;m.monitor&&e!=4&&m.monitor(m.s,l);if(m.Heartbeat&&m.Heartbeat.enabled){l=[];l.push(a.name);if(!a.Fd)a.Fd=!0,l.push(a.length),l.push(a.playerName),a.ra?(l.push(a.wb),l.push(a.ac),l.push(a.bc),l.push(a.xa),m.Heartbeat.callMethodWhenReady("openAd",l)):
m.Heartbeat.callMethodWhenReady("open",l),l=[],l.push(a.name);e==0?m.Heartbeat.callMethodWhenReady("close",l):(l.push(g),e==1?(l.push(a.Ea),l.push(a.R),l.push(a.Bb),m.Heartbeat.callMethodWhenReady("play",l)):e==2?m.Heartbeat.callMethodWhenReady("stop",l):e==3?m.Heartbeat.callMethodWhenReady("monitor",l):e==5?m.Heartbeat.callMethodWhenReady("complete",l):e==7&&m.Heartbeat.callMethodWhenReady("click",l));a.ca>=0&&(y=!1)}e==0&&m.De(d);if(y&&a.kc==H){d={};d.contextData={};d.linkTrackVars=D;d.linkTrackEvents=
C;if(!d.linkTrackVars)d.linkTrackVars="";if(!d.linkTrackEvents)d.linkTrackEvents="";m.lf(d,a);d.linkTrackVars||(d["!linkTrackVars"]=1);d.linkTrackEvents||(d["!linkTrackEvents"]=1);m.s.track(d);if(a.lc)a.Ea=j,a.R=c,a.fc=!0,a.lc=!1;else if(a.V>0)a.fc=!1;a.gc="";a.ub=a.vb=0;a.V-=Math.floor(a.V);a.ca=g;a.kc++}}}return a};m.hf=function(d,e,g,j,c){var a=0;if(d&&(!m.autoTrackMediaLengthRequired||e&&e>0)){if(!m.list||!m.list[d]){if(g==1||g==3)m.open(d,e,"HTML5 Video",c),a=1}else a=1;a&&m.fa(d,g,j,-1,0)}};
m.attach=function(d){var e,g,j;if(d&&d.tagName&&d.tagName.toUpperCase()=="VIDEO"){if(!m.$a)m.$a=function(c,a,b){var f,h;if(m.autoTrack){f=c.currentSrc;(h=c.duration)||(h=-1);if(b<0)b=c.currentTime;m.hf(f,h,a,b,c)}};e=function(){m.$a(d,1,-1)};g=function(){m.$a(d,1,-1)};m.ma(d,"play",e);m.ma(d,"pause",g);m.ma(d,"seeking",g);m.ma(d,"seeked",e);m.ma(d,"ended",function(){m.$a(d,0,-1)});m.ma(d,"timeupdate",e);j=function(){!d.paused&&!d.ended&&!d.seeking&&m.$a(d,3,-1);setTimeout(j,1E3)};j()}};m.ma=function(m,
e,g){m.attachEvent?m.attachEvent("on"+e,g):m.addEventListener&&m.addEventListener(e,g,!1)};if(m.completeByCloseOffset==void 0)m.completeByCloseOffset=1;if(m.completeCloseOffsetThreshold==void 0)m.completeCloseOffsetThreshold=1;var E=new function(m){this.xe=function(e){this.s=e;this.enabled=!1;this.r=new this.Fa.yf.$d(e)};this.open=function(e,g,m){this.r.open(e,g,m)};this.openAd=function(e,g,m,c,a,b,f){this.r.openAd(e,g,m,c,a,b,f)};this.close=function(e){this.r.close(e)};this.play=function(e,m,d,c,
a){this.r.play(e,m,d,c,a)};this.monitor=function(e,m){this.r.monitor(e,m)};this.stop=function(e,m){this.r.stop(e,m)};this.click=function(e,m){this.r.click(e,m)};this.complete=function(e,m){this.r.complete(e,m)};this.setup=function(e){this.r.Hf(e)};this.bufferStart=function(){this.r.kf()};this.bitrateChange=function(e){this.r.jf(e)};this.updateQoSInfo=function(e,m,d){this.r.Nf(e,m,d)};this.adBreakStart=function(m){this.r.ef(m)};this.adBreakEnd=function(){this.r.df()};this.trackError=function(m,g,d){this.r.Lf(m,
g,d)};this.__setPsdkVersion=function(m){this.r.ye(m)};(function(m){if(typeof g==="undefined")var g={};if(typeof d==="undefined")var d={};d.event||(d.event={});d.a||(d.a={});d.C||(d.C={});d.Wa||(d.Wa={});d.I||(d.I={});(function(c){c.extend=function(a,b){function f(){this.constructor=a}for(var h in b)b.hasOwnProperty(h)&&(a[h]=b[h]);f.prototype=b.prototype;a.prototype=new f;a.o=b.prototype;return a}})(g);(function(c){c.ea=function(a,b){var f=[],h;for(h in b)b.hasOwnProperty(h)&&typeof b[h]==="function"&&
f.push(h);for(h=0;h<f.length;h++){var c=f[h];a.prototype[c]=b[c]}}})(g);(function(c){c.vd={Ad:function(){this.Z&&this.Z.apply(this,arguments);this.Z=i}}})(g);(function(c){c.da={Ua:function(a){this.Na=!0;this.ob=a;this.Na=!1},log:function(a){this.Na&&window.console&&window.console.log&&window.console.log(this.ob+a)},info:function(a){this.Na&&window.console&&window.console.info&&window.console.info(this.ob+a)},warn:function(a){this.Na&&window.console&&window.console.warn&&window.console.warn(this.ob+
a)},error:function(a){if(this.Na&&window.console&&window.console.error)throw a=this.ob+a,window.console.error(a),Error(a);}}})(g);(function(c){function a(a,f){this.type=a;this.data=f}a.Lb="success";a.Ib="error";c.T=a})(g);(function(c){function a(){this.B={}}a.prototype.addEventListener=function(a,f,h){a&&f&&(this.B[a]=this.B[a]||[],this.B[a].push({mf:f,ud:h||window}))};a.prototype.dispatchEvent=function(a){if(a.type)for(var f in this.B)if(this.B.hasOwnProperty(f)&&a.type===f){var h=this.B[f];for(f=
0;f<h.length;f++)h[f].mf.call(h[f].ud,a);break}};a.prototype.Ya=function(a){if(a){var f,h;for(h in this.B)if(this.B.hasOwnProperty(h)){for(f=this.B[h].length-1;f>=0;f--)this.B[h][f].ud===a&&this.B[h].splice(f,1);this.B[h].length||delete this.B[h]}}else this.B={}};c.Oc=a})(g);(function(c){function a(){if(!a.prototype.Ma)a.prototype.Ma=new b;return a.prototype.Ma}var b=c.Oc;c.W=a})(g);(function(c){function a(){}function b(){b.o.constructor.call(this)}var f=c.T,h=c.Oc;a.Qc="GET";c.extend(b,h);b.prototype.load=
function(a){if(a&&a.method&&a.url){var h=this;a.qa.onreadystatechange=function(){if(a.qa.readyState===4){var m={};m[b.ie]=a.qa.status;a.qa.status===200?(m[b.ee]=a.qa.responseText,h.dispatchEvent(new c.T(f.Lb,m))):h.dispatchEvent(new c.T(f.Ib,m))}};a.qa.open(a.method,a.url,!0);a.qa.send()}};b.prototype.close=function(){this.Ya()};b.ie="status";b.ee="response";c.we=a;c.ve=function(a){this.url=a||i;this.method=i;this.qa=new XMLHttpRequest};c.ue=b})(g);(function(c,a){function b(){}b.ya="report";b.la=
"what";b.oc="account";b.Xc="sc_tracking_server";b.cd="tracking_server";b.Ec="check_status_server";b.Rc="job_id";b.mb="publisher";b.Zc="stream_type";b.Tc="ovp";b.Yc="sdk";b.Cc="channel";b.Gc="debug_tracking";b.dd="track_local";b.nb="visitor_id";b.bb="analytics_visitor_id";b.ib="marketing_cloud_visitor_id";b.z="name";b.hb="length";b.lb="player_name";b.X="timer_interval";b.bd="tracking_interval";b.Dc="check_status_interval";b.Mb="track_external_errors";b.Uc="parent_name";b.Vc="parent_pod";b.Wc="parent_pod_position";
b.Jb="parent_pod_offset";b.xa="parent_pod_cpm";b.N="offset";b.Kb="source";b.Jc="error_id";b.eb="bitrate";b.Pc="fps";b.Hc="dropped_frames";a.event.ja=b})(g,d);(function(c,a){function b(a,h){b.o.constructor.call(this,a,h)}c.extend(b,c.T);b.Ga="api_destroy";b.Pf="api_init";b.Fb="api_config";b.wc="api_open_main";b.vc="api_open_ad";b.sc="api_close";b.xc="api_play";b.uc="api_monitor";b.zc="api_stop";b.rc="api_click";b.tc="api_complete";b.Ac="api_track_error";b.yc="api_qos_info";b.pc="api_bitrate_change";
b.qc="api_buffer_start";b.Gb="api_pod_offset";a.event.Ha=b})(g,d);(function(c,a){function b(a,h){b.o.constructor.call(this,a,h)}c.extend(b,c.T);b.wa="context_data_available";a.event.Fc=b})(g,d);(function(c,a){function b(a,h){b.o.constructor.call(this,a,h)}c.extend(b,c.T);b.ha="data_request";b.fb="data_response";b.ta={Ia:"tracking_timer_interval",Sc:"main_video_publisher"};a.event.Hb=b})(g,d);(function(c,a){function b(a,h){b.o.constructor.call(this,a,h)}c.extend(b,c.T);b.jb="network_check_status_complete";
a.event.kb=b})(g,d);(function(c,a){function b(a,h){b.o.constructor.call(this,a,h)}c.extend(b,c.T);b.TIMER_TRACKING_TICK="TIMER_TRACKING_TICK";b.TIMER_TRACKING_ENABLE="TIMER_TRACKING_ENABLE";b.TIMER_TRACKING_DISABLE="TIMER_TRACKING_DISABLE";b.TIMER_CHECK_STATUS_TICK="TIMER_CHECK_STATUS_TICK";b.TIMER_CHECK_STATUS_ENABLE="TIMER_CHECK_STATUS_ENABLE";b.TIMER_CHECK_STATUS_DISABLE="TIMER_CHECK_STATUS_DISABLE";a.event.Ja=b})(g,d);(function(c,a){function b(a,b){this.value=a;this.hint=b}function f(a){this.ec=
a;this.data={}}b.ka="short";f.prototype.c=function(a,b,f){var c=this;return function(){arguments.length&&(c[a]=arguments[0],c.Od(b,arguments[0],f));return c[a]}};f.prototype.Od=function(a,f,c){this.data[a]=new b(f,c)};a.a.K=f;a.a.Ic=b})(g,d);(function(c,a){function b(a,c){b.o.constructor.call(this,a);this.Of=this.c("_year","year",f.ka);this.Af=this.c("_month","month",f.ka);this.pf=this.c("_day","day",f.ka);this.vf=this.c("_hour","hour",f.ka);this.zf=this.c("_minute","minute",f.ka);this.Ef=this.c("_second",
"second",f.ka);this.Of(c.getUTCFullYear());this.Af(c.getUTCMonth()+1);this.pf(c.getUTCDate());this.vf(c.getUTCHours());this.zf(c.getUTCMinutes());this.Ef(c.getUTCSeconds())}var f=a.a.Ic;c.extend(b,a.a.K);a.a.Wd=b})(g,d);(function(c,a){function b(){b.o.constructor.call(this,"asset");this.sb=this.c("_cpm","cpm",i);this.H=this.c("_adId","ad_id",i);this.Ab=this.c("_resolver","resolver",i);this.xb=this.c("_podId","pod_id",i);this.yb=this.c("_podPosition","pod_position",i);this.zb=this.c("_podSecond","pod_second",
i);this.length=this.c("_length","length",i);this.sb("");this.H("");this.Ab("");this.xb("");this.yb("");this.zb(0);this.length(0);if(arguments.length&&arguments[0]instanceof b){var a=arguments[0];this.sb(a.sb());this.H(a.H());this.Ab(a.Ab());this.xb(a.xb());this.yb(a.yb());this.zb(a.zb());this.length(a.length())}}c.extend(b,a.a.K);a.a.Bc=b})(g,d);(function(c,a){function b(){b.o.constructor.call(this,"asset");this.type=this.c("_type","type",i);this.i=this.c("_videoId","video_id",i);this.M=this.c("_publisher",
"publisher",i);this.n=this.c("_adData","ad_data",i);this.duration=this.c("_duration","duration",i);this.type("");this.i("");this.M("");this.n(i);this.duration(0);if(arguments.length&&arguments[0]instanceof b){var a=arguments[0];this.type(a.type());this.i(a.i());this.M(a.M());this.duration(a.duration());(a=a.n())&&this.n(new f(a))}}var f=a.a.Bc;c.extend(b,a.a.K);b.ed="vod";b.qe="live";b.oe="linear";b.za="ad";a.a.cb=b})(g,d);(function(c,a){function b(a){b.o.constructor.call(this,"event");this.bf=a;
this.type=this.c("_type","type",i);this.count=this.c("_count","count",i);this.ic=this.c("_totalCount","total_count",i);this.duration=this.c("_duration","duration",i);this.jc=this.c("_totalDuration","total_duration",i);this.va=this.c("_playhead","playhead",i);this.id=this.c("_id","id",i);this.source=this.c("_source","source",i);this.cc=this.c("_prevTs","prev_ts",i);this.Mf=this.c("_tsAsDate","ts_as_date",i);this.cf=function(){var a=this.bf*1E3;this.Mf(new f(this.ec,new Date(Math.floor(this.Tb/a)*a)))};
this.Db=function(){if(arguments.length)this.Tb=arguments[0],this.Od("ts",this.Tb,i),this.cf();return this.Tb};this.type("");this.count(0);this.ic(0);this.duration(0);this.jc(0);this.va(0);this.id("");this.source("");this.cc(-1);this.Db((new Date).getTime())}var f=a.a.Wd;c.extend(b,a.a.K);b.ia="start";b.Nc="play";b.Mc="pause";b.Yd="buffer";b.Xd="bitrate_change";b.Zd="error";b.Kc="active";b.Lc="complete";a.a.gb=b})(g,d);(function(c,a){function b(){b.o.constructor.call(this,"stream");this.Ub=this.c("_bitrate",
"bitrate",i);this.Cd=this.c("_fps","fps",i);this.yd=this.c("_droppedFrames","dropped_frames",i);this.Ub(0);this.Cd(0);this.yd(0)}c.extend(b,a.a.K);a.a.ce=b})(g,d);(function(c,a){function b(){b.o.constructor.call(this,"sc");this.Za=this.c("_sessionId","rsid",i);this.trackingServer=this.c("_trackingServer","tracking_server",i);this.Za("");this.trackingServer("")}c.extend(b,a.a.K);a.a.ne=b})(g,d);(function(c,a){function b(){b.o.constructor.call(this,"sp");this.Ca=this.c("_ovp","ovp",i);this.Da=this.c("_sdk",
"sdk",i);this.channel=this.c("_channel","channel",i);this.playerName=this.c("_playerName","player_name",i);this.Ca("unknown");this.Da("unknown");this.channel("unknown");this.playerName("")}c.extend(b,a.a.K);a.a.ke=b})(g,d);(function(c,a){function b(){b.o.constructor.call(this,"event");this.Za=this.c("_sessionId","sid",i);this.Za("")}c.extend(b,a.a.K);a.a.le=b})(g,d);(function(c,a){function b(){b.o.constructor.call(this,"stream");this.Vb=this.c("_cdn","cdn",i);this.name=this.c("_name","name",i);this.Vb("");
this.name("");if(arguments.length&&arguments[0]instanceof b){var a=arguments[0];this.Vb(a.Vb());this.name(a.name())}}c.extend(b,a.a.K);a.a.$c=b})(g,d);(function(c,a){function b(){b.o.constructor.call(this,"user");this.Yb=this.c("_device","device",i);this.country=this.c("_country","country",i);this.city=this.c("_city","city",i);this.latitude=this.c("_latitude","latitude",i);this.longitude=this.c("_longitude","longitude",i);this.ab=this.c("_visitorId","id",i);this.Ta=this.c("_analyticsVisitorId","aid",
i);this.Va=this.c("_marketingCloudVisitorId","mid",i);this.Yb("");this.country("");this.city("");this.latitude("");this.longitude("");this.ab("");this.Ta("");this.Va("");if(arguments.length&&arguments[0]instanceof b){var a=arguments[0];this.Yb(a.Yb());this.country(a.country());this.city(a.city());this.latitude(a.latitude());this.longitude(a.longitude());this.ab(a.ab());this.Ta(a.Ta());this.Va(a.Va())}}c.extend(b,a.a.K);a.a.hd=b})(g,d);(function(c,a){a.a.ge=function(a,c,h,m,d){this.ba=a;this.f=c;this.mc=
h;this.hc=m;this.Xa=d}})(g,d);(function(c,a){var b=a.a.gb;a.a.fe=function(a,h,c){this.If=a;this.Ff=h;this.Gf=c;this.F=[];this.Sa=function(a){this.F.push(a)};this.Wf=function(){return this.F};this.rf=function(){if(this.F.length)for(var a=this.F.length-1;a>=0;a--)this.F[a].ba.type()===b.Mc&&this.F.splice(a,1)}}})(g,d);(function(c,a){function b(){}b.prototype.Ld=F();b.prototype.Md=F();b.prototype.S=F();b.prototype.Kd=F();b.prototype.Nd=F();a.a.je=b})(g,d);(function(c,a){function b(){this.Ua("[QuerystringSerializer] > ");
this.Y=function(a){return a?a+"&":""};this.pd=function(a){return a.substring(0,a.length-1)};this.Ze=function(a){var b=[],c;for(c in a.data)if(a.data.hasOwnProperty(c)){var h=a.data[c],f=h.value;h=h.hint;var d=i;f===i||typeof f==="undefined"||(typeof f==="number"?d=this.Kd(c,f,a.ec,h):typeof f==="string"?d=this.Nd(c,f,a.ec,h):f instanceof m?d=this.S(f):this.warn("Unable to serialize DAO. Field: "+c+". Value: "+f+"."),d&&b.push(d))}return b}}var f=c.ea,h=c.da,m=a.a.K,d=a.a.Ic;c.extend(b,a.a.je);f(b,
h);b.prototype.Ld=function(a){for(var b=[],c=a.F,h=0;h<c.length;h++){var f=this.Md(c[h])+"&";f+=this.Y(this.S(a.If));f+=this.Y(this.S(a.Ff));f+=this.Y(this.S(a.Gf));f=this.pd(f);b.push(f)}return b};b.prototype.Md=function(a){var b=this.Y(this.S(a.ba));b+=this.Y(this.S(a.f));b+=this.Y(this.S(a.mc));b+=this.Y(this.S(a.hc));b+=this.Y(this.S(a.Xa));return b=this.pd(b)};b.prototype.S=function(a){a=this.Ze(a);for(var b="",c=0;c<a.length;c++)b+=c==a.length-1?a[c]:a[c]+"&";return b};b.prototype.Kd=function(a,
b,c,h){var f="l";if(b!=i&&b!==void 0&&!isNaN(b))return h&&typeof h==="string"&&h===d.ka&&(f="h"),f+":"+c+":"+a+"="+b;return i};b.prototype.Nd=function(a,b,c){if(b)return"s:"+c+":"+a+"="+b;return i};a.a.de=b})(g,d);(function(c,a){function b(a){this.Pd=0;this.J=a;this.tb=!1}function f(){if(f.prototype.Ma)return f.prototype.Ma;var a=this;this.Be=0;this.aa={};this.na=function(){clearInterval(this.ld);g().Ya(this)};this.Ue=function(){this.Be++;for(var a in this.aa)if(this.aa.hasOwnProperty(a)){var b=this.aa[a];
if(b.tb&&(b.Pd++,b.Pd%b.J===0)){var c={};c[e.X]=b.J;g().dispatchEvent(new d(d[a],c))}}};g().addEventListener(m.Ga,this.na,this);this.ld=setInterval(function(){a.Ue()},u*1E3);this.wf=function(a){return(a=this.aa[a])&&a.tb};this.td=function(a,c){this.aa[a]=new b(c)};this.qf=function(a){delete this.aa[a]};this.Jf=function(a){if(a=this.aa[a])a.tb=!0};this.Kf=function(a){if(a=this.aa[a])a.tb=!1};f.prototype.Ma=this}var h=c.ea,m=a.event.Ha,d=a.event.Ja,e=a.event.ja,g=c.W,u=1;h(f,c.da);new f;a.I.se=f})(g,
d);(function(c,a){function b(a,b,c,f){this.Ua("[Timer] > ");this.J=f;this.ga=a;this.zd=b;this.xd=c;d().td(this.ga,this.J);this.na=function(){this.wd()};this.Xe=function(){this.start()};this.We=function(){this.stop()};h().addEventListener(m.Ga,this.na,this);this.zd&&h().addEventListener(this.zd,this.Xe,this);this.xd&&h().addEventListener(this.xd,this.We,this)}var f=c.ea,h=c.W,m=a.event.Ha,d=a.I.se;f(b,c.da);b.prototype.start=function(){this.info("Starting timer: "+this.ga);d().Jf(this.ga)};b.prototype.stop=
function(){this.info("Stopping timer: "+this.ga);d().Kf(this.ga)};b.prototype.wd=function(){h().Ya(this);d().qf(this.ga)};b.prototype.setInterval=function(a){var b=d().wf(this.ga);this.stop();this.J=a;d().td(this.ga,this.J);b&&this.start()};a.I.gd=b})(g,d);(function(c,a){function b(){b.o.constructor.call(this,d.TIMER_TRACKING_TICK,d.TIMER_TRACKING_ENABLE,d.TIMER_TRACKING_DISABLE,g);this.Oa=function(a){(a=a.data[e.bd])?a!==this.J&&this.setInterval(a):this.setInterval(g)};this.Pb=function(a){switch(a.data[e.la]){case h.ta.Ia:a=
{},a[e.X]=this.J,f().dispatchEvent(new h(h.fb,a))}};f().addEventListener(m.jb,this.Oa,this);f().addEventListener(h.ha,this.Pb,this)}var f=c.W,h=a.event.Hb,m=a.event.kb,d=a.event.Ja,e=a.event.ja,g=10;c.extend(b,a.I.gd);a.I.te=b})(g,d);(function(c,a){function b(){b.o.constructor.call(this,g.TIMER_CHECK_STATUS_TICK,g.TIMER_CHECK_STATUS_ENABLE,g.TIMER_CHECK_STATUS_DISABLE,h);var a=this;setTimeout(function(){a.Fe()},200);this.Fe=function(){var a={};a[e.X]=this.J;m().dispatchEvent(new g(g.TIMER_CHECK_STATUS_TICK,
a))};this.Oa=function(a){(a=a.data[e.Dc])?a!==this.J&&(a>f?this.setInterval(f):this.setInterval(a)):this.setInterval(h)};m().addEventListener(d.jb,this.Oa,this)}var f=600,h=60,m=c.W,d=a.event.kb,e=a.event.ja,g=a.event.Ja;c.extend(b,a.I.gd);a.I.Sd=b})(g,d);(function(c,a){var b=a.I.Sd,f=a.I.te;a.I.Td=function(){this.Rf=new b;this.Uf=new f}})(g,d);(function(c,a){function b(a){this.Ce=a}var f=c.W,h=a.event.ja,m=a.event.kb;b.prototype.parse=function(){var a=i,b=i,c=i,d;window.Qf?d=(new DOMParser).parseFromString(this.Ce,
"text/xml"):(d=new ActiveXObject("Microsoft.XMLDOM"),d.async=!1,d.loadXML(this.data));var e;(e=parseInt(d.getElementsByTagName("trackingInterval")[0].childNodes[0].nodeValue,10))&&(a=e);(e=parseInt(d.getElementsByTagName("setupCheckInterval")[0].childNodes[0].nodeValue,10))&&(b=e);(e=parseInt(d.getElementsByTagName("trackExternalErrors")[0].childNodes[0].nodeValue,10))&&(c=e===1);d={};d[h.bd]=a;d[h.Dc]=b;d[h.Mb]=c;f().dispatchEvent(new m(m.jb,d))};a.Wa.me=b})(g,d);(function(c,a){function b(a){this.Ua("[Network] > ");
this.Aa=this.qd=this.md=!1;this.af=a;this.od=this.kd=this.rd=i;this.Ob=function(a){a=a.data;this.rd=a[x.cd];this.kd=a[x.Ec];this.od=a[x.Rc];this.md=a[x.Gc];this.qd=a[x.dd];this.Aa=!0};this.na=function(){m().Ya(this)};this.Se=function(a){function b(){h.close()}function c(a){e.log("Failed to send heartbeat report: "+JSON.stringify(a));h.close()}if(this.Aa){a=this.af.Ld(a.data[x.ya]);var h;if(!this.qd)for(var f=0;f<a.length;f++){var m=new j(this.rd+"/?__job_id="+this.od+"&"+a[f]);m.method=g.Qc;this.md&&
this.log(m.method+" : "+m.url);h=new A;h.addEventListener(d.Lb,b,this);h.addEventListener(d.Ib,c,this);h.load(m)}var e=this}};this.Ve=function(){this.Z=function(a){if(a=a[x.mb]){a=a.replace(/[^a-zA-Z0-9]+/,"-").toLocaleLowerCase();a=new j(this.kd+a+".xml?r="+(new Date).getTime());a.method=g.Qc;var b=new A;b.addEventListener(d.Lb,function(a){a.data&&(new l(a.data.response)).parse();b.close()},this);b.addEventListener(d.Ib,function(a){c.log("Failed to obtain the config. settings: "+JSON.stringify(a));
b.close()},this);this.info("Get new settings from: "+a.url);b.load(a);var c=this}};var a={};a[x.la]=o.ta.Sc;m().dispatchEvent(new o(o.ha,a))};this.Qb=function(a){this.Ad(a.data)};m().addEventListener(o.fb,this.Qb,this);m().addEventListener(e.Fb,this.Ob,this);m().addEventListener(e.Ga,this.na,this);m().addEventListener(y.wa,this.Se,this);m().addEventListener(p.TIMER_CHECK_STATUS_TICK,this.Ve,this)}var f=c.ea,h=c.da,d=c.T,m=c.W,e=a.event.Ha,g=c.we,j=c.ve,A=c.ue,x=a.event.ja,o=a.event.Hb,p=a.event.Ja,
y=a.event.Fc,l=a.Wa.me;f(b,c.vd);f(b,h);a.Wa.be=b})(g,d);(function(c,a){a.C.Vd=function(){this.oa={};this.pa={};this.Tf={};this.Dd=function(a,c,h){a=c+"."+h+"."+a;this.oa[a]||(this.oa[a]=0);return this.oa[a]};this.Cf=function(a,c,h){this.oa[c+"."+h+"."+a]=0};this.Hd=function(a,c,h){a=c+"."+h+"."+a;this.oa[a]||(this.oa[a]=0);this.oa[a]++};this.Ed=function(a,c,h){a=c+"."+h+"."+a;this.pa[a]||(this.pa[a]=0);return this.pa[a]};this.Df=function(a,c,h){this.pa[c+"."+h+"."+a]=0};this.Gd=function(a,c,h,m){a=
c+"."+h+"."+a;this.pa[a]||(this.pa[a]=0);this.pa[a]+=m}}})(g,d);(function(c,a){function b(){this.nd={};this.Eb=function(a){var b=a.f;b=(b.n()?b.n().H():b.i())+"."+b.type()+"."+a.O;this.info("Inserting key: "+b);this.nd[b]=a};this.L=function(a){var b=a.f;a=(b.n()?b.n().H():b.i())+"."+b.type()+"."+a.O;this.info("Requesting key: "+a);return this.nd[a]}}var f=c.ea;f(b,c.da);a.C.ae=b})(g,d);(function(c,a){var b=a.a.gb,f=a.a.cb,h=a.a.hd,m=a.a.$c;a.C.fd=function(a,c,d,e,g,x){this.timestamp=new Date;this.f=
new f(a);this.mc=new h(c);this.hc=new m(d);this.O=g;this.Xa=e;this.va=x;this.A=void 0;this.sf=function(){if(this.O===b.Kc)return this.f.i();return this.f.type()===f.za?this.f.n().H():this.f.i()};this.tf=function(){return this.O===b.ia&&this.f.type()!==f.za?0:1}}})(g,d);(function(c,a){a.C.re=function(){this.U=[];this.uf=function(){return this.U.slice()};this.ff=function(a){for(var c=-1,h=this.U.length-1;h>=0;h--){if(a.timestamp>=this.U[h].timestamp)break;c=h}c>0?this.U.splice(h,0,a):this.U.push(a)}}})(g,
d);(function(c,a){function b(a){this.Ua("[ReporterHelper] > ");this.j=a;this.Ka=function(a,b,c){var h=this.j.La,m=a.O,f=a.sf(),e=a.f.type(),j=m===d.ia?0:a.va;h.Hd(m,f,e);h.Gd(m,f,e,b);c=new d(c);c.type(m);c.count(a.tf());c.duration(b);c.ic(h.Dd(m,f,e));c.jc(h.Ed(m,f,e));c.va(j);c.Db(a.timestamp.getTime());c.cc(a.A?a.A.timestamp.getTime():-1);return new g(c,a.f,a.mc,a.hc,a.Xa)};this.jd=function(a,b){if(a.F.length){var c=new e(this.j.e);c.type(this.j.pb);c.n(i);c=new j(c,this.j.p,this.j.G,this.j.v,
d.Kc,this.j.m[this.j.e.i()]);c.A=this.j.u.L(c);this.j.u.Eb(c);a.Sa(this.Ka(c,b*1E3,b))}};this.Qa=function(a,b){return b.getTime()-a.getTime()};this.Xb=function(a,b,c){var m=new h(this.j.rb,this.j.Ba,this.j.qb);m.Sa(this.Ka(a,0,b));c&&this.jd(m,b);return m};this.sd=function(a,b,c){var m,f=new h(this.j.rb,this.j.Ba,this.j.qb),g=this.j.U.uf(),j=[],r=i,n=0;for(n=m=0;n<g.length;n++)m=g[n],m.timestamp>a&&m.timestamp<=b&&j.push(m),m.timestamp<a&&(r=m);this.info("-------------TRACK REPORT----------------");
this.info("Interval: ["+a.getTime()+" , "+b.getTime()+"]. Tracking interval: "+c);this.info("-----------------------------------------");for(n=0;n<g.length;n++)this.info("["+g[n].timestamp.getTime()+"] :"+g[n].O+" | "+g[n].f.type());this.info("-----------------------------------------");for(n=0;n<j.length;n++)this.info("["+j[n].timestamp.getTime()+"] :"+j[n].O+" | "+j[n].f.type());this.info("-----------------------------------------");if(r){if(r.A)r.A.timestamp=r.timestamp;r.timestamp=j.length?new Date(j[0].timestamp.getTime()-
1):new Date(b.getTime()-1);m=r.f.n()?r.f.n().H():r.f.i();r.va=this.j.m[m]}if(j.length){g=0;r&&(g=r.O===d.ia&&r.f!==e.za?this.Qa(r.timestamp,j[0].timestamp):this.Qa(a,j[0].timestamp),f.Sa(this.Ka(r,g,c)));for(n=0;n<j.length;n++){a=j[n];g=n==j.length-1?this.Qa(a.timestamp,b):this.Qa(a.timestamp,j[n+1].timestamp);r=!1;var t=f.F;for(m=0;m<t.length;m++){var q=t[m];a.f.type()===q.f.type()&&a.O===q.ba.type()&&(r=a.f.type()===e.za?q.f.n().H()===a.f.n().H():q.f.i()===a.f.i());if(r){t=q.ba;var v=q.f.type();
m=q.f.n()?q.f.n().H():q.f.i();var u=this.j.La;u.Hd(t.type(),m,v);u.Gd(t.type(),m,v,g);q.Xa=a.Xa;t.va(this.j.m[m]);t.duration(t.duration()+g);t.ic(u.Dd(t.type(),m,v));t.jc(u.Ed(t.type(),m,v));t.Db(a.timestamp.getTime());break}}if(!r)this.info("Adding event to report: "+a.O),m=a.f.n()?a.f.n().H():a.f.i(),a.va=this.j.m[m],f.Sa(this.Ka(a,g,c))}}else r&&f.Sa(this.Ka(r,this.Qa(a,b),c));f.rf();this.jd(f,c);for(n=0;n<f.F.length;n++)b=f.F[n].ba,this.info("["+b.Db()+"/"+b.cc()+"] :"+b.type());return f}}var m=
c.ea,h=a.a.fe,d=a.a.gb,e=a.a.cb,g=a.a.ge,j=a.C.fd;m(b,c.da);a.C.he=b})(g,d);(function(c,a){function b(){this.Ua("[Context] > ");this.l=!1;this.pb=i;this.Nb=!1;this.k=this.Rb=i;this.g={Cb:i,M:i};this.Ra=this.Z=i;this.m={};this.Pa=new e(this);this.U=new g;this.u=new u;this.qb=new A;this.rb=new x;this.Ba=new p;this.e=new o;this.p=new y;this.G=new l;this.v=new w;this.La=new r;this.Ob=function(a){this.info("Call detected: onApiConfig().");a=a.data;this.rb.Za(a[k.oc]);this.rb.trackingServer(a[k.Xc]);this.g.M=
a[k.mb];this.Ba.Ca(a[k.Tc]);this.Ba.Da(a[k.Yc]);this.Ba.channel(a[k.Cc]);d().dispatchEvent(new n(n.TIMER_CHECK_STATUS_ENABLE))};this.na=function(){d().Ya(this)};this.Me=function(a){this.info("Call detected: onApiOpenMain().");a=a.data;this.$e();this.k=a[k.z];this.m[this.k]=0;this.Ba.playerName(a[k.lb]);this.p.ab(a[k.nb]);this.p.Ta(a[k.bb]);this.p.Va(a[k.ib]);this.e.i(this.k);this.e.duration(a[k.hb]);this.pb=this.e.type(a[k.Zc]);this.G.name(a[k.z]);this.Ee();d().dispatchEvent(new n(n.TIMER_TRACKING_ENABLE));
var b=new j(this.e,this.p,this.G,this.v,z.ia,0);b.A=this.u.L(b);this.$(b);this.Z=function(a){a=this.Pa.Xb(b,a[k.X],!0);a.F[0].ba.count(1);this.La.Cf(z.ia,this.e.i(),this.e.type());this.La.Df(z.ia,this.e.i(),this.e.type());var c={};c[k.ya]=a;d().dispatchEvent(new B(B.wa,c))};a={};a[k.la]=q.ta.Ia;d().dispatchEvent(new q(q.ha,a));this.l=!0};this.Le=function(a){if(this.l){if(this.info("Call detected: onApiOpenAd()."),this.l){a=a.data;this.e.i()||this.e.i(a[k.Uc]);this.k=a[k.z];this.m[this.k]=0;var b=
new s;b.H(this.k);b.length(a[k.hb]);b.Ab(a[k.lb]);b.sb(a[k.xa]);b.xb(a[k.Vc]);b.zb(this.Rb);b.yb(a[k.Wc]+"");this.e.n(b);this.e.type(o.za);a=new j(this.e,this.p,this.G,this.v,z.ia,0);a.A=this.u.L(a);this.$(a);a=new j(this.e,this.p,this.G,this.v,z.Nc,this.m[this.k]);a.A=this.u.L(a);this.$(a)}}else this.warn("Call detected: onApiOpenAd() for inactive viewing session.")};this.Je=function(a){this.l?(this.info("Call detected: onApiClose()."),this.l||this.warn("Call detected: onApiClose() for inactive viewing session."),
a.data[k.z]===this.e.i()?this.Ae():this.ze()):this.warn("Call detected: onApiClose() for inactive viewing session.")};this.Ne=function(a){this.l?(this.info("Call detected: onApiPlay()."),a=a.data,this.p.ab(a[k.nb]),this.p.Ta(a[k.bb]),this.p.Va(a[k.ib]),this.k=a[k.z],this.m[this.k]=Math.floor(a[k.N]),d().dispatchEvent(new n(n.TIMER_TRACKING_ENABLE)),a=new j(this.e,this.p,this.G,this.v,z.Nc,this.m[this.k]),a.A=this.u.L(a),this.$(a)):this.warn("Call detected: onApiPlay() for inactive viewing session.")};
this.Qe=function(a){this.l?(this.info("Call detected: onApiStop()."),a=a.data,this.k=a[k.z],this.m[this.k]=Math.floor(a[k.N]),a=new j(this.e,this.p,this.G,this.v,z.Mc,this.m[this.k]),a.A=this.u.L(a),this.$(a),d().dispatchEvent(new n(n.TIMER_TRACKING_DISABLE))):this.warn("Call detected: onApiStop() for inactive viewing session.")};this.Ie=function(){this.l?this.info("Call detected: onApiClick()."):this.warn("Call detected: onApiClick() for inactive viewing session.")};this.Ke=function(){this.l?this.info("Call detected: onApiComplete()."):
this.warn("Call detected: onApiComplete() for inactive viewing session.")};this.Pe=function(a){this.l?(this.info("Call detected: onApiQoSInfo()."),a=a.data,this.v.Ub(a[k.eb]),this.v.Cd(a[k.Pc]),this.v.yd(a[k.Hc])):this.warn("Call detected: onApiQoSInfo() for inactive viewing session.")};this.Ge=function(a){if(this.l){if(this.info("Call detected: onApiBitrateChange()."),this.l){this.v.Ub(a.data[k.eb]);var b=new j(this.e,this.p,this.G,this.v,z.Xd,this.m[this.k]);b.A=this.u.L(b);this.u.Eb(b);this.Z=
function(a){a=this.Pa.Xb(b,a[k.X],!1);var c={};c[k.ya]=a;d().dispatchEvent(new B(B.wa,c))};a={};a[k.la]=q.ta.Ia;d().dispatchEvent(new q(q.ha,a))}}else this.warn("Call detected: onApiBitrateChange() for inactive viewing session.")};this.He=function(){if(this.l){this.info("Call detected: onApiBufferStart().");var a=new j(this.e,this.p,this.G,this.v,z.Yd,this.m[this.k]);a.A=this.u.L(a);this.$(a)}else this.warn("Call detected: onApiBufferStart() for inactive viewing session.")};this.Re=function(a){if(this.l){this.info("Call detected: onApiTrackError().");
var b=a.data;if(!(this.Nb&&b[k.Kb]!==I)){var c=new j(this.e,this.p,this.G,this.v,z.Zd,Math.floor(b[k.N]));c.A=this.u.L(c);this.u.Eb(c);this.Z=function(a){a=this.Pa.Xb(c,a[k.X],!1);var m=a.F[0];m.ba.id(b[k.Jc]);m.ba.source(b[k.Kb]);m={};m[k.ya]=a;d().dispatchEvent(new B(B.wa,m))};a={};a[k.la]=q.ta.Ia;d().dispatchEvent(new q(q.ha,a))}}else this.warn("Call detected: onApiTrackError() for inactive viewing session.")};this.Oe=function(a){this.l?(this.info("Call detected: onApiPodOffset()."),this.Rb=Math.floor(a.data[k.Jb])):
this.warn("Call detected: onApiPodOffset() for inactive viewing session.")};this.Te=function(a){this.l?(this.info("Call detected: onMonitor()."),this.k=a.data[k.z],this.m[this.k]=Math.floor(a.data[k.N])):this.warn("Call detected: onMonitor() for inactive viewing session.")};this.Ye=function(a){if(this.l){this.info("Call detected: onTimerTrackingTick().");var b=new Date;a=this.Pa.sd(this.Ra||new Date(0),b,a.data[k.X]);var c={};c[k.ya]=a;d().dispatchEvent(new B(B.wa,c));this.Ra=b}else this.warn("Call detected: onTimerTrackingTick() for inactive viewing session.")};
this.Oa=function(a){this.info("Call detected: onCheckStatusComplete().");if(a.data[k.Mb]!==i)this.Nb=a.data[k.Mb]};this.Pb=function(a){this.info("Call detected: onDataRequest().");switch(a.data[k.la]){case q.ta.Sc:a={},a[k.mb]=this.g.M,d().dispatchEvent(new q(q.fb,a))}};this.Qb=function(a){this.info("Call detected: onDataResponse().");this.Ad(a.data)};this.$e=function(){this.l=!1;this.pb=i;this.Nb=!1;this.m={};this.Ra=this.Rb=i;this.La=new r;this.u=new u;this.U=new g;this.p=new y;this.G=new l;this.v=
new w;this.qb=new A;this.e=new o;this.e.M(this.g.M);this.e.type(this.g.Cb)};this.Ee=function(){this.qb.Za(""+(new Date).getTime()+Math.floor(Math.random()*1E9))};this.$=function(a){this.U.ff(a);this.u.Eb(a)};this.Ae=function(){this.m[this.e.i()]==-1&&(this.m[this.e.i()]=this.e.duration());var a=new j(this.e,this.p,this.G,this.v,z.Lc,this.m[this.e.i()]);a.A=this.u.L(a);this.$(a);this.Z=function(a){var b=new Date;a=this.Pa.sd(this.Ra||new Date(0),b,a[k.X]);var c={};c[k.ya]=a;d().dispatchEvent(new B(B.wa,
c));this.Ra=b};a={};a[k.la]=q.ta.Ia;d().dispatchEvent(new q(q.ha,a));d().dispatchEvent(new n(n.TIMER_TRACKING_DISABLE));this.l=!1};this.ze=function(){var a=new j(this.e,this.p,this.G,this.v,z.Lc,this.m[this.k]);a.A=this.u.L(a);this.$(a);this.e.type(this.pb);this.k=this.e.i();this.e.n(i)};d().addEventListener(v.Fb,this.Ob,this);d().addEventListener(v.Ga,this.na,this);d().addEventListener(v.wc,this.Me,this);d().addEventListener(v.vc,this.Le,this);d().addEventListener(v.sc,this.Je,this);d().addEventListener(v.xc,
this.Ne,this);d().addEventListener(v.zc,this.Qe,this);d().addEventListener(v.rc,this.Ie,this);d().addEventListener(v.tc,this.Ke,this);d().addEventListener(v.yc,this.Pe,this);d().addEventListener(v.pc,this.Ge,this);d().addEventListener(v.qc,this.He,this);d().addEventListener(v.Ac,this.Re,this);d().addEventListener(v.Gb,this.Oe,this);d().addEventListener(v.uc,this.Te,this);d().addEventListener(n.TIMER_TRACKING_TICK,this.Ye,this);d().addEventListener(E.jb,this.Oa,this);d().addEventListener(q.ha,this.Pb,
this);d().addEventListener(q.fb,this.Qb,this)}var m=c.ea,h=c.da,d=c.W,e=a.C.he,g=a.C.re,j=a.C.fd,u=a.C.ae,A=a.a.le,x=a.a.ne,o=a.a.cb,p=a.a.ke,y=a.a.hd,l=a.a.$c,w=a.a.ce,r=a.C.Vd,n=a.event.Ja,E=a.event.kb,q=a.event.Hb,v=a.event.Ha,B=a.event.Fc,k=a.event.ja,z=a.a.gb,s=a.a.Bc,I="sourceErrorSDK";m(b,c.vd);m(b,h);a.C.Ud=b})(g,d);(function(c){function a(a){this.q=a;this.D=function(){var a=this.Aa&&(this.q.analyticsVisitorID||this.q.marketingCloudVisitorID||this.q.visitorID);a||this.warn("Unable to track! Is configured: "+
this.Aa+" analyticsVisitorID: "+this.q.analyticsVisitorID+" marketingCloudVisitorID: "+this.q.marketingCloudVisitorID+" visitorID: "+this.q.visitorID);return a};this.Aa=!1;this.j=new t;this.Sf=new w(new A);this.ld=new j;this.Sb=i;this.g={trackingServer:i,Id:i,M:i,Cb:i,Ca:i,Da:i,channel:i,debugTracking:!1,Rd:!1}}var b=g.ea,m=g.W,d=c.event.ja,e=c.event.Ha,j=c.I.Td,w=c.Wa.be,t=c.C.Ud,u=c.a.cb,A=c.a.de;b(a,g.da);a.prototype.Hf=function(a){this.info("Setting-up the media-fork instance");this.g.debugTracking=
this.q.debugTracking;this.g.Rd=this.q.trackLocal;if(a){if(a.hasOwnProperty("trackingServer"))this.g.trackingServer=a.trackingServer;if(a.hasOwnProperty("jobId"))this.g.Id=a.jobId;if(a.hasOwnProperty("publisher"))this.g.M=a.publisher;if(a.hasOwnProperty("ovp"))this.g.Ca=a.ovp;if(a.hasOwnProperty("sdk"))this.g.Da=a.sdk;if(a.hasOwnProperty("channel"))this.g.channel=a.channel;if(a.hasOwnProperty("streamType"))this.g.Cb=a.streamType===u.ed||a.streamType===u.qe||a.streamType===u.oe||a.streamType===u.za?
a.streamType:u.ed;if(this.q.__primetime)this.g.Ca="primetime";if(this.Sb!=i)this.g.Da=this.Sb;a={};a[d.oc]=this.q.account;a[d.Xc]=this.q.trackingServer;a[d.cd]=this.g.trackingServer;a[d.Ec]=this.g.trackingServer+"/settings/";a[d.Rc]=this.g.Id;a[d.mb]=this.g.M;a[d.Tc]=this.g.Ca;a[d.Yc]=this.g.Da;a[d.Cc]=this.g.channel;a[d.Gc]=this.g.debugTracking;a[d.dd]=this.g.Rd;m().dispatchEvent(new e(e.Fb,a));this.Aa=!0}};a.prototype.open=function(a,b,c){this.info("Call detected: open().");if(this.D()){var g={};
g[d.nb]=this.q.visitorID;g[d.bb]=this.q.analyticsVisitorID;g[d.ib]=this.q.xf;g[d.z]=a;g[d.hb]=b;g[d.Zc]=this.g.Cb;g[d.lb]=c;m().dispatchEvent(new e(e.wc,g))}};a.prototype.openAd=function(a,b,c,g,j,t,r){this.info("Call detected: openAd().");if(this.D()){var n={};n[d.z]=a;n[d.hb]=b;n[d.lb]=c;n[d.Uc]=g;n[d.Vc]=j;n[d.Wc]=t;n[d.xa]=r;m().dispatchEvent(new e(e.vc,n))}};a.prototype.close=function(a){this.info("Call detected: close().");if(this.D()){var b={};b[d.z]=a;m().dispatchEvent(new e(e.sc,b))}};a.prototype.play=
function(a,b){this.info("Call detected: play().");if(this.D()){var c={};c[d.nb]=this.q.visitorID;c[d.bb]=this.q.analyticsVisitorID;c[d.ib]=this.q.xf;c[d.z]=a;c[d.N]=b;m().dispatchEvent(new e(e.xc,c))}};a.prototype.monitor=function(a,b){this.info("Call detected: monitor().");var c={};c[d.z]=a;c[d.N]=b;m().dispatchEvent(new e(e.uc,c))};a.prototype.stop=function(a,b){this.info("Call detected: stop().");if(this.D()){var c={};c[d.z]=a;c[d.N]=b;m().dispatchEvent(new e(e.zc,c))}};a.prototype.click=function(a,
b){this.info("Call detected: click().");if(this.D()){var c={};c[d.z]=a;c[d.N]=b;m().dispatchEvent(new e(e.rc,c))}};a.prototype.complete=function(a,b){this.info("Call detected: complete().");if(this.D()){var c={};c[d.z]=a;c[d.N]=b;m().dispatchEvent(new e(e.tc,c))}};a.prototype.wd=function(){m().dispatchEvent(new e(e.Ga))};a.prototype.Lf=function(a,b,c){if(this.D()){var g={};g[d.Kb]=a;g[d.Jc]=b;g[d.N]=c;m().dispatchEvent(new e(e.Ac,g))}};a.prototype.Nf=function(a,b,c){if(this.D()){var g={};g[d.eb]=
a;g[d.Pc]=b;g[d.Hc]=c;m().dispatchEvent(new e(e.yc,g))}};a.prototype.jf=function(a){if(this.D()){var b={};b[d.eb]=a;m().dispatchEvent(new e(e.pc,b))}};a.prototype.kf=function(){this.D()&&m().dispatchEvent(new e(e.qc))};a.prototype.ef=function(a){if(this.D()){var b={};b[d.Jb]=a;m().dispatchEvent(new e(e.Gb,b))}};a.prototype.df=function(){if(this.D()){var a={};a[d.Jb]=i;m().dispatchEvent(new e(e.Gb,a))}};a.prototype.ye=function(a){this.Sb=a};c.$d=a})(d);m.Fa||(m.Fa={});m.Fa.of||(m.Fa.of=g);m.Fa.yf=
d})(this);this.xe(m)}(m.s);E.callMethodWhenReady=function(d,m){s.visitor!=i&&(s.isReadyToTrack()?E[d].apply(this,m):s.callbackWhenReadyToTrack(E,E[d],m))};m.Heartbeat=E;m.gf=function(){var d,e;if(m.autoTrack&&(d=m.s.d.getElementsByTagName("VIDEO")))for(e=0;e<d.length;e++)m.attach(d[e])};m.ma(w,"load",m.gf)}
/*
 START CLICKMAP MODULE

 The following module enables ClickMap tracking in Adobe Analytics. ClickMap
 allows you to view data overlays on your links and content to understand how
 users engage with your web site. If you do not intend to use ClickMap, you
 can remove the following block of code from your AppMeasurement.js file.
 Additional documentation on how to configure ClickMap is available at:
 https://marketing.adobe.com/resources/help/en_US/analytics/clickmap/getting-started-admins.html
*/
function AppMeasurement_Module_ClickMap(h){function l(a,d){var b,c,n;if(a&&d&&(b=e.c[d]||(e.c[d]=d.split(","))))for(n=0;n<b.length&&(c=b[n++]);)if(-1<a.indexOf(c))return null;p=1;return a}function q(a,d,b,c){var e,f;if(a.dataset&&(f=a.dataset[d]))e=f;else if(a.getAttribute)if(f=a.getAttribute("data-"+b))e=f;else if(f=a.getAttribute(b))e=f;if(!e&&h.useForcedLinkTracking&&(e="",d=a.onclick?""+a.onclick:"")){b=d.indexOf(c);var k,g;if(0<=b){for(b+=10;b<d.length&&0<="= \t\r\n".indexOf(d.charAt(b));)b++;
if(b<d.length){f=b;for(k=g=0;f<d.length&&(";"!=d.charAt(f)||k);)k?d.charAt(f)!=k||g?g="\\"==d.charAt(f)?!g:0:k=0:(k=d.charAt(f),'"'!=k&&"'"!=k&&(k=0)),f++;if(d=d.substring(b,f))a.e=new Function("s","var e;try{s.w."+c+"="+d+"}catch(e){}"),a.e(h)}}}return e||h.w[c]}function r(a,d,b){var c;return(c=e[d](a,b))&&(p?(p=0,c):l(m(c),e[d+"Exclusions"]))}function s(a,d,b){var c;if(a&&!(1===(c=a.nodeType)&&(c=a.nodeName)&&(c=c.toUpperCase())&&u[c])&&(1===a.nodeType&&(c=a.nodeValue)&&(t[t.length]=c),b.a||b.t||
b.s||!a.getAttribute||((c=a.getAttribute("alt"))?b.a=c:(c=a.getAttribute("title"))?b.t=c:"IMG"==(""+a.nodeName).toUpperCase()&&(c=a.getAttribute("src"))&&(b.s=c)),(c=a.childNodes)&&c.length))for(a=0;a<c.length;a++)s(c[a],0,b)}function m(a){if(null==a||void 0==a)return a;try{return a.replace(RegExp("^[\\s\\n\\f\\r\\t\t-\r \u00a0\u1680\u180e\u2000-\u200a\u2028\u2029\u205f\u3000\ufeff]+","mg"),"").replace(RegExp("[\\s\\n\\f\\r\\t\t-\r \u00a0\u1680\u180e\u2000-\u200a\u2028\u2029\u205f\u3000\ufeff]+$",
"mg"),"").replace(RegExp(" {2,}","mg")," ").substring(0,254)}catch(d){}}var e=this;e.s=h;var g=window;g.s_c_in||(g.s_c_il=[],g.s_c_in=0);e._il=g.s_c_il;e._in=g.s_c_in;e._il[e._in]=e;g.s_c_in++;e._c="s_m";e.c={};var p=0,u={SCRIPT:1,STYLE:1,LINK:1,CANVAS:1};e._g=function(){var a,d,b,c=h.contextData,e=h.linkObject;(a=h.pageName||h.pageURL)&&(d=r(e,"link",h.linkName))&&(b=r(e,"region"))&&(c["a.clickmap.page"]=a.substring(0,255),c["a.clickmap.link"]=128<d.length?d.substring(0,128):d,c["a.clickmap.region"]=
127<b.length?b.substring(0,127):b,c["a.clickmap.pageIDType"]=h.pageName?1:0)};e.link=function(a,d){var b;if(d)b=l(m(d),e.linkExclusions);else if((b=a)&&!(b=q(a,"sObjectId","s-object-id","s_objectID"))){var c;(c=l(m(a.innerText||a.textContent),e.linkExclusions))||(s(a,c=[],b={a:void 0,t:void 0,s:void 0}),(c=l(m(c.join(""))))||(c=l(m(b.a?b.a:b.t?b.t:b.s?b.s:void 0))));b=c}return b};e.region=function(a){for(var d,b=e.regionIDAttribute||"id";a&&(a=a.parentNode);){if(d=q(a,b,b,b))return d;if("BODY"==a.nodeName)return"BODY"}}}

/* END CLICKMAP MODULE */
/*
============== DO NOT ALTER ANYTHING BELOW THIS LINE ! ===============

AppMeasurement for JavaScript version: 1.6
Copyright 1996-2015 Adobe, Inc. All Rights Reserved
More info available at http://www.omniture.com
*/
function AppMeasurement(){var a=this;a.version="1.6";var k=window;k.s_c_in||(k.s_c_il=[],k.s_c_in=0);a._il=k.s_c_il;a._in=k.s_c_in;a._il[a._in]=a;k.s_c_in++;a._c="s_c";var q=k.AppMeasurement.zb;q||(q=null);var r=k,n,t;try{for(n=r.parent,t=r.location;n&&n.location&&t&&""+n.location!=""+t&&r.location&&""+n.location!=""+r.location&&n.location.host==t.host;)r=n,n=r.parent}catch(u){}a.ob=function(a){try{console.log(a)}catch(b){}};a.za=function(a){return""+parseInt(a)==""+a};a.replace=function(a,b,d){return!a||
0>a.indexOf(b)?a:a.split(b).join(d)};a.escape=function(c){var b,d;if(!c)return c;c=encodeURIComponent(c);for(b=0;7>b;b++)d="+~!*()'".substring(b,b+1),0<=c.indexOf(d)&&(c=a.replace(c,d,"%"+d.charCodeAt(0).toString(16).toUpperCase()));return c};a.unescape=function(c){if(!c)return c;c=0<=c.indexOf("+")?a.replace(c,"+"," "):c;try{return decodeURIComponent(c)}catch(b){}return unescape(c)};a.fb=function(){var c=k.location.hostname,b=a.fpCookieDomainPeriods,d;b||(b=a.cookieDomainPeriods);if(c&&!a.cookieDomain&&
!/^[0-9.]+$/.test(c)&&(b=b?parseInt(b):2,b=2<b?b:2,d=c.lastIndexOf("."),0<=d)){for(;0<=d&&1<b;)d=c.lastIndexOf(".",d-1),b--;a.cookieDomain=0<d?c.substring(d):c}return a.cookieDomain};a.c_r=a.cookieRead=function(c){c=a.escape(c);var b=" "+a.d.cookie,d=b.indexOf(" "+c+"="),f=0>d?d:b.indexOf(";",d);c=0>d?"":a.unescape(b.substring(d+2+c.length,0>f?b.length:f));return"[[B]]"!=c?c:""};a.c_w=a.cookieWrite=function(c,b,d){var f=a.fb(),e=a.cookieLifetime,g;b=""+b;e=e?(""+e).toUpperCase():"";d&&"SESSION"!=
e&&"NONE"!=e&&((g=""!=b?parseInt(e?e:0):-60)?(d=new Date,d.setTime(d.getTime()+1E3*g)):1==d&&(d=new Date,g=d.getYear(),d.setYear(g+5+(1900>g?1900:0))));return c&&"NONE"!=e?(a.d.cookie=c+"="+a.escape(""!=b?b:"[[B]]")+"; path=/;"+(d&&"SESSION"!=e?" expires="+d.toGMTString()+";":"")+(f?" domain="+f+";":""),a.cookieRead(c)==b):0};a.G=[];a.ba=function(c,b,d){if(a.ta)return 0;a.maxDelay||(a.maxDelay=250);var f=0,e=(new Date).getTime()+a.maxDelay,g=a.d.visibilityState,m=["webkitvisibilitychange","visibilitychange"];
g||(g=a.d.webkitVisibilityState);if(g&&"prerender"==g){if(!a.ca)for(a.ca=1,d=0;d<m.length;d++)a.d.addEventListener(m[d],function(){var c=a.d.visibilityState;c||(c=a.d.webkitVisibilityState);"visible"==c&&(a.ca=0,a.delayReady())});f=1;e=0}else d||a.l("_d")&&(f=1);f&&(a.G.push({m:c,a:b,t:e}),a.ca||setTimeout(a.delayReady,a.maxDelay));return f};a.delayReady=function(){var c=(new Date).getTime(),b=0,d;for(a.l("_d")?b=1:a.na();0<a.G.length;){d=a.G.shift();if(b&&!d.t&&d.t>c){a.G.unshift(d);setTimeout(a.delayReady,
parseInt(a.maxDelay/2));break}a.ta=1;a[d.m].apply(a,d.a);a.ta=0}};a.setAccount=a.sa=function(c){var b,d;if(!a.ba("setAccount",arguments))if(a.account=c,a.allAccounts)for(b=a.allAccounts.concat(c.split(",")),a.allAccounts=[],b.sort(),d=0;d<b.length;d++)0!=d&&b[d-1]==b[d]||a.allAccounts.push(b[d]);else a.allAccounts=c.split(",")};a.foreachVar=function(c,b){var d,f,e,g,m="";e=f="";if(a.lightProfileID)d=a.K,(m=a.lightTrackVars)&&(m=","+m+","+a.ga.join(",")+",");else{d=a.e;if(a.pe||a.linkType)m=a.linkTrackVars,
f=a.linkTrackEvents,a.pe&&(e=a.pe.substring(0,1).toUpperCase()+a.pe.substring(1),a[e]&&(m=a[e].yb,f=a[e].xb));m&&(m=","+m+","+a.B.join(",")+",");f&&m&&(m+=",events,")}b&&(b=","+b+",");for(f=0;f<d.length;f++)e=d[f],(g=a[e])&&(!m||0<=m.indexOf(","+e+","))&&(!b||0<=b.indexOf(","+e+","))&&c(e,g)};a.o=function(c,b,d,f,e){var g="",m,p,k,w,n=0;"contextData"==c&&(c="c");if(b){for(m in b)if(!(Object.prototype[m]||e&&m.substring(0,e.length)!=e)&&b[m]&&(!d||0<=d.indexOf(","+(f?f+".":"")+m+","))){k=!1;if(n)for(p=
0;p<n.length;p++)m.substring(0,n[p].length)==n[p]&&(k=!0);if(!k&&(""==g&&(g+="&"+c+"."),p=b[m],e&&(m=m.substring(e.length)),0<m.length))if(k=m.indexOf("."),0<k)p=m.substring(0,k),k=(e?e:"")+p+".",n||(n=[]),n.push(k),g+=a.o(p,b,d,f,k);else if("boolean"==typeof p&&(p=p?"true":"false"),p){if("retrieveLightData"==f&&0>e.indexOf(".contextData."))switch(k=m.substring(0,4),w=m.substring(4),m){case "transactionID":m="xact";break;case "channel":m="ch";break;case "campaign":m="v0";break;default:a.za(w)&&("prop"==
k?m="c"+w:"eVar"==k?m="v"+w:"list"==k?m="l"+w:"hier"==k&&(m="h"+w,p=p.substring(0,255)))}g+="&"+a.escape(m)+"="+a.escape(p)}}""!=g&&(g+="&."+c)}return g};a.hb=function(){var c="",b,d,f,e,g,m,p,k,n="",r="",s=e="";if(a.lightProfileID)b=a.K,(n=a.lightTrackVars)&&(n=","+n+","+a.ga.join(",")+",");else{b=a.e;if(a.pe||a.linkType)n=a.linkTrackVars,r=a.linkTrackEvents,a.pe&&(e=a.pe.substring(0,1).toUpperCase()+a.pe.substring(1),a[e]&&(n=a[e].yb,r=a[e].xb));n&&(n=","+n+","+a.B.join(",")+",");r&&(r=","+r+",",
n&&(n+=",events,"));a.events2&&(s+=(""!=s?",":"")+a.events2)}if(a.visitor&&1.5<=parseFloat(a.visitor.version)&&a.visitor.getCustomerIDs){e=q;if(g=a.visitor.getCustomerIDs())for(d in g)Object.prototype[d]||(f=g[d],e||(e={}),f.id&&(e[d+".id"]=f.id),f.authState&&(e[d+".as"]=f.authState));e&&(c+=a.o("cid",e))}a.AudienceManagement&&a.AudienceManagement.isReady()&&(c+=a.o("d",a.AudienceManagement.getEventCallConfigParams()));for(d=0;d<b.length;d++){e=b[d];g=a[e];f=e.substring(0,4);m=e.substring(4);!g&&
"events"==e&&s&&(g=s,s="");if(g&&(!n||0<=n.indexOf(","+e+","))){switch(e){case "supplementalDataID":e="sdid";break;case "timestamp":e="ts";break;case "dynamicVariablePrefix":e="D";break;case "visitorID":e="vid";break;case "marketingCloudVisitorID":e="mid";break;case "analyticsVisitorID":e="aid";break;case "audienceManagerLocationHint":e="aamlh";break;case "audienceManagerBlob":e="aamb";break;case "authState":e="as";break;case "pageURL":e="g";255<g.length&&(a.pageURLRest=g.substring(255),g=g.substring(0,
255));break;case "pageURLRest":e="-g";break;case "referrer":e="r";break;case "vmk":case "visitorMigrationKey":e="vmt";break;case "visitorMigrationServer":e="vmf";a.ssl&&a.visitorMigrationServerSecure&&(g="");break;case "visitorMigrationServerSecure":e="vmf";!a.ssl&&a.visitorMigrationServer&&(g="");break;case "charSet":e="ce";break;case "visitorNamespace":e="ns";break;case "cookieDomainPeriods":e="cdp";break;case "cookieLifetime":e="cl";break;case "variableProvider":e="vvp";break;case "currencyCode":e=
"cc";break;case "channel":e="ch";break;case "transactionID":e="xact";break;case "campaign":e="v0";break;case "latitude":e="lat";break;case "longitude":e="lon";break;case "resolution":e="s";break;case "colorDepth":e="c";break;case "javascriptVersion":e="j";break;case "javaEnabled":e="v";break;case "cookiesEnabled":e="k";break;case "browserWidth":e="bw";break;case "browserHeight":e="bh";break;case "connectionType":e="ct";break;case "homepage":e="hp";break;case "events":s&&(g+=(""!=g?",":"")+s);if(r)for(m=
g.split(","),g="",f=0;f<m.length;f++)p=m[f],k=p.indexOf("="),0<=k&&(p=p.substring(0,k)),k=p.indexOf(":"),0<=k&&(p=p.substring(0,k)),0<=r.indexOf(","+p+",")&&(g+=(g?",":"")+m[f]);break;case "events2":g="";break;case "contextData":c+=a.o("c",a[e],n,e);g="";break;case "lightProfileID":e="mtp";break;case "lightStoreForSeconds":e="mtss";a.lightProfileID||(g="");break;case "lightIncrementBy":e="mti";a.lightProfileID||(g="");break;case "retrieveLightProfiles":e="mtsr";break;case "deleteLightProfiles":e=
"mtsd";break;case "retrieveLightData":a.retrieveLightProfiles&&(c+=a.o("mts",a[e],n,e));g="";break;default:a.za(m)&&("prop"==f?e="c"+m:"eVar"==f?e="v"+m:"list"==f?e="l"+m:"hier"==f&&(e="h"+m,g=g.substring(0,255)))}g&&(c+="&"+e+"="+("pev"!=e.substring(0,3)?a.escape(g):g))}"pev3"==e&&a.c&&(c+=a.c)}return c};a.v=function(a){var b=a.tagName;if("undefined"!=""+a.Cb||"undefined"!=""+a.sb&&"HTML"!=(""+a.sb).toUpperCase())return"";b=b&&b.toUpperCase?b.toUpperCase():"";"SHAPE"==b&&(b="");b&&(("INPUT"==b||
"BUTTON"==b)&&a.type&&a.type.toUpperCase?b=a.type.toUpperCase():!b&&a.href&&(b="A"));return b};a.va=function(a){var b=a.href?a.href:"",d,f,e;d=b.indexOf(":");f=b.indexOf("?");e=b.indexOf("/");b&&(0>d||0<=f&&d>f||0<=e&&d>e)&&(f=a.protocol&&1<a.protocol.length?a.protocol:l.protocol?l.protocol:"",d=l.pathname.lastIndexOf("/"),b=(f?f+"//":"")+(a.host?a.host:l.host?l.host:"")+("/"!=h.substring(0,1)?l.pathname.substring(0,0>d?0:d)+"/":"")+b);return b};a.H=function(c){var b=a.v(c),d,f,e="",g=0;return b&&
(d=c.protocol,f=c.onclick,!c.href||"A"!=b&&"AREA"!=b||f&&d&&!(0>d.toLowerCase().indexOf("javascript"))?f?(e=a.replace(a.replace(a.replace(a.replace(""+f,"\r",""),"\n",""),"\t","")," ",""),g=2):"INPUT"==b||"SUBMIT"==b?(c.value?e=c.value:c.innerText?e=c.innerText:c.textContent&&(e=c.textContent),g=3):c.src&&"IMAGE"==b&&(e=c.src):e=a.va(c),e)?{id:e.substring(0,100),type:g}:0};a.Ab=function(c){for(var b=a.v(c),d=a.H(c);c&&!d&&"BODY"!=b;)if(c=c.parentElement?c.parentElement:c.parentNode)b=a.v(c),d=a.H(c);
d&&"BODY"!=b||(c=0);c&&(b=c.onclick?""+c.onclick:"",0<=b.indexOf(".tl(")||0<=b.indexOf(".trackLink("))&&(c=0);return c};a.rb=function(){var c,b,d=a.linkObject,f=a.linkType,e=a.linkURL,g,m;a.ha=1;d||(a.ha=0,d=a.clickObject);if(d){c=a.v(d);for(b=a.H(d);d&&!b&&"BODY"!=c;)if(d=d.parentElement?d.parentElement:d.parentNode)c=a.v(d),b=a.H(d);b&&"BODY"!=c||(d=0);if(d&&!a.linkObject){var p=d.onclick?""+d.onclick:"";if(0<=p.indexOf(".tl(")||0<=p.indexOf(".trackLink("))d=0}}else a.ha=1;!e&&d&&(e=a.va(d));e&&
!a.linkLeaveQueryString&&(g=e.indexOf("?"),0<=g&&(e=e.substring(0,g)));if(!f&&e){var n=0,r=0,q;if(a.trackDownloadLinks&&a.linkDownloadFileTypes)for(p=e.toLowerCase(),g=p.indexOf("?"),m=p.indexOf("#"),0<=g?0<=m&&m<g&&(g=m):g=m,0<=g&&(p=p.substring(0,g)),g=a.linkDownloadFileTypes.toLowerCase().split(","),m=0;m<g.length;m++)(q=g[m])&&p.substring(p.length-(q.length+1))=="."+q&&(f="d");if(a.trackExternalLinks&&!f&&(p=e.toLowerCase(),a.ya(p)&&(a.linkInternalFilters||(a.linkInternalFilters=k.location.hostname),
g=0,a.linkExternalFilters?(g=a.linkExternalFilters.toLowerCase().split(","),n=1):a.linkInternalFilters&&(g=a.linkInternalFilters.toLowerCase().split(",")),g))){for(m=0;m<g.length;m++)q=g[m],0<=p.indexOf(q)&&(r=1);r?n&&(f="e"):n||(f="e")}}a.linkObject=d;a.linkURL=e;a.linkType=f;if(a.trackClickMap||a.trackInlineStats)a.c="",d&&(f=a.pageName,e=1,d=d.sourceIndex,f||(f=a.pageURL,e=0),k.s_objectID&&(b.id=k.s_objectID,d=b.type=1),f&&b&&b.id&&c&&(a.c="&pid="+a.escape(f.substring(0,255))+(e?"&pidt="+e:"")+
"&oid="+a.escape(b.id.substring(0,100))+(b.type?"&oidt="+b.type:"")+"&ot="+c+(d?"&oi="+d:"")))};a.ib=function(){var c=a.ha,b=a.linkType,d=a.linkURL,f=a.linkName;b&&(d||f)&&(b=b.toLowerCase(),"d"!=b&&"e"!=b&&(b="o"),a.pe="lnk_"+b,a.pev1=d?a.escape(d):"",a.pev2=f?a.escape(f):"",c=1);a.abort&&(c=0);if(a.trackClickMap||a.trackInlineStats||a.ClickMap){var b={},d=0,e=a.cookieRead("s_sq"),g=e?e.split("&"):0,m,p,k,e=0;if(g)for(m=0;m<g.length;m++)p=g[m].split("="),f=a.unescape(p[0]).split(","),p=a.unescape(p[1]),
b[p]=f;f=a.account.split(",");m={};for(k in a.contextData)k&&!Object.prototype[k]&&"a.clickmap."==k.substring(0,11)&&(m[k]=a.contextData[k],a.contextData[k]="");a.c=a.o("c",m)+(a.c?a.c:"");if(c||a.c){c&&!a.c&&(e=1);for(p in b)if(!Object.prototype[p])for(k=0;k<f.length;k++)for(e&&(g=b[p].join(","),g==a.account&&(a.c+=("&"!=p.charAt(0)?"&":"")+p,b[p]=[],d=1)),m=0;m<b[p].length;m++)g=b[p][m],g==f[k]&&(e&&(a.c+="&u="+a.escape(g)+("&"!=p.charAt(0)?"&":"")+p+"&u=0"),b[p].splice(m,1),d=1);c||(d=1);if(d){e=
"";m=2;!c&&a.c&&(e=a.escape(f.join(","))+"="+a.escape(a.c),m=1);for(p in b)!Object.prototype[p]&&0<m&&0<b[p].length&&(e+=(e?"&":"")+a.escape(b[p].join(","))+"="+a.escape(p),m--);a.cookieWrite("s_sq",e)}}}return c};a.jb=function(){if(!a.wb){var c=new Date,b=r.location,d,f,e=f=d="",g="",m="",k="1.2",n=a.cookieWrite("s_cc","true",0)?"Y":"N",q="",s="";if(c.setUTCDate&&(k="1.3",(0).toPrecision&&(k="1.5",c=[],c.forEach))){k="1.6";f=0;d={};try{f=new Iterator(d),f.next&&(k="1.7",c.reduce&&(k="1.8",k.trim&&
(k="1.8.1",Date.parse&&(k="1.8.2",Object.create&&(k="1.8.5")))))}catch(t){}}d=screen.width+"x"+screen.height;e=navigator.javaEnabled()?"Y":"N";f=screen.pixelDepth?screen.pixelDepth:screen.colorDepth;g=a.w.innerWidth?a.w.innerWidth:a.d.documentElement.offsetWidth;m=a.w.innerHeight?a.w.innerHeight:a.d.documentElement.offsetHeight;try{a.b.addBehavior("#default#homePage"),q=a.b.Bb(b)?"Y":"N"}catch(u){}try{a.b.addBehavior("#default#clientCaps"),s=a.b.connectionType}catch(x){}a.resolution=d;a.colorDepth=
f;a.javascriptVersion=k;a.javaEnabled=e;a.cookiesEnabled=n;a.browserWidth=g;a.browserHeight=m;a.connectionType=s;a.homepage=q;a.wb=1}};a.L={};a.loadModule=function(c,b){var d=a.L[c];if(!d){d=k["AppMeasurement_Module_"+c]?new k["AppMeasurement_Module_"+c](a):{};a.L[c]=a[c]=d;d.Na=function(){return d.Ra};d.Sa=function(b){if(d.Ra=b)a[c+"_onLoad"]=b,a.ba(c+"_onLoad",[a,d],1)||b(a,d)};try{Object.defineProperty?Object.defineProperty(d,"onLoad",{get:d.Na,set:d.Sa}):d._olc=1}catch(f){d._olc=1}}b&&(a[c+"_onLoad"]=
b,a.ba(c+"_onLoad",[a,d],1)||b(a,d))};a.l=function(c){var b,d;for(b in a.L)if(!Object.prototype[b]&&(d=a.L[b])&&(d._olc&&d.onLoad&&(d._olc=0,d.onLoad(a,d)),d[c]&&d[c]()))return 1;return 0};a.mb=function(){var c=Math.floor(1E13*Math.random()),b=a.visitorSampling,d=a.visitorSamplingGroup,d="s_vsn_"+(a.visitorNamespace?a.visitorNamespace:a.account)+(d?"_"+d:""),f=a.cookieRead(d);if(b){f&&(f=parseInt(f));if(!f){if(!a.cookieWrite(d,c))return 0;f=c}if(f%1E4>v)return 0}return 1};a.M=function(c,b){var d,
f,e,g,m,k;for(d=0;2>d;d++)for(f=0<d?a.oa:a.e,e=0;e<f.length;e++)if(g=f[e],(m=c[g])||c["!"+g]){if(!b&&("contextData"==g||"retrieveLightData"==g)&&a[g])for(k in a[g])m[k]||(m[k]=a[g][k]);a[g]=m}};a.Ga=function(c,b){var d,f,e,g;for(d=0;2>d;d++)for(f=0<d?a.oa:a.e,e=0;e<f.length;e++)g=f[e],c[g]=a[g],b||c[g]||(c["!"+g]=1)};a.cb=function(a){var b,d,f,e,g,m=0,k,n="",q="";if(a&&255<a.length&&(b=""+a,d=b.indexOf("?"),0<d&&(k=b.substring(d+1),b=b.substring(0,d),e=b.toLowerCase(),f=0,"http://"==e.substring(0,
7)?f+=7:"https://"==e.substring(0,8)&&(f+=8),d=e.indexOf("/",f),0<d&&(e=e.substring(f,d),g=b.substring(d),b=b.substring(0,d),0<=e.indexOf("google")?m=",q,ie,start,search_key,word,kw,cd,":0<=e.indexOf("yahoo.co")&&(m=",p,ei,"),m&&k)))){if((a=k.split("&"))&&1<a.length){for(f=0;f<a.length;f++)e=a[f],d=e.indexOf("="),0<d&&0<=m.indexOf(","+e.substring(0,d)+",")?n+=(n?"&":"")+e:q+=(q?"&":"")+e;n&&q?k=n+"&"+q:q=""}d=253-(k.length-q.length)-b.length;a=b+(0<d?g.substring(0,d):"")+"?"+k}return a};a.Ma=function(c){var b=
a.d.visibilityState,d=["webkitvisibilitychange","visibilitychange"];b||(b=a.d.webkitVisibilityState);if(b&&"prerender"==b){if(c)for(b=0;b<d.length;b++)a.d.addEventListener(d[b],function(){var b=a.d.visibilityState;b||(b=a.d.webkitVisibilityState);"visible"==b&&c()});return!1}return!0};a.Y=!1;a.D=!1;a.Ta=function(){a.D=!0;a.i()};a.W=!1;a.Q=!1;a.Qa=function(c){a.marketingCloudVisitorID=c;a.Q=!0;a.i()};a.T=!1;a.N=!1;a.Ia=function(c){a.analyticsVisitorID=c;a.N=!0;a.i()};a.V=!1;a.P=!1;a.Ka=function(c){a.audienceManagerLocationHint=
c;a.P=!0;a.i()};a.U=!1;a.O=!1;a.Ja=function(c){a.audienceManagerBlob=c;a.O=!0;a.i()};a.La=function(c){a.maxDelay||(a.maxDelay=250);return a.l("_d")?(c&&setTimeout(function(){c()},a.maxDelay),!1):!0};a.X=!1;a.C=!1;a.na=function(){a.C=!0;a.i()};a.isReadyToTrack=function(){var c=!0,b=a.visitor;a.Y||a.D||(a.Ma(a.Ta)?a.D=!0:a.Y=!0);if(a.Y&&!a.D)return!1;b&&b.isAllowed()&&(a.W||a.marketingCloudVisitorID||!b.getMarketingCloudVisitorID||(a.W=!0,a.marketingCloudVisitorID=b.getMarketingCloudVisitorID([a,a.Qa]),
a.marketingCloudVisitorID&&(a.Q=!0)),a.T||a.analyticsVisitorID||!b.getAnalyticsVisitorID||(a.T=!0,a.analyticsVisitorID=b.getAnalyticsVisitorID([a,a.Ia]),a.analyticsVisitorID&&(a.N=!0)),a.V||a.audienceManagerLocationHint||!b.getAudienceManagerLocationHint||(a.V=!0,a.audienceManagerLocationHint=b.getAudienceManagerLocationHint([a,a.Ka]),a.audienceManagerLocationHint&&(a.P=!0)),a.U||a.audienceManagerBlob||!b.getAudienceManagerBlob||(a.U=!0,a.audienceManagerBlob=b.getAudienceManagerBlob([a,a.Ja]),a.audienceManagerBlob&&
(a.O=!0)),a.W&&!a.Q&&!a.marketingCloudVisitorID||a.T&&!a.N&&!a.analyticsVisitorID||a.V&&!a.P&&!a.audienceManagerLocationHint||a.U&&!a.O&&!a.audienceManagerBlob)&&(c=!1);a.X||a.C||(a.La(a.na)?a.C=!0:a.X=!0);a.X&&!a.C&&(c=!1);return c};a.k=q;a.p=0;a.callbackWhenReadyToTrack=function(c,b,d){var f;f={};f.Xa=c;f.Wa=b;f.Ua=d;a.k==q&&(a.k=[]);a.k.push(f);0==a.p&&(a.p=setInterval(a.i,100))};a.i=function(){var c;if(a.isReadyToTrack()&&(a.p&&(clearInterval(a.p),a.p=0),a.k!=q))for(;0<a.k.length;)c=a.k.shift(),
c.Wa.apply(c.Xa,c.Ua)};a.Oa=function(c){var b,d,f=q,e=q;if(!a.isReadyToTrack()){b=[];if(c!=q)for(d in f={},c)f[d]=c[d];e={};a.Ga(e,!0);b.push(f);b.push(e);a.callbackWhenReadyToTrack(a,a.track,b);return!0}return!1};a.gb=function(){var c=a.cookieRead("s_fid"),b="",d="",f;f=8;var e=4;if(!c||0>c.indexOf("-")){for(c=0;16>c;c++)f=Math.floor(Math.random()*f),b+="0123456789ABCDEF".substring(f,f+1),f=Math.floor(Math.random()*e),d+="0123456789ABCDEF".substring(f,f+1),f=e=16;c=b+"-"+d}a.cookieWrite("s_fid",
c,1)||(c=0);return c};a.t=a.track=function(c,b){var d,f=new Date,e="s"+Math.floor(f.getTime()/108E5)%10+Math.floor(1E13*Math.random()),g=f.getYear(),g="t="+a.escape(f.getDate()+"/"+f.getMonth()+"/"+(1900>g?g+1900:g)+" "+f.getHours()+":"+f.getMinutes()+":"+f.getSeconds()+" "+f.getDay()+" "+f.getTimezoneOffset());a.visitor&&(a.visitor.eb&&(a.authState=a.visitor.eb()),!a.supplementalDataID&&a.visitor.getSupplementalDataID&&(a.supplementalDataID=a.visitor.getSupplementalDataID("AppMeasurement:"+a._in,
a.expectSupplementalData?!1:!0)));a.l("_s");a.Oa(c)||(b&&a.M(b),c&&(d={},a.Ga(d,0),a.M(c)),a.mb()&&(a.analyticsVisitorID||a.marketingCloudVisitorID||(a.fid=a.gb()),a.rb(),a.usePlugins&&a.doPlugins&&a.doPlugins(a),a.account&&(a.abort||(a.trackOffline&&!a.timestamp&&(a.timestamp=Math.floor(f.getTime()/1E3)),f=k.location,a.pageURL||(a.pageURL=f.href?f.href:f),a.referrer||a.Ha||(a.referrer=r.document.referrer),a.Ha=1,a.referrer=a.cb(a.referrer),a.l("_g")),a.ib()&&!a.abort&&(a.jb(),g+=a.hb(),a.qb(e,g),
a.l("_t"),a.referrer=""))),c&&a.M(d,1));a.abort=a.supplementalDataID=a.timestamp=a.pageURLRest=a.linkObject=a.clickObject=a.linkURL=a.linkName=a.linkType=k.s_objectID=a.pe=a.pev1=a.pev2=a.pev3=a.c=a.lightProfileID=0};a.tl=a.trackLink=function(c,b,d,f,e){a.linkObject=c;a.linkType=b;a.linkName=d;e&&(a.j=c,a.r=e);return a.track(f)};a.trackLight=function(c,b,d,f){a.lightProfileID=c;a.lightStoreForSeconds=b;a.lightIncrementBy=d;return a.track(f)};a.clearVars=function(){var c,b;for(c=0;c<a.e.length;c++)if(b=
a.e[c],"prop"==b.substring(0,4)||"eVar"==b.substring(0,4)||"hier"==b.substring(0,4)||"list"==b.substring(0,4)||"channel"==b||"events"==b||"eventList"==b||"products"==b||"productList"==b||"purchaseID"==b||"transactionID"==b||"state"==b||"zip"==b||"campaign"==b)a[b]=void 0};a.tagContainerMarker="";a.qb=function(c,b){var d,f=a.trackingServer;d="";var e=a.dc,g="sc.",k=a.visitorNamespace;f?a.trackingServerSecure&&a.ssl&&(f=a.trackingServerSecure):(k||(k=a.account,f=k.indexOf(","),0<=f&&(k=k.substring(0,
f)),k=k.replace(/[^A-Za-z0-9]/g,"")),d||(d="2o7.net"),e=e?(""+e).toLowerCase():"d1","2o7.net"==d&&("d1"==e?e="112":"d2"==e&&(e="122"),g=""),f=k+"."+e+"."+g+d);d=a.ssl?"https://":"http://";e=a.AudienceManagement&&a.AudienceManagement.isReady();d+=f+"/b/ss/"+a.account+"/"+(a.mobile?"5.":"")+(e?"10":"1")+"/JS-"+a.version+(a.vb?"T":"")+(a.tagContainerMarker?"-"+a.tagContainerMarker:"")+"/"+c+"?AQB=1&ndh=1&pf=1&"+(e?"callback=s_c_il["+a._in+"].AudienceManagement.passData&":"")+b+"&AQE=1";a.ab(d);a.da()};
a.ab=function(c){a.g||a.kb();a.g.push(c);a.fa=a.u();a.Fa()};a.kb=function(){a.g=a.nb();a.g||(a.g=[])};a.nb=function(){var c,b;if(a.ka()){try{(b=k.localStorage.getItem(a.ia()))&&(c=k.JSON.parse(b))}catch(d){}return c}};a.ka=function(){var c=!0;a.trackOffline&&a.offlineFilename&&k.localStorage&&k.JSON||(c=!1);return c};a.wa=function(){var c=0;a.g&&(c=a.g.length);a.A&&c++;return c};a.da=function(){if(!a.A)if(a.xa=q,a.ja)a.fa>a.J&&a.Da(a.g),a.ma(500);else{var c=a.Va();if(0<c)a.ma(c);else if(c=a.ua())a.A=
1,a.pb(c),a.tb(c)}};a.ma=function(c){a.xa||(c||(c=0),a.xa=setTimeout(a.da,c))};a.Va=function(){var c;if(!a.trackOffline||0>=a.offlineThrottleDelay)return 0;c=a.u()-a.Ca;return a.offlineThrottleDelay<c?0:a.offlineThrottleDelay-c};a.ua=function(){if(0<a.g.length)return a.g.shift()};a.pb=function(c){if(a.debugTracking){var b="AppMeasurement Debug: "+c;c=c.split("&");var d;for(d=0;d<c.length;d++)b+="\n\t"+a.unescape(c[d]);a.ob(b)}};a.Pa=function(){return a.marketingCloudVisitorID||a.analyticsVisitorID};
a.S=!1;var s;try{s=JSON.parse('{"x":"y"}')}catch(x){s=null}s&&"y"==s.x?(a.S=!0,a.R=function(a){return JSON.parse(a)}):k.$&&k.$.parseJSON?(a.R=function(a){return k.$.parseJSON(a)},a.S=!0):a.R=function(){return null};a.tb=function(c){var b,d,f;a.Pa()&&2047<c.length&&("undefined"!=typeof XMLHttpRequest&&(b=new XMLHttpRequest,"withCredentials"in b?d=1:b=0),b||"undefined"==typeof XDomainRequest||(b=new XDomainRequest,d=2),b&&a.AudienceManagement&&a.AudienceManagement.isReady()&&(a.S?b.pa=!0:b=0));!b&&
a.lb&&(c=c.substring(0,2047));!b&&a.d.createElement&&a.AudienceManagement&&a.AudienceManagement.isReady()&&(b=a.d.createElement("SCRIPT"))&&"async"in b&&((f=(f=a.d.getElementsByTagName("HEAD"))&&f[0]?f[0]:a.d.body)?(b.type="text/javascript",b.setAttribute("async","async"),d=3):b=0);b||(b=new Image,b.alt="");b.ra=function(){try{a.la&&(clearTimeout(a.la),a.la=0),b.timeout&&(clearTimeout(b.timeout),b.timeout=0)}catch(c){}};b.onload=b.ub=function(){b.ra();a.$a();a.Z();a.A=0;a.da();if(b.pa){b.pa=!1;try{var c=
a.R(b.responseText);AudienceManagement.passData(c)}catch(d){}}};b.onabort=b.onerror=b.bb=function(){b.ra();(a.trackOffline||a.ja)&&a.A&&a.g.unshift(a.Za);a.A=0;a.fa>a.J&&a.Da(a.g);a.Z();a.ma(500)};b.onreadystatechange=function(){4==b.readyState&&(200==b.status?b.ub():b.bb())};a.Ca=a.u();if(1==d||2==d){var e=c.indexOf("?");f=c.substring(0,e);e=c.substring(e+1);e=e.replace(/&callback=[a-zA-Z0-9_.\[\]]+/,"");1==d?(b.open("POST",f,!0),b.send(e)):2==d&&(b.open("POST",f),b.send(e))}else if(b.src=c,3==d){if(a.Aa)try{f.removeChild(a.Aa)}catch(g){}f.firstChild?
f.insertBefore(b,f.firstChild):f.appendChild(b);a.Aa=a.Ya}b.abort&&(a.la=setTimeout(b.abort,5E3));a.Za=c;a.Ya=k["s_i_"+a.replace(a.account,",","_")]=b;if(a.useForcedLinkTracking&&a.F||a.r)a.forcedLinkTrackingTimeout||(a.forcedLinkTrackingTimeout=250),a.aa=setTimeout(a.Z,a.forcedLinkTrackingTimeout)};a.$a=function(){if(a.ka()&&!(a.Ba>a.J))try{k.localStorage.removeItem(a.ia()),a.Ba=a.u()}catch(c){}};a.Da=function(c){if(a.ka()){a.Fa();try{k.localStorage.setItem(a.ia(),k.JSON.stringify(c)),a.J=a.u()}catch(b){}}};
a.Fa=function(){if(a.trackOffline){if(!a.offlineLimit||0>=a.offlineLimit)a.offlineLimit=10;for(;a.g.length>a.offlineLimit;)a.ua()}};a.forceOffline=function(){a.ja=!0};a.forceOnline=function(){a.ja=!1};a.ia=function(){return a.offlineFilename+"-"+a.visitorNamespace+a.account};a.u=function(){return(new Date).getTime()};a.ya=function(a){a=a.toLowerCase();return 0!=a.indexOf("#")&&0!=a.indexOf("about:")&&0!=a.indexOf("opera:")&&0!=a.indexOf("javascript:")?!0:!1};a.setTagContainer=function(c){var b,d,
f;a.vb=c;for(b=0;b<a._il.length;b++)if((d=a._il[b])&&"s_l"==d._c&&d.tagContainerName==c){a.M(d);if(d.lmq)for(b=0;b<d.lmq.length;b++)f=d.lmq[b],a.loadModule(f.n);if(d.ml)for(f in d.ml)if(a[f])for(b in c=a[f],f=d.ml[f],f)!Object.prototype[b]&&("function"!=typeof f[b]||0>(""+f[b]).indexOf("s_c_il"))&&(c[b]=f[b]);if(d.mmq)for(b=0;b<d.mmq.length;b++)f=d.mmq[b],a[f.m]&&(c=a[f.m],c[f.f]&&"function"==typeof c[f.f]&&(f.a?c[f.f].apply(c,f.a):c[f.f].apply(c)));if(d.tq)for(b=0;b<d.tq.length;b++)a.track(d.tq[b]);
d.s=a;break}};a.Util={urlEncode:a.escape,urlDecode:a.unescape,cookieRead:a.cookieRead,cookieWrite:a.cookieWrite,getQueryParam:function(c,b,d){var f;b||(b=a.pageURL?a.pageURL:k.location);d||(d="&");return c&&b&&(b=""+b,f=b.indexOf("?"),0<=f&&(b=d+b.substring(f+1)+d,f=b.indexOf(d+c+"="),0<=f&&(b=b.substring(f+d.length+c.length+1),f=b.indexOf(d),0<=f&&(b=b.substring(0,f)),0<b.length)))?a.unescape(b):""}};a.B="supplementalDataID timestamp dynamicVariablePrefix visitorID marketingCloudVisitorID analyticsVisitorID audienceManagerLocationHint authState fid vmk visitorMigrationKey visitorMigrationServer visitorMigrationServerSecure charSet visitorNamespace cookieDomainPeriods fpCookieDomainPeriods cookieLifetime pageName pageURL referrer contextData currencyCode lightProfileID lightStoreForSeconds lightIncrementBy retrieveLightProfiles deleteLightProfiles retrieveLightData pe pev1 pev2 pev3 pageURLRest".split(" ");
a.e=a.B.concat("purchaseID variableProvider channel server pageType transactionID campaign state zip events events2 products audienceManagerBlob tnt".split(" "));a.ga="timestamp charSet visitorNamespace cookieDomainPeriods cookieLifetime contextData lightProfileID lightStoreForSeconds lightIncrementBy".split(" ");a.K=a.ga.slice(0);a.oa="account allAccounts debugTracking visitor trackOffline offlineLimit offlineThrottleDelay offlineFilename usePlugins doPlugins configURL visitorSampling visitorSamplingGroup linkObject clickObject linkURL linkName linkType trackDownloadLinks trackExternalLinks trackClickMap trackInlineStats linkLeaveQueryString linkTrackVars linkTrackEvents linkDownloadFileTypes linkExternalFilters linkInternalFilters useForcedLinkTracking forcedLinkTrackingTimeout trackingServer trackingServerSecure ssl abort mobile dc lightTrackVars maxDelay expectSupplementalData AudienceManagement".split(" ");
for(n=0;250>=n;n++)76>n&&(a.e.push("prop"+n),a.K.push("prop"+n)),a.e.push("eVar"+n),a.K.push("eVar"+n),6>n&&a.e.push("hier"+n),4>n&&a.e.push("list"+n);n="latitude longitude resolution colorDepth javascriptVersion javaEnabled cookiesEnabled browserWidth browserHeight connectionType homepage".split(" ");a.e=a.e.concat(n);a.B=a.B.concat(n);a.ssl=0<=k.location.protocol.toLowerCase().indexOf("https");a.charSet="UTF-8";a.contextData={};a.offlineThrottleDelay=0;a.offlineFilename="AppMeasurement.offline";
a.Ca=0;a.fa=0;a.J=0;a.Ba=0;a.linkDownloadFileTypes="exe,zip,wav,mp3,mov,mpg,avi,wmv,pdf,doc,docx,xls,xlsx,ppt,pptx";a.w=k;a.d=k.document;try{a.lb="Microsoft Internet Explorer"==navigator.appName}catch(y){}a.Z=function(){a.aa&&(k.clearTimeout(a.aa),a.aa=q);a.j&&a.F&&a.j.dispatchEvent(a.F);a.r&&("function"==typeof a.r?a.r():a.j&&a.j.href&&(a.d.location=a.j.href));a.j=a.F=a.r=0};a.Ea=function(){a.b=a.d.body;a.b?(a.q=function(c){var b,d,f,e,g;if(!(a.d&&a.d.getElementById("cppXYctnr")||c&&c["s_fe_"+a._in])){if(a.qa)if(a.useForcedLinkTracking)a.b.removeEventListener("click",
a.q,!1);else{a.b.removeEventListener("click",a.q,!0);a.qa=a.useForcedLinkTracking=0;return}else a.useForcedLinkTracking=0;a.clickObject=c.srcElement?c.srcElement:c.target;try{if(!a.clickObject||a.I&&a.I==a.clickObject||!(a.clickObject.tagName||a.clickObject.parentElement||a.clickObject.parentNode))a.clickObject=0;else{var m=a.I=a.clickObject;a.ea&&(clearTimeout(a.ea),a.ea=0);a.ea=setTimeout(function(){a.I==m&&(a.I=0)},1E4);f=a.wa();a.track();if(f<a.wa()&&a.useForcedLinkTracking&&c.target){for(e=c.target;e&&
e!=a.b&&"A"!=e.tagName.toUpperCase()&&"AREA"!=e.tagName.toUpperCase();)e=e.parentNode;if(e&&(g=e.href,a.ya(g)||(g=0),d=e.target,c.target.dispatchEvent&&g&&(!d||"_self"==d||"_top"==d||"_parent"==d||k.name&&d==k.name))){try{b=a.d.createEvent("MouseEvents")}catch(n){b=new k.MouseEvent}if(b){try{b.initMouseEvent("click",c.bubbles,c.cancelable,c.view,c.detail,c.screenX,c.screenY,c.clientX,c.clientY,c.ctrlKey,c.altKey,c.shiftKey,c.metaKey,c.button,c.relatedTarget)}catch(q){b=0}b&&(b["s_fe_"+a._in]=b.s_fe=
1,c.stopPropagation(),c.stopImmediatePropagation&&c.stopImmediatePropagation(),c.preventDefault(),a.j=c.target,a.F=b)}}}}}catch(r){a.clickObject=0}}},a.b&&a.b.attachEvent?a.b.attachEvent("onclick",a.q):a.b&&a.b.addEventListener&&(navigator&&(0<=navigator.userAgent.indexOf("WebKit")&&a.d.createEvent||0<=navigator.userAgent.indexOf("Firefox/2")&&k.MouseEvent)&&(a.qa=1,a.useForcedLinkTracking=1,a.b.addEventListener("click",a.q,!0)),a.b.addEventListener("click",a.q,!1))):setTimeout(a.Ea,30)};a.Ea();a.loadModule("ClickMap")}
function s_gi(a){var k,q=window.s_c_il,r,n,t=a.split(","),u,s,x=0;if(q)for(r=0;!x&&r<q.length;){k=q[r];if("s_c"==k._c&&(k.account||k.oun))if(k.account&&k.account==a)x=1;else for(n=k.account?k.account:k.oun,n=k.allAccounts?k.allAccounts:n.split(","),u=0;u<t.length;u++)for(s=0;s<n.length;s++)t[u]==n[s]&&(x=1);r++}x||(k=new AppMeasurement);k.setAccount?k.setAccount(a):k.sa&&k.sa(a);return k}AppMeasurement.getInstance=s_gi;window.s_objectID||(window.s_objectID=0);
function s_pgicq(){var a=window,k=a.s_giq,q,r,n;if(k)for(q=0;q<k.length;q++)r=k[q],n=s_gi(r.oun),n.setAccount(r.un),n.setTagContainer(r.tagContainerName);a.s_giq=0}s_pgicq();

function AppMeasurement_Module_Integrate(l){var c=this;c.s=l;var e=window;e.s_c_in||(e.s_c_il=[],e.s_c_in=0);c._il=e.s_c_il;c._in=e.s_c_in;c._il[c._in]=c;e.s_c_in++;c._c="s_m";c.list=[];c.add=function(d,b){var a;b||(b="s_Integrate_"+d);e[b]||(e[b]={});a=c[d]=e[b];a.a=d;a.e=c;a._c=0;a._d=0;void 0==a.disable&&(a.disable=0);a.get=function(b,d){var f=document,h=f.getElementsByTagName("HEAD"),k;if(!a.disable&&(d||(v="s_"+c._in+"_Integrate_"+a.a+"_get_"+a._c),a._c++,a.VAR=v,a.CALLBACK="s_c_il["+c._in+"]."+
a.a+".callback",a.delay(),h=h&&0<h.length?h[0]:f.body))try{k=f.createElement("SCRIPT"),k.type="text/javascript",k.setAttribute("async","async"),k.src=c.c(a,b),0>b.indexOf("[CALLBACK]")&&(k.onload=k.onreadystatechange=function(){a.callback(e[v])}),h.firstChild?h.insertBefore(k,h.firstChild):h.appendChild(k)}catch(l){}};a.callback=function(b){var c;if(b)for(c in b)Object.prototype[c]||(a[c]=b[c]);a.ready()};a.beacon=function(b){var d="s_i_"+c._in+"_Integrate_"+a.a+"_"+a._c;a.disable||(a._c++,d=e[d]=
new Image,d.src=c.c(a,b))};a.script=function(b){a.get(b,1)};a.delay=function(){a._d++};a.ready=function(){a._d--;a.disable||l.delayReady()};c.list.push(d)};c._g=function(d){var b,a=(d?"use":"set")+"Vars";for(d=0;d<c.list.length;d++)if((b=c[c.list[d]])&&!b.disable&&b[a])try{b[a](l,b)}catch(e){}};c._t=function(){c._g(1)};c._d=function(){var d,b;for(d=0;d<c.list.length;d++)if((b=c[c.list[d]])&&!b.disable&&0<b._d)return 1;return 0};c.c=function(c,b){var a,e,g,f;"http"!=b.toLowerCase().substring(0,4)&&
(b="http://"+b);l.ssl&&(b=l.replace(b,"http:","https:"));c.RAND=Math.floor(1E13*Math.random());for(a=0;0<=a;)a=b.indexOf("[",a),0<=a&&(e=b.indexOf("]",a),e>a&&(g=b.substring(a+1,e),2<g.length&&"s."==g.substring(0,2)?(f=l[g.substring(2)])||(f=""):(f=""+c[g],f!=c[g]&&parseFloat(f)!=c[g]&&(g=0)),g&&(b=b.substring(0,a)+encodeURIComponent(f)+b.substring(e+1)),a=e));return b}}

/*
 ============== DO NOT ALTER ANYTHING BELOW THIS LINE ! ============

 Adobe Visitor API for JavaScript version: 1.1
 Copyright 1996-2013 Adobe, Inc. All Rights Reserved
 More info available at http://www.omniture.com
*/
function Visitor(k){var a=this;a.version="1.1";var f=window;f.s_c_in||(f.s_c_il=[],f.s_c_in=0);a._c="Visitor";a._il=f.s_c_il;a._in=f.s_c_in;a._il[a._in]=a;f.s_c_in++;var i=f.document,h=f.z;h||(h=null);var j=f.A;j||(j=!0);var l=f.w;l||(l=!1);a.s=function(){var a;!a&&f.location&&(a=f.location.hostname);if(a)if(/^[0-9.]+$/.test(a))a="";else{var d=a.split("."),b=d.length-1,e=b-1;1<b&&2>=d[b].length&&0>",am,aq,ax,cc,cf,cg,ch,cv,cz,de,dj,dk,eu,fm,fo,ga,gd,gf,gl,gm,gq,gs,gw,hm,li,lu,md,mh,mp,mq,ms,ne,nl,nu,pm,si,sk,sm,sr,su,tc,td,tf,tg,tk,tv,va,vg,vu,wf,yt,".indexOf(","+
d[b]+",")&&e--;if(0<e)for(a="";b>=e;)a=d[b]+(a?".":"")+a,b--}return a};a.cookieRead=function(a){var d=(";"+i.cookie).split(" ").join(";"),b=d.indexOf(";"+a+"="),e=0>b?b:d.indexOf(";",b+1);return 0>b?"":decodeURIComponent(d.substring(b+2+a.length,0>e?d.length:e))};a.cookieWrite=function(c,d,b){var e=a.cookieLifetime,g,d=""+d,e=e?(""+e).toUpperCase():"";b&&"SESSION"!=e&&"NONE"!=e?(g=""!=d?parseInt(e?e:0):-60)?(b=new Date,b.setTime(b.getTime()+1E3*g)):1==b&&(b=new Date,g=b.getYear(),b.setYear(g+2+(1900>
g?1900:0))):b=0;return c&&"NONE"!=e?(i.cookie=c+"="+encodeURIComponent(d)+"; path=/;"+(b?" expires="+b.toGMTString()+";":"")+(a.k?" domain="+a.k+";":""),a.cookieRead(c)==d):0};a.b=h;a.j=function(a,d){try{"function"==typeof a?a.apply(f,d):a[1].apply(a[0],d)}catch(b){}};a.u=function(c,d){d&&(a.b==h&&(a.b={}),void 0==a.b[c]&&(a.b[c]=[]),a.b[c].push(d))};a.p=function(c,d){if(a.b!=h){var b=a.b[c];if(b)for(;0<b.length;)a.j(b.shift(),d)}};a.c=h;a.t=function(c,d,b){!d&&b&&b();var e=i.getElementsByTagName("HEAD")[0],
g=i.createElement("SCRIPT");g.type="text/javascript";g.setAttribute("async","async");g.src=d;e.firstChild?e.insertBefore(g,e.firstChild):e.appendChild(g);a.c==h&&(a.c={});a.c[c]=setTimeout(b,a.loadTimeout)};a.q=function(c){a.c!=h&&a.c[c]&&(clearTimeout(a.c[c]),a.c[c]=0)};a.l=l;a.m=l;a.isAllowed=function(){if(!a.l&&(a.l=j,a.cookieRead(a.cookieName)||a.cookieWrite(a.cookieName,"T",1)))a.m=j;return a.m};a.a=h;a.n=l;a.i=function(){if(!a.n){a.n=j;var c=a.cookieRead(a.cookieName),d,b,e,g,f=new Date;if(c&&
"T"!=c){c=c.split("|");1==c.length%2&&c.pop();for(d=0;d<c.length;d+=2)if(b=c[d].split("-"),e=b[0],g=c[d+1],b=1<b.length?parseInt(b[1]):0,e&&g&&(!b||f.getTime()<1E3*b))a.f(e,g,1),0<b&&(a.a["expire"+e]=b)}if(!a.d("MCAID")&&(c=a.cookieRead("s_vi")))c=c.split("|"),1<c.length&&0<=c[0].indexOf("v1")&&(g=c[1],d=g.indexOf("["),0<=d&&(g=g.substring(0,d)),g&&g.match(/^[0-9a-fA-F\-]+$/)&&a.f("MCAID",g))}};a.v=function(){var c="",d,b;for(d in a.a)!Object.prototype[d]&&a.a[d]&&"expire"!=d.substring(0,6)&&(b=a.a[d],
c+=(c?"|":"")+d+(a.a["expire"+d]?"-"+a.a["expire"+d]:"")+"|"+b);a.cookieWrite(a.cookieName,c,1)};a.d=function(c){return a.a!=h?a.a[c]:h};a.f=function(c,d,b){a.a==h&&(a.a={});a.a[c]=d;b||a.v()};a.o=function(c,d){var b=new Date;b.setTime(b.getTime()+1E3*d);a.a==h&&(a.a={});a.a["expire"+c]=Math.floor(b.getTime()/1E3)};a.r=function(a){if(a&&("object"==typeof a&&(a=a.visitorID?a.visitorID:a.id?a.id:a.uuid?a.uuid:""+a),a&&(a=a.toUpperCase(),"NOTARGET"==a&&(a="NONE")),!a||"NONE"!=a&&!a.match(/^[0-9a-fA-F\-]+$/)))a=
"";return a};a.g=function(c,d){var b;a.q(c);b=a.d(c);b||(b=a.r(d))&&a.f(c,b);if("object"==typeof d){var e=86400;"MCAAMID"==c&&(void 0!=d.id_sync_ttl&&d.id_sync_ttl&&(e=parseInt(d.id_sync_ttl)),a.o(c,e),a.o("MCAAMLH",e),d.dcs_region&&a.f("MCAAMLH",d.dcs_region))}a.p(c,["NONE"!=b?b:""])};a.e=h;a.h=function(c,d,b){if(a.isAllowed()){a.i();var e=a.d(c);if(e)return"NONE"==e&&a.j(b,[""]),"NONE"!=e?e:"";if(a.e==h||void 0==a.e[c])a.e==h&&(a.e={}),a.e[c]=j,a.t(c,d,function(){if(!a.d(c)){var b="";if("MCMID"==
c){var d=b="",e,f,h=10,i=10;for(e=0;19>e;e++)f=Math.floor(Math.random()*h),b+="0123456789".substring(f,f+1),h=0==e&&9==f?3:10,f=Math.floor(Math.random()*i),d+="0123456789".substring(f,f+1),i=0==e&&9==f?3:10;b+=d}a.g(c,b)}});a.u(c,b)}return""};a.setMarketingCloudVisitorID=function(c){a.g("MCMID",c)};a.getMarketingCloudVisitorID=function(c){var d=a.marketingCloudServer,b="";a.loadSSL&&a.marketingCloudServerSecure&&(d=a.marketingCloudServerSecure);d&&(b="http"+(a.loadSSL?"s":"")+"://"+d+"/id?d_rtbd=json&d_cid="+
encodeURIComponent(a.namespace)+"&d_cb=s_c_il%5B"+a._in+"%5D.setMarketingCloudVisitorID");return a.h("MCMID",b,c)};a.setAudienceManagerVisitorID=function(c){a.g("MCAAMID",c)};a.getAudienceManagerVisitorID=function(c){var d=a.audienceManagerServer,b="";a.loadSSL&&a.audienceManagerServerSecure&&(d=a.audienceManagerServerSecure);d&&(b="http"+(a.loadSSL?"s":"")+"://"+d+"/id?d_rtbd=json&d_cb=s_c_il%5B"+a._in+"%5D.setAudienceManagerVisitorID");return a.h("MCAAMID",b,c)};a.getAudienceManagerLocationHint=
function(){a.i();var c=a.d("MCAAMLH");return c?c:0};a.setAnalyticsVisitorID=function(c){a.i();a.g("MCAID",c)};a.getAnalyticsVisitorID=function(c){var d=a.trackingServer,b="";a.loadSSL&&a.trackingServerSecure&&(d=a.trackingServerSecure);d&&(b="http"+(a.loadSSL?"s":"")+"://"+d+"/id?callback=s_c_il%5B"+a._in+"%5D.setAnalyticsVisitorID");return a.h("MCAID",b,c)};a.getVisitorID=function(c){return a.getMarketingCloudVisitorID(c)};a.namespace=k;a.cookieName="AMCV_"+k;a.k=a.s();a.loadSSL=0<=f.location.protocol.toLowerCase().indexOf("https");
a.loadTimeout=500;a.marketingCloudServer=a.audienceManagerServer="dpm.demdex.net"}Visitor.getInstance=function(k){var a,f=window.s_c_il,i;if(f)for(i=0;i<f.length;i++)if((a=f[i])&&"Visitor"==a._c&&a.namespace==k)return a;return new Visitor(k)};
