/**
 * @file
 * HCP2 Specific JavaScript for Web Standards.
 */

(function ($) {

  Drupal.behaviors.pfizer_webstandards_hcp2 = {
    attach: function (context, settings) {
      context = $(context);

      Drupal.pfizer_webstandards_hcp2.preparePoll(context, settings);
      Drupal.pfizer_webstandards_hcp2.prepareFiveStar(context, settings);
    }
  };

  Drupal.pfizer_webstandards_hcp2 = (function() {

    return {
      /**
       * Handles poll submission events.
       * 
       * @return {[type]} [description]
       */
      'preparePoll' : function(context, settings) {
        if (typeof build_s == 'undefined') {
          return;
        }

        // find all poll forms by classname
        var forms = context.find('.pfizerpoll-form').parents('form');
        
        if (forms.length) {
          $.each(forms, function (i, form) {
            form = $(form);

            var submit_handler = function (e) {
              // if the user made a selection, we send the event; this could be multiple elements
              // (in case of checkboxes)
              var checked = form.find('input[type=radio]:checked,input[type=checkbox]:checked');
              var values = [];

              for (var i = 0; i < checked.length; i++) {
                values.push($(checked[i]).val());
              }

              var value = values.join(',');
              if (value) {
                var s = build_s();

                // event41 is 'Poll Submitted'
                var event_num = 'event41';
                s.events = s.apl(s.events, event_num, ',', 2);

                // page url
                s.prop23 = 'poll:' + s.custom.scStrip(location.href.split(':')[1]);

                // poll type (customer feedback vs quick poll)
                s.prop63 = form.attr('data-im-poll-type') == 'customer_feedback' ? 'Customer Feedback' : 'Quick Poll';

                // category name
                s.prop64 = form.attr('data-im-question-category');

                // poll question
                s.prop65 = form.attr('data-im-question');

                // poll answer
                s.prop66 = value;

                s.linkTrackVars = s.apl(s.linkTrackVars, 'events,prop23,prop63,prop64,prop65,prop66',',', 2);
                s.linkTrackEvents = s.apl(s.linkTrackEvents, event_num, ',', 2);

                s.useForcedLinkTracking = true;
                s.forcedLinkTrackingTimeout = 500;

                s.tl(true, 'o', 'content_poll');
                s.pfClearVars();
              }
            };

            // bind submit handler
            form.find('input[type=submit]').mousedown(submit_handler);
          })
        }
      },

      /**
       * Handles fivestar submission events.
       * 
       * @return {[type]} [description]
       */
      'prepareFiveStar': function(context, settings) {
        if (typeof build_s == 'undefined') {
          return;
        }

        // find all fivestar forms by classname
        var forms = context.find('form.fivestar-widget');
        if (forms.length) {
          $.each(forms, function (i, form) {
            form = $(form);

            var submit_handler = function (e) {
              // extract submitted rating from the anchor's text
              var value = $(this).text();
              var matches = value.match(/(\d)\/\d$/i);

              // if the user made a selection, we send the event
              if (typeof matches[1] != 'undefined') {
                var rating = matches[1];

                // user may rate, then click the same star again; we want to avoid sending
                // duplicate ratings
                if (form.attr('pfizer-ws-hcp2-processed') && form.attr('pfizer-ws-hcp2-processed') == rating) {
                  return;
                }

                var s = build_s();

                s.events = s.apl(s.events, 'event40,event42=' + rating, ',', 2);
                s.eVar3 = 'rating:' + s.pageName;
                s.eVar62 = 'rating' + rating;

                s.linkTrackVars = s.apl(s.linkTrackVars, 'events,eVar3,eVar62',',', 2);
                s.linkTrackEvents = s.apl(s.linkTrackEvents, 'event40,event42', ',', 2);

                s.useForcedLinkTracking = true;
                s.forcedLinkTrackingTimeout = 500;

                s.tl(true, 'o', 'content_rate');
                s.pfClearVars();

                // track submitted rating
                form.attr('pfizer-ws-hcp2-processed', rating);
              }
            };

            // bind submit handler
            form.find('.star a').click(submit_handler);
          })
        }
      }

    };

  })();

})(jQuery);


/**
 * HCP2 specific JS function.
 *
 * @param s
 *   Report Suite Object
 */
function pfizerWebstandardsHCP2(s) {
  "use strict";

  // Check if we are on a search results page:
  var searchUrls = ['search/*', 'search/site/*'];
  if (pfizerWebstandardsUrlMatch(searchUrls)) {
    // We are on a search page.
    if (typeof pfizerWebstandardsHcp2Keywords !== 'undefined' && pfizerWebstandardsHcp2Keywords) {
      // All HCP searches are site wide.
      s.pfSearch(pfizerWebstandardsHcp2Keywords, pfizerWebstandardsHcp2SearchType, pfizerWebstandardsHcp2NumFound);
    }

    s.pfSiteTool('Search');
  }

  jQuery('.BrightcoveExperience').once(function () {
    s.pfVideoOpen();
  });

  pfizerWsCheckBanner(s);

  var mkto_segment = s.Util.getQueryParam('mkto_segment');
  if (mkto_segment.length > 0) {
    s.eVar66 = mkto_segment;
    s.linkTrackVars = s.apl(s.linkTrackVars, 'eVar66', ',', 2);
  }

  var utm_medium = s.Util.getQueryParam('utm_medium');
  if (utm_medium.length > 0) {
    s.eVar67 = utm_medium;
    s.linkTrackVars = s.apl(s.linkTrackVars, 'eVar67', ',', 2);
  }

  var utm_term = s.Util.getQueryParam('utm_term');
  if (utm_term.length > 0) {
    s.eVar70 = utm_term;
    s.linkTrackVars = s.apl(s.linkTrackVars, 'eVar70', ',', 2);
  }

  var utm_source = s.Util.getQueryParam('utm_source');
  if (utm_source.length > 0) {
    s.eVar71 = utm_source;
    s.linkTrackVars = s.apl(s.linkTrackVars, 'eVar71', ',', 2);
  }

  var utm_campaign = s.Util.getQueryParam('utm_campaign');
  if (utm_campaign.length > 0) {
    s.eVar72 = utm_campaign;
    s.linkTrackVars = s.apl(s.linkTrackVars, 'eVar72', ',', 2);
  }
}

