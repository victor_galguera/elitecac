<?php
/**
 * @file
 * Include file to manage the Web Standards custom links configuration.
 *
 * Note: uses ctools export crud to make the data exportable by features.
 */

/**
 * This function provides the custom links admin form.
 */
function pfizer_webstandards_customlinks_add_edit_form($form, &$form_state) {

  if (count($form_state['build_info']['args']) === 1) {
    ctools_include('export');
    // We are editing the form; act as appropriate.
    $link = ctools_export_crud_load('pfizer_webstandards_customlink', $form_state['build_info']['args'][0]);
  }

  if (isset($link)) {
    // Machine name is already set:
    $form['machine_name'] = array(
      '#markup' => 'Machine Name: ' . check_plain($link->machine_name),
    );
  }
  else {
    // Adding a new item, display the Machine Name box.
    $form['machine_name'] = array(
      '#required' => '1',
      '#description' => t('A unique ID - used for exporting this data; use lowercase letters (a-z), numbers (0-9) and underscores (_) only.'),
      '#weight' => '0',
      '#type' => 'textfield',
      '#title' => t('Machine Name'),
    );
  }
  $form['link_type'] = array(
    '#required' => '1',
    '#key_type_toggled' => '1',
    '#description' => t('The type of the link to be tracked. Can be "e" for exit link, "d" for download link, or "o" for any other link. ** If the s.linkType value doesn\'t match this property value the "funcName" will not be called.'),
    '#weight' => '1',
    '#type' => 'radios',
    '#options' => array(
      'e' => t('Exit link (e)'),
      'd' => t('Download link (d)'),
      'o' => t('Other link (o)'),
    ),
    '#title' => t('Type'),
    '#default_value' => isset($link) ? $link->link_type : '',
  );
  $form['href'] = array(
    '#required' => '0',
    '#description' => t('The "href" attribute of the link to be tracked. The value can be a partial match (ex. a value of "domain.com" will match "sub.domain.com" as well as "domain.com/foo").<br /><br />If the s.linkURL value doesn’t match this value, the "funcName" will not be called. When this property is null (not defined), the CustomLinkTracker plugin will call the associated "funcName" for all links of that type.'),
    '#weight' => '2',
    '#type' => 'textfield',
    '#title' => t('Href'),
    '#default_value' => isset($link) ? $link->href : '',
  );
  $form['function_name'] = array(
    '#required' => '1',
    '#description' => t('The function/plugin to be called/executed when a visitor clicks the link in question.'),
    '#weight' => '4',
    '#type' => 'textfield',
    '#title' => t('Function Name'),
    '#default_value' => isset($link) ? $link->function : '',
  );
  $form['object'] = array(
    '#required' => '0',
    '#description' => t("An object that is passed to the custom 'funcName' plugin. This can be used for additional customization. The value of this property can be a string, integer or any type of JavaScript object. Note: the Object value will be rendered directly, it will not be surrounded by {} or ''’; please add these to the value yourself. This is to maintain compatibility with the ways of using this field - please see the Adobe document for details. "),
    '#weight' => '4',
    '#type' => 'textfield',
    '#title' => t('Object'),
    '#default_value' => isset($link) ? $link->object : '',
  );

  $form['interaction_status'] = array(
    '#required' => '0',
    '#description' => t("When configuring an Exit Link, you may optionally pre-select a value for INTERACTION_STATUS (s.prop43)."),
    '#weight' => '4.5',
    '#type' => 'textfield',
    '#title' => t('Interaction Status'),
    '#default_value' => isset($link) ? $link->interaction_status : '',
  );

  $form['weight_section'] = array(
    '#weight' => '5',
    '#type' => 'fieldset',
    '#collapsible' => '0',
    '#collapsed' => '0',
    '#title' => t('Weight'),
    '#description' => t('The heavier items will sink and the lighter items will be positioned nearer the top.'),
  );

  $form['weight_section']['weight'] = array(
    '#required' => '1',
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => isset($link) ? $link->weight : 0,
    '#weight' => '5',
    '#delta' => 50,
  );

  // Add a submit button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
    '#weight' => '100',
  );
  $form['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/config/system/webstandards/customlinks'),
    '#weight' => '101',
  );

  return $form;
}

/**
 * Submit function for pfizer_webstandards_customlinks_add_edit_form().
 */
function pfizer_webstandards_customlinks_add_edit_form_submit($form, &$form_state) {
  ctools_include('export');

  $link = NULL;

  if (count($form_state['build_info']['args']) === 1) {
    // We are editing the form; act as appropriate.
    $link = ctools_export_crud_load('pfizer_webstandards_customlink', $form_state['build_info']['args'][0]);
  }
  else {
    // We are adding a new item.
    $link = ctools_export_crud_new('pfizer_webstandards_customlink');
    $link->machine_name = $form_state['values']['machine_name'];
  }

  $link->link_type = $form_state['values']['link_type'];
  $link->href = $form_state['values']['href'];
  $link->function = $form_state['values']['function_name'];
  $link->object = $form_state['values']['object'];
  $link->interaction_status = $form_state['values']['interaction_status'];
  $link->weight = $form_state['values']['weight'];

  ctools_export_crud_save('pfizer_webstandards_customlink', $link);
  drupal_set_message(t('Saved item: @item_name', array('@item_name' => $link->machine_name)));

  pfizer_webstandards_generate_js_build_pfconfig();

  drupal_goto('admin/config/system/webstandards/customlinks');
}

/**
 * Validate function for pfizer_webstandards_customlinks_add_edit_form().
 */
function pfizer_webstandards_customlinks_add_edit_form_validate($form, &$form_state) {
  if (count($form_state['build_info']['args']) === 0) {
    // Creating a new item, verify the machine name is supplied, valid and
    // unique.
    $name = $form_state['values']['machine_name'];

    if (!isset($name)) {
      form_error($form['machine_name'], t("Please supply a Machine Name."));
    }

    if (!preg_match('/^[a-z0-9_]*$/', $name)) {
      form_error($form['machine_name'], t("The Machine Name '!name' must only contain lowercase letters (a-z), numbers (0-9) and underscores (_).", array('!name' => check_plain($name))));
    }

    // Check if the machine name is unique:
    ctools_include('export');

    $existing_link = ctools_export_crud_load('pfizer_webstandards_customlink', $name);
    if (isset($existing_link)) {
      form_error($form['machine_name'], t("The Machine Name '!name' is already in use, please select another.", array('!name' => check_plain($name))));
    }
  }
}

/**
 * This function provides the custom links delete form.
 */
function pfizer_webstandards_customlinks_delete_form($form, &$form_state) {
  if (count($form_state['build_info']['args']) === 1) {
    ctools_include('export');
    // We are editing the form; act as appropriate.
    $link = ctools_export_crud_load('pfizer_webstandards_customlink', $form_state['build_info']['args'][0]);
  }

  // If there is no link selected, return to the list.
  if (!isset($link)) {
    drupal_goto('admin/config/system/webstandards/customlinks');
  }

  $form['message'] = array(
    '#markup' => $link->export_type > 1 ?
      t('Are you sure you want to revert "@name"?', array('@name' => check_plain($link->machine_name))) :
      t('Are you sure you want to delete "@name"?', array('@name' => check_plain($link->machine_name))),
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );

  // Add a submit and cancel button.
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => 'Delete',
    '#weight' => '100',
  );

  $form['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/config/system/webstandards/customlinks'),
    '#weight' => '101',
  );

  return $form;
}

/**
 * This function provides the custom links delete form submit handler.
 */
function pfizer_webstandards_customlinks_delete_form_submit($form, &$form_state) {
  // Use ctools crud to update the data so it can be exported.
  ctools_include('export');

  ctools_export_crud_delete('pfizer_webstandards_customlink', $form_state['build_info']['args'][0]);

  pfizer_webstandards_generate_js_build_pfconfig();

  drupal_goto('admin/config/system/webstandards/customlinks');
}
