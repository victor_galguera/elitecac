<?php
/**
 * @file
 * Template for generating the urlConfig JS object.
 *
 * Available variables:
 * $url_array - an array of URL objects.
 */
?>
/*
 * URLConfig file - used by SiteCatalyst, Generated by Pfizer Web Standards.
 *
 * Created: <?php echo date('Y-m-d H:i:s') . "\n"; ?>
 */

// Function to check all URLs and set values as appropriate.
function pfizerWebstandardsCheckUrls(s) {
  "use strict";

<?php 
  foreach ($url_array as $array_item) {
    echo "  // " . $array_item->machine_name . "\n";
    echo "  var urls = new Array();\n";

    // Cycle through the array of URLs.
    $url_list = preg_split('/\r\n|\r|\n/', check_plain($array_item->url));

    $if_condition = 'pfizerWebstandardsUrlMatch(urls)';

    if (in_array('*', $url_list)) {
      $if_condition = '1';
    }

    // check for a <front> item in the list of URLs
    if (($index = array_search('<front>', preg_split('/\r\n|\r|\n/', $array_item->url))) !== FALSE) {
      // if we have the item, we replace it (in the check_plain'd data) with language-enabled versions 
      // of homepages

      $langs = language_list();
      $langs = array_keys($langs);
      $homepage = variable_get('site_frontpage', '');

      $new_urls = array(
        '',
        '/',
        $homepage,
        $homepage . '/',
      );

      foreach ($langs as $lang) {
        $new_urls[] = $lang;
        $new_urls[] = $lang . '/';
        $new_urls[] = $lang . '/' . $homepage;
        $new_urls[] = $lang . '/' . $homepage . '/';
      }

      array_splice($url_list, $index, 1, $new_urls);
    }

    foreach ($url_list as $url_item) {
      echo "  urls.push('" . check_plain($url_item) . "');\n";
    }

    echo "\n  if (" . $if_condition .") {\n";

    // Work out the variables update.
    $variables = unserialize($array_item->variables);
    foreach ($variables as $key => $value) {
      if ($key === 'javascript') {
        // This is the JS - just render it out. Note - do not check plain.
        echo $value;
      }
      else {
        $ckey = check_plain($key);

        $prefix = "";
        if ($if_condition == '1') {
          $prefix = "  ";

          echo "    if (typeof s." . $ckey . " == 'undefined' || s." . $ckey . " == '') {\n";
        }

        echo $prefix . "    s." . $ckey . " = '" . check_plain($value) . "';\n";
        echo $prefix . "    s.linkTrackVars = s.apl(s.linkTrackVars, '" . $key . "', ',', 2);\n";

        if ($if_condition == '1') {
          echo "    }\n";
        }

        echo "\n";
      }
    }

    echo "  }\n\n";
  }

echo "}";

