<div class="container">
    <div style="max-width: 475px; margin: 0 auto;">
        <?php
        //$form['name']['#attributes']['placeholder'] = t( 'Username' );
        //$form['pass']['#attributes']['placeholder'] = t( 'Password' );
        print drupal_render($form['name']);
        print drupal_render($form['pass']);
        print drupal_render($form['url']);
        print drupal_render($form['captcha']);
        print drupal_render($form['form_build_id']);
        print drupal_render($form['form_id']);
        print drupal_render($form['form_token']);
        ?>
        <?php
        //$attributes = array('attributes' => array('class' => 'forgot'));
        print '<a href="/user/password" class="forgot">' . t('Forgot password') . '</a>';
        ?>
        <br>
        <br>
        <?php
        //print_r($form);

        print drupal_render($form['actions']);
        ?>
    </div>
</div>