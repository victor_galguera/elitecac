; Drush make file for pfizer_lift dependencies.

api = 2
core = "7.x"

projects[] = drupal

projects[acquia_lift][subdir] = "contrib"
projects[acquia_lift][version] = "2.0-rc2"

projects[personalize][subdir] = "contrib"
projects[personalize][version] = "2.0-rc1"
projects[personalize][patch][2849573][url] = "https://www.drupal.org/files/issues/add_check_before_setting_entity_type-2849573-2.patch"
projects[personalize][patch][2849573][md5] = "890277c100eb63b4a7ef2ad35950514d"

projects[visitor_actions][subdir] = "contrib"
projects[visitor_actions][version] = "1.4"

projects[entity_context][subdir] = "contrib"
projects[entity_context][version] = "1.0-rc8"

; Libraries

; Library: chosen
; ---------------------------------------
libraries[chosen][destination] = "libraries"
libraries[chosen][download][type] = "get"
libraries[chosen][download][url] = "https://github.com/harvesthq/chosen/releases/download/v1.1.0/chosen_v1.1.0.zip"
libraries[chosen][directory] = "chosen"

; Library: qtip
; ---------------------------------------
libraries[qtip][destination] = "libraries"
libraries[qtip][download][type] = "get"
libraries[qtip][download][url] = "https://raw.githubusercontent.com/Craga89/qTip1/master/1.0.0-rc3/jquery.qtip-1.0.0-rc3.min.js"
libraries[qtip][directory] = "qtip"

; Library: D3
; ---------------------------------------
libraries[d3][destination] = "libraries"
libraries[d3][download][type] = "get"
libraries[d3][download][url] = https://github.com/mbostock/d3/releases/download/v3.4.11/d3.zip
libraries[d3][directory] = "d3"

; Library: Rickshaw
; ---------------------------------------
libraries[rickshaw][destination] = "libraries"
libraries[rickshaw][download][type] = "get"
libraries[rickshaw][download][url] = https://github.com/shutterstock/rickshaw/archive/v1.5.0.zip
libraries[rickshaw][directory] = "rickshaw"
