(function ($) {
  var context = 'pfizer_lift';
  var sent_epermissions_completed = false;

  Drupal.behaviors.pfizer_lift = {
    attach: function (context, settings) {
      context = $(context);

      var pl = Drupal.pfizer_lift;
      pl.init(settings);
      pl.processServerSideActions(settings);

      // enables transmission of base metrics to third-party frames
      pl.frameCommunication(context, settings);

      if (pl.isInitialized()) {
        // custom event handling
        pl.trackRegistrationStart(context, settings);
        pl.trackLoginStart(context, settings);
        pl.trackPolls(context, settings);
        pl.trackFiveStar(context, settings);
        pl.trackCostCoverage(context, settings);
        pl.trackEsamplingStart(context, settings);
        pl.trackWebinars(context, settings);
        pl.trackMarketoForms(context, settings);
        pl.trackMaterialOrder(context, settings);
        pl.trackEpermissions(context, settings);
        pl.trackAddThis(context, settings);
        pl.trackLinks(context, settings); // this must be the last item!
      }
    }
  };

  /**
   * Visitor Context object.
   */
  Drupal.personalize = Drupal.personalize || {};
  Drupal.personalize.visitor_context = Drupal.personalize.visitor_context || {};
  Drupal.personalize.visitor_context.pfizer_lift = {
    'getContext': function(enabled) {
      if (!Drupal.settings.hasOwnProperty('pfizer_lift')) {
        return [];
      }

      var i = 0;
      var context_values = {};

      for (i in enabled) {
        if (enabled.hasOwnProperty(i) && Drupal.settings.pfizer_lift.contexts.hasOwnProperty(i)) {
          context_values[i] = Drupal.settings.pfizer_lift.contexts[i];
        }
      }
      
      return context_values;
    }
  };
  
  // Keeps track of whether we've captured identity or not.
  var identityCaptured = false;

  /**
   * Sends captureIdentity and user_login events.
   * @param  {[type]} context   [description]
   * @return {[type]}           [description]
   */
  var userLoginEvent = function (context) {
    if (identityCaptured) {
      return;
    }

    var event_data = Drupal.pfizer_lift.getBaseMetrics();

    _tcaq.push(['captureIdentity', context['customer_id'], 'account', event_data]);
    identityCaptured = true;

    // event_data gets modified by the above identity call; rebuild it:
    event_data = {};
    event_data = Drupal.pfizer_lift.getBaseMetrics();

    var lift = Drupal.pfizer_lift;

    // if we're on the esampling "request samples" page after logging in, then 
    // we  need to fire the esampling_request_start event in place of user_login
    if (window.location.pathname.indexOf('hcp/request-samples') === 1) {
      lift.sendEvent(['captureView', 'esampling_request_start', event_data]);
    }
    // otherwise fire normal user_login event
    else {
      lift.sendEvent(['captureView', 'user_login', event_data]);
    }
  };

  /**
   * Sends a user_register event.
   * @param  {[type]} context   [description]
   * @return {[type]}           [description]
   */
  var userRegisterEvent = function (context) {
    var event_data = Drupal.pfizer_lift.getBaseMetrics();
    var lift = Drupal.pfizer_lift;

    lift.sendEvent(['captureView', 'user_register', event_data]);
  };

  /**
   * Sends a portal_form_submit event.
   * @param  {[type]} context [description]
   * @return {[type]}         [description]
   */
  var portalFormSubmitEvent = function (context) {
    var event_data = Drupal.pfizer_lift.getBaseMetrics();
    var lift = Drupal.pfizer_lift;

    event_data.event_udf28 = context.form_id;

    lift.sendEvent(['captureView', 'portal_form_submit', event_data]);
  };

  /**
   * Pfizer Lift object.
   */
  Drupal.pfizer_lift = (function() {
    var initialized = false;
    var initializing = false;
    var have_tc = false;
    var udf_values = {};

    var handler_map = {};

    // flag to track webinar unregistration event
    var sent_webinar_unregister = false;

    /**
    * Miscellaneous helpers.
    */
   
    /**
     * Returns true if attribute flag is set on context. If flag is not
     * present, it is set automatically.
     * 
     * @param  {[type]} context [description]
     * @param  {[type]} id      [description]
     * @return {[type]}         [description]
     */
    function checkAttr(context, id) {
      id = id + '-processed';

      context = $(context);

      if (context.attr(id)) {
        return true;
      }

      context.attr(id, id);
      return false;
    }

    return {
      'built_udf': false,

      'init': function(settings) {
        if (initialized || initializing) {
          return;
        }

        initializing = true;

        this.built_udf = new Promise(function (resolve, reject) {
          // map event handlers
          handler_map['pfizer_lift_user_login'] = userLoginEvent;
          handler_map['pfizer_lift_user_register'] = userRegisterEvent;
          handler_map['pfizer_lift_portal_form_submit'] = portalFormSubmitEvent;

          initialized = true;

          if (typeof _tcaq == 'object' && typeof _tcaq.push == 'function') {
            have_tc = true;
          }

          var original

          // aggregate UDF field data for later use in trackLinks(); code mostly from acquia_lift_profiles.js
          var mappings = settings.acquia_lift_profiles.mappings || {};
          var context_separator = settings.acquia_lift_profiles.mappingContextSeparator;
          var plugins = {};
          var reverseMapping = {};

          for (var type in mappings) {
            if (mappings.hasOwnProperty(type)) {
              for (var udf in mappings[type]) {
                if (mappings[type].hasOwnProperty(udf)) {
                  // We maintain a reverse mapping of all the UDFs that use each
                  // context, so we can easily assign values once the contexts have
                  // been retrieved.
                  if (!reverseMapping.hasOwnProperty(mappings[type][udf])) {
                    reverseMapping[mappings[type][udf]] = [];
                  }

                  reverseMapping[mappings[type][udf]].push(udf);

                  var context = mappings[type][udf].split(context_separator);
                  var pluginName = context[0];
                  var context_name = context[1];

                  if (!plugins.hasOwnProperty(pluginName)) {
                    plugins[pluginName] = {};
                  }

                  plugins[pluginName][context_name] = context_name;
                }
              }
            }
          }

          var callback = function(contextValues) {
            for (var pluginName in contextValues) {
              if (contextValues.hasOwnProperty(pluginName)) {
                for (var contextName in contextValues[pluginName]) {
                  if (contextValues[pluginName].hasOwnProperty(contextName)) {
                    var fullContextName = pluginName + context_separator + contextName;

                    if (reverseMapping.hasOwnProperty(fullContextName)) {
                      // Set this is as the value for all UDFs that use this context.
                      for (var i in reverseMapping[fullContextName]) {
                        if (reverseMapping[fullContextName].hasOwnProperty(i)) {
                          udf_values[reverseMapping[fullContextName][i]] = contextValues[pluginName][contextName];
                        }
                      }
                    }
                  }
                }
              }
            }

            resolve();
          };

          // this will actually populate udf_values via the callback function
          Drupal.personalize.getVisitorContexts(plugins, callback);
        });

        // inject a promise into the Personalize callback chain so we can wait
        // for personalization decisions to be completed before making other
        // DOM manipulations
        window['liftDecisions'] = new Promise(function (resolve, reject) {
          if (Drupal.settings.personalize.option_sets) {

            $(Object.keys(Drupal.settings.personalize.option_sets)).each(function (i, key) {
              var set = Drupal.settings.personalize.option_sets[key];
              var agent_type = Drupal.settings.personalize.agent_map[set.agent].type;

              var originalGDFP = Drupal.personalize.agents.acquia_lift_target.getDecisionsForPoint;
              Drupal.personalize.agents[agent_type].getDecisionsForPoint = function (agent_name, visitor_context, choices, decision_point, fallbacks, cb) {
                // create a wrapper for the original callback
                var callback = function (selection) {
                  // call original callback
                  cb(selection);

                  // now resolve the promise; it doesn't matter if we have other
                  // personalization items on the page
                  resolve();
                };

                // pass the new callback wrapper to the original getDecisionsForPoint
                originalGDFP(agent_name, visitor_context, choices, decision_point, fallbacks, callback);
              };
            });
          }
        });
      },

      /**
       * Returns true if init() has completed.
       * @return {[type]} [description]
       */
      'isInitialized': function () {
        return (initialized && have_tc);
      },

      /**
       * Returns all base metrics.
       * @return {[type]} [description]
       */
      'getBaseMetrics': function() {
        return $.extend({
          'pfizer': true,
          'evalSegments': true,
          'content_title': 'Untitled',
          'content_type': 'page',
          'page_type': 'content page',
          'content_section': '',
          'content_keywords': '',
          'post_id': '',
          'published_date': '',
          'thumbnail_url': '',
          'persona': '',
          'engagement_score':'1',
          'author':''
        }, Drupal.settings.acquia_lift_profiles.pageContext, udf_values);
      },

      /**
       * Returns value of cookie.
       * Function from http://www.w3schools.com/js/js_cookies.asp.
       * 
       * @param  {[type]} cname [description]
       * @return {[type]}       [description]
       */
      'getCookie': function (cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
          var c = ca[i];
          
          while (c.charAt(0) == ' ') {
            c = c.substring(1);
          }
      
          if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
          }
        }

        return '';
      },

      /**
       * Sends a postMessage to iframes with base metric data.
       * @param  {[type]} context  [description]
       * @param  {[type]} settings [description]
       * @return {[type]}          [description]
       */
      'frameCommunication': function(context, settings) {
        var self = this;

        /**
         * Sends postMessage to frame with base metrics and other data.
         *
         * @param  {[type]} frame The iframe element. Not wrapped in jQuery!
         * @return {[type]}       [description]
         */
        var preparePostMessage = function (frame) {
          if (checkAttr($(frame), 'pfizer-lift-frame')) {
            return;
          }

          var data = {
            'base_metrics': JSON.stringify(self.getBaseMetrics()),
            'customer_id': Drupal.settings.pfizer_lift.customer_id,
            'tc_ptid': self.getCookie('tc_ptid'),
            'tc_ptidexpiry': self.getCookie('tc_ptidexpiry'),
            'tc_ttid': self.getCookie('tc_ttid'),
            'tc_wq': self.getCookie('tc_wq')
          };

          var frame_src = $(frame).attr('src');
          if (!frame_src.match(/^http/)) {
            frame_src = location.protocol + frame_src;
          }

          // simple way of parsing URL
          var url = document.createElement('a');
          url.href = frame_src;

          frame.onload = function () {
            frame.contentWindow.postMessage(data, url.protocol + '//' + url.host);
          };
        };

        /**
         * Helper to priodically run the `check` callback until non-false is returned, at which
         * point the returned value is passed to the `success` callback.
         * 
         * @param  {[type]} try_count Number of max attempts to look for element.
         * @param  {[type]} interval  Interval between attempts (milliseconds).
         * @param  {[type]} check     Callback that searches for element. Returns element if found.
         * @param  {[type]} success   Callback when check() succeeds.
         * @param  {[type]} fail      Callback when check() fails found.
         * @return {[type]}           [description]
         */
        var wait_and_check = function (try_count, interval, check, success, fail) {
          // attempt the check()
          if (try_count) {
            // one interation of the interval
            setTimeout(function() {
              try_count--;

              var item = check();
              if (item) {
                return success(item);
              }

              wait_and_check(try_count, interval, check, success, fail);
            }, interval);
          }
          // we've ran out of attempts
          else if (typeof fail == 'function') {
            fail();
          }
        }

        // pitcher videos
          var pitcher_flow = function () {
            var check = function () {
              var frame = $('iframe[src*="edetail.pfizer.com"],iframe[src*="player.pitcher.com"]');
              if (frame.length) {
                return frame;
              }

              return false;
            };

            var success = function (frame) {
              preparePostMessage(frame[0]);
            };

            wait_and_check(20, 100, check, success);
          };

          // assign the flow to click event on trigger anchors
          var pitcher_links = $('.pitcher-link a[href*="edetail.pfizer.com"],.pitcher-link a[href*="player.pitcher.com"],.pfizer-pitcher a[href="#pitcher-field"]');
          $(pitcher_links).each(function (i, el) {
            if (checkAttr($(el), 'pfizer-lift-pitcher-trigger')) {
              return;
            }

            $(el).click(pitcher_flow);
          });

          // assign the flow to submit event on captcha-form (in modal)
          var pitcher_modal_inputs = $('#pfizer-hcp2-edetail-pitcher-public-form input[type=submit]');
          $(pitcher_modal_inputs).each(function (i, el) {
            if (checkAttr($(el), 'pfizer-lift-pitcher-trigger')) {
              return;
            }

            $(el).mousedown(pitcher_flow);
          });

        // digital copay cards
          var copay_scripts = context.find('script[src*="/pfizer-offers/widget/"]');
          $(copay_scripts).each(function (i, el) {
            if (checkAttr($(el), 'pfizer-lift-copay-trigger')) {
              return;
            }

            var check = function () {
              var cp_card_dialog = context.find('.ui-dialog #pfizer-offers-dialog');
              if (cp_card_dialog && cp_card_dialog.length) {
                return cp_card_dialog;
              }

              return false;
            };

            var success = function (dialogs) {
              // must loop as there may be multiple offers on the page
              dialogs.each(function (i, dialog) {
                dialog = $(dialog);

                if (!checkAttr(dialog, 'pfizer-lift-copay-frame')) {
                  var frame = dialog.find('#pfizer-offers-iframe');
                  if (frame.length) {
                    preparePostMessage(frame[0]);
                  }
                }
              });
            };

            wait_and_check(20, 500, check, success);
          });
      },

      /**
       * A wrapper for _tcaq.push(); adds base tracking to contentView
       * event data.
       * @param  {[type]} event [description]
       * @return {[type]}       [description]
       */
      'sendEvent': function(event) {
        if (typeof _tcaq == 'undefined') {
          return;
        }

        var event_data = this.getBaseMetrics();

        // we start with the base udf values, and add/overwrite with the event's data
        if (event.length >= 2) {
          event_data = $.extend(event_data, event[2]);
        }

        event[2] = event_data;

        _tcaq.push(event);
      },

      /**
       * Sends an event to TC.
       *
       * @param eventName
       */
      'processEvent': function(eventName, context) {
        if (handler_map.hasOwnProperty(eventName)) {
          handler_map[eventName](context);
        }
      },

      /**
       * Goes through the server-side actions and calls the appropriate function for
       * each one, passing in the event context.
       *
       * @param settings
       */
      'processServerSideActions': function (settings) {
        if (!settings.hasOwnProperty('pfizer_lift')) {
          return;
        }

        if (settings.pfizer_lift.serverSideActions) {
          for (var actionName in settings.pfizer_lift.serverSideActions) {
            if (settings.pfizer_lift.serverSideActions.hasOwnProperty(actionName)) {
              for (var i in settings.pfizer_lift.serverSideActions[actionName]) {
                if (settings.pfizer_lift.serverSideActions[actionName].hasOwnProperty(i) && !settings.pfizer_lift.serverSideActions[actionName][i].processed) {
                  // Process the event.
                  this.processEvent(actionName, settings.pfizer_lift.serverSideActions[actionName][i]);
                  // Mark this event has having been processed so that it doesn't get sent again.
                  settings.pfizer_lift.serverSideActions[actionName][i].processed = 1;
                }
              }
            }
          }
        }
      },

      /**
       * Generic helper for tracking of specific events off of link clicks.
       * 
       * @param  {[type]} context    [description]
       * @param  {[type]} settings   [description]
       * @param  {[type]} selector   [description]
       * @param  {[type]} event_name [description]
       * @return {[type]}            [description]
       */
      'trackLinkEvent': function (context, settings, selector, event_name) {
        var lift = this;

        var sendClickEvent = function (e) {
          // base data
          var event_data = {
            referrer: window.location.href,
            event_udf14: this.href
          };

          lift.sendEvent(['captureView', event_name, event_data]);
        }

        // find all relevant anchors
        context.find(selector).each(function (i, el) {
          el = $(el);

          // set a pfizer-lift-processed attribute so this link can be excluded
          // from the link_click event
          checkAttr(el, 'pfizer-lift');

          // add new handler
          el.on('click', sendClickEvent);
        });
      },

      /**
       * Handles registration start events.
       * 
       * @param  {[type]} context  [description]
       * @param  {[type]} settings [description]
       * @return {[type]}          [description]
       */
      'trackRegistrationStart': function (context, settings) {
        if ($('body').hasClass('logged-in')) {
          return;
        }

        // some sort of trickery in the Hamburger menu results in creation of DOM structure
        // that bypasses context; we need to manually catch this case and re-build a new
        // context to work with
        if (typeof context.context !== 'undefined' && context.context.id == 'block-pfizer-hcp-hcp-content-menu') {
          var new_context_selector = '.' + context.context.className.replace(/\s/g, '.');
          context = $(new_context_selector);
        }

        var selector = 'a.grv_register_open,a[href*="registration.aspx"],a[href*="user/register"]';
        this.trackLinkEvent(context, settings, selector, 'user_register_start');
      },

      /**
       * Handles login start events.
       * 
       * @param  {[type]} context  [description]
       * @param  {[type]} settings [description]
       * @return {[type]}          [description]
       */
      'trackLoginStart': function (context, settings) {
        if ($('body').hasClass('logged-in')) {
          return;
        }
        
        // some sort of trickery in the Hamburger menu results in creation of DOM structure
        // that bypasses context; we need to manually catch this case and re-build a new
        // context to work with
        if (typeof context.context !== 'undefined' && context.context.id == 'block-pfizer-hcp-hcp-content-menu') {
          var new_context_selector = '.' + context.context.className.replace(/\s/g, '.');
          context = $(new_context_selector);
        }

        var selector = 'a.pfizer-hcp2-login.login,a.pfizer-hcp2-login,a[href*="user/login"],a[href*="sign-in-register"]';
        this.trackLinkEvent(context, settings, selector, 'user_login_start');
      },

      /**
       * Handles poll submission events.
       * 
       * @param  {[type]} context  [description]
       * @param  {[type]} settings [description]
       * @return {[type]}          [description]
       */
      'trackPolls' : function(context, settings) {
        var lift = this;

        // find all poll forms
        var wrappers = context.find('.pfizerpoll-form');
        if (wrappers.length) {
          $.each(wrappers, function (i, wrapper) {
            var form = $(wrapper).parents('form');

            var submit_handler = function (e) {
              // if the user made a selection, we send the event; this could be multiple elements
              // (in case of checkboxes)
              var checked = form.find('input[type=radio]:checked,input[type=checkbox]:checked');
              var values = [];

              for (var i = 0; i < checked.length; i++) {
                values.push($(checked[i]).val());
              }

              if (value = values.join(',')) {
                var event_data = {
                  // poll type
                  event_udf23: form.attr('data-im-poll-type') == 'customer_feedback' ? 'Customer Feedback Poll' : 'Quick Poll',
                  // question category
                  event_udf21: form.attr('data-im-question-category'),
                  // question
                  content_title: form.attr('data-im-question'),
                  // answer
                  event_udf15: value
                };

                lift.sendEvent(['captureView', 'content_poll', event_data]);
              }
            };

            // bind submit handler
            form.find('input[type=submit]').mousedown(submit_handler);
          })
        }
      },

      /**
       * Handles fivestar submission events.
       * 
       * @param  {[type]} context  [description]
       * @param  {[type]} settings [description]
       * @return {[type]}          [description]
       */
      'trackFiveStar': function(context, settings) {
        var lift = this;

        // find all fivestar forms by classname
        var forms = context.find('form.fivestar-widget');
        if (forms.length) {
          $.each(forms, function (i, form) {
            form = $(form);

            var submit_handler = function (e) {
              // extract submitted rating from the anchor's text
              var value = $(this).text();
              var matches = value.match(/(\d)\/\d$/i);

              // if the user made a selection, we send the event
              if (typeof matches[1] != 'undefined') {
                var event_data = {
                  event_udf15: matches[1]
                };

                // user may rate, then click the same star again; we want to avoid sending
                // duplicate ratings
                if (form.attr('pfizer-lift-processed') && form.attr('pfizer-lift-processed') == matches[1]) {
                  return;
                }

                lift.sendEvent(['captureView', 'content_rate', event_data]);

                // track submitted rating
                form.attr('pfizer-lift-processed', matches[1]);
              }
            };

            // bind submit handler
            form.find('.star a').click(submit_handler);
          })
        }
      },
      
      /**
       * Handles Cost & Coverage submission events.
       * 
       * @param  {[type]} context  [description]
       * @param  {[type]} settings [description]
       * @return {[type]}          [description]
       */
      'trackCostCoverage': function(context, settings) {
        var lift = this;

        var tool_wrapper = context.find('.cost-coverage-tool-wrapper');
        var search_results = $(tool_wrapper).find('.plan-information,#cc-tool-no-results');

        if (search_results.length) {
          var event_data = {
            // zip code
            event_udf26: $(tool_wrapper).find('.form-item-zip input').val(),
            // coverage plan type
            event_udf27: $(tool_wrapper).find('input[name=health_plan_type]:checked').val()
          };

          lift.sendEvent(['captureView', 'cost_coverage', event_data]);
        }
      },

      /**
       * Handles eSampling links.
       * 
       * @param  {[type]} context  [description]
       * @param  {[type]} settings [description]
       * @return {[type]}          [description]
       */
      'trackEsamplingStart': function(context, settings) {
        var selector = 'a[href*="/hcp/request-samples"],a[href*="esamplesweb.pfizer.com"]';
        this.trackLinkEvent(context, settings, selector, 'esampling_request_start');
      },

      /**
       * Handles Webinar un/registration events.
       * 
       * @param  {[type]} context  [description]
       * @param  {[type]} settings [description]
       * @return {[type]}          [description]
       */
      'trackWebinars': function(context, settings) {
        var lift = this;

        // webinar registration
        var register = context.find('input[name=webinar_registration_action]');
        if (register.length && !checkAttr(register, 'pfizer-lift-webinar-register')) {
          register.mousedown(function() {
            var event_data = {};
            lift.sendEvent(['captureView', 'webinar_live_signup', event_data]);
          });
        }

        // webinar unregistration
        var unregister = context.find('input[name=webinar_deregistration_action],input[name=webinar_deregistration_visible]');
        
        if (unregister.length && !checkAttr(unregister, 'pfizer-lift-webinar-unregister')) {
          unregister.mousedown(function() {
            if (!lift.sent_webinar_unregister) {
              var event_data = {};
              lift.sendEvent(['captureView', 'webinar_live_unregister', event_data]);
 
              lift.sent_webinar_unregister = true;
            }
          });
        }

        // pre-event survey
        /*
        var survey = context.find('.node-hcp22-pre-event-survey input[type=submit]');
        if (survey.length && !checkAttr(survey, 'webinar-survey')) {
          survey.mousedown(function() {
            var event_data = {};

            var form = survey.parents('form');
            if (form.length) {
              event_data.event_udf28 = form.attr('id');
            }

            lift.sendEvent(['captureView', 'webinar_survey_submit', event_data]);
          });
        }
        */
      },

      /**
       * Handles tracking of submit events for Marketo forms.
       * 
       * @param  {[type]} context  [description]
       * @param  {[type]} settings [description]
       * @return {[type]}          [description]
       */
      'trackMarketoForms' : function (context, settings) {
        var lift = this;

        var forms = context.find('form.marketo-form');
        $(forms).each(function (i, form) {
          form = $(form);

          form.submit(function (e) {
            // a 1-second timeout allows Marketo's submit handlers to finish
            // validation and show error messages
            setTimeout(function() {
              var got_error = false;
              var errors = form.find('.mktoError');
              errors.each(function (j, error) {
                if ($(error).css('display') !== 'none') {
                  got_error = true;
                }
              });

              if (got_error) {
                return;
              }

              var event_data = {};
              event_data.event_udf28 = form.attr('id');
              lift.sendEvent(['captureView', 'marketo_form_submission', event_data]);
            }, 1000);
          });
        });
      },

      /**
       * Tracks start and completion events for Material Order.
       * 
       * @param  {[type]} context  [description]
       * @param  {[type]} settings [description]
       * @return {[type]}          [description]
       */
      'trackMaterialOrder': function (context, settings) {
        var lift = this;

        if (typeof(Storage) == 'undefined') {
          return;
        }

        // get the existing session storage data
        var data = localStorage.getItem('pfizer lift');
        data = (data == null) ? {} : JSON.parse(data);

        // default structure
        if (!data.material_order) {
          data.material_order = {
            start_timestamp: 0,
            finish_timestamp: 0
          };
        }

        var now = Math.floor(Date.now() / 1000);

        // start event
        if (location.pathname.match('^/support/material-ordering/{0,1}$')) {
          // we send the event only if it was previously sent more than 30 minutes ago
          if (now - data.material_order.start_timestamp > 60*30) {
            data.material_order.start_timestamp = now;
            data.material_order.finish_timestamp = 0;

            var event_data = {};
            lift.sendEvent(['captureView', 'materials_request_start', event_data]);
          }
        }
        // finish event
        else if (location.pathname.match('^/support/material-ordering/complete_order/{0,1}$')) {
          // we only send the event if the finish timestamp is unset (ie. the user has recently
          // gone through the order form)
          if (data.material_order.finish_timestamp == 0) {
            data.material_order.finish_timestamp = now;
            data.material_order.start_timestamp = 0;

            var event_data = {};
            lift.sendEvent(['captureView', 'materials_request_finish', event_data]);
          }
        }

        // save session storage data
        try {
          localStorage.setItem('pfizer lift', JSON.stringify(data)); 
        } 
        catch(e) {
          console.log(e);
        }
      },

      /**
       * Tracks start and completion events for ePermissions Form.
       * 
       * @param  {[type]} context  [description]
       * @param  {[type]} settings [description]
       * @return {[type]}          [description]
       */
      'trackEpermissions': function (context, settings) {
        var submit_button = context.find('#pfizer-hcp2-epermission-submit');
        if (!submit_button.length) {
          return;
        }

        var lift = this;

        // if we have the submit button, then the form is present, and we 
        // fire the initiated event
        var event_data = Drupal.pfizer_lift.getBaseMetrics();
        event_data.person_udf6 = 'GCP'; // authentication type
        event_data.person_udf7 = 'ePermission (GCP)'; // registration type

        // user is logged in, so we need up update registration type
        if (typeof localStorage.janrainCaptureProfileData !== 'undefined') {
          event_data.person_udf6 = 'GRV';
          event_data.person_udf7 = 'ePermission (GRV Update)';
        }

        // flag to prevent duplicate events
        if (!checkAttr($('body'), 'pfizer-lift-epermissions-start')) {
          lift.sendEvent(['captureView', 'epermissions_registration_initiated', event_data]);
        }

        // track the form submission ajax event and check for presence of 
        // has-error class to exclude form-rebuilds due to validation issues
        $(document).ajaxComplete(function(event, xhr, settings) {
          if (settings.url.match(/system\/ajax$/) && settings.data.match(/form_id=pfizer_hcp2_epermissions_form_builder/)) {
            if (!xhr.responseText.match(/has-error/) && !sent_epermissions_completed) {
              // grab the GUID set by the AJAX request, and use it to identify the user
              var guid = $('#pfizer-hcp2-epermissions-form-builder').attr('pfizer-lift-guid');
              if (guid) {
                // include the ID as part of subsequent events
                event_data.person_udf4 = guid;

                // send captureIdentity; we copy the data object to prevent leaking data 
                // into subsequent events
                var event_data_copy = jQuery.extend(true, {}, event_data);
                _tcaq.push(['captureIdentity', guid, 'account', event_data_copy]);
                identityCaptured = true;
              }

              lift.sendEvent(['captureView', 'epermissions_registration_completed', event_data]);

              sent_epermissions_completed = true;
            }
          }
        });
      },

      /**
       * Tracks start and completion events for AddThis widget (Social Sharing).
       * 
       * @param  {[type]} context  [description]
       * @param  {[type]} settings [description]
       * @return {[type]}          [description]
       */
      'trackAddThis': function (context, settings) {
        if (typeof window.addthis == 'undefined') {
          return;
        }

        var lift = this;

        $('body').once('pfizer-lift-add-this', function () {
          window.addthis.addEventListener('addthis.menu.share', function (e) {
            var event_data = {};
            event_data.event_udf17 = e.data.service; // channel used to share
            lift.sendEvent(['captureView', 'social_sharing_initiated', event_data]);
          });

          window.addthis.addEventListener('addthis.menu.close', function (e) {
            var event_data = {};
            event_data.event_udf17 = e.data.pane; // channel used to share
            lift.sendEvent(['captureView', 'social_sharing_completed', event_data]);
          });
        });
      },

      /**
       * Handles click events for all links on the page.
       * 
       * @param  {[type]} context  [description]
       * @param  {[type]} settings [description]
       * @return {[type]}          [description]
       */
      'trackLinks' : function (context, settings) {
        var lift = this;

        var sendClickEvent = function (e) {
          // base data
          var event_data = {
            referrer: window.location.href,
            event_udf14: this.href,
            content_title: this.pathname.split('/').pop(),
            content_type: ''
          };
          
          var parts = event_data.content_title.split('.');
          if (parts.length > 1) {
            event_data.content_type = parts.pop();
          }
          
          var event_name = 'link_click';
          if (event_data.content_type == 'pdf') {
            event_name = 'file_download';
          }

          lift.sendEvent(['captureView', event_name, event_data]);
        }
        
        // find all anchors, exluding:
        //  the ones within the #admin-menu wrapper,
        //  links in fivestar widgets,
        //  links with pfizer-lift-processed class
        $('a:not(#admin-menu a,.fivestar-widget a,a[pfizer-lift-processed])').each(function (i, el) {
          el = $(el);

          // this may get called multiple times for a single page load; ensure we don't
          // add multiple instances of our click handler by removing it prior to adding
          el.off('.pfizer_lift');

          // add new handler
          el.on('click.pfizer_lift', sendClickEvent);
        });
      }
    }
  })();

})(jQuery);

