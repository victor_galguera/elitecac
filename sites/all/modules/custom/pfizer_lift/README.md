The provided make file pulls in required dependencies.

Pfizer Lift
===========
This module provides base configuration.

Pfizer Lift creates a JS promise that is resolved when all on-page personalization is complete. If other code manipulates DOM, it is advised to use the promise to ensure no race condition occurs.

An example could look like this:

    (function($) {
      var init = false;
    
      Drupal.behaviors.custom_js = {
        attach: function(context, settings) {
          if (init) {
            return;
          }
    
          liftDecisions.then(function () {
            // DOM manipulation here
          });
    
          init = true;
        }
      };
    })(jQuery);

Pfizer Lift GRV
===========
Provides GRV-specific functionality; namely, triggering of the 'user_login' action upon GRV login.

Pfizer Lift HCP Settings
===========
A Feature that provides default HCP settings for some variables and Visitor Actions.
