<?php
/**
 * @file
 * pfizer_webstandards_hcp.default_pfizer_webstandards_urls.inc
 */

/**
 * Implements hook_default_pfizer_webstandards_url().
 */
function pfizer_webstandards_default_pfizer_webstandards_url() {
  $export = array();

  $url = new stdClass();
  $url->disabled = FALSE; /* Edit this to true to make a default url disabled initially */
  $url->api_version = 1;
  $url->machine_name = 'lead_type_login';
  $url->weight = -49;
  $url->url = '*/login';
  $url->variables = 'a:1:{s:10:"javascript";s:31:"s.pfLead(\'login\', \'initiated\');";}';
  $export['lead_type_login'] = $url;

  $url = new stdClass();
  $url->disabled = FALSE; /* Edit this to true to make a default url disabled initially */
  $url->api_version = 1;
  $url->machine_name = 'site_tool_newsletter';
  $url->weight = -48;
  $url->url = '*/newsletter*';
  $url->variables = 'a:1:{s:10:"javascript";s:27:"s.pfSiteTool(\'newsletter\');";}';
  $export['site_tool_newsletter'] = $url;

  $url = new stdClass();
  $url->disabled = FALSE; /* Edit this to true to make a default url disabled initially */
  $url->api_version = 1;
  $url->machine_name = 'site_tool_search';
  $url->weight = -47;
  $url->url = '*/search*';
  $url->variables = 'a:1:{s:10:"javascript";s:23:"s.pfSiteTool(\'search\');";}';
  $export['site_tool_search'] = $url;

  return $export;
}
