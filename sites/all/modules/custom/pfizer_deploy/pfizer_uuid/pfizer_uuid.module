<?php

/**
 * @file
 * Code for Pfizer UUID.
 */

/**
 * Implements hook_menu().
 */
function pfizer_uuid_menu() {
  $items = array();

  $items['admin/reports/pfizer-uuid'] = array(
    'title' => 'Pfizer UUID Report',
    'page callback' => 'pfizer_uuid_report_page',
    'access arguments' => array('administer deployments'),
    'weight' => -50,
  );

  return $items;
}

/**
 * Implements hook_init().
 */
function pfizer_uuid_init() {
  $item = menu_get_item();

  if (!empty($item['path']) && substr($item['path'], 0, 22) == 'admin/structure/deploy') {
    pfizer_uuid_duplicate_check();
  }
}

/**
 * Page callback to display duplicate UUID report.
 *
 * @return array
 *   A renderable array to build the page.
 */
function pfizer_uuid_report_page() {

  $status = pfizer_uuid_duplicate_status();

  // There are UUID duplicates.
  if ($status['status']) {
    $description = t('<p><strong>UUID issues</strong> have been found. This may include duplicate UUIDs or schema errors.</p>');

    $header = array(
      t('Entity Type'),
      t('Table'),
      t('Field'),
      t('Label'),
      t('UUID'),
      t('Count'),
    );

    foreach ($status['description'] as $type => $items) {
      foreach ($items as $id => $item) {
        $rows[$id] = array(
          $type,
          $item['table'],
          $item['field'],
          (!empty($item['label']) ? $item['label'] : ' '),
          $item['uuid'],
          $item['count'],
        );
      }
    }

    $build['status_table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    );
  }
  else {
    $description = t('<p><strong>No UUID issues</strong> have been found.</p>');
  }

  $build['description'] = array(
    '#markup' => $description,
    '#weight' => -100,
  );

  $build['footer'] = array(
    '#markup' => t('<p>Bean revisions are currently excluded from UUID checks as they generate a duplicate revision.</p>'),
    '#weight' => 100,
  );

  return $build;
}

/**
 * Helper function to check if there are UUID collisions.
 *
 * @return array
 *   A keyed array of entity info that use UUIDs.
 */
function pfizer_uuid_duplicate_check() {
  $status = pfizer_uuid_duplicate_status();

  if ($status['status']) {
    $args = array('@report' => url('admin/reports/pfizer-uuid'));
    drupal_set_message(t('<a href="@report">UUID issues</a> have been found. This may include duplicate UUIDs or schema errors.', $args), 'error', FALSE);
  }

  return $status;
}

/**
 * Helper function to find UUID collisions.
 *
 * @return array
 *   A keyed array of entity info that use UUIDs.
 */
function pfizer_uuid_duplicate_status() {
  $status = FALSE;
  $description = array();

  $entity_schema = _pfizer_uuid_schema_get();

  foreach ($entity_schema as $entity_type => $info) {

    foreach ($info['keys'] as $table => $key) {
      $run_query = TRUE;

      // Skip certain revisions for now as they generate duplicate revision
      // UUIDs.
      if (in_array($table, array('bean_revision', 'menu_links_revision'))) {
        continue;
      }

      $fields = array($key);

      // Don't use label on menu links because of PHP fatal error.
      if ($entity_type != 'menu_link' && isset($info['entity label'])) {
        $fields[] = $info['entity label'];
      }

      // Check all fields and make sure they exist.
      foreach ($fields as $field) {
        if (!db_field_exists($table, $field)) {
          $run_query = FALSE;
          $status = TRUE;
          $description[$entity_type][] = array(
            'table' => $table,
            'field' => $key,
            'uuid' => '',
            'count' => '',
            'label' => t('Schema error: field is missing.'),
          );
        }
      }

      // There are schema issues so skip the query to check fields for dupes.
      if (!$run_query) {
        continue;
      }

      $query = db_select($table, 'uuid')
        ->fields('uuid', $fields)
        ->condition($key, '', '!=')
        ->groupBy($key);

      $query->addExpression("COUNT($key)", 'uuid_count');
      $query->havingCondition('uuid_count', 1, '>');
      $results = $query->execute();

      foreach ($results as $result) {
        $status = TRUE;
        $label = (!empty($info['entity label']) && !empty($result->{$info['entity label']}) ? $result->{$info['entity label']} : '');
        $description[$entity_type][] = array(
          'table' => $table,
          'field' => $key,
          'uuid' => $result->{$key},
          'count' => $result->uuid_count,
          'label' => $label,
        );
      }
    }
  }

  if ($status) {
    $status = array(
      'status' => $status,
      'description' => $description,
    );
  }

  return $status;
}

/**
 * Helper function to return schema data on entities using UUID.
 *
 * @return array
 *   A keyed array of entities that use UUIDs and schema info.
 */
function _pfizer_uuid_schema_get() {
  $schema = array();
  $entity_info = entity_get_info();

  foreach ($entity_info as $entity_type => $info) {
    // Only add the entity if there is a base table and UUID defined.
    if (!empty($info['base table']) && !empty($info['entity keys']['uuid'])) {
      // Set some top level info.
      $schema[$entity_type] = array(
        'label' => (!empty($info['label']) ? $info['label'] : $entity_type),
      );
      // Set keys.
      $schema[$entity_type]['keys'] = array(
        $info['base table'] => $info['entity keys']['uuid'],
      );

      // Grab revision table and revision UUID if it exists.
      if (!empty($info['revision table']) && !empty($info['entity keys']['revision uuid'])) {
        // Set keys.
        $schema[$entity_type]['keys'] += array(
          $info['revision table'] => $info['entity keys']['revision uuid'],
        );
      }
      // Grab the entity specific label if it exists.
      if (!empty($info['entity keys']['label'])) {
        $schema[$entity_type] += array(
          'entity label' => $info['entity keys']['label'],
        );
      }
    }
  }

  return $schema;
}
