<?php

/**
 * @file
 * Hooks provided by cemhub module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the data before the file is processed.
 *
 * @param array $data
 *   An array containing two master keys:
 *   - components: Stores the registered values ​​of the webform components.
 *   - uii: CEMHUB UII previously generated.
 * @param int $webform_nid
 *   The node id of the related webform.
 * @param int $webform_sid
 *   The webform submission ID that is being processed.
 */
function hook_cemhub_data_alter(&$data, $webform_nid, $webform_sid) {
}

/**
 * @} End of "addtogroup hooks".
 */
