Pfizer Webstandards 7.x-2.4.3, 2016-06-08
-------------------------------------
- Internal Search tracking not working in webstandard (Site Catalyst)

Pfizer Webstandards 7.x-2.4.2, 2016-05-26
-------------------------------------
- Includes Pfizer Webstandards GRV 7.x-1.1.1; update fixes incorrect function arguments in pfizer_webstandards_grv_janrain_capture_profile_sync()

Pfizer Webstandards 7.x-2.4.1, 2016-03-18
-------------------------------------
- Introduce automation of eVar65/prop67 via pfizer_webstandards_hcp2 for country identification.
- Refinment to move all analytic requrement from HCP Portal 2.7.0 release into its own module
  pfizer_webstandards_hcp27.
- Bugfix: Correct incorrect field reference in pfizer_webstandards_hcp26 that causes notices
  to appear.
- Bugfix: Add condition to pfizer_webstandards_26 to ensure that Drupal behaviours are not prevented
  from executing when build_s (more likely  s_code.js) is unavailable.

Pfizer Webstandards 7.x-2.4.0, 2016-02-23
-------------------------------------
- Fix condition in `s_code.js` for correct tracking events segments on scrolling the video.
- Introduced eVar65/prop67 for country identification.
- Introduced prop68 to identify the current indication.

Pfizer Webstandards 7.x-2.3.4, 2016-01-19
-------------------------------------
Change logic in submit function for pfizer_hcp2_material_ordering_review_confirm: preparing products data for omniture.

Pfizer Webstandards 7.x-2.3.3, 2015-12-31
-------------------------------------
Fix js error in hcp26_plugin.js behaviour which broke further work of omniture for material ordering

Pfizer Webstandards 7.x-2.3.2, 2015-12-3
-------------------------------------
Remove changing linkUrl to lower case in s_code.js for external links treated by pfizer_exit_popup module (Pfizer Pro US).

Pfizer Webstandards 7.x-2.3, 2015-11-23
-------------------------------------
- Added a fix in s_code.js for external links treated by pfizer_exit_popup module (Pfizer Pro US).

Pfizer Webstandards 7.x-2.2, 2015-11-05
-------------------------------------
- Run s.pfVideoOpen() on pages with Brightcove videos.
  Video Open(event18) is implemented on page view of any page which has
  Brightcove video embedded in it. This will work on all HCP portals 2.x.

Pfizer Webstandards 7.x-2.1, 2015-10-13
-------------------------------------
- added pfizer_webstandards_hcp_26
  It expands standard "Web Standards" functionality to track events/variables which have appeared in HCP Portal 2.5.

Pfizer Webstandards 7.x-2.0, 2015-09-03
-------------------------------------
- removing depricated function in favour of pfizer_hcp\Context

Pfizer Webstandards 7.x-1.12, 2015-08-20
-------------------------------------
- added tracking for FiveStar ratings and Poll submissions
- refactored build_s() to use the s_gi() function instead of instantiating a new AppMeasurements
- added s.pfClearVars() to clear custom data from s_code object (should be called after every s.tl() call)
- added missing linkTrackEvents to pfLogin and pfRegistration
- added brightcove_legacy object to track copies of brightcove-related functions for upcoming Lift integration

Pfizer Webstandards 7.x-1.11, 2015-05-29
-------------------------------------
- added JS minification
- updated declaration of edetail functions to avoid errors due to 'use strict'
- fixed defulat/default typo throughout modules and filenames

Pfizer Webstandards 7.x-1.10, 2015-04-30
-------------------------------------
- de-obfuscated s.pfRegistration()
- added s.pfLogin() for tracking interactions during login process

Pfizer Webstandards 7.x-1.9, 2015-04-08
-------------------------------------
- added Marketo-related code to pfizer_webstandards_hcp2's JS; pulls in query string params
- added a check to ensure user is logged in prior to appending HCPID and GUID to eDetail presentation's link path
- added CHANGELOG.txt
