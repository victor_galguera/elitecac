Pfizer Security 7.x-2.5.0, 2017-04-07
-------------------------------------
- Added Securepages patch for d.o issue #2381857 to allow configurable HTTP response code of redirect.

Pfizer Security 7.x-2.4.1, 2017-03-03
-------------------------------------
- [#40] Added Security Kit patch to address d.o issue #2657346.

Pfizer Security 7.x-2.4.0, 2017-02-09
-------------------------------------
- Updated Paranoia to 1.7 and removed all patches.
- Updated Email Confirm to 1.3.

Pfizer Security 7.x-2.3.0, 2017-01-25
-------------------------------------
- [#42] Added Paranoia patches to prevent devel execute PHP form block d.o issue #2846876.
- [#34] Added Username Enumeration Prevention patch to address d.o issue #2587683.

Pfizer Security 7.x-2.2.1, 2016-10-10
-------------------------------------
- Removed unused Strongarm variables.
- Enable Automated Logout module in hook_install and remove commented out dependency.

Pfizer Security 7.x-2.2.0, 2016-09-13
-------------------------------------
- Added functionality to hide Pfizer Security from the Modules administration
page and show item on Status page instead.

Pfizer Security 7.x-2.1.0, 2016-08-12
-------------------------------------
- Updated Hacked to 2.0-beta6.
- Updated Paranoia to 1.6 and removed committed patch for d.o issue #2565759.

Pfizer Security 7.x-2.0.1, 2016-08-08
-------------------------------------
- [#33] Added Youtube file MIME to whitelist to allow Youtube uploads using the media module.

Pfizer Security 7.x-2.0.0, 2016-07-04
-------------------------------------
- Removed Antivirus.
- Update Password Policy to 2.0-alpha6.
- Removed Password Policy patch to address d.o issue #2489918 as it was addressed in latest version.
- Updated Session Limit to 2.2.
- Updated Username Enumeration Prevention to 1.2.
- Removed Session Limit and Automated Logout variables from Strongarm to support properly reverted features.

Pfizer Security 7.x-1.7.2, 2016-05-18
-------------------------------------
- [#31] Updated autocomplete attribute to target specific array item.

Pfizer Security 7.x-1.7.1, 2016-03-28
-------------------------------------
- [DPT-274] Updated Security Kit patch to address PHP notice.
- [#28] Updated readme.

Pfizer Security 7.x-1.7.0, 2016-03-14
-------------------------------------
- [DPT-204] Enhanced legacy clickjacking defense with two new options.

Pfizer Security 7.x-1.6.0, 2016-02-01
-------------------------------------
- [DPT-66] Enhanced deny login variable to support Pfizer VPN and allow GRV users to view profile.

Pfizer Security 7.x-1.5.0, 2015-11-18
-------------------------------------
- Updated to use semantic versioning for platform 2.5.0 release.

Pfizer Security 7.x-1.5, 2015-11-05
-------------------------------------
- Updated Autologout to 4.4.
- Updated Mollom to 2.15.
- Updated Session Limit to 2.1.

Pfizer Security 7.x-1.5-beta3, 2015-10-19
-------------------------------------
- Added patch to Paranoia to make purging inactive users optional.

Pfizer Security 7.x-1.5-beta2, 2015-08-10
-------------------------------------
- Added patch to Password Policy to address bug on edit profile causing fatal PHP error (issue #2489918).

Pfizer Security 7.x-1.5-beta1, 2015-07-08
-------------------------------------
- Updated Email Change Confirmation to 1.2.
- Updated Mollom to 2.14.
- Updated Paranoia to 1.5.
- Updated Password Policy to 2.0-alpha5.
- Updated Username Enumeration Prevention to 1.1.

Pfizer Security 7.x-1.4, 2015-06-15
-------------------------------------
- Removed Global Redirect as a dependency to fix redirection bugs.

Pfizer Security 7.x-1.3, 2015-06-10
-------------------------------------
- Official release.

Pfizer Security 7.x-1.3-beta2, 2015-06-03
-------------------------------------
- Updated Paranoia patch to fix PHP notices in conjunction with FPA.
- Removed Taxonomy Autocomplete permission and simply added a variable to prevent taxonomy autocomplete menu item from being unset.

Pfizer Security 7.x-1.3-beta1, 2015-05-29
-------------------------------------
- Added "Access taxonomy autocomplete" permission instead of just unsetting it.

Pfizer Security 7.x-1.2, 2015-05-04
-------------------------------------
- Official release.

Pfizer Security 7.x-1.2-beta2, 2015-04-13
-------------------------------------
- [DPT-66] Changed /user path to only be accessible by authenticated users when deny_login enabled.

Pfizer Security 7.x-1.2-beta1, 2015-04-06
-------------------------------------
- Added Global Redirect and make dependency.
- Updated Security Kit to version 1.9.
- Updated Paranoia to version 1.4.
- Updated Password Policy to version 2.0 Alpha4.
- Updated Security Review to version 1.2.
- Changed consecutive character count from 2 to 3 on default password policy.
- Added Paranoia patch to disable risky permission checkboxes rather than unset.

Pfizer Security 7.x-1.1, 2015-02-04
-------------------------------------
- Added .po files (used for translations) to mime type whitelist.
- Update Mollom in make file from 2.9 to 2.13 to address moderately critical security advisory.
