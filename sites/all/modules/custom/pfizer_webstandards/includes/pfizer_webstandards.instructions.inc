<?php
/**
 * @file
 * Include file to display an instructions form.
 */

/**
 * This function provides the admin instructions form.
 */
function pfizer_webstandards_instructions_form($form, &$form_state) {
  $form['pfizer_webstandards_intro'] = array(
    '#weight' => 0,
    '#markup' => t('The following contains instructions for adding additional tracking functionality to a Web Standards compatible website.'),
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );

  $form['error_tracking'] = array(
    '#weight' => '1',
    '#type' => 'fieldset',
    '#title' => t('Error Page Tracking'),
    '#collapsible' => '1',
    '#collapsed' => '0',
  );
  $form['error_tracking']['description'] = array(
    '#weight' => 0,
    '#markup' => t('To enable tracking of 404 pages you must add the following JavaScript code to your 404 page:'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  $form['error_tracking']['code'] = array(
    '#weight' => 1,
    '#markup' => t('s.pfError404(window.location.href);'),
    '#prefix' => '<p class="code">',
    '#suffix' => '</p>',
  );

  $form['search_tracking'] = array(
    '#weight' => '2',
    '#type' => 'fieldset',
    '#title' => t('Search Tracking'),
    '#collapsible' => '1',
    '#collapsed' => '0',
  );
  $form['search_tracking']['description'] = array(
    '#weight' => 0,
    '#markup' => t('To enable tracking of search results you will need to add some logic to the search results page:'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  $form['search_tracking']['code'] = array(
    '#weight' => 1,
    '#markup' => t('s.pfSearch({search term}, {type of search}, {number of results});'),
    '#prefix' => '<p class="code">',
    '#suffix' => '</p>',
  );
  $form['search_tracking']['notes'] = array(
    '#weight' => 2,
    '#markup' => t("{search term} should be the search term used by the user. {type of search} is a string that represents the type of search performed (e.g. 'product'). {number of results} is the count of results that matched the search term, set this to 0 if there are no results."),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );

  $form['video_tracking'] = array(
    '#weight' => '3',
    '#type' => 'fieldset',
    '#title' => t('Video Tracking'),
    '#collapsible' => '1',
    '#collapsed' => '0',
  );
  $form['video_tracking']['description'] = array(
    '#weight' => 0,
    '#markup' => t('Brightcove video tracking is automatically  included in the s_code file; however there are a couple of changes that may need to be made to fully enable it:'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  $form['video_tracking']['brightcove_configuration'] = array(
    '#weight' => 3,
    '#markup' => t("Please configure Brightcove as follows:"),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  $form['video_tracking']['brightcove_configuration_notes'] = array(
    '#weight' => 4,
    '#markup' => t('<li>Log-in to the Video Cloud.</li>
<li>Go to Home -> Account Settings.</li>
<li>Enable Universal Delivery Service (or http delivery): This may vary, check with your BC account manager.</li>
<li>Enable HTML5 Player Delivery.</li>
<li>Go to Video Cloud -> Publishing.</li>
<li>Select a player and click the "Settings".</li>
<li>In the Global tab check the boxes for "Enable ActionScript/JavaScript APIs" and "Enable HTML5 Delivery".</li>
<li>This will need to be done for each player in use.</li>'),
    '#prefix' => '<ol class="code">',
    '#suffix' => '</ol>',
  );
  $form['video_tracking']['brightcove_configuration_markup'] = array(
    '#weight' => 5,
    '#markup' => t("Add the following parameters to the Brightcove object:"),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  $form['video_tracking']['brightcove_configuration_markup_code'] = array(
    '#weight' => 6,
    '#markup' => t('&lt;param name="includeAPI" value="true" /&gt;<br />&lt;param name="templateLoadHandler" value="myTemplateLoaded" /&gt;'),
    '#prefix' => '<p class="code">',
    '#suffix' => '</p>',
  );

  $form['survey_tracking'] = array(
    '#weight' => '3',
    '#type' => 'fieldset',
    '#title' => t('Survey Tracking'),
    '#collapsible' => '1',
    '#collapsed' => '0',
  );
  $form['survey_tracking']['description'] = array(
    '#weight' => 0,
    '#markup' => t('To enable tracking of surveys, a code change may be required - along with some JavaScript:'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  $form['survey_tracking']['survey_offered_description'] = array(
    '#weight' => 1,
    '#markup' => t('On the page where a visitor is invited to complete a survey, add the following JavaScript:'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  $form['survey_tracking']['survey_offered_code'] = array(
    '#weight' => 2,
    '#markup' => t('s.pfSurvey("{THE SURVEY NAME}", "offered");'),
    '#prefix' => '<p class="code">',
    '#suffix' => '</p>',
  );
  $form['survey_tracking']['survey_completed_description'] = array(
    '#weight' => 3,
    '#markup' => t('On the page where a visitor is invited to complete a survey, add the following JavaScript:'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  $form['survey_tracking']['survey_completed_code'] = array(
    '#weight' => 4,
    '#markup' => t('s.pfSurvey("{THE SURVEY NAME}", "completed", qa);'),
    '#prefix' => '<p class="code">',
    '#suffix' => '</p>',
  );
  $form['survey_tracking']['survey_completed_notes'] = array(
    '#weight' => 5,
    '#markup' => t('Where qa is a comma delimited list of questions and answers, e.g.:'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  $form['survey_tracking']['survey_completed_notes_code'] = array(
    '#weight' => 6,
    '#markup' => t('var qa = "what color is the sky?-blue,who won the last world series?-sf,who is the president of the us?-obama";'),
    '#prefix' => '<p class="code">',
    '#suffix' => '</p>',
  );

  $form['self_details'] = array(
    '#weight' => '3',
    '#type' => 'fieldset',
    '#title' => t('Self Details'),
    '#collapsible' => '1',
    '#collapsed' => '0',
  );
  $form['self_details']['description'] = array(
    '#weight' => 0,
    '#markup' => t('To enable Web Standards in Self Detail Gardens sites; configure the Web Standards module and then export the JavaScript file below. Add the URL and Config files to the ‘Custom’ tab in the ‘JavaScript Libraries’ configuration page in the Gardens site. Place them in the page head.'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );
  $form['self_details']['javascript_files'] = array(
    '#weight' => 1,
    '#markup' => t('<li>Config File: <a href="@url" target="_blank ">link</a></li>',
      array(
        '@url' => file_create_url(pfizer_webstandards_generate_pfconfig_path()),
      )) .
      t('<li>URL File: <a href="@url" target="_blank ">link</a></li>',
        array(
          '@url' => file_create_url(pfizer_webstandards_generate_urlconfig_path()),
        )),
    '#prefix' => '<ol class="code">',
    '#suffix' => '</ol>',
  );
  $form['self_details']['js_block_description'] = array(
    '#weight' => 2,
    '#markup' => t("Add the following code in a block on all pages; ensure that you replace the 'example.com' link with the correct value for your site and that you replace #s_code file# with the path to your s_code file:"),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );

  // Note - Must encode HTML, but also retain hard returns.
  $html_example = htmlentities('<!-- SiteCatalyst code version: H.20.3.Copyright 1996-2013 Adobe, Inc. -->') . '<br/>';
  $html_example .= htmlentities('<script type="text/javascript" language="JavaScript" src="#s_code file#"></script>') . '<br/>';
  $html_example .= htmlentities('<script type="text/javascript" language="JavaScript"><!--') . '<br/>';
  $html_example .= htmlentities('/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/') . '<br/>';
  $html_example .= htmlentities('var s_code=s.t();if(s_code)document.write(s_code)//--></script>') . '<br/>';
  $html_example .= htmlentities('<script language="JavaScript" type="text/javascript"><!--') . '<br/>';
  $html_example .= htmlentities('if(navigator.appVersion.indexOf(\'MSIE\')>=0)document.write(unescape(\'%3C\')+\'\\!-\'+\'-\')') . '<br/>';
  $html_example .= htmlentities('//--></script><noscript><a href="http://www.omniture.com" title="Web Analytics"><img src="http://examplecom.112.2O7.net/b/ss/examplecom/1/H.20.3--NS/0/5595703"') . '<br/>';
  $html_example .= htmlentities('height="1" width="1" border="0" alt="" /></a></noscript><!--/DO NOT REMOVE/-->') . '<br/>';
  $html_example .= htmlentities('<!-- End SiteCatalyst code version: H.20.3. -->') . '<br/>';

  $form['self_details']['js_block_code'] = array(
    '#weight' => 3,
    '#markup' => $html_example,
    '#prefix' => '<p class="code">',
    '#suffix' => '</p>',
  );

  // Add the admin css file:
  $path = drupal_get_path('module', 'pfizer_webstandards');
  $form['#attached']['css'][] = $path . '/resources/css/admin.css';

  // Note = the following are not included in this function:
  // Webinar - 12.3.5 & 12.3.7
  // Self Detail - 12.5.1
  // E-Detail 12.5.2
  return $form;
}
