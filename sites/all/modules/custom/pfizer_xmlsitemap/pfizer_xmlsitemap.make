; Drush make file for pfizer_xmlsitemap dependencies.

api = 2
core = "7.x"

projects[] = drupal

projects[xmlsitemap][subdir] = "contrib"
projects[xmlsitemap][version] = "2.3"
