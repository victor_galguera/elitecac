<?php
/**
 * @file
 * pfizer_webstandards.default_pfizer_webstandards_customlinks.inc
 */

/**
 * Implements hook_default_pfizer_webstandards_customlink().
 */
function pfizer_webstandards_default_pfizer_webstandards_customlink() {
  $export = array();

  $customlink = new stdClass();
  $customlink->disabled = FALSE; /* Edit this to true to make a default customlink disabled initially */
  $customlink->api_version = 1;
  $customlink->machine_name = 'download_link';
  $customlink->href = '';
  $customlink->link_type = 'd';
  $customlink->function = 'pfDownloadLink';
  $customlink->object = '';
  $customlink->interaction_status = '';
  $customlink->weight = -50;
  $export['download_link'] = $customlink;

  $customlink = new stdClass();
  $customlink->disabled = FALSE; /* Edit this to true to make a default customlink disabled initially */
  $customlink->api_version = 1;
  $customlink->machine_name = 'exit_link';
  $customlink->href = '';
  $customlink->link_type = 'e';
  $customlink->function = 'pfTrackInteractionStatus';
  $customlink->object = '';
  $customlink->interaction_status = '';
  $customlink->weight = -50;
  $export['exit_link'] = $customlink;

  return $export;
}