Pfizer Encrypt
==============

## Overview

*Pfizer Encrypt module supporting Webform 4.x and Encrypt 2.x*

Pfizer Encrypt 1.x should not be used. Always use Pfizer Encrypt 2.x or 3.x instead depending on whether you require support for Webform 3.x (Pfizer Encrypt 2.x) or Webform 4.x (Pfizer Encrypt 3.x). If you are not using Webform, always use the latest version of Pfizer Encrypt.

Pfizer Encrypt 2.x introduced support for a new, default key provider *Pfizer Private Key File*. No other key provider should be used and will not be supported.

To check if there are Webform components that can be encrypted, there is a drush command:

```
drush pfizer-encrypt-webform-check
```

It will only return `TRUE` if there are components that are not encrypted that should be. It excludes the following types: fieldset, file, markup and pagebreak.

## Modules

- [Encrypt](https://www.drupal.org/project/encrypt) 7.x-2.2 plus Legacy Pfizer Encrypt decryption patch: Provides base encryption functionality
- [Real AES](https://www.drupal.org/project/real_aes) 7.x-1.0: Provides "Authenticated AES" encryption method plugin using [OpenSSH PHP extension](http://php.net/manual/en/book.openssl.php) and [Defuse PHP-encryption library](https://github.com/defuse/php-encryption)
- [Field Encryption](https://www.drupal.org/project/field_encrypt) 1.0-beta2: Encrypts entity fields
- [Webform Encrypt](https://www.drupal.org/project/webform_encrypt) 7.x-1.1 plus Webform 4.x patch: Support for Webform 4.x encrypted submission data

## What's new?

- Webform 4.x support.

*Use Pfizer Encrypt 2.x if your project requires Webform 3.x.*

## Important

As of version 2.0.1, Pfizer Encrypt now generates a test encryption key for local development environments. Data should be encrypted for testing and/or development purposes only and never moved from the development environment. Decryption of data from a development environment on Acquia or sandbox environments will result in a fatal PHP error.

## Troubleshooting

### PHP Errors

```
Notice: Undefined index: value in entity_metadata_field_property_get() (line 438 of /mnt/www/html/subscription/docroot/sites/all/modules/contrib/entity/modules/callbacks.inc).
```

The above error can occur if decryption failed on a field using the Field Encrypt module. It usually only happens if the encryption method is no longer available or the private key is incorrect.
