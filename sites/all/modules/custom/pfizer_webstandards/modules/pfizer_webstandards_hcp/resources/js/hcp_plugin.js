/**
 * @file
 * HCP Specific JavaScript for Web Standards.
 */

(function ($) {

  Drupal.behaviors.pfizer_webstandards_hcp = {};

  Drupal.behaviors.pfizer_webstandards_hcp.attach = function (context, settings) {
    var form = $('form.entitytype-hcp_contact_us-form');

    if (form.length) {
      $(form).submit(function() {
        var s = build_s();

        s.eVar27 = 'UNBRANDED';
        s.eVar28 = 'WEBSITE';
        s.prop28 = 'UNBRANDED';
        s.prop29 = 'WEBSITE';
        s.prop41 = 'PBT';
        s.prop43 = 'REMOTEDETAILBOOKING';

        s.linkTrackVars ='events,eVar27,eVar28,prop28,prop29,prop41,prop43';

        s.forcedLinkTrackingTimeout = 500;

        s.tl(true, 'o', 'WEBSITE VISIT');
        s.pfClearVars();
      });
    }
  };

})(jQuery);

/**
 * HCP specific JS function.
 *
 * @param s
 *   Report Suite Object
 */
function pfizerWebstandardsHCP(s) {
  "use strict";

  // Check if we are on a search results page:
  var searchUrls = ['*/search/site/*'];
  if (pfizerWebstandardsUrlMatch(searchUrls)) {
    // We are on a search page.
    if (typeof pfizerWebstandardsHcpKeywords !== 'undefined' && pfizerWebstandardsHcpKeywords) {
      // All HCP searches are site wide.
      s.pfSearch(pfizerWebstandardsHcpKeywords, 'site', pfizerWebstandardsHcpNumFound);
    }
  }

  pfizerWsCheckBanner(s);

  // Event31:
  // Defined as: 'on page load for pages linked to via an internal Campaign
  // Spot.'
  // s_code file will automatically detect any pages with intcmp in the url. No
  // need for any bespoke code anymore.
}
