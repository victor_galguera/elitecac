<?php

/**
 * @file
 * Contains pfizer-uuid-duplicate command implementation.
 */

/**
 * Implements hook_drush_command().
 */
function pfizer_uuid_drush_command() {

  $items['pfizer-uuid-duplicate'] = array(
    'description' => 'Checks for entities with duplicate UUIDs.',
    'aliases' => array('pud'),
    'outputformat' => array(
      'default' => 'key-value',
      'pipe-format' => 'json',
      'label' => 'UUID Duplicates',
      'output-data-type' => 'format-single',
    ),
  );

  return $items;
}

/**
 * Callback for the pfizer-uuid-duplicates command.
 */
function drush_pfizer_uuid_duplicate() {
  $status = pfizer_uuid_duplicate_status();
  return $status['status'];
}
