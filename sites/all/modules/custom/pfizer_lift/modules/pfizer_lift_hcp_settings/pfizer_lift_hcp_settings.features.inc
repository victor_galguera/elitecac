<?php
/**
 * @file
 * pfizer_lift_hcp_settings.features.inc
 */
/**
 * Implements hook_ctools_plugin_api().
 */
function pfizer_lift_hcp_settings_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == "visitor_actions" && $api == "visitor_actions") {
    return array("version" => "1");
  }
}