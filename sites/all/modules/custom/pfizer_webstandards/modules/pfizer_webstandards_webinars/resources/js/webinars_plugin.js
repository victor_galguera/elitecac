/**
 * @file
 * Webinars Specific JavaScript for Web Standards.
 */

/**
 * Webinars specific JS function.
 *
 * @param s
 *   Report Suite Object
 */
function pfizerWebstandardsWebinars(s) {
  "use strict";

  // Event 5 - Onsite Searches
  // Check if we are on a search results page:
  var searchUrls = ['*/search*'];
  if (pfizerWebstandardsUrlMatch(searchUrls)) {
    // We are on a search page.
    if (typeof pfizerWebstandardsWebinarsNumFound !== 'undefined' && pfizerWebstandardsWebinarsNumFound) {
      // All HCP searches are site wide.
      s.pfSearch(pfizerWebstandardsWebinarsKeywords, 'WEBINAR', pfizerWebstandardsWebinarsNumFound);
    }
  }

  // Registrations
  // Event 8 - Registrations Initiated.
  // Event 8 will be sent on pages with a div class
  //   'event-main-buttons-unregistered'
  // This is the register box.
  if (jQuery('div.event-main-buttons-unregistered').length > 0) {
    s.pfRegistration('WEBINAR', 'initiated');
  }

  // Survey
  // Event 10 - Survey Offered
  // Event 11 - Survey Complete

  // Webinar
  // Event 22 - Webinar Abandon
  // Event 25 - Webinar Complete

  // Banner Adverts:
  // There are two banner advert events to be configured.
  // On Page Load - check to see if there is a banner advert; if so call
  // pfBannerAdImpression. Defined as any a element with a href containing
  // intcmp=
  jQuery('a[href*="intcmp="]').each(function () {
    // Cycle through all the matches (if there are any). As this is the page
    // load we can only log one 'intcmp' value so stop after the first one.
    // Find the ? in the href.
    var href = jQuery(this).attr('href'),
      questionPos = href.indexOf('?'),
      urlSectionArray,
      loopCount,
      valueToReturn = null;

    if (questionPos >= 0) {
      href = href.substring(questionPos + 1);

      // split into an array
      urlSectionArray = href.split('&');

      // Cycle through the url array
      for (loopCount = 0; loopCount < urlSectionArray.length; loopCount++) {
        // Check to see if this is the correct value.
        if (urlSectionArray[loopCount].substring(0, 7) === 'intcmp=') {
          valueToReturn = urlSectionArray[loopCount].substring(7);
          s.pfBannerAdImpression(valueToReturn);

          // Break the each loop by returning false;
          return false;
        }
      }
    }
  });

  // Event31:
  // Defined as: 'on page load for pages linked to via an internal Campaign
  // Spot.'
  // s_code file will automatically detect any pages with intcmp in the url. No
  // need for any bespoke code anymore.
}

/**
 * Add on click events after document has loaded.
 *
 * Event 9 - Registration Completed.
 * Event 28 - Webinar Registration Completed.
 */

Drupal.behaviors.pfizerWebStandardsWebinars = {
  attach: function (context, settings) {
    jQuery('body', context).once(function () {
      "use strict";

      // Event 9 and event 28 will be sent when a user clicks either of the register
      // buttons.

      var register = function (webinarName) {
        var s = build_s();
        s.events = s.apl(s.events, 'event9', ',', 2);
        s.eVar9 = 'WEBINAR';
        s.prop43 = 'REGISTRATION';
        s.linkTrackVars = 'events,eVar9,prop43';
        s.linkTrackEvents = s.apl(s.linkTrackEvents, 'event9', ',', 2);
        s.tl(true, 'o', 'Registration Completed');
        s.pfClearVars();

        var s = build_s();
        s.events = s.apl(s.events, 'event28', ',', 2);
        s.prop29 = 'WEBINAR';
        s.eVar28 = 'D=c29';
        s.prop30 = webinarName;
        s.eVar29 = 'D=c30';
        s.linkTrackVars = 'events,prop29,prop30,eVar28,eVar29';
        s.linkTrackEvents = s.apl(s.linkTrackEvents, 'event28', ',', 2);
        s.tl(true, 'o', 'webinar registration');
        s.pfClearVars();

        var s = build_s();
        s.events = s.apl(s.events, 'event1', ',', 2);
        s.eVar1 = 'webinar registration';
        s.linkTrackVars = 'events,eVar1';
        s.linkTrackEvents = s.apl(s.linkTrackEvents, 'event1', ',', 2);
        s.tl(true, 'o', 'lead initiated');
        s.pfClearVars();
      };

      // Class 'event-main-bottom-button1' or 'event-main-bottom-button2'
      jQuery('div.event-main-buttons-unregistered a.event-main-bottom-button1,div.event-main-buttons-unregistered a.event-main-bottom-button2').mouseup(function () {
        register(jQuery('h1').text());
      });

      // form button elements must use the mousedown event
      jQuery('div#pfizer-hcp2-webinar-registration form input[type=submit]').mousedown(function () {
        register(jQuery('h1').text());
      });

      jQuery('span.live-event.unregistered').each(function () {
        var $title = jQuery(this).parent().find('div.views-field-title');

        jQuery(this).find('a').mouseup(function () {
          register($title.find('span a').html());
        });
      });
    });
  }
};





