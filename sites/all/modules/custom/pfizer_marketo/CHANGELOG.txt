Pfizer Marketo 7.x-1.0.0 2017-01-09
-------------------------------------
- Updated JS to pull in global configuration from S3.

Pfizer Marketo 7.x-1.0-beta2 2015-03-17
-------------------------------------
- Initial module release.
- Updated Readme.