/*
 * Entry-points for Brightcove video integration.
 *
 * Function names cannot be changed without altering <param> tags within every
 * video implementation's code.
 *
 * These functions should not be wrapped within jQuery.
 *
 * Pfizer_webstandards' s_code.js file contains legacy implementations of the Loaded
 * and Ready event handlers. Because this pfizer_lift_video.js file is intentionally
 * loaded later than s_code.js, we can overwrite those functions. In order to preserve
 * legacy functionality we must call the legacy functions.
 * 
 * @param  {[type]} id [description]
 * @return {[type]}    [description]
 */

// track a separate BC object for every experience ID
var lift_bc_objects = [];
function myTemplateLoaded (id) {
  if (!lift_bc_objects.hasOwnProperty(id)) {
    lift_bc_objects[id] = new lift_bc();
  }

  var lift = lift_bc_objects[id];
  lift.load(id);

  // call legacy handler in s_code.js
  if (legacy_brightcove && legacy_brightcove.hasOwnProperty('myTemplateLoaded')) {
    legacy_brightcove.myTemplateLoaded(id);
  }
}

function onTemplateReady (evt) {
  var id = evt.target.experience.id;

  if (!lift_bc_objects.hasOwnProperty(id)) {
    lift_bc_objects[id] = new lift_bc();
  }

  var lift = lift_bc_objects[id];
  lift.ready(evt);

  // call legacy handler in s_code.js
  if (legacy_brightcove && legacy_brightcove.hasOwnProperty('onTemplateReady')) {
    legacy_brightcove.onTemplateReady(evt);
  }
}

/**
 * Lift's Brightcove object for tracking video events.
 * 
 * @return {[type]}         [description]
 */
var lift_bc = function() {
  "use strict";

  var player;
  var APIModules;
  var mediaEvent;
  var videoPlayer;

  var video_properties = {
    custom_fields: {},
    duration: 0,
    index: 0,
    // speaker: '',
    title: ''
  };

  // flags for tracking load & ready calls
  var did_load = false;
  var did_ready = false;

  // flags for tracking progress marker events
  var sent_25 = false;
  var sent_50 = false;
  var sent_75 = false;
  var sent_completed = false;
  var video_started = false;

  // save existing beforeunload and unload handlers, if any
  var old_beforeunload = false;
  var old_unload = false;
  
  if (typeof window.onbeforeunload != 'undefined') {
    old_beforeunload = window.onbeforeunload;
  }

  if (typeof window.onunload != 'undefined') {
    old_unload = window.onunload;
  }

  // bind new event handlers for onBeforeUnload and onUnload so we can
  // track video_abandon
  window.onbeforeunload = function (evt) {
    onAbandon(evt);

    if (old_beforeunload) {
      old_beforeunload(evt);
    }
  };

  window.oneunload = function (evt) {
    onAbandon(evt);

    if (old_unload) {
      old_unload(evt);
    }
  };

  /**
   * Generic handler for all events.
   * 
   * @param  {[type]} evt [description]
   * @return {[type]}     [description]
   */
  var genericEventHandler = function (evt) {
    video_properties.index = Math.round(evt.position);
  };

  /**
   * Event handler for 'begin' media event.
   * 
   * @param  {[type]} evt [description]
   * @return {[type]}     [description]
   */
  var onBegin = function (evt) {
    // any custom fields associated with this video
    video_properties.custom_fields = evt.media.customFields;
    // video duration
    video_properties.duration = Math.round(evt.media.length / 1000);
    // video title
    video_properties.title = evt.media.displayName;

    // flag video as started; necessary for video_abandon event
    video_started = true;

    // base metrics
    if (Drupal.pfizer_lift) {
      pfaLoader.set_pm_item('base_metrics', Drupal.pfizer_lift.getBaseMetrics());

      // we don't need separte pfaLoader objects for different videos on the same
      // page because a) the base metrics will be the same between the videos and
      // b) all video-specific info is always passed as part of video_properties
      // and thus each call to pfaLoader.trigger() will result in the event data
      // being built from scratch
    }

    pfaLoader.trigger('video_start', video_properties);
  };

  /**
   * Event handler for 'complete' media event.
   * 
   * @param  {[type]} evt [description]
   * @return {[type]}     [description]
   */
  var onComplete = function (evt) {
    pfaLoader.trigger('video_complete', video_properties);
    sent_completed = true;
  };

  /**
   * Event handler for 'progress' media event.
   * 
   * @param  {[type]} evt [description]
   * @return {[type]}     [description]
   */
  var onProgress = function (evt) {
    // video progress percentage
    var percentage = Math.round((video_properties.index / video_properties.duration) * 100);

    if (!sent_25 && percentage >= 25) {
      sent_25 = true;
      pfaLoader.trigger('video_25', video_properties);
    }

    if (!sent_50 && percentage >= 50) {
      sent_50 = true;
      pfaLoader.trigger('video_50', video_properties);
    }

    if (!sent_75 && percentage >= 75) {
      sent_75 = true;
      pfaLoader.trigger('video_75', video_properties);
    }
  };

  /**
   * Event handler for onBeforeUnload and onUnload window events.
   * 
   * @param  {[type]} evt [description]
   * @return {[type]}     [description]
   */
  var onAbandon = function (evt) {
    if (!video_started || sent_completed) {
      return;
    }

    sent_completed = true;

    pfaLoader.trigger('video_abandon', video_properties);
  };

  return {
    'load': function (id) {
      if (did_load) {
        return;
      }

      did_load = true;

      player = brightcove.api.getExperience(id);
      APIModules = brightcove.api.modules.APIModules;
      mediaEvent = brightcove.api.events.MediaEvent;
    },

    'ready': function (evt) {
      if (did_ready) {
        return;
      }

      did_ready = true;

      videoPlayer = player.getModule(APIModules.VIDEO_PLAYER);

      videoPlayer.addEventListener(mediaEvent.BEGIN, function (evt) {
        genericEventHandler(evt);
        onBegin(evt);
      });

      videoPlayer.addEventListener(mediaEvent.COMPLETE, function (evt) {
        genericEventHandler(evt);
        onComplete()(evt);
      });

      videoPlayer.addEventListener(mediaEvent.PROGRESS, function (evt) {
        genericEventHandler(evt);
        onProgress(evt);
      });

      videoPlayer.addEventListener(mediaEvent.PLAY,        genericEventHandler);
      videoPlayer.addEventListener(mediaEvent.STOP,        genericEventHandler);
      videoPlayer.addEventListener(mediaEvent.SEEK_NOTIFY, genericEventHandler);
    }
  };
};
