/******************************************************************************
    MediaPlatform - PFA integration
******************************************************************************/
(function ($) {

  Drupal.behaviors.pfizer_analytics_mediaplatform = {
    mp_webinar_states: [],

    attach: function (context, settings) {
      context = $(context);

      var body = $('body.logged-in');
      if (!body.length) {
        return;
      }

      var mp = Drupal.pfa_mediaplatform;
      mp.init();

      // setup Portal-originating events that will trigger PFA
        // pre-2.7 portal
        // bind to webinar registration button to fire Registration Start event
        var register_btn = context.find('input[name="webinar_registration_action"]');
        if (register_btn.length) {
          register_btn.mousedown(function (e) {
            e.preventDefault();

            // avoid duplicate-bind issues
            if (checkAttr(register_btn, 'pfizer_analytics_mediaplatform')) {
              return;
            }

            mp.register_start();
          });
        }

        // pre-2.7 Portal 
        // check context for confirmation modal, and fire Registration Complete event
        var modal = $('#dialog #pfizer-hcp2-registration .message.registered');
        var dereg = $('#dialog #pfizer-hcp2-deregistration');
        if (modal.length && (!dereg.length || dereg.css('display') == 'none') && checkAttr(modal, 'pfizer_analytics_mediaplatform')) {
          mp.register_complete();
        }

        // this will ensure we only run this once
        if (typeof document.mediaplatform_actinium == 'undefined') {
          document.mediaplatform_actinium = {
            saw_register: false,
            saw_unregister: false,
            sent_reg_start: false // flag to mark execution of the registration start event
          };

          var mpa = document.mediaplatform_actinium;

          // base selector for targeting un/reg buttons
          var selector = '#node-' + Drupal.settings.pfizer_analytics_mediaplatform.webinar_id + ' .webinar-register';
          var selector_button = selector + '  .register-button a';

          var check = function () {
            // send registration started event on check of checkbox
            var checkbox = $(selector + ' .control-checkbox input[type=checkbox]');
            if (checkbox.length && !mp.check_attr(checkbox, 'pfizer-analytics-mediaplatform')) {
              // reset flags
              mpa.sent_reg_start = false;

              checkbox.click(function (e) {
                if (checkbox.attr('checked')) {
                  mpa.sent_reg_start = true;

                  mp.register_start();
                }
              });
            }

            var reg_button = $(document).find(selector_button).not('.registered');
            if (reg_button.length) {
              // prevents link_click event from being fired when this link is clicked
              mp.check_attr(reg_button, 'pfizer-lift'); 

              mpa.saw_register = true;

              if (mpa.saw_unregister) {
                mpa.saw_unregister = false;
                reg_button.removeAttr('pfizer-analytics-mediaplatform-processed');
              }

              if (!mp.check_attr(reg_button, 'pfizer-analytics-mediaplatform')) {
                reg_button.off('click.pfaUnregister');

                reg_button.on('click.pfaRegister', function () {
                  if (checkbox.length && checkbox.attr('checked') !== 'checked') {
                    return;
                  }

                  // send registration start if it has not already been sent
                  if (!mpa.sent_reg_start) {
                    mp.register_start();
                  }

                  // send completion
                  mp.register_complete();
                });
              }

              mpa.saw_register = true;
            }

            var unreg_button = $(document).find(selector_button + '.registered');
            if (unreg_button.length) {
              // prevents link_click event from being fired when this link is clicked
              mp.check_attr(unreg_button, 'pfizer-lift'); 

              mpa.saw_unregister = true;

              if (mpa.saw_register) {
                mpa.saw_register = false;
                unreg_button.removeAttr('pfizer-analytics-mediaplatform-processed');
              }

              if (!mp.check_attr(unreg_button, 'pfizer-analytics-mediaplatform')) {
                unreg_button.off('click.pfaRegister');

                unreg_button.on('click.pfaUnregister', function () {
                  mpa.sent_reg_start = false;
                });
              }
            }

            // we never want to stop calling this function, so always return false
            return false;
          };

          mp.wait_and_check(-1, 500, check, function () {}, function () {});
        }

      // append pass-through data to Mediaplatform elements for consumption by PFA 
      // from Mediaplatform's side
        // append base Lift metrics into a base_metrics query string param on the Visit Lobby link
        if (typeof Drupal.pfizer_lift.built_udf !== 'undefined') {
          Drupal.pfizer_lift.built_udf.then(function () {
            var params = [];
            var base_params = Drupal.pfizer_lift.getBaseMetrics();
            for (var key in base_params) {
              if (!key.match(/^(person_|event_|touch_|pfizer)/)) {
                continue;
              }

              params.push(key + '=' + base_params[key]);
            }

            // webinar start date
            if (typeof Drupal.settings.pfizer_analytics_mediaplatform.webinar_date !== 'undefined') {
              params.push('event_udf36=' + Drupal.settings.pfizer_analytics_mediaplatform.webinar_date);
            }

            // webinar guid
            if (typeof Drupal.settings.pfizer_analytics_mediaplatform.webinar_guid !== 'undefined') {
              params.push('webinar_guid=' + Drupal.settings.pfizer_analytics_mediaplatform.webinar_guid);
            }

            // webinar duration
            if (typeof Drupal.settings.pfizer_analytics_mediaplatform.webinar_duration !== 'undefined') {
              params.push('webinar_duration=' + Drupal.settings.pfizer_analytics_mediaplatform.webinar_duration);
            }

            params = {
              base_metrics: params.join('|')
            };

            var pfa_exposed = Drupal.settings.pfizer_analytics;

            // pass-through tracking codes
            if (typeof pfa_exposed.tracking_codes !== 'undefined') {
              for (var key in pfa_exposed.tracking_codes) {
                params[key] = pfa_exposed.tracking_codes[key];
              }
            }

            // pass-through webstandards data
            if (typeof pfa_exposed.webstandards !== 'undefined') {
              for (var key in pfa_exposed.webstandards) {
                params['ws_' + key] = pfa_exposed.webstandards[key];
              }
            }

            var search_for_lobby_links = function (selector) {
              var lobby_links = context.find(selector);
              if (lobby_links.length) {
                lobby_links.each(function (i, el) {
                  el = $(el);
                  
                  if (!mp.check_attr(el, 'pfizer-analytics-mediaplatform')) {
                    el.attr('href', mp.build_href(el.attr('href'), params));

                    el.click(function () {
                      mp.webinar_start();
                    });
                  }
                });
              }
            };

            // pre-2.7 Portal
            search_for_lobby_links('.pfizer_analytics_mediaplatform_lobby_link');

            // 2.7+ Portal
            $(document).on('webinar:init', function(e, event) {
              // check to make sure base metrics haven't already been added
              if (event.launch_url.indexOf('touch_udf') == -1) {
                search_for_lobby_links('.node.webinar .webinar-launch a');
              }
            });
          });
        }
    }
  };

  /**
   * Mediaplatform object for tracking video events.
   */
  Drupal.pfa_mediaplatform = (function() {
    var attributes = {};
    var do_init = false;

    return {
      'init': function(settings) {
        if (typeof do_init !== 'boolean') {
          return;
        }

        do_init = new Promise(function (resolve, reject) {
          pfizer_analytics.mediaplatform.pfaLoader.init('mediaplatform', 'v1', 'dev');
          attributes.title = $('h1 div').text();

          if (typeof Drupal.pfizer_lift.built_udf == 'undefined') {
            reject();
          }

          // pull-in base metrics
          Drupal.pfizer_lift.built_udf.then(function () {
            var base_metrics = Drupal.pfizer_lift.getBaseMetrics();

            // webinar start date
            if (typeof Drupal.settings.pfizer_analytics_mediaplatform.webinar_date !== 'undefined') {
              base_metrics['event_udf36'] = Drupal.settings.pfizer_analytics_mediaplatform.webinar_date;
            }

            // webinar guid
            if (typeof Drupal.settings.pfizer_analytics_mediaplatform.webinar_guid !== 'undefined') {
              base_metrics['webinar_guid'] = Drupal.settings.pfizer_analytics_mediaplatform.webinar_guid;
            }

            // webinar duration
            if (typeof Drupal.settings.pfizer_analytics_mediaplatform.webinar_duration !== 'undefined') {
              base_metrics['webinar_duration'] = Drupal.settings.pfizer_analytics_mediaplatform.webinar_duration;
            }

            pfizer_analytics.mediaplatform.pfaLoader.set_pm_item('base_metrics', base_metrics);

            resolve();
          });
        });
      },

      /**
       * Returns true if attribute flag is set on context. If flag is not
       * present, it is set automatically.
       * 
       * @param  {[type]} context [description]
       * @param  {[type]} id      [description]
       * @return {[type]}         [description]
       */
      'check_attr': function (context, id) {
        id = id + '-processed';

        context = $(context);

        if (context.attr(id)) {
          return true;
        }

        context.attr(id, id);
        return false;
      },

      /**
       * Helper to priodically run the `check` callback until non-false is returned, at which
       * point the returned value is passed to the `success` callback.
       * 
       * @param  {[type]} try_count [description]
       * @param  {[type]} interval  [description]
       * @param  {[type]} check     [description]
       * @param  {[type]} success   [description]
       * @param  {[type]} fail      [description]
       * @return {[type]}           [description]
       */
      'wait_and_check': function (try_count, interval, check, success, fail) {
        var self = this;

        // attempt the check()
        if (try_count != 0) {
          // one interation of the interval
          setTimeout(function () {
            try_count--;

            var item = check();
            if (item) {
              return success(item);
            }

            self.wait_and_check(try_count, interval, check, success, fail);
          }, interval);
        }
        // we've ran out of attempts
        else if (typeof fail == 'function') {
          fail();
        }
      },

      /**
       * Appends Lift parameters to given href.
       * 
       * @param  {[type]} href   [description]
       * @param  {[type]} params [description]
       * @return {[type]}        [description]
       */
      'build_href': function (href, params) {
        if (href.indexOf('?') == -1) {
          href += '?';
        }

        return href += $.param(params);
      },

      /**
       * Sends event.
       * 
       * @param  {[type]} name [description]
       * @param  {[type]} attr [description]
       * @return {[type]}      [description]
       */
      'trigger': function (name, attr) {
        do_init.then(function () {
          pfizer_analytics.mediaplatform.pfaLoader.trigger(name, attr);
        })
      },

      /**
       * Fires register_start event
       * @return {[type]} [description]
       */
      'register_start': function () {
        this.trigger('webinar_registration_start', attributes);
      },

      /**
       * Fires register_complete event
       * @return {[type]} [description]
       */
      'register_complete': function () {
        this.trigger('webinar_registration_complete', attributes);
      },

      /**
       * Fires webinar_start event
       * @return {[type]} [description]
       */
      'webinar_start': function () {
        this.trigger('webinar_start', attributes);
      }
    }
  })();

})(jQuery);

