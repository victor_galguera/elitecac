; Drush make file for pfizer_security dependencies.

api = 2
core = "7.x"

projects[] = drupal

projects[autologout][subdir] = "security"
projects[autologout][version] = "4.4"

projects[email_confirm][subdir] = "security"
projects[email_confirm][version] = "1.3"

projects[flood_control][subdir] = "security"
projects[flood_control][version] = "1.0"

projects[globalredirect][subdir] = "security"
projects[globalredirect][version] = "1.5"

projects[hacked][subdir] = "security"
projects[hacked][version] = "2.0-beta6"

projects[m4032404][subdir] = "contrib"
projects[m4032404][version] = "1.0-beta2"

projects[mollom][subdir] = "security"
projects[mollom][version] = "2.15"

projects[paranoia][subdir] = "security"
projects[paranoia][version] = "1.7"

projects[password_policy][subdir] = "security"
projects[password_policy][version] = "2.0-alpha6"

projects[seckit][subdir] = "security"
projects[seckit][version] = "1.9"
projects[seckit][patch][i2644474][url] = "https://www.drupal.org/files/issues/seckit-X-frame-bust-clickjacking-2644474-9.patch"
projects[seckit][patch][i2644474][md5] = "d180490cd17fb6e7d357bd16c0a13274"
projects[seckit][patch][i2657346][url] = "https://www.drupal.org/files/issues/remove_h1-2657346-3-D7.patch"
projects[seckit][patch][i2657346][md5] = "8f9fa85a3dd9b39d8955443728ceb557"

projects[securepages][subdir] = "security"
projects[securepages][version] = "1.0-beta2"
projects[securepages][patch][i2381857][url] = "https://www.drupal.org/files/issues/securepages-select-http-code_2.patch"
projects[securepages][patch][i2381857][md5] = "5f0df344f883654fb7126cb29d651920"

projects[security_review][subdir] = "security"
projects[security_review][version] = "1.2"

projects[session_limit][subdir] = "security"
projects[session_limit][version] = "2.2"

projects[username_enumeration_prevention][subdir] = "security"
projects[username_enumeration_prevention][version] = "1.2"
; https://www.drupal.org/node/2587683#comment-11618427
projects[username_enumeration_prevention][patch][i2587683][url] = "https://www.drupal.org/files/issues/2587683-limit-error-message-to-be-shown-only-on-submit-instead-of-validation_0.patch"
projects[username_enumeration_prevention][patch][i2587683][md5] = "d26560d13b3e516386bd87613b7ae9a1"
