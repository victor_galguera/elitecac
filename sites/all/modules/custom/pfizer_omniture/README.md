# pfizer_omniture

Drupal Pfizer Omniture module:

## Overview

Pfizer Omniture Module is enhancement of existing Omniture Module
This module has two fields to enter production and non production report suite.
It allows user to enter both production and non production report suite simultaneously.

## Installation

1. Go to "Administer" -> "Modules" and enable the pfizer_omniture module.
2. If previous omniture module(just 'omniture') exits, all the data except the report suite will be copied
   in the new module and old module will be disabled.
3. If old omniture module does not exist, then all the configuration changes has to passed through
   features from one environment to other.

**Report Suite Variables**
A report suite defines the complete, independent reporting on a chosen website.
Commonly for each project is generated tow report suites, one for DEV using during development to testing and approval
and the other for PROD, which should be used ONLY in production environments.

To set these two values, we must use Drupal variables, you can use `local.settings.php` for this:
```
$conf['pfizer_omniture_s_account_non_prod'] = 'xxxx';
$conf['pfizer_omniture_s_account_prod'] = 'yyyyy';
```

More information about Report Suites: https://marketing.adobe.com/resources/help/en_US/reference/report_suite_gloss.html

## Usage  
Access `/admin/config/omniture` to configure the general settings.  
All the tracking must be made by using Javascript, there is an example in the JS config settings at: `/admin/config/omniture/javascript-settings`

