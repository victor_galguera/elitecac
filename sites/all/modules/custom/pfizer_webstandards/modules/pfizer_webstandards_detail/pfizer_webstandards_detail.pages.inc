<?php

/**
 * Form callback for /admin/config/system/webstandards/detail
 * 
 * @param  [type] $form       [description]
 * @param  [type] $form_state [description]
 * @return [type]             [description]
 */
function pfizer_webstandards_detail_form($form, &$form_state) {
  $fields = pfizer_webstandards_detail_fields();

  $form['#attached']['css'][] = WS_DETAIL_PATH . '/resources/css/pfizer_webstandards_detail.css';

  $form['desc'] = array(
    '#markup' => t('This page provides configurable settings for the detailed metrics.'),
  );

  $form['fields'] = array(
    '#tree' => TRUE,
  );

  $form['fields']['config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configurable Metrics'),
    '#description' => t('The values of these metrics can be configured. These values 
      will be provided as part of all requests, unless overridden on an individual
      page/request using the !urls functionality.', array(
        '!urls' => l(t('URLs'), 'admin/config/system/webstandards/urls'),
      )),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['fields']['dynamic'] = array(
    '#type' => 'fieldset',
    '#title' => t('Dynamic Metrics'),
    '#description' => t('The values of these metrics are set dynamically by the 
      application, but they can be enabled or disabled using the options below.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['fields']['fixed'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fixed Metrics'),
    '#description' => t('The values of these metrics are fixed and cannot be changed.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  array_walk($fields, function($field, $key) use (&$form) {
    // we don't need to run either name or sc_name through t()
    $title = $field['name'];
    if ($field['name'] != $field['sc_name']) {
      $title .= ': ' . $field['sc_name'];
    }

    if ($field['group'] == 'dynamic') {
      ensure_set($form['fields']['dynamic'], 'values', array(
        '#type' => 'checkboxes',
        '#title' => t('Enabled metrics'),
        '#options' => array(),
        '#default_value' => array(),
      ));

      $form['fields']['dynamic']['values']['#options'][$field['name']] = $title;

      if (isset($field['status']) && $field['status']) {
        $form['fields']['dynamic']['values']['#default_value'][] = $field['name'];
      }
    }
    else {
      $form['fields'][$field['group']][$field['name']] = array(
        '#title' => check_plain($title),
        '#description' => t(check_plain($field['desc'])),
        '#type' => 'textfield',
        '#default_value' => isset($field['value']) ? $field['value'] : '',
      );
    }

    // only fields in the config group should be editable; the
    // rest are in the form for informational purposes
    if ($field['group'] != 'config') {
      $form['fields'][$field['group']][$field['name']]['value']['#disabled'] = TRUE;
    }
  });

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Submit handler for pfizer_webstandards_detail_form();
 * 
 * @param  [type] $form       [description]
 * @param  [type] $form_state [description]
 * @return [type]             [description]
 */
function pfizer_webstandards_detail_form_submit($form, &$form_state) {
  ctools_include('export');

  // process configurable metrics; these can contain string values
  // which are used to hardcode the data sent to SiteCatalyst
  if (isset($form_state['values']['fields']['config'])) {
    array_walk($form_state['values']['fields']['config'], function ($value, $name) {
      // load or create a ctools object
      if (!$field = ctools_export_crud_load('pfizer_webstandards_detail', $name)) {
        $field = ctools_export_crud_new('pfizer_webstandards_detail');
        $field->machine_name = $name;
      }

      // set or update the value & save the object
      $field->value = $value;
      ctools_export_crud_save('pfizer_webstandards_detail', $field);
    });
  }

  // process dynamic metrics; the values of these metrics are set at run-time,
  // and may vary depending on the context. tracking of these metrics can be
  // enabled or disabled ('status' flag) from the UI
  if (isset($form_state['values']['fields']['dynamic']['values'])) {
    array_walk($form_state['values']['fields']['dynamic']['values'], function ($value, $name) {
      // load or create a ctools object
      if (!$field = ctools_export_crud_load('pfizer_webstandards_detail', $name)) {
        $field = ctools_export_crud_new('pfizer_webstandards_detail');
        $field->machine_name = $name;
      }

      // set or update the value & save the object
      $field->status = $value ? 1 : 0;
      ctools_export_crud_save('pfizer_webstandards_detail', $field);
    });
  }

  drupal_set_message(t('Configuration settings have been saved.'));
}

/**
 * Menu callback for /admin/config/system/webstandards/detail/export
 * 
 * @return [type] [description]
 */
function pfizer_webstandards_detail_export() {
  $fields = pfizer_webstandards_detail_prep_fields_js();

  $js_fields = array();
  array_walk($fields, function ($field) use (&$js_fields) {
    if (!isset($field['exportable']) || !$field['exportable']) {
      return;
    }

    // build field object
    $js_field = array();
    array_walk($field, function($value, $name) use (&$js_field) {
      // prepare name:value pair
      
      // strip line breaks otherwise JS will complain
      $value = str_replace("\n", ' ', $value);
      // escape double quotes
      $value = str_replace('"', '\"', check_plain($value));
      // sanity check
      $value = check_plain($value);

      $js_field[] = "\t" . $name . ': "' . $value . '"';
    });

    // save field object
    $js_fields[] = "{\n" . implode(",\n", $js_field) . "\n}";
  });

  // log the Detail IM plugin with Pfizer Web Standards
  header('content-type: application/javascript');
  header('Content-Disposition: attachment; filename="detail_metrics.js"');

  $js_code = '';

  // build array of field objects and save where the detail_plugin.js will expect this code
  $js_code .= "Drupal.settings.pfizer_webstandards_detail = [" . implode(",\n", $js_fields) . "];";
  $js_code .= "\n\n";

  // add the browser-storage JS for tracking of pages in session
  $js_code .= file_get_contents(WS_DETAIL_PATH . '/resources/js/detail_browser_storage.js');
  $js_code .= "\n\n";

  // append the actual detail_plugin.js
  $js_code .= file_get_contents(WS_DETAIL_PATH . '/resources/js/detail_plugin.js');

  echo $js_code;
  exit();
}
