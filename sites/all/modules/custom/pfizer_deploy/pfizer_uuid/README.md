# Pfizer UUID

Pfizer UUID inspects entity types and looks for duplicate UUIDs. If duplicate
UUIDs or revision UUIDs exist, it is reported in the following locations:

- Pfizer UUID report (admin/reports/pfizer-uuid)
- Drupal Status page (admin/reports/status)
- Deploy related pages (admin/structure/deploy)

This module is a dependency of Pfizer Deploy and should always be enabled.

It also includes a drush command `pfizer-uuid-duplicate` that will output
the boolean result of a duplicate UUID check.

## Known Issues

Bean Revisions are being excluded because they generate duplicate revision
UUIDs and this is not known to cause issues.
