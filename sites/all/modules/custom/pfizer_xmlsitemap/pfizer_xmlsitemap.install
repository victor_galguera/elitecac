<?php

/**
 * @file
 * Install, update and uninstall functions.
 */

/**
 * Creates Pfizer SST sitemap.
 */
function pfizer_xmlsitemap_create_sst_sitemap() {
  // Disable the Pfizer SST context on any existing sitemaps to start with.
  $sitemaps = xmlsitemap_sitemap_load_multiple(FALSE);
  foreach ($sitemaps as $sitemap) {
    $sitemap->context['pfizer_sst'] = FALSE;
    xmlsitemap_sitemap_save($sitemap);
  }

  // Create a new sitemap with the Pfizer SST context enabled.
  $sitemap = new stdClass();
  $sitemap->smid = NULL;
  $context = xmlsitemap_get_current_context();
  $context['pfizer_sst'] = TRUE;
  $sitemap->context = $context;
  xmlsitemap_sitemap_save($sitemap);

  // List nodes.
  $accepted_node_types = array();
  $nodes = node_type_get_names();
  foreach ($nodes as $key => $node) {
    array_push($accepted_node_types, $key);
  }
  variable_set('pfizer_xmlsitemap_saved_content_types', $accepted_node_types);

  // List menus.
  if (module_exists('menu')) {
    $excluded_menus = pfizer_xmlsitemap_get_excluded_menus();

    $menus = menu_get_menus();
    $accepted_menus = array();
    foreach ($menus as $key => $menu) {
      if (!in_array($key, $excluded_menus)) {
        array_push($accepted_menus, $key);
      }
    }

    variable_set('pfizer_xmlsitemap_saved_menus', $accepted_menus);
  }

  // List Taxonomies.
  if (module_exists('taxonomy')) {
    $taxonomies = taxonomy_vocabulary_get_names();
    $accepted_taxonomies = array();
    foreach ($taxonomies as $key => $taxonomy) {
      array_push($accepted_taxonomies, $key);
    }

    variable_set('pfizer_xmlsitemap_saved_taxonomies', $accepted_taxonomies);
  }
}

/**
 * Implements hook_install().
 */
function pfizer_xmlsitemap_install() {
  pfizer_xmlsitemap_create_sst_sitemap();
}

/**
 * Implements hook_enable().
 */
function pfizer_xmlsitemap_enable() {
  if (module_exists('menu')) {
    module_enable(array('xmlsitemap_menu'), TRUE);
  }

  if (module_exists('taxonomy')) {
    module_enable(array('xmlsitemap_taxonomy'), TRUE);
  }

  pfizer_xmlsitemap_index_links();
  pfizer_xmlsitemap_mark_sitemaps_to_be_regenerated();
  drupal_cron_run();
  pfizer_xmlsitemap_delete_non_configured_sitemaps();
}

/**
 * Implements hook_uninstall().
 */
function pfizer_xmlsitemap_uninstall() {
  // Delete the variables.
  db_delete('variable')->condition('name', 'pfizer_xmlsitemap_%', 'LIKE')->execute();

  // Remove all sitemaps with the Pfizer SST context enabled.
  $sitemaps = xmlsitemap_sitemap_load_multiple(FALSE);
  foreach ($sitemaps as $sitemap) {
    if (isset($sitemap->context['pfizer_sst']) && $sitemap->context['pfizer_sst']) {
      xmlsitemap_sitemap_delete($sitemap->smid);
    }
  }
}

/**
 * Index xmlsitemap links.
 */
function pfizer_xmlsitemap_update_7100() {
  module_load_include('module', 'pfizer_xmlsitemap');
  pfizer_xmlsitemap_index_links();
  drupal_cron_run();
  pfizer_xmlsitemap_delete_non_configured_sitemaps();
}

/**
 * Re-create Pfizer SST sitemap, if not exist.
 */
function pfizer_xmlsitemap_update_7101() {
  module_load_include('module', 'pfizer_xmlsitemap');

  if (!pfizer_xmlsitemap_is_sst_sitemap_created()) {
    pfizer_xmlsitemap_create_sst_sitemap();
    pfizer_xmlsitemap_index_links();
  }
}

/**
 * Delete and Reindex Pfizer SST Sitemap.
 */
function pfizer_xmlsitemap_update_7102() {
  module_load_include('module', 'pfizer_xmlsitemap');

  if (!pfizer_xmlsitemap_is_sst_sitemap_created()) {
    pfizer_xmlsitemap_create_sst_sitemap();
  }

  pfizer_xmlsitemap_delete_index_links();
  pfizer_xmlsitemap_index_links();
  pfizer_xmlsitemap_mark_sitemaps_to_be_regenerated();
}

/**
 * DPT-686: Remove unpublished content from pfizer_sst sitemap.
 */
function pfizer_xmlsitemap_update_7103() {
  module_load_include('module', 'pfizer_xmlsitemap');

  // Reindex Pfizer SST Sitemap.
  pfizer_xmlsitemap_delete_index_links();
  pfizer_xmlsitemap_index_links();
  pfizer_xmlsitemap_mark_sitemaps_to_be_regenerated();
}
