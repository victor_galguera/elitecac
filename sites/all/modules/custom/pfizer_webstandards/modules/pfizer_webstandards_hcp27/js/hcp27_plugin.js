(function ($, w) {
  'use strict';
  Drupal.behaviors = Drupal.behaviors || {};
  Drupal.settings = Drupal.settings || {};
  Drupal.settings.pfizerHcp2Im = Drupal.settings.pfizerHcp2Im || {hcp_web_asset_type: ''};

  Drupal.behaviors.pfizerWebstandardsHcp27 = {
    attach: function (context, settings) {
      var page = $('body', context);
      page.once('pfizerWebstandardsHcp27', function () {
        if (typeof build_s === 'undefined') {
          return;
        }
        var s = build_s();
        $(document).on('ready', function() {
          if ($('#pfizer-hcp2-epermission-submit').length > 0) {
            s.events = s.apl(s.events, 'event8', ',', 1);
            s.eVar3 = 'D=pageName';
            if (typeof localStorage.janrainCaptureProfileData !== 'undefined') {
              s.eVar9 = 'ePermission (GRV Update)';
            }
            else {
              s.eVar9 = 'ePermission (GCP)';
            }
            s.linkTrackVars = s.apl(s.linkTrackVars, 'eVar3,eVar9', ',', 1);
            s.linkTrackEvents = s.apl(s.linkTrackEvents, 'event8', ',', 2);
            s.tl(true, 'o', 'pfizer_hcp2_epermissions');
            s.pfClearVars();
          }
        });

        // Addthis click tracking.
        if (typeof w.addthis !== 'undefined') {
          w.addthis.addEventListener('addthis.menu.share', function (e) {
            var p29 = Drupal.settings.pfizerHcp2Im.hcp_web_asset_type || '';
            s.events = s.apl(s.events, 'event45', ',', 1);
            s.eVar3 = 'D=pageName';
            s.eVar64 = e.data.service;
            s.prop29 = p29;
            s.eVar28 = 'D=c29';
            s.linkTrackVars = s.apl(s.linkTrackVars, 'prop29,eVar3,eVar64,eVar28', ',', 2);
            s.linkTrackEvents = s.apl(s.linkTrackEvents, 'event45', ',', 2);
            s.tl(this, 'o', 'pfizer_hcp2_addthis');
            s.pfClearVars();
          });
        }
      });
    }
  }
})(jQuery, window);

/**
 * HCP27 specific JS function.
 *
 * @param s
 *   Report Suite Object
 */
function pfizerWebstandardsHcp27(s) {
  // End of ePermission registration tracking.
  if (typeof jQuery.cookie === 'undefined') {
    // Return ... we can't determine anything.
    return;
  }
  var hcpUser = jQuery.cookie('hcp-ep-user');
  // Only gets triggered if the started flag has been found.
  if (hcpUser && hcpUser !== '') {
    // Delete the existing cookie.
    jQuery.cookie('hcp-ep-user', null, { 'path':'/'});
    s.eVar3 = 'D=pageName';
    s.eVar9 = 'ePermission (GRV Update)';
    if (typeof localStorage.janrainCaptureProfileData === 'undefined') {
      s.eVar45 = hcpUser;
      s.eVar46 = 'gcp';
      s.eVar9 = 'ePermission (GCP)';
    }
    s.events = s.apl(s.events, 'event9', ',', 1);
    s.linkTrackVars = s.apl(s.linkTrackVars, 'eVar3,eVar9', ',', 2);
    s.linkTrackEvents = s.apl(s.linkTrackEvents, 'event9', ',', 2);
  }
}
