; Drush make file for pfizer_deploy dependencies.

api = 2
core = "7.x"

projects[] = drupal

; Deploy Add-on includes deploy related dependencies.
projects[deploy_addon][subdir] = "contrib"
projects[deploy_addon][version] = "1.0"

; Include other non-deploy related modules.
projects[diff][subdir] = "contrib"
projects[diff][version] = "3.2"

projects[panelizer_deploy][subdir] = "contrib"
projects[panelizer_deploy][version] = "1.3"

projects[rules][subdir] = "contrib"
projects[rules][version] = "2.10"

; Only Used for WF Tools. Specific commit fixes a critical issue in Services
; Basic Auth module with error handling for invalid or incorrect authentication
; credentials.
projects[services_basic_auth][subdir] = "contrib"
projects[services_basic_auth][download][branch] = "7.x-1.x"
projects[services_basic_auth][download][revision] = "f809639"
; Patching improper string comparison causing potential SA.
projects[services_basic_auth][patch][i2565155][url] = "https://s3-us-west-2.amazonaws.com/drupal-platform/patches/services_basic_auth-2565155.patch"
projects[services_basic_auth][patch][i2565155][md5] = "22ec70a00a21d2634ead504b97b5129e"

projects[stage_file_proxy][subdir] = "contrib"
projects[stage_file_proxy][version] = "1.7"
