# Overview
This module extends functionality provided by the SiteCatalyst module to allow
for configurable properties and variables to be set on a per URL basis as well
as an Admin UI for configuring the pfConfig object.

There are various additional sub modules included that provide site specific
extensions to this functionality.

Note: as of 7.x-1.1.1; Pfizer Connect is no longer a prerequisite; there are
modules for integration with Pfizer Connect and GRV - both have their own
prerequisites. Enable whichever you need for your site.

# Changelog
## Drupal Module
The changelog is maintained in the `CHANGELOG.txt`. All enhancements to the module should be updated in the changelog.

## SiteCatalyst s_code.js
The changelog for the `s_code.js` is maintained in the `/resources/javascript/s_code_changelog.txt` file. All changes to the `s_code.js` should be updated there.

# Versioning

HCP Portals of version 2.4 and newer should use releases from the 7.x-2.x branch; this is the branch that will be used for all new development.

Earlier portal versions use 1.x releases off the (legacy) master branch.

# detailed_im

Detailed Interaction Management

## Setup

Copy `pfizer_webstandards` to your vendor channel site's `sites/all/modules/custom` directory.

For example, on the HCP, the `pfizer_webstandards` module should be at `/sites/all/modules/custom/pfizer_webstandards`.

Enable the *Pfizer Web Standards Detail* feature.

Configure settings at `site.com/admin/config/system/webstandards/detail`.

## Configuration

The Detailed IM module extends the original WS functionality in the following ways:

1. New "Detail" tab on the `admin/config/system/webstandards` page, leading to the configuration form at `admin/config/system/webstandards/detail`.

  The settings in the "Configurable Metrics" section allow configuration of reasonable defaults for the specified eVars and props; these values will be used for all pages, unless a more-specific value is configured for a URL using the URL-mapping configuration.

  The settings in the "Dynamic Metrics" allow toggling the enabled/disabled status of various eVars and props. If disabled, the values will not be sent to SiteCatalyst.

  The settings in "Fixed Metrics" show hard-coded values (currently empty).

2. Addition of `eVar45` and `prop50` configuration fields to the add/edit URL mapping forms.

3. Addition of Detailed metrics file download link to the Self Detail instructions at `admin/config/system/webstandards/instructions`, and the Detailed IM plugin registration code to the code block on the same page.

## How it works

The module implements `hook_page_alter()` to register the `pfizerWebstandardsDetail` WS plugin.

The same hook calls `pfizer_webstandards_detail_prep_fields_js()`, which creates a map of SiteCatalyst fields to their values (and other data), as applicable for the current URL (and possibly other criteria). This map is passed to Drupal's JavaScript object (`Drupal.settings.pfizer_webstandards_detail`) for use by the `pfizerWebstandardsDetail` plugin, which is also added in the same place.

The last piece of functionality in this hook is tracking of all & unique page hits in the session.

The `pfizerWebstandardsDetail` plugin checks for the presence of a `tpn` query parameter (setting its value to `eVar45`), then loops through all fields present in `Drupal.settings.pfizer_webstandards_detail` and sets their values in the s_code object (unless the field is set to not override existing values and a value already exists for that field).

When downloading the Detailed IM JS file for use in Gardens sites, the values of `Drupal.settings.pfizer_webstandards_detail` are auto-generated and prepended to the file, followed by the contents of the `detail_browser_storage.js` file (which implements the same page tracking functionality that's stored in the session, but in JS and using the browser's `sessionStorage`, if available). Lastly, the `detail_plugin.js` contents are appended for a complete Gardens JS implementation.

## Pitcher Player eDetail Implementation

The Pitcher player uses its own copy of the s_code.js file. It is important to send Pitcher the updated s_code.js whenever changes are made to that file.


## s_code.js Deployment
Deployment of the `s_code.js` can be accomplished but using the `s3cmd` utility and the following shell script. The shell script should be placed in your path, and called from the `/resources/javascript` path.

For example: `/$ ~/copy_im_scode.sh dev` or `/$ ~/copy_im_scode.sh prod`

### Shell Script

```shell
#!/usr/bin/env bash

AWS_S3_BUCKET=s3://pfe_im/js
DEV_PATH=dev
PROD_PATH=prod
TMP_PATH=/tmp
OUTPUT_FILE=s_code.js
env=$1


gzip -c -9 s_code.js > $TMP_PATH/$OUTPUT_FILE

if [ "$env" == "prod" ]; then
  s3cmd put -P -m text/javascript --add-header 'Content-Encoding:gzip' $TMP_PATH/$OUTPUT_FILE $AWS_S3_BUCKET/$PROD_PATH/$OUTPUT_FILE
elif [ "$env" == "dev" ]; then
  s3cmd put -P -m text/javascript --add-header 'Content-Encoding:gzip' $TMP_PATH/$OUTPUT_FILE $AWS_S3_BUCKET/$DEV_PATH/$OUTPUT_FILE
fi
```
### s3cmd
[s3cmd](http://s3tools.org/s3cmd) is a is a command line tool for uploading, retrieving and managing data in Amazon S3. It is best suited for power users who don't fear command line. It is also ideal for scripts, automated backups triggered from cron, etc.

#### Installation
s3cmd is available in most Linux package repositories.

For Windows: http://pixpuffindev.blogspot.ca/2012/07/setting-up-s3cmd-for-windows-for.html

For Max OSX: http://www.kodkast.com/blogs/aws/how-to-install-s3cmd-tool-on-mac-os or if using Homebrew, you can type `brew install s3cmd gnupg`.

Once installed you will need to type `s3cmd --configure` and follow the prompts to set your AWS key and secret, and encryption key phrase.




