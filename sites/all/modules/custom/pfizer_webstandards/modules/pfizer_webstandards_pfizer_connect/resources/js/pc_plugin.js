/**
 * @file
 * Pfizer Connect Specific JavaScript for Web Standards.
 */

var pfizerWebStandardsPCSettings = null;

(function($) {
  Drupal.behaviors.pfizerWebStandardsPC = {
    attach:  function (context, settings)
    {
      $('body', context).once(function() {
        "use strict";

        // The Drupal settings global var is not guaranteed set at the time
        // s_code runs - store a copy here for use on callback if needed.
        pfizerWebStandardsPCSettings =
          settings.pfizerWebStandards.pfizerConnect;

        // Register the HCP function.
        pfizerWebstandardsRegisterWebStandardPlugin(
          "pfizerWebstandardsPfizerConnect");
      });
    }
  };
})(jQuery);


/**
 * HCP specific JS function.
 *
 * @param s
 *   Report Suite Object
 */
function pfizerWebstandardsPfizerConnect(s) {

  "use strict";

  if (pfizerWebStandardsPCSettings) {
    //Automatically set the s variables for the user.

    s.eVar10 = pfizerWebStandardsPCSettings.userInfo.profession;
    s.eVar11 = pfizerWebStandardsPCSettings.userInfo.specialty;
    s.eVar21 = pfizerWebStandardsPCSettings.userIsAuthenticated ?
      'logged in' : 'anonymous';
    s.eVar25 = pfizerWebStandardsPCSettings.currentLanguage;

    alert (s.eVar10);
    alert (s.eVar11);
    alert (s.eVar21);
    alert (s.eVar25);

    s.prop21 = s.eVar21;
    s.prop26 = s.eVar25;

    alert (s.prop21);
    alert (s.prop26);
  }
}
