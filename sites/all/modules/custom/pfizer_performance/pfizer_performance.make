; Drush make file for pfizer_performance.

api = 2
core = "7.x"

projects[] = drupal

projects[acquia_purge][subdir] = "contrib"
projects[acquia_purge][version] = "1.3"

projects[cdn][subdir] = "contrib"
projects[cdn][version] = "2.8"

projects[cloudflare][subdir] = "contrib"
projects[cloudflare][version] = "1.0-beta4"

projects[entitycache][subdir] = "contrib"
projects[entitycache][version] = "1.5"

projects[expire][subdir] = "contrib"
projects[expire][version] = "2.0-rc4"

projects[fast_404][subdir] = "contrib"
projects[fast_404][version] = "1.5"

projects[memcache][subdir] = "contrib"
projects[memcache][version] = "1.5"

projects[views_litepager][subdir] = "contrib"
projects[views_litepager][version] = "3.0"
