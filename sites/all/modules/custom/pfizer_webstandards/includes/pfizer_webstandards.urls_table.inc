<?php
/**
 * @file
 * Include file to manage the Web Standards URL table.
 *
 * Note: see tabledrag_example for details of how this works. Comments have been
 * retained where possible to assist with understanding how it works.
 */

/**
 * This function provides the custom links table form.
 */
function pfizer_webstandards_urls_table_form($form, &$form_state) {

  // Identify that the elements in 'urls' are a collection, to
  // prevent Form API from flattening the array when submitted.
  $form['urls_table']['#tree'] = TRUE;

  // Get the custom links data.
  // Load the export include.
  module_load_include('inc', 'pfizer_webstandards', 'includes/pfizer_webstandards.data_access');
  $table_data = pfizer_webstandards_data_access_url_get_data();

  // Iterate through each row; each row is an $item.
  foreach ($table_data as $item) {

    // Create a form entry for this item.
    //
    // Each entry will be an array using the the unique id for that item as
    // the array key, and an array of table row data as the value.
    $url_list = preg_split('/\r\n|\r|\n/', check_plain($item->url));

    $display_url = '<ul>';
    foreach ($url_list as $url_item) {
      $display_url .= '<li>' . $url_item . '</li>';
    }
    $display_url .= '</ul>';

    // Work out the variables display.
    $variables = unserialize($item->variables);
    $display_variables = '';

    if ($variables != FALSE) {
      $display_variables = '<ul>';
      foreach ($variables as $var_id => $var_key) {
        $display_variables .= '<li>' . check_plain($var_id) . ' = ' . check_plain($var_key) . '</li>';
      }
      $display_variables .= '</ul>';
    }

    $delete_link_text = t('Delete');

    if ($item->export_type > 1) {
      $delete_link_text = t('Revert');
    }

    $form['urls_table'][$item->machine_name] = array(

      'machine_name' => array(
        '#markup' => check_plain($item->machine_name),
      ),

      'url' => array(
        '#markup' => $display_url,
      ),

      'variables' => array(
        '#markup' => $display_variables,
      ),

      'edit' => array(
        '#markup' => l(t('Edit'), 'admin/config/system/webstandards/urls/' . check_plain($item->machine_name) . '/edit') . ' ' .
        l($delete_link_text, 'admin/config/system/webstandards/urls/' . check_plain($item->machine_name) . '/delete'),
      ),

      // The 'weight' field will be manipulated as we move the items around in
      // the table using the tabledrag activity.  We use the 'weight' element
      // defined in Drupal's Form API.
      'weight' => array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#default_value' => $item->weight,
        '#delta' => 50,
        '#title_display' => 'invisible',
      ),
    );

    // Add a 'save' buttons:
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save Changes'));
  }

  return $form;
}

/**
 * Theme callback for the pfizer_webstandards_urls_table_form form.
 *
 * This uses the standard Drupal 'tabledrag' functionality.
 * Note: see tabledrag_example for details of how this works. Comments have been
 * retained where possible to assist with understanding how it works.
 *
 * @param array $variables
 *   Array of variables.
 *
 * @return string
 *   The rendered form.
 */
function theme_pfizer_webstandards_urls_table_form($variables) {
  $form = $variables['form'];

  // Initialize the variable which will store our table rows.
  $rows = array();

  // Iterate over each element in our $form['urls_table'] array.
  foreach (element_children($form['urls_table']) as $machine_name) {

    // Before we add our 'weight' column to the row, we need to give the
    // element a custom class so that it can be identified in the
    // drupal_add_tabledrag call.
    $form['urls_table'][$machine_name]['weight']['#attributes']['class'] = array('urls-item-weight');

    // We are now ready to add each element of our $form data to the $rows
    // array, so that they end up as individual table cells when rendered
    // in the final table.  We run each element through the drupal_render()
    // function to generate the final html markup for that element.
    $rows[] = array(
      'data' => array(
        drupal_render($form['urls_table'][$machine_name]['machine_name']),
        drupal_render($form['urls_table'][$machine_name]['url']),
        drupal_render($form['urls_table'][$machine_name]['variables']),
        drupal_render($form['urls_table'][$machine_name]['edit']),
        drupal_render($form['urls_table'][$machine_name]['weight']),
      ),
      // To support the tabledrag behaviour, we need to assign each row of the
      // table a class attribute of 'draggable'.
      'class' => array('draggable'),
    );
  }

  // We now define the table header values.  Ensure that the 'header' count
  // matches the final column count for your table.
  $header = array(
    t('Machine Name'),
    t('Url'),
    t('Variables'),
    t('Operations'),
    t('Weight'),
  );

  // We also need to pass the drupal_add_tabledrag() function an id which will
  // be used to identify the <table> element containing our tabledrag form.
  $table_id = 'urls-items-table';

  // We can render our tabledrag table for output.
  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
  ));

  // And then render any remaining form elements (such as our submit button).
  $output .= drupal_render_children($form);

  // We now call the drupal_add_tabledrag() function.
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'urls-item-weight');

  return $output;
}

/**
 * Submit callback for the pfizer_webstandards_urls_table_form form.
 *
 * Updates the 'weight' column for each element in our table
 */
function pfizer_webstandards_urls_table_form_submit($form, &$form_state) {
  // Use ctools crud to update the data.
  ctools_include('export');

  // Loop through the posted data and update the weights.
  foreach ($form_state['values']['urls_table'] as $machine_name => $posted_item) {
    $link = ctools_export_crud_load('pfizer_webstandards_url', $machine_name);
    $link->weight = $posted_item['weight'];
    ctools_export_crud_save('pfizer_webstandards_url', $link);
  }

  pfizer_webstandards_generate_js_build_urlconfig();
}
