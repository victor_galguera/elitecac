<?php
/**
 * @file
 * pfizer_administration.features.inc
 */

/**
 * Implements hook_views_api().
 */
function pfizer_administration_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
