<?php
/**
 * @file
 * Include file to manage the Web Standards custom links table.
 *
 * Note: see tabledrag_example for details of how this works. Comments have been
 * retained where possible to assist with understanding how it works.
 */

/**
 * This function provides the custom links table form.
 */
function pfizer_webstandards_customlinks_table_form($form, &$form_state) {

  // Identify that the elements in 'customlinks_table' are a collection, to
  // prevent Form API from flattening the array when submitted.
  $form['customlinks_table']['#tree'] = TRUE;

  // Get the custom links data.
  module_load_include('inc', 'pfizer_webstandards', 'includes/pfizer_webstandards.data_access');
  $table_data = pfizer_webstandards_data_access_customlinks_get_data();

  // Iterate through each row; each row is an $item.
  foreach ($table_data as $item) {

    $delete_link_text = t('Delete');

    if ($item->export_type > 1) {
      $delete_link_text = t('Revert');
    }

    // Create a form entry for this item.
    //
    // Each entry will be an array using the the unique id for that item as
    // the array key, and an array of table row data as the value.
    $form['customlinks_table'][$item->machine_name] = array(

      'href' => array(
        '#markup' => check_plain($item->href),
      ),

      'machine_name' => array(
        '#markup' => check_plain($item->machine_name),
      ),

      'link_type' => array(
        '#markup' => _pfizer_webstandards_customlinks_table_format_type_string($item->link_type),
      ),

      'edit' => array(
        '#markup' => l(t('Edit'), 'admin/config/system/webstandards/customlinks/' . check_plain($item->machine_name) . '/edit') . ' ' .
        l($delete_link_text, 'admin/config/system/webstandards/customlinks/' . check_plain($item->machine_name) . '/delete'),
      ),

      // The 'weight' field will be manipulated as we move the items around in
      // the table using the tabledrag activity.  We use the 'weight' element
      // defined in Drupal's Form API.
      'weight' => array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#default_value' => $item->weight,
        '#delta' => 50,
        '#title_display' => 'invisible',
      ),
    );

    // Add an 'add' and 'save' buttons:
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save Changes'));
  }

  return $form;
}

/**
 * Theme callback for the pfizer_webstandards_customlinks_table_form form.
 *
 * This uses the standard Drupal 'tabledrag' functionality.
 * Note: see tabledrag_example for details of how this works. Comments have been
 * retained where possible to assist with understanding how it works.
 *
 * @param array $variables
 *   Array of variables.
 *
 * @return string
 *   The rendered form.
 */
function theme_pfizer_webstandards_customlinks_table_form($variables) {
  $form = $variables['form'];

  // Initialize the variable which will store our table rows.
  $rows = array();

  // Iterate over each element in our $form['customlinks_table'] array.
  foreach (element_children($form['customlinks_table']) as $machine_name) {

    // Before we add our 'weight' column to the row, we need to give the
    // element a custom class so that it can be identified in the
    // drupal_add_tabledrag call.
    $form['customlinks_table'][$machine_name]['weight']['#attributes']['class'] = array('customlinks-item-weight');

    // We are now ready to add each element of our $form data to the $rows
    // array, so that they end up as individual table cells when rendered
    // in the final table.  We run each element through the drupal_render()
    // function to generate the final html markup for that element.
    $rows[] = array(
      'data' => array(
        drupal_render($form['customlinks_table'][$machine_name]['href']),
        drupal_render($form['customlinks_table'][$machine_name]['machine_name']),
        drupal_render($form['customlinks_table'][$machine_name]['link_type']),
        drupal_render($form['customlinks_table'][$machine_name]['edit']),
        drupal_render($form['customlinks_table'][$machine_name]['weight']),
      ),
      // To support the tabledrag behaviour, we need to assign each row of the
      // table a class attribute of 'draggable'.
      'class' => array('draggable'),
    );
  }

  // We now define the table header values.  Ensure that the 'header' count
  // matches the final column count for your table.
  $header = array(
    t('Href'),
    t('Machine Name'),
    t('Type'),
    t('Operations'),
    t('Weight'),
  );

  // We also need to pass the drupal_add_tabledrag() function an id which will
  // be used to identify the <table> element containing our tabledrag form.
  $table_id = 'customlinks-items-table';

  // We can render our tabledrag table for output.
  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
  ));

  // And then render any remaining form elements (such as our submit button).
  $output .= drupal_render_children($form);

  // We now call the drupal_add_tabledrag() function.
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'customlinks-item-weight');

  return $output;
}

/**
 * Formats a 'type' id for display.
 *
 * @param string $type_id
 *   The id to format (o, e, or d)
 *
 * @return string
 *   The same, formatted string.
 */
function _pfizer_webstandards_customlinks_table_format_type_string($type_id) {
  $formatted_string = '';

  switch ($type_id) {
    case 'o':
      $formatted_string = 'Other';
      break;

    case 'e':
      $formatted_string = 'Exit Link';
      break;

    case 'd':
      $formatted_string = 'Download';
      break;

  }

  return $formatted_string;
}

/**
 * Submit callback for the pfizer_webstandards_customlinks_table_form form.
 *
 * Updates the 'weight' column for each element in our table
 */
function pfizer_webstandards_customlinks_table_form_submit($form, &$form_state) {
  // Use ctools crud to update the data.
  ctools_include('export');

  // Loop through the posted data and update the weights.
  foreach ($form_state['values']['customlinks_table'] as $machine_name => $posted_item) {
    $link = ctools_export_crud_load('pfizer_webstandards_customlink', $machine_name);
    $link->weight = $posted_item['weight'];
    ctools_export_crud_save('pfizer_webstandards_customlink', $link);
  }

  // Re-export the config file.
  pfizer_webstandards_generate_js_build_pfconfig();
}
