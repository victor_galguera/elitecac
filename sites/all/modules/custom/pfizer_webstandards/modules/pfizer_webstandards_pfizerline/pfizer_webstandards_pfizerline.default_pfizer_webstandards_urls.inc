<?php
/**
 * @file
 * pfizer_webstandards_pfizerline.default_pfizer_webstandards_urls.inc
 */

/**
 * Implements hook_default_pfizer_webstandards_url().
 */
function pfizer_webstandards_pfizerline_default_pfizer_webstandards_url() {
  $export = array();

  $url = new stdClass();
  $url->disabled = FALSE; /* Edit this to true to make a default url disabled initially */
  $url->api_version = 1;
  $url->machine_name = '404_page';
  $url->weight = 0;
  $url->url = '*';
  $url->variables = 'a:1:{s:10:"javascript";s:172:"// Check if the page is not a 404 using the canonical link
if (jQuery(\'h1\').text() === \'404/403 - Sorry Something Went Wrong\') {
  s.pfError404(window.location.href);
} ";}';
  $export['404_page'] = $url;

  $url = new stdClass();
  $url->disabled = FALSE; /* Edit this to true to make a default url disabled initially */
  $url->api_version = 1;
  $url->machine_name = 'ask_pfizer_url';
  $url->weight = 0;
  $url->url = '*/ask-pfizer
*/ask-pfizer/*';
  $url->variables = 'a:1:{s:10:"javascript";s:36:"s.pfLead(\'ask-pfizer\', \'initiated\');";}';
  $export['ask_pfizer_url'] = $url;

  $url = new stdClass();
  $url->disabled = FALSE; /* Edit this to true to make a default url disabled initially */
  $url->api_version = 1;
  $url->machine_name = 'banner_adverts';
  $url->weight = 0;
  $url->url = '*';
  $url->variables = 'a:1:{s:10:"javascript";s:1204:"// Banner Adverts:
// There are two banner advert events to be configured.
// On Page Load - check to see if there is a banner advert; if so call
// pfBannerAdImpression. Defined as any a element with a href containing
// intcmp=
jQuery(\'a[href*="intcmp="]\').each(function () {
  // Cycle through all the matches (if there are any). As this is the page
  // load we can only log one \'intcmp\' value so stop after the first one.
  // Find the ? in the href.
  var href = jQuery(this).attr(\'href\'),
    questionPos = href.indexOf(\'?\'),
    urlSectionArray,
    loopCount,
    valueToReturn = null;
   if (questionPos >= 0) {
    href = href.substring(questionPos + 1);
     // split into an array
    urlSectionArray = href.split(\'&\');
     // Cycle through the url array
    for (loopCount = 0; loopCount < urlSectionArray.length; loopCount++) {
      // Check to see if this is the correct value.
      if (urlSectionArray[loopCount].substring(0, 7) === \'intcmp=\') {
        valueToReturn = urlSectionArray[loopCount].substring(7);
        s.pfBannerAdImpression(valueToReturn);
         // Break the each loop by returning false;
        return false;
      }
    }
  }
});
";}';
  $export['banner_adverts'] = $url;

  return $export;
}
