<?php

/**
 * @file
 * Plugin definition for the Pfizer Private Key key provider.
 * THIS KEY PROVIDER IS DEPRECATED AND SHOULD NEVER BE USED.
 */

$plugin = pfizer_encrypt_pfizer_private_key_encrypt_key_providers();

/**
 * Implements MODULE_FILENAME_encrypt_key_providers().
 */
function pfizer_encrypt_pfizer_private_key_encrypt_key_providers() {
  return array(
    'title' => t('Pfizer Legacy Private Key'),
    'description' => t('Use legacy Pfizer Encrypt key.'),
    'key callback' => 'pfizer_encrypt_legacy_private_key',
    'settings form' => NULL,
    'static key' => TRUE,
    'deprecated' => TRUE,
  );
}

/**
 * Key callback for Pfizer.
 */
function pfizer_encrypt_legacy_private_key() {
  // Use localhost if there's no subscription information available.
  $key = isset($_ENV['AH_SITE_GROUP']) ? $_ENV['AH_SITE_GROUP'] : 'localhost';

  // Key cannot be too long for this encryption.
  $key = drupal_substr($key, 0, 32);

  // Temporary PHP 5.6 fix to automatically pad key to next valid size of
  // 16, 24 or 32.
  if (strlen($key) <= 16) {
    $key_length = 16;
  }
  elseif (strlen($key) <= 24) {
    $key_length = 24;
  }
  else {
    $key_length = 32;
  }

  // Pad key with null bytes emulating PHP 5.5 and previous versions.
  $key = str_pad($key, $key_length, "\0");

  return $key;
}
