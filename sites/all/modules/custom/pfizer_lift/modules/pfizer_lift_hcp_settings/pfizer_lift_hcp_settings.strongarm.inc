<?php
/**
 * @file
 * pfizer_lift_hcp_settings.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function pfizer_lift_hcp_settings_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'acquia_lift_profiles_tracked_actions';
  $strongarm->value = array(
    0 => 'user_login',
    1 => 'user_register',
    2 => 'scroll_to_bottom',
    3 => 'marketo_form_submission',
    4 => 'user_register_start',
  );
  $export['acquia_lift_profiles_tracked_actions'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'acquia_lift_profiles_vocabulary_mappings';
  $strongarm->value = array(
    'content_section' => (int) pfizer_lift_get_vocabulary_id('hcp_specialty'),
    'content_keywords' => (int) pfizer_lift_get_vocabulary_id('hcp_med_cond'),
    'persona' => (int) pfizer_lift_get_vocabulary_id('hcp_profession'),
  );
  $export['acquia_lift_profiles_vocabulary_mappings'] = $strongarm;

  // the variable name changed in the newer versions of acquia_lift_profiles;
  // @TODO: clean up when we upgrade to the new Lift version
  $export['acquia_lift_profiles_field_mappings'] = $strongarm;

  return $export;
}
