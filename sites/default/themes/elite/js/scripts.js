jQuery( document ).ready(function() {
    jQuery( ".group-left" ).prepend( jQuery( "#edit-picture") );
    /*jQuery( ".countdown" ).append( jQuery( ".time") );*/
    jQuery( ".countdown" ).appendTo( ".time" );

    jQuery('<a href="/user/register" class="btn btn-default">Registrarse</a>').appendTo(".page-user-login #edit-actions");

    jQuery( ".form-item-pass-pass1" ).removeClass( "col-sm-6 col-md-4" );
    jQuery( ".form-item-pass-pass2" ).removeClass( "col-sm-6 col-md-4" );

    jQuery(".form-item-files-picture-upload .help-block").insertAfter(jQuery(".form-item-files-picture-upload label"));
});

jQuery( window ).on( "load", function() {
    jQuery('html, body').animate({
        scrollTop: $('.not-front .main-container').offset().top
    }, 'slow');
});
