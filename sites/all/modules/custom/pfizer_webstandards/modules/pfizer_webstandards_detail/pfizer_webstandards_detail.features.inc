<?php
/**
 * @file
 * pfizer_webstandards_detail.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pfizer_webstandards_detail_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "pfizer_webstandards_detail" && $api == "default_detail_fields") {
    return array("version" => "1");
  }
}
