<?php
namespace entity_context\handler;

/**
 * Handler for Text Formatted field type.
 */
class TextFormatted extends \entity_context\core\FieldHandler {
  /**
   * Returns value for field.
   * @return [type] [description]
   */
  public function getValue() {
    $wrapper = $this->getWrapper();
    $context = $this->getContext();

    // account for different value data structure
    $value_src = $wrapper->{$context['field']};
    if (isset($value_src->value)) {
      $value_src = $value_src->value;
    } 

    // get actual value and clean it up
    $value = $value_src->value(array('sanitize' => TRUE));
    $value = trim(check_plain(strip_tags($value)));

    return $value;
  }
}