Pfizer Analytics 7.x-1.0.1, 2016-08-12
-------------------------------------
- exposed query-string tracking codes (cpm, cbn, intcmp)
- exposed sitecatalyst report suite id
- passed through tracking codes and report suite id to Mediaplatform's PFA implementation

Pfizer Analytics 7.x-1.0, 2016-08-11
-------------------------------------
- initial module release
- pfizer_analytics_mediaplatform provides functionality relating to Actinium 2.8