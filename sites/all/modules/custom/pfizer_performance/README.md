Pfizer Performance
==================

Includes the following contrib modules:

- Acquia Purge
- CDN
- Cache Expiration
- Cloudflare
- Entity Cache
- Fast 404
- Memcache
- Views Lite Pager

##Acquia Purge

Add the following settings to your local.settings.php file:
```
// Set to TRUE if you can access the site with http protocol.
$conf['acquia_purge_http'] = FALSE;
// Set to TRUE if you can access the site with https protocol.
// Only used if you have "Varnish through SSL" enabled on Acquia.
$conf['acquia_purge_https'] = TRUE;
```

##Cache Expiration

To use Acquia Purge, you need to use this module.

##CDN

CDN provides Content Delivery Network integration.

##Cloudflare

This module corrects $_SERVER["REMOTE_ADDR"] so it contains the IP address of your visitor, not CloudFlare's reverse proxy server. It integrates with CloudFlare's Threat API so you can ban and whitelist IP addresses from the Drupal Comment administration screen. It also integrates with CloudFlare's Spam API.

##Fast 404

New sites automatically have the following settings added:

```
// Include Fast 404 module.
include_once('./sites/all/modules/contrib/fast_404/fast_404.inc');

# Disallowed extensions. Any extension in here will not be served by Drupal and
# will get a fast 404.
$conf['fast_404_exts'] = '/^(?!robots).*\.(txt|png|gif|jpe?g|css|js|ico|swf|flv|cgi|bat|pl|dll|exe|asp)$/i';

# Array of whitelisted URL fragment strings that conflict with fast_404.
$conf['fast_404_string_whitelisting'] = array('cdn/farfuture', '/advagg_');

# Default fast 404 error message.
$conf['fast_404_html'] = '<html xmlns="http://www.w3.org/1999/xhtml"><head><title>File Not Found</title></head><body><h1>Not Found</h1><p>The requested URL "@path" was not found on this server.</p></body></html>';

# Call the extension checking now. This will skip any logging of 404s.
fast_404_ext_check();
```

##Entity Cache

Only use this module in conjunction with the Memcache module as there are minimal, if any, performance gains when using database caching.

If your site uses Bean, then you should also enable the Bean Entitycache module.

##Memcache

Add the following to the local.settings.ini file:

```
$conf['cache_backends'][] = 'sites/all/modules/performance/memcache/memcache.inc';
$conf['cache_default_class'] = 'MemCacheDrupal';
$conf['cache_class_cache_form'] = 'DrupalDatabaseCache';
$conf['lock_inc'] = 'sites/all/modules/performance/memcache/memcache-lock.inc';
// @todo Some debate as to whether we should use stampede protection or not.
// For now it's disabled.
// $conf['memcache_stampede_protection'] = TRUE;
```

##Views Lite Pager

Avoid expensive count queries by using this module. Enable the module and in your views, select "Lite Pager" for a must faster, although simplified, pager.

#Custom modules

Includes the following custom modules:

- Pfizer Performance Purge

##Pfizer Performance Purge

This module allows you to purge the varnish cache on the production environment from stage. It is a temporary module until we come up with a longer term solution for implementing this.

To use update your local.settings.php file:

```
if (isset($_ENV['AH_SITE_ENVIRONMENT'])) {
  switch ($_ENV['AH_SITE_ENVIRONMENT']) {
    case 'test':

      // Only enable this on stage.
      $conf['pfizer_performance_purge_deploy'] = 1;

      break;
  }
}
```

Then set some variables on stage manually:

```
// Replace www.domain.com (and up to 4 more domains in the array) with your domain.
// The domain(s) specified must match the environments you are trying to purge cache
// on otherwise you will get an Acquia Cloud API error.
drush @sub.env php-eval "variable_set('pfizer_performance_purge_domains', array('www.domain.com'));"

// Add an Acquia API username and key. These must be encrypted.
drush @sub.env php-eval "variable_set('pfizer_performance_purge_username', encrypt('api-cloud-username-usually-email'));"
drush @sub.env php-eval "variable_set('pfizer_performance_purge_key', encrypt('api-cloud-key'));"

// Override subscription variable otherwise uses current Acquia subscription.
drush @sub.env vset pfizer_performance_purge_sub subscription

// Purge cache on environment other than prod.
drush @sub.env vset pfizer_performance_purge_env environment

// Debug is turned on by default which adds a log entry to watchdog.
drush @sub.env vset pfizer_performance_purge_debug 0
```
