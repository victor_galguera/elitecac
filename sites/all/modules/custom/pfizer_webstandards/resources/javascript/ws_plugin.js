/**
 * @file
 * WS Specific JavaScript for Web Standards.
 */

/**
 * Core WS Plugin.
 * 
 * @param  {[type]} s [description]
 * @return {[type]}   [description]
 */
function pfizerWS(s) {
  "use strict";

  // metrics set by module
  try {
    for (var i = 0; i < Drupal.settings.pfizer_ws.fields.length; i++) {
      var field = Drupal.settings.pfizer_ws.fields[i];

      s[field.name] = field.value;
    }
  }
  catch(e) {
    console.log('WS Error: ' + e.message);
  }
}


/**
 * Banner Adverts:
 * There are two banner advert events to be configured.
 * On Page Load - check to see if there is a banner advert; if so call
 * pfBannerAdImpression. Defined as any
 * element with a href containing intcmp=
 * @param  {[type]} s [description]
 * @return {[type]}   [description]
 */
function pfizerWsCheckBanner(s) {
  jQuery('a[href*="intcmp="]').each(function () {
    // Cycle through all the matches (if there are any). As this is the page
    // load we can only log one 'intcmp' value so stop after the first one.
    // Find the ? in the href.
    var href = jQuery(this).attr('href'),
      questionPos = href.indexOf('?'),
      urlSectionArray,
      loopCount,
      valueToReturn = null;

    if (questionPos >= 0) {
      href = href.substring(questionPos + 1);

      // split into an array
      urlSectionArray = href.split('&');

      // Cycle through the url array
      for (loopCount = 0; loopCount < urlSectionArray.length; loopCount++) {
        // Check to see if this is the correct value.
        if (urlSectionArray[loopCount].substring(0, 7) === 'intcmp=') {
          valueToReturn = urlSectionArray[loopCount].substring(7);
          s.pfBannerAdImpression(valueToReturn);

          // Break the each loop by returning false;
          return false;
        }
      }
    }
  });
}