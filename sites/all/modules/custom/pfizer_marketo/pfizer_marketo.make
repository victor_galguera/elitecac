; Drush make file for pfizer_marketo dependencies.

api = 2
core = "7.x"

projects[] = drupal

projects[marketo_ma][subdir] = "contrib"
projects[marketo_ma][version] = "1.5"
projects[marketo_ma][patch][i2462629][url] = "https://www.drupal.org/files/issues/2462629-munchkin-script-1.patch"
projects[marketo_ma][patch][i2462629][md5] = "787066b4028cee22f0529e5be3e7f047"
