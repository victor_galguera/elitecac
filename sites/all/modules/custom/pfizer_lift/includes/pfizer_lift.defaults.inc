<?php 

/**
 * Returns default variable configuration for Acquia Lift Profiles.
 * 
 * @return Array [description]
 */
function pfizer_lift_get_lift_profiles_vars() {
  if (defined('ENTITY_CONTEXT_SEPARATOR')) {
    $separator = ENTITY_CONTEXT_SEPARATOR;
    $catchall = ENTITY_CONTEXT_CATCHALL;
  }
  else {
    $separator = '--';
    $catchall = 'entity_context_catchall';
  }

  // default options at /admin/config/content/personalize/acquia_lift_profiles
  $variables = array(
    'acquia_lift_profiles_account_name' => 'PFIZEREU',
    'acquia_lift_profiles_api_url' => 'api-liftweb-eu1.lift.acquia.com',
    'acquia_lift_profiles_access_key' => 'yOAr7tWNSVrc8aXtRS4S',
    'acquia_lift_profiles_secret_key' => 'JzU0ht4ubYX1Wsb9xb034ykbgV98ViwyjaPzK89b',
    'acquia_lift_profiles_js_path' => 'cdn.lift.acquia.com/PFIZEREU/tc.min.js',
    'pfizer_lift_profiles' => array(
      'dev' => array(
        'account_name' => 'PFIZERSANDBOX',
        'api_url' => 'api-liftweb-eu1.lift.acquia.com',
        'access_key' => 's60We1uJo5Vs3XsuNQ7L',
        'secret_key' => 'Ek8iMnHjqpectvE1ebu1ENKvsgyQKhIVHqP99qNm',
        'js_path' => 'cdn.lift.acquia.com/PFIZERSANDBOX/tc.min.js',
      ),
      'stage' => array(
        'account_name' => 'PFIZERSANDBOX2',
        'api_url' => 'api-liftweb-eu1.lift.acquia.com',
        'access_key' => 'UbekCwyBkSXnYUmodz4V',
        'secret_key' => '7eK1ndq0SLglJt3pMl64oT2nwh0ep5kRMkf6C8YX',
        'js_path' => 'cdn.lift.acquia.com/PFIZERSANDBOX2/tc.js',
      ),
      'prod' => array(
        'account_name' => 'PFIZEREU',
        'api_url' => 'api-liftweb-eu1.lift.acquia.com',
        'access_key' => 'yOAr7tWNSVrc8aXtRS4S',
        'secret_key' => 'JzU0ht4ubYX1Wsb9xb034ykbgV98ViwyjaPzK89b',
        'js_path' => 'cdn.lift.acquia.com/PFIZEREU/tc.min.js',
      ),
    ),
    'acquia_lift_profiles_capture_identity' => TRUE,
    'acquia_lift_profiles_identity_param' => 'tpn',
    'acquia_lift_profiles_identity_type_param' => 'identitytype',
    'acquia_lift_profiles_tracked_actions' => array(
      'scroll_to_bottom',
      'marketo_form_submission',
    ),
    'acquia_lift_profiles_field_mappings' => array(
      'content_section' => 'taxonomy_context__hcp_specialty',
      'content_keywords' => 'taxonomy_context__hcp_med_cond',
      'persona' => 'taxonomy_context__hcp_profession',
    ),
    'acquia_lift_profiles_udf_mappings' => array(
      '#disable' => FALSE,
      'person' => array(
        'person_udf4' => 'pfizer_lift__customer_id',
        'person_udf5' => 'pfizer_lift__guid',
        'person_udf6' => 'pfizer_lift__auth_type',
        'person_udf7' => 'pfizer_lift__profession',
        'person_udf8' => 'pfizer_lift__specialty',
        'person_udf9' => 'pfizer_lift__epermission',
      ),
      'touch' => array(
        'touch_udf1' => 'pfizer_lift__platform',
        'touch_udf2' => 'pfizer_lift__logged_in',
        'touch_udf3' => 'querystring_context__mkto_segment',
        'touch_udf4' => 'pfizer_lift__campaign_country',
      ),
      'event' => array(
        'event_udf4'  => 'pfizer_lift__brand_name',
        'event_udf5'  => 'pfizer_lift__brand_id',
        'event_udf6'  => 'pfizer_lift__medical_conditions',
        'event_udf7'  => 'pfizer_lift__featured',
        'event_udf8'  => 'pfizer_lift__build_number',
        'event_udf9'  => 'entity_context__node' . $separator . $catchall . $separator . 'field_indication_single' . $separator . 'indication_name',
        'event_udf11'  => 'entity_context__node' . $separator . $catchall . $separator . 'hcp_web_asset_type',
        'event_udf12'  => 'entity_context__node' . $separator . $catchall . $separator . 'hcp_primary_message',
        'event_udf13'  => 'entity_context__node' . $separator . $catchall . $separator . 'hcp_primary_message_category',
        'event_udf16' => 'pfizer_lift__content_source',
        'event_udf35' => 'pfizer_lift__language',
        'event_udf37' => 'entity_context__node' . $separator . $catchall . $separator . 'hcp_content_origin',
      ),
    ),
    'entity_context_default_udf_values' => array(

    ),
    'pfizer_lift_default_udf_values' => array(
      'event' => array(
        'event_udf4' => 'UNBRANDED',
        'event_udf7' => 'no',
        'event_udf16' => 'Pfizer',
      ),
    ),
  );

  // backwards copmatibility for older versions of acquia_lift, which used a 
  // different variable name
  $variables['acquia_lift_profiles_vocabulary_mappings'] = $variables['acquia_lift_profiles_field_mappings'];

  // dynamically update acquia profiles config based on environment
  $env = pfizer_lift_detect_env();
  array_walk($variables['pfizer_lift_profiles'][$env], function ($value, $key) use (&$variables) {
    $var_name = 'acquia_lift_profiles_' . $key;

    if (isset($variables[$var_name])) {
      $variables[$var_name] = $value;
    }
  });

  return $variables;
}

/**
 * Returns default variable configuration for Browser Contexts.
 * 
 * @return Array [description]
 */
function pfizer_lift_get_browser_contexts_vars() {
  module_load_include('module', 'personalize_url_context', 'personalize_url_context');

  $query_string_contexts = _personalize_url_context_get_utm_parameter_names();
  $query_string_contexts[] = 'mkto_segment';
  $query_string_contexts[] = 'tpn';
  $query_string_contexts[] = 'utm_name';

  $cookie_contexts = array();

  // default options at /admin/config/content/personalize/browser_context
  $variables = array(
    'personalize_url_querystring_contexts' => $query_string_contexts,
    'personalize_cookie_contexts' => $cookie_contexts,
  );

  return $variables;
}

/**
 * Returns default variable configuration for Entity Contexts.
 * 
 * @return [type] [description]
 */
function pfizer_lift_get_entity_contexts_vars() {
  // default options at /admin/config/content/personalize/entity_context
  $variables = array(
    'entity_context_entity_types' => array('node' => 'node'),
  );

  return $variables;
}

/**
 * Detects the current environment based on the presense of the AH_PRODUCTION environment variable.
 * @return [type] [description]
 */
function pfizer_lift_detect_env() {
  $env = 'dev';

  if (isset($_ENV['AH_PRODUCTION']) && $_ENV['AH_PRODUCTION']) {
    $env = 'prod';
  }
  else if (preg_match('#.+stg\.prod\.acquia-sites\.com#', $_SERVER['HTTP_HOST'], $matches)) {
    $env = 'stage';
  }

  return $env;
}