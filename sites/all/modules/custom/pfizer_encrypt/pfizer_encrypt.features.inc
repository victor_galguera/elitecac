<?php
/**
 * @file
 * pfizer_encrypt.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pfizer_encrypt_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_encrypt_default_configs().
 */
function pfizer_encrypt_encrypt_default_configs() {
  $configs = array();

  // Exported configuration: default.
  $configs['default'] = array(
    'name' => 'default',
    'label' => 'Default',
    'description' => 'The default configuration.',
    'method' => 'authenticated_aes',
    'method_settings' => '',
    'provider' => 'pfizer_private_key_file',
    'provider_settings' => '',
    'enabled' => 1,
    'created' => 1439940782,
    'changed' => 1439941740,
  );

  return $configs;
}
