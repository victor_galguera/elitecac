<?php
/**
 * @file
 * pfizer_webstandards_webinars.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pfizer_webstandards_webinars_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "pfizer_webstandards" && $api == "default_pfizer_webstandards_urls") {
    return array("version" => "1");
  }
}
