<?php
/**
 * @file
 * Include file to manage the Web Standards urls configuration.
 */

/**
 * Function to add a variable entry to the render array for the add/edit form.
 *
 * @param array $form
 *   The form render array.
 * @param string $section
 *   The ID of the section (e.g. 'evars');
 * @param string $name
 *   The display name of the variable - will be used in lCase for the ID,
 *   Do not t() this, it will be translated internally.
 * @param string $description
 *   The description of the variable - Do not t() this, it will be translated
 *   internally.
 * @param array $variables
 *   Array containing the variable values deserialized from the blob.
 * @param int $weight
 *   The weight for the display array item.
 */
function _pfizer_webstandards_urls_add_edit_form_create_variable(&$form, $section, $name, $description, &$variables, $weight) {
  $form[$section]['blobstore_' . $name] = array(
    '#required' => '0',
    '#description' => t($description),
    '#weight' => $weight,
    '#type' => 'textfield',
    '#title' => t($name),
    '#default_value' => (isset($variables) && isset($variables[$name])) ? $variables[$name] : '',
  );
}

/**
 * This function provides the custom links admin form.
 */
function pfizer_webstandards_urls_add_edit_form($form, &$form_state) {

  $url = NULL;

  if (count($form_state['build_info']['args']) === 1) {
    ctools_include('export');
    // We are editing the form; act as appropriate.
    $url = ctools_export_crud_load('pfizer_webstandards_url', $form_state['build_info']['args'][0]);
  }

  if (isset($url)) {
    // Machine name is already set:
    $form['machine_name'] = array(
      '#markup' => 'Machine Name: ' . check_plain($url->machine_name),
    );
  }
  else {
    // Adding a new item, display the Machine Name box.
    $form['machine_name'] = array(
      '#required' => '1',
      '#description' => t('A unique ID - used for exporting this data; use lowercase letters (a-z), numbers (0-9) and underscores (_) only.'),
      '#weight' => '0',
      '#type' => 'textfield',
      '#title' => t('Machine Name'),
    );
  }

  $form['urls'] = array(
    '#required' => '1',
    '#description' => t('Enter a list of URLs to match against, separate each URL with a return. You can use the * as a wildcard. Use the format "/level1/level2/level3" - do not include the server name or protocol.'),
    '#weight' => '0',
    '#default_value' => isset($url) ? $url->url : '',
    '#type' => 'textarea',
    '#title' => t('URLs'),
  );
  $form['weight_section'] = array(
    '#weight' => '1',
    '#type' => 'fieldset',
    '#collapsible' => '0',
    '#collapsed' => '0',
    '#title' => t('Weight'),
    '#description' => t('The heavier items will sink and the lighter items will be positioned nearer the top.'),
  );
  $form['weight_section']['weight'] = array(
    '#required' => '1',
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => isset($url) ? $url->weight : 0,
    '#weight' => '1',
    '#delta' => 50,
  );
  $form['variables'] = array(
    '#weight' => 2,
    '#markup' => t('The following sections list all eVars and sProps defined in the Pfizer Web Standards implementation. These will be set on the pages which match the above URLs.'),
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );

  $variables = NULL;
  if (isset($url)) {
    $variables = unserialize($url->variables);
  }

  // Note: Any form item that is posted and has an ID that begins with
  // 'blobstore_' will automatically be added to the blob.
  // eVars.
  $form['evars'] = array(
    '#weight' => '3',
    '#description' => t('Default values for eVars for pages that match the URL. Note: these may be overwritten by the s_code file.'),
    '#type' => 'fieldset',
    '#collapsible' => '1',
    '#collapsed' => '0',
    '#title' => t('eVars'),
  );
  $form['sprops'] = array(
    '#weight' => '4',
    '#description' => t('Default values for sProps for pages that match the URL. Note: these may be overwritten by the s_code file.'),
    '#type' => 'fieldset',
    '#collapsible' => '1',
    '#collapsed' => '0',
    '#title' => t('sProps'),
  );
  $form['standard'] = array(
    '#weight' => '5',
    '#description' => t('Default values for Standard Variables for pages that match the URL. Note: these may be overwritten by the s_code file.'),
    '#type' => 'fieldset',
    '#collapsible' => '1',
    '#collapsed' => '0',
    '#title' => t('Standard Variables'),
  );
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar1', 'Lead Type', $variables, 0);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar2', 'Support Request Type', $variables, 1);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar3', 'Page Name (custom)', $variables, 2);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar4', 'Internal Campaigns', $variables, 3);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar5', 'Internal Search Terms', $variables, 4);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar7', 'Download File Name', $variables, 6);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar8', 'Survey Name', $variables, 7);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar9', 'Registration Type', $variables, 8);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar10', 'Visitor Profession', $variables, 9);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar11', 'Visitor Specialty', $variables, 10);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar14', 'Product Name', $variables, 13);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar15', 'Time Parting', $variables, 14);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar18', 'New/Repeat Visitor', $variables, 17);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar19', 'Video Title', $variables, 18);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar20', 'Internal Search Type', $variables, 19);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar21', 'Login Status', $variables, 20);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar22', 'Survey Response', $variables, 21);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar23', 'Site Tool Name', $variables, 22);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar26', 'Sample Name', $variables, 25);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar27', 'Brand Name', $variables, 26);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar28', 'Content/Interaction Type', $variables, 27);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar29', 'Webinar Name', $variables, 28);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar30', 'E-Detail Name', $variables, 29);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar35', 'Survey Offer Date', $variables, 34);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars', 'eVar36', 'Survey Complete Date', $variables, 35);

  // NOTE - the following have been removed from this version of the Web
  // Standards - they are retained in-case they are needed in subsequent builds.
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars',
  // 'eVar6', 'Partner/Site Name', $variables, 5);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars',
  // 'eVar12', 'Content Type', $variables, 11);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars',
  // 'eVar13', 'Content ID', $variables, 12);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars',
  // 'eVar16', 'Day of Week (not needed with new evar15)', $variables, 15);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars',
  // 'eVar17', 'Weekend/Weekday (not needed with new evar15)', $variables, 16);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars',
  // 'eVar24', 'Visitor Type', $variables, 23);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars',
  // 'eVar25', 'Country Name', $variables, 24);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars',
  // 'eVar31', 'Video Category', $variables, 30);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars',
  // 'eVar32', 'ONSITE Campaign Name / ID', $variables, 31);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars',
  // 'eVar33', 'Mobile Device Type (not used yet)', $variables, 32);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars',
  // 'eVar34', 'IP Addresses', $variables, 33);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars',
  // 'eVar37', 'Mobile App Name', $variables, 36);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'evars',
  // 'eVar38', 'Internal Banner Ad Page Name', $variables, 37)
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop1', 'Site Section', $variables, 0);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop2', 'Sub Section', $variables, 1);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop3', 'Sub Section Level 2', $variables, 2);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop4', 'Sub Section Level 3', $variables, 3);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop5', 'Internal Search Terms', $variables, 4);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop6', 'Null Search Terms', $variables, 5);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop7', 'Download File Name', $variables, 6);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop8', 'Download Page Name', $variables, 7);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop9', 'Support Tools', $variables, 8);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop11', 'Internal Search Origination Pages', $variables, 10);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop14', 'Product Name', $variables, 13);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop15', 'Time Parting', $variables, 14);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop16', 'Preview Page', $variables, 15);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop17', '404 URL', $variables, 16);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop18', 'New/Repeat Visitor', $variables, 17);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop19', 'Video Title', $variables, 18);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop20', 'Internal Search Type', $variables, 19);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop21', 'Login Status', $variables, 20);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop23', 'Document URL', $variables, 22);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop27', 'Sample Name', $variables, 26);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop28', 'Brand Name', $variables, 27);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop29', 'Content/Interaction Type', $variables, 28);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop30', 'Webinar Name', $variables, 29);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop31', 'E-Detail Name', $variables, 30);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop36', 'Time/Date stamp', $variables, 35);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop37', 'Survey Offer Date', $variables, 36);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop38', 'Survey Complete Date', $variables, 37);

  // NOTE - the following have been removed from this version of the Web
  // Standards - they are retained in-case they are needed in subsequent builds.
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops',
  // 'prop10', 'Form Analysis', $variables, 9);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops',
  // 'prop12', 'Content Type', $variables, 11);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops',
  // 'prop13', 'Content ID', $variables, 12);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops',
  // 'prop22', 'Login Pathing', $variables, 21);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops',
  // 'prop24', 'Critical Paths', $variables, 23);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops',
  // 'prop25', 'Visitor Type', $variables, 24);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops',
  // 'prop26', 'Country Name', $variables, 25);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops',
  // 'prop32', 'Video Category', $variables, 31);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops',
  // 'prop33', 'ONSITE Campaign Name / ID', $variables, 32);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops',
  // 'prop34', 'Mobile Device Type', $variables, 33);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops',
  // 'prop35', 'IP Addresses', $variables, 34);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops',
  // 'prop39', 'Mobile App Name', $variables, 38);
  // _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops',
  // 'prop40', 'Internal Banner Ad Page Name', $variables, 39);
  // New Interaction Management Options.
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop41', 'Interaction Management: HEADER_VENDOR_ID', $variables, 40);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop42', 'Interaction Management: COMMON_TRACKING_CODE', $variables, 41);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'sprops', 'prop43', 'Interaction Management: COMMON_INTERACTION_STATUS', $variables, 42);

  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'standard', 'pageName', 'Pages', $variables, 0);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'standard', 'channel', 'Site Sections', $variables, 1);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'standard', 'server', 'Server', $variables, 2);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'standard', 'zip', 'Zip Codes', $variables, 3);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'standard', 'hier1', 'Site Directory Hierarchy (hier 1)', $variables, 4);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'standard', 'hier3', 'Site Directory Hierarchy (hier 3)', $variables, 5);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'standard', 'pageType', 'Page Type', $variables, 6);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'standard', 'products', 'Products', $variables, 7);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'standard', 'state', 'States', $variables, 8);
  _pfizer_webstandards_urls_add_edit_form_create_variable($form, 'standard', 'campaign', 'Campaigns', $variables, 9);

  $form['custom_script'] = array(
    '#weight' => '6',
    '#description' => t('You can enter custom JavaScript in the box below; it will be executed before s.t();.'),
    '#type' => 'fieldset',
    '#collapsible' => '1',
    '#collapsed' => '0',
    '#title' => t('Custom JavaScript Code'),
  );
  // Note: Store the JS in the blob, along with the variables - this is for
  // extensibility purposes.
  $form['custom_script']['blobstore_javascript'] = array(
    '#required' => '0',
    '#weight' => '0',
    '#default_value' => isset($variables['javascript']) ? $variables['javascript'] : '',
    '#type' => 'textarea',
    '#title' => t('JavaScript'),
  );

  // Add a submit button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
    '#weight' => '100',
  );
  $form['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/config/system/webstandards/urls'),
    '#weight' => '101',
  );

  return $form;
}

/**
 * Returns an array of all 'blobstore' values from a passed in array.
 *
 * @param array $values
 *   An array of posted data.
 *
 * @return array
 *   array of values.
 */
function _pfizer_webstandards_urls_parse_posted_array($values) {
  $return_array = array();

  foreach ($values as $key => $value) {
    if (strpos($key, 'blobstore_') === 0 && strlen($value) > 0) {
      // We have a value to serialise.
      $return_array[substr($key, 10)]  = $value;
    }
  }

  return $return_array;
}

/**
 * Submit function for pfizer_webstandards_urls_add_edit_form().
 */
function pfizer_webstandards_urls_add_edit_form_submit($form, &$form_state) {
  ctools_include('export');

  $url = NULL;

  if (count($form_state['build_info']['args']) === 1) {
    // We are editing the form; act as appropriate.
    $url = ctools_export_crud_load('pfizer_webstandards_url', $form_state['build_info']['args'][0]);
  }
  else {
    // We are adding a new item.
    $url = ctools_export_crud_new('pfizer_webstandards_url');
    $url->machine_name = $form_state['values']['machine_name'];
  }

  $url->url = $form_state['values']['urls'];
  $url->weight = $form_state['values']['weight'];

  // Serialise all of the options to store in the blob.
  $url->variables = serialize(_pfizer_webstandards_urls_parse_posted_array($form_state['values']));

  // Save the data.
  ctools_export_crud_save('pfizer_webstandards_url', $url);
  drupal_set_message(t('Saved item: @item_name', array('@item_name' => $url->machine_name)));

  pfizer_webstandards_generate_js_build_urlconfig();

  drupal_goto('admin/config/system/webstandards/urls');
}

/**
 * Validate function for pfizer_urls_customlinks_add_edit_form().
 */
function pfizer_webstandards_urls_add_edit_form_validate($form, &$form_state) {

  if (count($form_state['build_info']['args']) === 0) {
    // Creating a new item, verify the machine name is supplied, valid and
    // unique.
    $name = $form_state['values']['machine_name'];

    if (!isset($name)) {
      form_error($form['machine_name'], t("Please supply a Machine Name."));
    }

    if (!preg_match('/^[a-z0-9_]*$/', $name)) {
      form_error($form['machine_name'], t("The Machine Name '!name' must only contain lowercase letters (a-z), numbers (0-9) and underscores (_).", array('!name' => check_plain($name))));
    }

    if (count(_pfizer_webstandards_urls_parse_posted_array($form_state['values'])) === 0) {
      form_error(t("Please enter at least one variable for this URL."));
    }

    // Check if the machine name is unique:
    ctools_include('export');

    $existing_link = ctools_export_crud_load('pfizer_webstandards_url', $name);
    if (isset($existing_link)) {
      form_error($form['machine_name'], t("The Machine Name '!name' is already in use, please select another.", array('!name' => check_plain($name))));
    }
  }
}

/**
 * This function provides the urls delete form.
 */
function pfizer_webstandards_urls_delete_form($form, &$form_state) {

  if (count($form_state['build_info']['args']) === 1) {
    ctools_include('export');
    // We are editing the form; act as appropriate.
    $url = ctools_export_crud_load('pfizer_webstandards_url', $form_state['build_info']['args'][0]);
  }

  // If there is no link selected, return to the list.
  if (!isset($url)) {
    drupal_goto('admin/config/system/webstandards/urls');
  }

  $form['message'] = array(
    '#markup' => $url->export_type > 1 ?
      t('Are you sure you want to revert "@name"?', array('@name' => check_plain($url->machine_name))) :
      t('Are you sure you want to delete "@name"?', array('@name' => check_plain($url->machine_name))),
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );

  // Add a submit and cancel button.
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => 'Delete',
    '#weight' => '100',
  );

  $form['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/config/system/webstandards/urls'),
    '#weight' => '101',
  );

  return $form;
}

/**
 * This function provides the custom links delete form submit handler.
 */
function pfizer_webstandards_urls_delete_form_submit($form, &$form_state) {
  // Use ctools crud to update the data so it can be exported.
  ctools_include('export');

  ctools_export_crud_delete('pfizer_webstandards_url', $form_state['build_info']['args'][0]);

  pfizer_webstandards_generate_js_build_urlconfig();

  drupal_goto('admin/config/system/webstandards/urls');
}
