<?php
/**
 * @file
 * Include file to manage the Web Standards troubleshooting options.
 */

/**
 * This function provides the troubleshooting admin form.
 */
function pfizer_webstandards_troubleshooting_form($form, &$form_state) {
  $form['pfizer_webstandards_ts_intro'] = array(
    '#weight' => 0,
    '#markup' => t('The following contains functions to help diagnose and fix deployment issues.'),
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );

  /*
  // Redeploy s_code file.
  $form['pfizer_webstandards_ts_redeploy'] = array(
    '#weight' => '1',
    '#type' => 'fieldset',
    '#title' => t('Redeploy s_code file'),
    '#collapsible' => '1',
    '#collapsed' => '0',
  );
  $form['pfizer_webstandards_ts_redeploy']['intro'] = array(
    '#weight' => 0,
    '#markup' => t('When the Web Standards module is installed it will deploy a
      copy of the ‘s_code.js’ file to the public files folder. If you are
      deploying the site using a database backup it is possible that this file
      will not exist on your server. Click this button to recreate the
      ‘s_code.js’ file and automatically configure the SiteCatalyst module to
      use this file:'),
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );
  $form['pfizer_webstandards_ts_redeploy']['redeploy'] = array(
    '#weight' => 1,
    '#type' => 'submit',
    '#value' => t('Redeploy s_code file'),
    '#submit' => array('pfizer_webstandards_troubleshooting_form_ts_redeploy_submit'),
  );
  */

  // Rebuild js files.
  $form['pfizer_webstandards_js_rebuild'] = array(
    '#weight' => '2',
    '#type' => 'fieldset',
    '#title' => t('Rebuild the JS files'),
    '#collapsible' => '1',
    '#collapsed' => '0',
  );
  $form['pfizer_webstandards_js_rebuild']['intro'] = array(
    '#weight' => 0,
    '#markup' => t('By default the JavaScript files are rebuilt each time Cron
      runs. If you are experiencing issues generating the files, click this
      button to rebuild the files and clear the site cache. Note – the JS cache
      will be cleared.'),
    '#prefix' => '<div>',
    '#suffix' => '</div>',
  );
  $form['pfizer_webstandards_js_rebuild']['rebuild'] = array(
    '#weight' => 1,
    '#type' => 'submit',
    '#value' => t('Rebuild the JS files'),
    '#submit' => array('pfizer_webstandards_troubleshooting_form_js_rebuild_submit'),
  );

  return $form;
}

/**
 * Submit handler for form - ts redeploy.
 */
function pfizer_webstandards_troubleshooting_form_ts_redeploy_submit($form, &$form_state) {
  // Redeploy the s-code file.

  // This code is copied from the install hook.
  pfizer_webstandards_generate_js_ensure_structure();

  $source_path = drupal_get_path('module', 'pfizer_webstandards') .
    '/resources/javascript/s_code.js';

  $dest_path = pfizer_webstandards_generate_scode_path();

  file_unmanaged_copy($source_path,
    $dest_path,
    FILE_EXISTS_REPLACE);

  // Generate a relative path for the s_code file by removing the base URL.
  $full_dest_path = file_create_url($dest_path);
  global $base_url;

  // Check if the full path begins with the base URL:
  if (strpos($full_dest_path, $base_url) === 0) {
    // It does, so remove it.
    $full_dest_path = substr($full_dest_path, strlen($base_url));
  }

  // If SiteCatalyst is installed then configure its variable to point to the
  // new JS file:
  if (module_exists('sitecatalyst')) {
    variable_set('sitecatalyst_js_file_location', $full_dest_path);
  }

  drupal_set_message(t('The s_code file has been re-deployed.'));

}

/**
 * Submit handler for form - js rebuild.
 */
function pfizer_webstandards_troubleshooting_form_js_rebuild_submit($form, &$form_state) {

  // Re-export the config files.
  pfizer_webstandards_generate_js_build_pfconfig();
  pfizer_webstandards_generate_js_build_urlconfig();

  drupal_clear_js_cache();

  drupal_set_message(t('The JS files have been rebuilt, and the cache cleared.'));
}
