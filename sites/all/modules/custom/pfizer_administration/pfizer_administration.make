; Drush make file for pfizer_administration dependencies.

api = 2
core = "7.x"

projects[] = drupal

projects[breakpoints][subdir] = "contrib"
projects[breakpoints][version] = "1.4"

projects[ckeditor][subdir] = "contrib"
projects[ckeditor][version] = "1.17"

projects[environment_indicator][subdir] = "contrib"
projects[environment_indicator][version] = "2.8"

projects[file_entity][subdir] = "contrib"
projects[file_entity][version] = "2.0-beta3"

projects[fpa][subdir] = "contrib"
projects[fpa][version] = "2.6"

projects[media][subdir] = "contrib"
projects[media][version] = "2.1"

projects[media_ckeditor][subdir] = "contrib"
projects[media_ckeditor][version] = "2.0-rc3"

projects[module_filter][subdir] = "contrib"
projects[module_filter][version] = "2.0"

projects[navbar][subdir] = "contrib"
projects[navbar][version] = "1.7"

projects[responsive_preview][version] = "1.x-dev"
projects[responsive_preview][subdir] = "contrib"
projects[responsive_preview][download][type] = "git"
projects[responsive_preview][download][revision] = "d741779"
projects[responsive_preview][download][branch] = "7.x-1.x"
; Before js processing, the phone image incorrectly positioned.
; https://drupal.org/node/2276789
projects[responsive_preview][patch][i2276789][url] = "http://drupal.org/files/issues/responsive_preview-phone_image_incorrectly_positioned-2276789-2.patch"
projects[responsive_preview][patch][i2276789][md5] = "d09599b524178b3ed488bc2366babf6f"

projects[site_verify][subdir] = "contrib"
projects[site_verify][version] = "1.2"

projects[shiny][type] = "theme"
projects[shiny][version] = "1.7"
projects[shiny][patch][i2348435][url] = "http://www.drupal.org/files/issues/shiny-wysiwyg_toolbar_fix.patch"
projects[shiny][patch][i2348435][md5] = "01479379516ccf17e34f84c090fbc663"

; Libraries for Mobile Friendly Navigation Bar
libraries[backbone][download][type] = "get"
libraries[backbone][download][url] = "https://github.com/jashkenas/backbone/archive/1.1.0.zip"

libraries[modernizr][download][type] = "get"
libraries[modernizr][download][url] = "https://github.com/Modernizr/Modernizr/archive/v2.7.0.tar.gz"

libraries[underscore][download][type] = "get"
libraries[underscore][download][url] = "https://github.com/jashkenas/underscore/archive/1.5.2.zip"

; CKEditor Library
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.6.1/ckeditor_4.6.1_full.zip"
