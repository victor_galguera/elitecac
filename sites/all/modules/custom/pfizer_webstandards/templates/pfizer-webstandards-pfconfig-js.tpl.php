<?php
/**
 * @file
 * Template for generating the pfConfig JS object.
 *
 * Available variables:
 * $mode - The current pfConfig 'mode'.
 * $development_report_suites - The list of development suites.
 * $production_report_suites - The list of production suites.
 * $development_domains - The list of development domains.
 * $production_domains - The list of production domains.
 * $site_section_prefix - The site section prefix value.
 * $site_section_delimiter - This site section delimiter value.
 * $page_name_base - The page name base value.
 * $page_name_prefix - The page name prefix value.
 * $page_name_delimiter - The page name delimiter value.
 * $custom_links - An array of customLinks objects.
 */
?>
/*
 * PFConfig file - used by SiteCatalyst, Generated by Pfizer Web Standards.
 *
 * Created: <?php print date('Y-m-d H:i:s') . "\n"; ?>
 */

var pfConfig = {
  mode: '<?php print $mode; ?>',
  reportSuites: {
    dev: '<?php print $development_report_suites; ?>',
    prod: '<?php print $production_report_suites; ?>'
  },
  domains: {
    dev: '<?php print $development_domains; ?>',
    prod: '<?php print $production_domains; ?>'
  },
  siteSection: {
    prefix: '<?php print $site_section_prefix; ?>',
    delimiter: '<?php print $site_section_delimiter; ?>'
  },
  pageName: {
    base: '<?php print $page_name_base; ?>',
    prefix: '<?php print $page_name_prefix; ?>',
    delimiter: '<?php print $page_name_delimiter; ?>',
    homePage: '<?php print $page_name_homepage; ?>'
  },
  customLinks: [<?php
    $links = array();
    for ($i = 0; $i < count($custom_links); $i++) {
      $link = $custom_links[$i];

      $data = array();
      foreach ($link as $key => $value) {
        if (!in_array($key, array('href', 'link_type', 'function', 'object', 'interaction_status'))) {
          continue;
        }

        // consistent naming is apparently overrated...
        if ($key == 'link_type') {
          $key = 'type';
        }
        elseif($key == 'function') {
          $key = 'funcName';
        }
        elseif ($key == 'object') {
          $key = 'obj';
        }

        $data[] = "    " . $key . ": '" . str_replace("'", "\\'", $value) . "'";
      }

      $links[] = "\n  {\n" . implode(",\n", $data) . "\n  }";
    }

    echo implode(",", $links) . "\n  ";
  ?>]
};
