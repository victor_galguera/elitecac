<?php

/**
 * @file
 * Plugin definition for Pfizer Encrypt.
 */

// This plugin simply calls the mycrypt_rij_256 plugin.
$plugin = array(
  'title' => t('Pfizer Encrypt'),
  'description' => t('Pfizer-specific encrypt mechanism using Mcrypt AES 256.'),
  'encrypt callback' => '_encrypt_encryption_methods_mcrypt_rij_256',
  'dependency callback' => '_mcrypt_extension_is_present',
  'path' => drupal_get_path('module', 'encrypt') . '/plugins/encryption_methods',
  'file' => 'mcrypt_rij_256.inc',
);
