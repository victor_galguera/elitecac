/**
 * @file
 * Edetail Specific JavaScript for Web Standards.
 */

/**
 * Overwrite the existing Edetail functions.
 *
 * goToSlide uses ajax so does not need to be overwritten
 * goToChapter() and concludePresentation() need to be overwritten.
 */

/**
 * Replace the functions after the page has loaded.
 */
Drupal.behaviors.pfizerWebStandardsEdetail = {
  attach: function (context, settings) {
    jQuery('body', context).once(function() {
      "use strict";

      // redefine global functions to add new functionality
      var original_gtc = null;
      if (typeof goToChapter == 'function') {
        original_gtc = goToChapter;
      }

      var goToChapter = function(chapter_id, slide_id) {
        unloadActions.is_completed = true;

        if (original_gtc) {
          original_gtc(chapter_id, slide_id);
        }
      };

      var original_cp = null;
      if (typeof concludePresentation == 'function') {
        original_cp = concludePresentation;
      }

      var concludePresentation = function() {
        unloadActions.is_completed = true;
        unloadActions.init();

        if (original_cp) {
          original_cp();
        }
      };
    });
  }
}

var unloadActions = {
  run : true, // flag to prevent more than one execution
  is_completed: false, // flag to mark presentation as completed
  init: function() {
    // make sure we're on an e-detail presentation page
    var match = ['*/e-details/*/*'];
    var not_match = ['*/e-details/*/conclude'];

    if (!pfizerWebstandardsUrlMatch(match) || pfizerWebstandardsUrlMatch(not_match)) {
      this.run = false;
      return;
    }

    // are we still allowed to run the events?
    if (!this.run) {
      return;
    }

    if (this.is_completed) {
      // fire complete event
      this.send_event('complete', 'event24');
    }
    else {
      // fire abandon event
      this.send_event('abandon', 'event23');
    }

    this.run = false;
  },
  send_event: function(name, num) {
    var s = build_s();

    s.events = s.apl(s.events, num, ',', 2);
    s.prop29 = 'EDETAIL';
    s.eVar28 = 'D=c29';

    s.prop31 = jQuery('div.view-brand-logo-and-title div.views-row-1 div.sub_title').text().trim();
    s.eVar30 = 'D=c31';

    s.linkTrackVars ='events,prop29,prop31,eVar28,eVar30';
    s.linkTrackEvents = s.apl(s.linkTrackEvents, num, ',', 2);
    
    s.useForcedLinkTracking = true;
    s.forcedLinkTrackingTimeout = 500;

    s.tl(true, 'o', 'EDETAIL ' +  name.toUpperCase());
    s.pfClearVars();
  }
};

// Window Unload event handlers
jQuery(window).bind('beforeunload', function() {
  unloadActions.init();
});

jQuery(window).bind('unload', function() {
  unloadActions.init();
});

/**
 * Edetail specific JS function.
 *
 * @param s
 *   Report Suite Object
 */
function pfizerWebstandardsEdetail(s) {
  "use strict";

  // Edetail Page View
  var matchUrls = ['*/e-details/*'];
  var notMatchUrls = ['*/e-details/*/*'];

  if (pfizerWebstandardsUrlMatch(matchUrls) &&
    !pfizerWebstandardsUrlMatch(notMatchUrls)
    ) {

    // Work out the edetail title:
    var eDetailTitle = jQuery ('div.view-brand-logo-and-title div.views-row-1 div.sub_title')
      .text().trim();

    // This is a start page for an edetail, fire the event:
    s.pfEdetail('start', eDetailTitle);
  }

  pfizerWsCheckBanner(s);
}
