# Pfizer XML sitemap

## Overview

This is an initial beta release of Pfizer XML sitemap. The initial goal of this project is to have a usable XML sitemap on every site for use with SST (Screenshot Tool).

It includes the following project in it's make file:

- [XML sitemap](https://drupal.org/project/xmlsitemap)

### Required Dependencies

- XML sitemap node

The node module is a dependency and will automatically add all content types.

### Optional Dependencies

- XML sitemap menu
- XML sitemap taxonomy

If you have the Taxonomy and Menu modules enabled, the optional dependencies will be enabled automatically and all available vocabularies and menus will be added to the Pfizer SST custom sitemap except a few that are specifically excluded.

## Install

- Set the platform version in Jenkins to use the current dev platform (7.x-2.5.x-dev) or dev HCP sub-platform (7.x-hcp-2.5.x-dev).

- Enable the module.

- Go to the Pfizer XML sitemap settings at Configuration > Search and metadata > Pfizer XML Sitemap Settings (admin/config/search/pfizer_xmlsitemap). Remove any additional content types, vocabularies or menus as required.

- Regenerate the cache files by going to XML sitemap configuration at Configuration > Search and metadata > XML sitemap settings (admin/config/search/xmlsitemap). Select the checkbox on the new sitemap URL ending in `?pfizer_sst=1` and click the **Update** button.

You can view the Pfizer custom SST XML sitemap by loading the following path: `sitemap.xml?pfizer_sst=1`

## Known Issues

If you enable this module and then enable the Taxonomy or Menu modules afterwards, the XML sitemap taxonomy or XML sitemap menu modules will not get automatically enabled and configured. Additionally, any vocabulary or meny added will need to be included in the Pfizer XML sitemap settings.

## @todo

- Add hook_modules_enabled implementation to automatically detect when Taxonomy and Menu modules are enabled to enable and configure respective XML sitemap sub-modules.
