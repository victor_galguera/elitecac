Pfizer Install Profile
======================

This is the current base Pfizer installation profile for all Drupal sites.

This enforces the usage of these modules:

* [Pfizer Administration](https://github.com/pfizer/pfizer_administration)
* [Pfizer Deploy](https://github.com/pfizer/pfizer_deploy)
* [Pfizer Security](https://github.com/pfizer/pfizer_security)
* [Pfizer XML Sitemap](https://github.com/pfizer/pfizer_xmlsitemap)
* [Redirect](https://drupal.org/project/redirect)

The Redirect module was added in order to fascilitate redirect caching on Varnish.
