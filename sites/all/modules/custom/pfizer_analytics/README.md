# Pfizer Analytics 

This repository contains modules for integration between Portal and [PFA Core](https://github.com/pfizer/pfizer_analytics_core).

### Module - Pfizer Analytics 

This module provides common functionality relating to PFA.

### Module - Pfizer Analytics Mediaplatform 

This module provides analytics integration for MediaPlatform webinars.



