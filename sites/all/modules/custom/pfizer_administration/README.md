# Pfizer Administration

This feature is a simple way to add the default Pfizer administration tools. It disables the core Toolbar module if it's enabled so you can enable it post standard install.

This module enforces revisions on nodes and blocks (Bean) and forces you to add a log message when updating those entities.

It displays platform information including the platform version and the last build time on the Drupal status page.

The Administration Menu module has been removed as of 2.x and should no longer used. The Navbar should be used instead and is enabled by default.

## Pfizer Administration Media

All media support has been moved to Pfizer Administration as of 2.x. It includes:

- [Media 2.x](https://www.drupal.org/project/media)
- [Media CKEditor 2.x](https://www.drupal.org/project/media_ckeditor)
- [CKEditor Library 4.x](http://ckeditor.com/)

To enable media support, simply enable the Pfizer Administration Media module.

## Known Issues

As of Pfizer Administration 2.6.0, [Media 2.0 is incompatible](https://www.drupal.org/project/media/releases/7.x-2.0) with Entity Translation versions prior to 1.0-beta6. Update to Entity Translation 1.0-beta6 or later.
