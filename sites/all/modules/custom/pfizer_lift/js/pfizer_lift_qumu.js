/******************************************************************************
    QUMU / KuluValley
******************************************************************************/

/**
 * Lift's Qumu / KuluValley object for tracking video events.
 * 
 * @return {[type]}         [description]
 */
var lift_qumu = function() {
  "use strict";

  // qumu API object
  var api = {};

  // video data
  var video_properties = {
    duration: 0,
    index: 0,
    // speaker: '',
    title: ''
  };

  // flags for tracking progress marker events
  var sent_25 = false;
  var sent_50 = false;
  var sent_75 = false;
  var sent_completed = false;
  var sent_started = false;

  // save existing beforeunload and unload handlers, if any
  var old_beforeunload = false;
  var old_unload = false;
  
  if (typeof window.onbeforeunload != 'undefined') {
    old_beforeunload = window.onbeforeunload;
  }

  if (typeof window.onunload != 'undefined') {
    old_unload = window.onunload;
  }

  // bind new event handlers for onBeforeUnload and onUnload so we can
  // track video_abandon
  window.onbeforeunload = function (evt) {
    onAbandon(evt);

    if (old_beforeunload) {
      old_beforeunload(evt);
    }
  };

  window.oneunload = function (evt) {
    onAbandon(evt);

    if (old_unload) {
      old_unload(evt);
    }
  };

  /**
   * Generic handler for all events.
   */
  var genericEventHandler = function (args) {
    api.get('currentTime', function (result) {
      video_properties.index = Math.round(result / 1000);
    });
  };

  /**
   * Event handler for 'Play' event.
   * 
   * @return {[type]} [description]
   */
  var onPlay = function () {
    if (sent_started) {
      return;
    }

    sent_started = true;

    // base metrics
    if (Drupal.pfizer_lift) {
      pfaLoader.set_pm_item('base_metrics', Drupal.pfizer_lift.getBaseMetrics());
    }

    pfaLoader.trigger('video_start', video_properties);
  };

  /**
   * Event handler for 'TimeUpdate' event.
   * 
   * @return {[type]} [description]
   */
  var onTimeUpdate = function () {
    // video progress percentage
    var percentage = (video_properties.index / video_properties.duration * 100).toFixed(0);

    if (!sent_25 && percentage >= 25) {
      sent_25 = true;
      pfaLoader.trigger('video_25', video_properties);
    }

    if (!sent_50 && percentage >= 50) {
      sent_50 = true;
      pfaLoader.trigger('video_50', video_properties);
    }

    if (!sent_75 && percentage >= 75) {
      sent_75 = true;
      pfaLoader.trigger('video_75', video_properties);
    }
  };

  /**
   * Event handler for 'Ended' event.
   * 
   * @return {[type]} [description]
   */
  var onEnded = function () {
    pfaLoader.trigger('video_complete', video_properties);
    sent_completed = true;
  };

  /**
   * Event handler for onBeforeUnload and onUnload window events.
   * 
   * @param  {[type]} evt [description]
   * @return {[type]}     [description]
   */
  var onAbandon = function (evt) {
    if (!sent_started || sent_completed) {
      return;
    }

    sent_completed = true;

    pfaLoader.trigger('video_abandon', video_properties);
  };

  return {
    /**
     * Initializes KV API and binds event handlers.
     * 
     * @param  {[type]} name     [description]
     * @param  {[type]} video_id [description]
     * @return {[type]}          [description]
     */
    'init': function (name, video_id) {
      api = new KV.Api(name);

      api.init(function(error) {
        if (error) {
          console.log('Error initializing Qumu Player API.');
          return;
        }

        api.get('duration', function (result) {
          video_properties.duration = Math.round(result / 1000);
        });

        if (video_id && Drupal.settings.pfizer_lift.hasOwnProperty('qumu') 
          && Drupal.settings.pfizer_lift.qumu.hasOwnProperty(video_id)) {

          video_properties.title = Drupal.settings.pfizer_lift.qumu[video_id].title;
          video_properties.type  = Drupal.settings.pfizer_lift.qumu[video_id].webinar_type;
        }

        api.bind('play', function () {
          genericEventHandler();
          onPlay();
        });

        api.bind('pause', function () {
          genericEventHandler();
        });

        api.bind('timeupdate', function () {
          genericEventHandler();
          onTimeUpdate();
        });

        api.bind('volumechange', function () {
          genericEventHandler();
        });

        api.bind('ended', function () {
          genericEventHandler();
          onEnded();
        });
      });
    }
  };
};

(function() {
  var lift_qumu_objects = [];

  var frames = document.querySelectorAll('iframe.kv-frame');
  for (var i = 0; i < frames.length; i++) {
    var name = frames[i].name;
    var src = frames[i].src;
    var video_id = false;

    if (matches = src.match(/kuluvalley\.com\/view\/([^\?]+)/)) {
      video_id = matches[1];
    }

    if (!lift_qumu_objects.hasOwnProperty(name)) {
      lift_qumu_objects[name] = new lift_qumu();
    }

    var lift = lift_qumu_objects[name];
    lift.init(name, video_id);
  }
})();

