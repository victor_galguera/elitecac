<?php

/**
 * @file
 * Common functions used by CEMHUB module.
 */

/**
 * Returns an array containing the configuration of integrated webforms.
 *
 * @return array
 *   An array containing the integrated webforms information.
 */
function cemhub_get_integrated_webforms_settings() {
  $webforms = cemhub_retrieve_integrated_webforms_settings();

  return $webforms;
}

/**
 * Returns the configuration of all activated integrated webforms.
 *
 * @return array
 *   An array containing the integrated webforms information.
 */
function cemhub_get_integrated_activated_webforms_settings() {
  $webforms_settings = cemhub_retrieve_integrated_webforms_settings();

  $active_webforms = array();
  foreach ($webforms_settings as $webform) {
    if (!empty($webform->active)) {
      $active_webforms[$webform->nid] = $webform;
    }
  }

  return $active_webforms ? $active_webforms : NULL;
}

/**
 * Returns the settings of a integrated webform.
 *
 * @param int $node_id
 *   The webform node ID.
 *
 * @return Ambigous
 *   Returns an array in case of success, FALSE otherwise.
 */
function cemhub_get_integrated_webform_settings_by_node_id($node_id) {
  $webforms = cemhub_get_integrated_webforms_settings();

  return isset($webforms[$node_id]) ? $webforms[$node_id] : NULL;
}

/**
 * Retrieves the configuration of integrated webforms.
 *
 * @return array
 *   Returns an array containing the settings of all integrated webforms.
 */
function cemhub_retrieve_integrated_webforms_settings() {
  $cache = &drupal_static(__FUNCTION__);

  if ($cache) {
    $webforms = $cache;
  }
  else {
    // For the first call, get all results and set in cache.
    $db_select = db_select('cemhub_forms_config', 'cf')->fields('cf')->orderBy('cf.form_priority');
    $webforms = $db_select->execute()->fetchAll();

    // Places the index of the array as webform nid.
    $webforms_rows_by_nid = array();
    foreach ($webforms as $webform) {
      $webforms_rows_by_nid[$webform->nid] = $webform;
    }
    $webforms = $webforms_rows_by_nid;

    if (!empty($webforms)) {
      // Sets the cache.
      $cache = $webforms;
    }
  }

  return $webforms;
}

/**
 * Checks if a webform is integrated with cemhub.
 *
 * @param int $nid
 *   The webform node id to check.
 *
 * @return bool
 *   Returns TRUE in case of success, FALSE otherwise.
 */
function cemhub_is_webform_integrated($nid) {
  $webform_settings = cemhub_get_integrated_webform_settings_by_node_id($nid);

  return !empty($webform_settings->active) ? TRUE : FALSE;
}

/**
 * Returns whether or not the CEMHUB integration is enabled.
 *
 * @return bool
 *   Returns TRUE if is enabled, FALSE otherwise.
 */
function cemhub_is_integration_enabled() {
  return (bool) variable_get('cemhub_integration_enabled', FALSE);
}

/**
 * Returns whether or not the set cookie for caimpaing value is enabled.
 *
 * @return bool
 *   Returns TRUE if is enabled, FALSE otherwise.
 */
function cemhub_campaign_tracking_use_cookie_instead_session() {
  return (bool) variable_get('cemhub_cookies_enabled');
}

/**
 * Updates CEMHUB configuration table.
 *
 * @param array $webform_config
 *   An array containing the data structured to alter in table.
 */
function cemhub_update_webform_config($webform_config) {
  db_update('cemhub_forms_config')
    ->fields($webform_config)
    ->condition('nid', $webform_config['nid'], '=')
    ->execute();
}

/**
 * Return the limit of submissions per day expected by the CEM HUB by webform.
 *
 * @param int $webform_nid
 *   The webform node ID.
 *
 * @return int
 *   Returns the limit number related to this webform.
 */
function cemhub_get_limit_submissions_per_day($webform_nid) {
  $webforms_settings = cemhub_get_integrated_webform_settings_by_node_id($webform_nid);
  $limit = isset($webforms_settings->limit_submissions) ? $webforms_settings->limit_submissions : CEMHUB_DEFAULT_LIMIT_SUBMISSION;

  return $limit;
}

/**
 * Saves information of integrated webform in config database table.
 *
 * @param array $webform_config
 *   An array containing the data structured to insert in table.
 */
function cemhub_insert_new_entry_of_webform_config($webform_config) {
  db_insert('cemhub_forms_config')
    ->fields($webform_config)
    ->execute();
}

/**
 * Checks all the required environment settings to perform a job.
 *
 * @throws Exception
 */
function cemhub_check_required_environment_settings() {
  cemhub_check_required_php_extensions();
  cemhub_check_private_file_system_path();
  cemhub_check_htaccess_in_private_system_path();
  cemhub_check_temporary_repository_files();
  cemhub_check_sftp_keys();
  cemhub_check_pgp_public_key();
}

/**
 * Checks whether private file system path exists or not.
 *
 * @throws Exception
 *
 * @return bool
 *   TRUE if exists.
 */
function cemhub_check_private_file_system_path() {
  if (!cemhub_get_private_file_system_path()) {
    throw new Exception('This module Requires Drupal Private file system path where private files will be stored. Set on: Configuration > Media > File system.');
  }

  return TRUE;
}

/**
 * Checks if the .htaccess file exist for private system folder.
 *
 * @throws Exception
 *
 * @return bool
 *   TRUE if exists.
 */
function cemhub_check_htaccess_in_private_system_path() {
  $file_private_path = variable_get('file_private_path');
  if ($file_private_path) {
    file_create_htaccess('private://', TRUE);
    if (!file_exists($file_private_path . '/.htaccess')) {
      throw new Exception("Security warning: Couldn't write .htaccess file.");
    }
  }

  return TRUE;
}

/**
 * Checks if required PHP extensions are loaded.
 *
 * @throws Exception
 *
 * @return bool
 *   TRUE in case of the extensions are loaded.
 */
function cemhub_check_required_php_extensions() {
  $extensions_list = array(
    CEMHUB_EXTENSION_SSH_NAME,
    CEMHUB_EXTENSION_GNUPG_NAME,
  );

  $not_loaded_extensions = array();
  foreach ($extensions_list as $extension) {
    if (!extension_loaded($extension)) {
      $not_loaded_extensions[] = $extension;
    }
  }

  if (count($not_loaded_extensions)) {
    throw new Exception('The extension(s) "' . implode(', ', $not_loaded_extensions) . '" must be installed and enabled on the server.');
  }

  return TRUE;
}

/**
 * Checks if PGP public key exist and is readable.
 *
 * @throws Exception
 *
 * @return bool
 *   Returns TRUE if case of success.
 */
function cemhub_check_pgp_public_key() {
  $key_location = 'private://' . variable_get('cemhub_pgp_key_name');
  $is_readable = is_readable($key_location);
  if (!$is_readable) {
    throw new Exception('The PGP Public Key "' . $key_location . '" does not exist or is not readable.');
  }

  return TRUE;
}

/**
 * Checks if temporary repository files exist and is writable.
 *
 * @throws Exception
 *
 * @return bool
 *   Returns TRUE if case of success, FALSE otherwise.
 */
function cemhub_check_temporary_repository_files() {
  $temp_files_path = cemhub_get_files_repository_path(TRUE);
  $checked_temporary_repository = file_prepare_directory($temp_files_path);
  if (empty($checked_temporary_repository)) {
    throw new Exception('Temporary repository path does not exist or is not writable, please fix this.');
  }

  return $checked_temporary_repository;
}

/**
 * Checks if the SFTP keys are in their correct location.
 *
 * @throws Exception
 *
 * @return bool
 *   TRUE in case of success.
 */
function cemhub_check_sftp_keys() {
  $keys = array(
    'public',
    'private',
  );

  $not_existing_keys = array();
  foreach ($keys as $key_type) {
    $retrieved_stored_key = variable_get('cemhub_sftp_' . $key_type . '_key');

    if (empty($retrieved_stored_key) || !file_exists('private://' . $retrieved_stored_key)) {
      $not_existing_keys[] = $key_type;
    }
  }

  if (count($not_existing_keys)) {
    throw new Exception('Not possible to find the SFTP key(s): ' . implode(', ', $not_existing_keys));
  }

  return TRUE;
}

/**
 * Returns relative Drupal's private file path for the site.
 *
 * If the first parameter is TRUE, return the absolute path.
 *
 * @param bool $absolute_path
 *   TRUE to use absolute path.
 *
 * @return string
 *   Private file system path, FALSE in case of failure.
 */
function cemhub_get_private_file_system_path($absolute_path = FALSE) {
  $stored_path = variable_get('file_private_path', FALSE);

  $return_path = $stored_path;

  if ($stored_path && $absolute_path) {
    $return_path = getcwd() . '/' . $stored_path;
  }

  return $return_path;
}

/**
 * Returns the local CEMHUB files repository path.
 *
 * @param bool $drupal_internal_path
 *   TRUE to use a drupal internal path.
 *
 * @return string
 *   Files repository path, FALSE in case of failure.
 */
function cemhub_get_files_repository_path($drupal_internal_path = FALSE) {
  $private_files_path = cemhub_get_private_file_system_path(TRUE);
  $stored_path = variable_get('cemhub_repository_files');

  $files_repository_path = $private_files_path . '/' . $stored_path;

  // If the directory does not exist, it is created.
  cemhub_create_directory($files_repository_path);

  if ($drupal_internal_path) {
    $files_repository_path = 'private://' . $stored_path;
  }

  return $files_repository_path;
}

/**
 * Returns an array with all the webforms available in the system.
 *
 * @return array
 *   An array containing the webforms.
 */
function cemhub_get_available_webforms() {
  $available_webforms = db_select('node', 'nd')
    ->fields('nd', array('title', 'nid'))
    ->condition('type', 'webform')
    ->execute()
    ->fetchAll();

  return $available_webforms;
}

/**
 * Formats the data in a format expected by functions of database integration.
 *
 * @see cemhub_insert_new_entry_of_webform_config
 * @see cemhub_update_webform_config
 */
function cemhub_forms_settings_formats_data_to_database_integration($form_state, $nid) {
  $fields_names = cemhub_get_integrated_webforms_names_fields_settings();

  $formated_data = array();
  foreach ($fields_names as $column) {
    if ($column != 'id' && $column != 'nid') {
      $formated_data[$column] = $form_state['values']['cemhub_form_nid' . $nid . '_' . $column];
    }
  }
  $formated_data['nid'] = $nid;

  return $formated_data;
}

/**
 * Get the name of the columns of the table form config.
 *
 * @see cemhub_forms_settings_formats_data_to_database_integration
 */
function cemhub_get_integrated_webforms_names_fields_settings() {
  $cache = &drupal_static(__FUNCTION__);

  if ($cache) {
    $fields_names = $cache;
  }
  else {
    $fields_names = db_query('SHOW COLUMNS FROM {cemhub_forms_config}')->fetchCol();

    if (!empty($fields_names)) {
      // Sets the cache.
      $cache = $fields_names;
    }
  }

  return $fields_names;
}

/**
 * Returns the 'title' and 'type' properties of a given field id.
 *
 * @param int $nid
 *   The fields's parent webform nid.
 * @param int $component_id
 *   Webform field component id.
 *
 * @return array
 *   An array containing the field properties.
 */
function cemhub_get_webform_field_properties($nid, $component_id) {
  $field_properties = db_select('webform_component', 'wc')
    ->fields('wc', array('form_key', 'type'))
    ->condition('wc.nid', $nid, '=')
    ->condition('wc.cid', $component_id, '=')
    ->execute()
    ->fetchAssoc();

  return $field_properties;
}

/**
 * Returns the mapped webform identified by a webform.
 *
 * @param int $nid
 *   The webform node id.
 *
 * @return array
 *   An array containing the fields data.
 */
function cemhub_get_mapped_webform_fields_by_nid($nid) {
  $fields = db_select('cemhub_fields', 'cf')
    ->fields('cf', array('cid', 'nid', 'question_code', 'answer_code', 'active'))
    ->condition('cf.nid', $nid, '=')
    ->execute();

  return $fields;
}

/**
 * Returns the options time to use to scheduling the cron job execution time.
 */
function cemhub_get_batch_run_time_options() {
  $hours = array();

  for ($hour = 0; $hour <= 23; $hour++) {
    $formatted_hour = str_pad($hour, 2, '0', STR_PAD_LEFT);
    $hours[$formatted_hour] = $formatted_hour . ' hr';

    if ($hour > 1) {
      $hours[$formatted_hour] .= 's';
    }
  }

  return $hours;
}

/**
 * Creates a CEMHUB mapping for the given field.
 *
 * @param int $field_cid
 *   Webform field component id.
 * @param int $webform_nid
 *   Webform node id.
 * @param string $question_code
 *   CEMHUB question code.
 * @param string $answer_code
 *   CEMHUB answer code.
 * @param int $active
 *   If is active or not.
 */
function cemhub_create_webform_fields_mapping($field_cid, $webform_nid, $question_code, $answer_code, $active) {
  db_insert('cemhub_fields')
    ->fields(
        array(
          'cid' => $field_cid,
          'nid' => $webform_nid,
          'question_code' => $question_code,
          'answer_code' => $answer_code,
          'active' => $active,
        )
        )
    ->execute();
}

/**
 * Updates a CEMHUB mapping for the given field.
 *
 * @param int $field_cid
 *   Webform field component ID.
 * @param int $webform_nid
 *   Webform node id.
 * @param string $question_code
 *   CEMHUB question code.
 * @param string $answer_code
 *   CEMHUB answer code.
 * @param int $active
 *   If is active or not.
 */
function cemhub_update_webform_fields_mapping($field_cid, $webform_nid, $question_code, $answer_code, $active) {
  db_update('cemhub_fields')
    ->fields(
            array(
              'question_code' => $question_code,
              'answer_code' => $answer_code,
              'active' => $active,
            )
        )
    ->condition('cid', $field_cid, '=')
    ->condition('nid', $webform_nid, '=')
    ->execute();
}

/**
 * Deletes the CEMHUB mapping for the given field.
 *
 * @param int $cid
 *   Webform component ID.
 * @param int $nid
 *   Webform node ID.
 */
function cemhub_delete_webform_field_mapping($cid, $nid) {
  db_delete('cemhub_fields')
    ->condition('cid', $cid)
    ->condition('nid', $nid)
    ->execute();
}

/**
 * Compares and synchronizes the configuration tables.
 *
 * Tables:
 *   - webform_component
 *   - cemhub_fields.
 *
 * @param int $webform_nid
 *   Webform node id to compare.
 */
function cemhub_sync_tables($webform_nid) {
  if (!empty($webform_nid)) {
    $webform_fields_cids = cemhub_get_webform_field_ids($webform_nid);
    $cemhub_mapped_fields_cids = cemhub_get_mapped_field_ids($webform_nid);

    $fields_to_insert = array_diff($webform_fields_cids, $cemhub_mapped_fields_cids);
    $fields_to_delete = array_diff($cemhub_mapped_fields_cids, $webform_fields_cids);

    if (!empty($fields_to_insert)) {
      foreach ($fields_to_insert as $cid_to_insert) {
        $question_code = '';
        $answer_code = '';
        $active = 0;

        cemhub_create_webform_fields_mapping(
          $cid_to_insert,
          $webform_nid,
          $question_code,
          $answer_code,
          $active
        );
      }
    }

    if (!empty($fields_to_delete)) {
      foreach ($fields_to_delete as $cid_to_delete) {
        cemhub_delete_webform_field_mapping($cid_to_delete, $webform_nid);
      }
    }
  }
}

/**
 * Returns the IDs of the fields that remain to the given webform.
 *
 * @param int $webform_nid
 *   Webform node id.
 *
 * @return array
 *   An array containing the webform field IDs.
 */
function cemhub_get_webform_field_ids($webform_nid) {
  $webform_fields_cids = db_select('webform_component', 'wc')
    ->fields('wc', array('cid'))
    ->condition('nid', $webform_nid, '=')
    ->execute()
    ->fetchCol();

  return $webform_fields_cids;
}

/**
 * Returns the IDs of the fields mapped by CEMHUB module.
 *
 * @param int $webform_nid
 *   The given webform node id.
 *
 * @return array
 *   Returns an array containing the mapped field IDs.
 */
function cemhub_get_mapped_field_ids($webform_nid) {
  $cemhub_mapped_fields_cids = db_select('cemhub_fields', 'cf')
    ->fields('cf', array('cid'))
    ->condition('nid', $webform_nid, '=')
    ->execute()
    ->fetchCol();

  return $cemhub_mapped_fields_cids;
}

/**
 * Deletes all webform references from CEMHUB.
 *
 * @param int $nid
 *   The webform node id.
 */
function cemhub_cleanup_webform_references($nid) {
  $tables = array(
    'cemhub_forms_config',
    'cemhub_fields',
    'cemhub_campaign_source',
  );

  foreach ($tables as $table) {
    db_delete($table)->condition('nid', $nid)->execute();
  }
}

/**
 * Checks if the system cron should run the CEMHUB job.
 *
 * Checks if the current time is in the same hour range specified in admin form
 * and if the integration by cron has not run today.
 *
 * @return bool
 *   TRUE if the integration must be performed, FALSE otherwise.
 */
function cemhub_cron_is_in_time_range() {
  $last_cron_job_status = cemhub_get_last_cron_job_status();

  $is_in_scheduled_hour_range = (variable_get('cemhub_batch_run_time') == date('H', REQUEST_TIME));
  $hasnt_ran_today = (date('Ymd', $last_cron_job_status['timestamp']) < date('Ymd'));

  $should_perform_job = ($is_in_scheduled_hour_range && $hasnt_ran_today);

  return $should_perform_job;
}

/**
 * If the given directory does not exist, it is created.
 *
 * @param string $directory
 *   The directory to be created.
 *
 * @return bool
 *   TRUE if the directory was successfully created, FALSE otherwise.
 */
function cemhub_create_directory($directory) {
  $created = FALSE;

  if (!is_dir($directory)) {
    if (!mkdir($directory, 0777, TRUE)) {
      watchdog('cemhub', 'Could not create directory: {$directory}');
    }
    else {
      $created = TRUE;
    }
  }

  return $created;
}

/**
 * Returns a array with all allowed campaign source parameters.
 *
 * @return array
 *   An array containing the campaign parameters data.
 */
function cemhub_get_allowed_external_campaign_parameters() {
  $possible_parameters = variable_get('cemhub_external_campaign_parameters');

  return explode("\n", $possible_parameters);
}

/**
 * Gets external Campaign source code ID by GET method.
 */
function cemhub_get_incoming_campaign_source_code_id() {
  $campaign_source_id = NULL;

  $allowed_parameters = cemhub_get_allowed_external_campaign_parameters();
  foreach ($allowed_parameters as $parameter) {
    if (array_key_exists(trim($parameter), $_GET)) {
      $campaign_source_id = $_GET[trim($parameter)];
    }
  }

  return $campaign_source_id;
}

/**
 * Gets campaign source code session value.
 *
 * @return string
 *   The campaign source code value.
 */
function cemhub_get_campaign_source_code_session_value() {
  $campaign_source = NULL;

  if (isset($_SESSION[CEMHUB_CAMPAIGN_SOURCE_SESSION])) {
    $campaign_source = $_SESSION[CEMHUB_CAMPAIGN_SOURCE_SESSION];
  }

  return $campaign_source;
}

/**
 * Gets campaign source code cookie value.
 *
 * @return string
 *   The campaign source code value.
 */
function cemhub_get_campaign_source_code_cookie_value() {
  $campaign_source = NULL;

  if (isset($_COOKIE[CEMHUB_CAMPAIGN_SOURCE_COOKIES])) {
    $campaign_source = $_COOKIE[CEMHUB_CAMPAIGN_SOURCE_COOKIES];
  }

  return $campaign_source;
}

/**
 * Creates cookie to store campaing ID value.
 *
 * @return bool
 *   TRUE in case of success, FALSE otherwise.
 */
function cemhub_campaign_tracking_generate_cookie($campaign_id) {
  $cookie_time = variable_get('cemhub_cookie_time', 0);
  if (!empty($cookie_time)) {
    $cookie_time = time() + ($cookie_time * 60);
  }

  return setcookie(CEMHUB_CAMPAIGN_SOURCE_COOKIES, $campaign_id, $cookie_time, '/');
}

/**
 * Stores the incoming campaign source code id.
 *
 * Can be stored in a cookie or in a session, depends on the admin settings.
 */
function cemhub_store_incoming_campaign_source_code_id() {
  $campaign_source_id = cemhub_get_incoming_campaign_source_code_id();
  if ($campaign_source_id) {
    if (cemhub_campaign_tracking_use_cookie_instead_session()) {
      // Store on cookie.
      cemhub_campaign_tracking_generate_cookie($campaign_source_id);
    }
    else {
      // Store on session.
      cemhub_campaign_tracking_generate_session($campaign_source_id);
    }
  }
}

/**
 * Creates a session to store the campaing ID value.
 *
 * @param string $campaign_source_id
 *   The campaign source id.
 */
function cemhub_campaign_tracking_generate_session($campaign_source_id) {
  $_SESSION[CEMHUB_CAMPAIGN_SOURCE_SESSION] = $campaign_source_id;
}

/**
 * Retrieves the incoming campaign source code id.
 *
 * @return bool
 *   Returns the cookie value or session value, NULL otherwise.
 */
function cemhub_retrieve_incoming_campaign_source_code_id() {
  $cookie_value = cemhub_get_campaign_source_code_cookie_value();

  return $cookie_value ? $cookie_value : cemhub_get_campaign_source_code_session_value();
}

/**
 * Checks if the given campaign source ID is allowed to the related webform.
 *
 * @return bool
 *   TRUE if is allowed, FALSE otherwise.
 */
function cemhub_is_an_allowed_external_campaign_source_id($campaing_source_id, $webform_node_id) {
  $webform_settings = cemhub_get_integrated_webform_settings_by_node_id($webform_node_id);
  $allowed_external_campaign_sources = list_extract_allowed_values($webform_settings->external_campaign_source, 'list_text', FALSE);

  return in_array($campaing_source_id, $allowed_external_campaign_sources);
}

/**
 * Insert new entry for the external campaign source ID.
 *
 * @param int $node_id
 *   The webform node id.
 * @param int $submission_id
 *   The webform submission id.
 * @param string $campaign_source_id
 *   The campaing source id.
 */
function cemhub_insert_campaign_source_id_entry($node_id, $submission_id, $campaign_source_id) {
  db_insert('cemhub_campaign_source')
    ->fields(array(
      'sid' => $submission_id,
      'nid' => $node_id,
      'campaign' => $campaign_source_id,
    ))
    ->execute();
}

/**
 * Returns an array with the information of the generated files.
 */
function cemhub_get_generated_flat_files() {
  return file_scan_directory(cemhub_get_files_repository_path(TRUE), '/.*\.txt|.pgp$/');
}

/**
 * Checks if is to delete the flat files.
 *
 * @return bool
 *   TRUE if is to delete, FALSE otherwise.
 */
function cemhub_is_to_delete_flat_file() {
  return variable_get('cemhub_delete_flatfile', FALSE);
}

/**
 * Deletes file from Drupal file system and the file itself.
 *
 * @param string $file_name
 *   The file name that will be removed.
 *
 * @return bool
 *   TRUE on success. FALSE otherwise.
 */
function cemhub_delete_file($file_name) {
  $successfully_deleted = FALSE;

  $file_details = cemhub_retrieve_file_details($file_name);
  if (!empty($file_details)) {
    $successfully_deleted = file_delete($file_details, TRUE);
  }
  else {
    $repository_path = cemhub_get_files_repository_path(TRUE);
    $successfully_deleted = file_unmanaged_delete($repository_path . '/' . $file_name);
  }

  if ($successfully_deleted) {

    watchdog('cemhub', 'The file @file_name was deleted successfully.', array('@file_name' => $file_name));
  }
  else {
    watchdog('cemhub', 'It was not possible to delete file @file_name, checks the Drupal file log to view more details.', array('@file_name' => $file_name));
  }

  return $successfully_deleted;
}

/**
 * Retrieves information about a given file in Drupal file system.
 *
 * @param string $filename
 *   The file name that will be checked.
 *
 * @return object
 *   An object containing the file details.
 */
function cemhub_retrieve_file_details($filename) {
  $file_info = db_select('file_managed', 'f')
    ->fields('f', array('fid'))
    ->condition('filename', $filename, '=')
    ->execute()
    ->fetchAssoc();

  $file = NULL;
  if (!empty($file_info['fid'])) {
    $file = file_load($file_info['fid']);
  }

  return $file;
}

/**
 * Adds http headers to force to download a flat file.
 *
 * @param string $file_name
 *   The file name that will be exposed to download.
 */
function cemhub_download_flat_file($file_name) {
  $file_details = cemhub_retrieve_file_details($file_name);

  if ($file_details) {
    drupal_add_http_header('Content-type', 'octet/stream');
    drupal_add_http_header('Content-disposition', "attachment; filename=" . $file_details->filename . ";");
    drupal_add_http_header('Cache-Control', 'private');
    drupal_add_http_header('Content-Length', filesize($file_details->uri));
    readfile($file_details->uri);
    drupal_exit();
  }
}

/**
 * Returns an array with the SFTP settings.
 */
function cemhub_get_sftp_settings() {
  return array(
    'address' => variable_get('cemhub_sftp_address'),
    'file_destination_repo' => variable_get('cemhub_destination_repository_files'),
    'user_name' => variable_get('cemhub_sftp_userid'),
    'passphrase' => variable_get('cemhub_sftp_pass_ppk'),
    'public_key' => variable_get('cemhub_sftp_public_key'),
    'private_key' => variable_get('cemhub_sftp_private_key'),
    'folder_destination' => variable_get('cemhub_destination_repository_files'),
  );
}

/**
 * Fetches the values from cemhub_credentials table and set in the variable.
 */
function cemhub_assign_values() {
  $creds = '';
  $vendor = 'SHP';
  $env = 'NON_PROD';
  if (variable_get('cemhub_vendor_id') != NULL) {
    $vendor = strtoupper(variable_get('cemhub_vendor_id'));
  }

  if (isset($_ENV['AH_SITE_ENVIRONMENT']) && ($_ENV['AH_SITE_ENVIRONMENT'] == 'prod')) {
    $env = 'PROD';
  }

  $creds = cemhub_fetch_values($vendor, $env);

  cemhub_set_variable_values($creds);
}

/**
 * Sets the variable values.
 */
function cemhub_set_variable_values($creds) {
  if (!empty($creds)) {
    variable_set('cemhub_sftp_address', $creds['sftp_address']);
    variable_set('cemhub_sftp_userid', $creds['sftp_username']);
    variable_set('cemhub_sftp_pass_ppk', $creds['sftp_passphrase']);
    variable_set('cemhub_sftp_public_key', $creds['sftp_pub_key_path']);
    variable_set('cemhub_sftp_private_key', $creds['sftp_private_key_path']);
    variable_set('cemhub_destination_repository_files', $creds['file_destination_repo']);
    variable_set('cemhub_repository_files', $creds['repository_files']);
    variable_set('cemhub_pgp_key_name', $creds['pgp_cem_pub_key']);
  }

}

/**
 * Fetches the values from the cemhub_credentials table based on the env.
 */
function cemhub_fetch_values($vendor, $env) {
  $result = db_select('cemhub_credentials', 'c')
    ->fields('c')
    ->condition('c.vendor', $vendor)
    ->condition('c.environment', $env)
    ->execute();
  $record = $result->fetchAssoc();

  return $record;
}

/**
 * Print the CEMHUB latest job status in JSON format.
 */
function cemhub_print_job_status() {
  $last_cron_job_status = cemhub_get_last_cron_job_status();
  if ($last_cron_job_status) {
    $cron_delayed = $last_cron_job_status['timestamp'] < strtotime("-24 hours");

    drupal_json_output($last_cron_job_status + array('delayed' => $cron_delayed));
  }
}

/**
 * Returns the last cron job status.
 */
function cemhub_get_last_cron_job_status() {
  $default = array(
    'synchronized' => FALSE,
    'error' => FALSE,
    'timestamp' => NULL,
    'message' => NULL,
  );

  return variable_get('cemhub_last_job_status', $default);
}

/**
 * Display a warning message when get a missing environment settings.
 */
function cemhub_display_missing_required_environment_settings() {
  try {
    cemhub_check_required_environment_settings();
  }
  catch (Exception $e) {
    drupal_set_message(t('@missing_environment_settings', array('@missing_environment_settings' => $e->getMessage())), 'warning');
  }
}

/**
 * Checks whether the data is encrypted or not.
 *
 * @param mixed $data
 *   The data value to be checked.
 *
 * @return bool
 *   Returns TRUE in case of success, FALSE otherwise.
 */
function cemhub_is_data_encrypted($data) {
  // Data encrypted by encrypt module are serialized.
  $data = @unserialize($data);

  if ($data === FALSE) {
    return FALSE;
  }

  if (!empty($data['method'])) {
    return TRUE;
  }

  return FALSE;
}
