<?php
/**
 * @file
 * pfizer_webstandards_hcp.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function pfizer_webstandards_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pfizer_webstandards_include_files';
  $strongarm->value = 1;
  $export['pfizer_webstandards_include_files'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pfizer_webstandards_page_name_base';
  $strongarm->value = 'title';
  $export['pfizer_webstandards_page_name_base'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pfizer_webstandards_page_name_delimiter';
  $strongarm->value = '>';
  $export['pfizer_webstandards_page_name_delimiter'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pfizer_webstandards_page_name_homepage';
  $strongarm->value = '';
  $export['pfizer_webstandards_page_name_homepage'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pfizer_webstandards_page_name_prefix';
  $strongarm->value = '';
  $export['pfizer_webstandards_page_name_prefix'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pfizer_webstandards_rebuild_cron';
  $strongarm->value = 1;
  $export['pfizer_webstandards_rebuild_cron'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pfizer_webstandards_site_section_delimiter';
  $strongarm->value = '>';
  $export['pfizer_webstandards_site_section_delimiter'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pfizer_webstandards_site_section_prefix';
  $strongarm->value = '';
  $export['pfizer_webstandards_site_section_prefix'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'sitecatalyst_track_administrator';
  $strongarm->value = 1;
  $export['sitecatalyst_track_administrator'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'sitecatalyst_track_anonymous_user';
  $strongarm->value = 1;
  $export['sitecatalyst_track_anonymous_user'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'sitecatalyst_track_authenticated_user';
  $strongarm->value = 1;
  $export['sitecatalyst_track_authenticated_user'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'sitecatalyst_track_content_editor';
  $strongarm->value = 1;
  $export['sitecatalyst_track_content_editor'] = $strongarm;

  return $export;
}
