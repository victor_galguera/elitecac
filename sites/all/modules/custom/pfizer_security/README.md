Pfizer Security
===============

## Important Information

As of Pfizer Security version 2.2.0 and later, Pfizer Security is hidden and can no longer be disabled using the Modules administration page. Pfizer Security module information is displayed on the Status page when enabled.

## Overview

This feature includes several modules that are either enforced at build time or required to obtain approval from Pfizer's security team. This feature depends on very specific versions of several modules:

* [autologout](https://www.drupal.org/project/autologout) \(Enabled on install but not a dependency.\)
* [email_confirm](https://www.drupal.org/project/email_confirm)
* [flood_control](https://www.drupal.org/project/flood_control) \(Required\)
* [globalredirect](https://www.drupal.org/project/globalredirect)
* [hacked](https://www.drupal.org/project/hacked)
* [m4032404](https://www.drupal.org/project/m4032404) \(Required\)
* [mollom](https://www.drupal.org/project/mollom)
* [paranoia](https://www.drupal.org/project/paranoia) \(Required\)
* [password_policy](https://www.drupal.org/project/password_policy) \(Required\)
* [seckit](https://www.drupal.org/project/seckit) \(Required\)
* [securepages](https://www.drupal.org/project/securepages) \(Required\)
* [security_review](https://www.drupal.org/project/security_review)
* [session_limit](https://www.drupal.org/project/session_limit) \(Required\)
* [username_enumeration_prevention](https://www.drupal.org/project/username_enumeration_prevention) \(Required\)

## Modules

For complete documentation on each project included in Pfizer Security, use the links above and visit the drupal.org project pages.

###Auto Logout
This module limits sessions to 15 minutes and auto-logs out a user after that period of time.  A JS pop up offers to allow the user to continue their session, and if not clicked, the user is logged out of the site with a message stating they have been logged out due to inactivity. *Note: This module is no longer a dependency due to compatibility issues including with webinars however it should still be enabled whenever possible for security compliance.*

###Mollom
Mollom is a web service that analyzes the quality of content posted to websites and tries to determine whether this content is spam or not. This service should replace captcha for sites with low traffic. To use this module, please request a key pair to the support team at:
DL-BT-DIGITALMARKETING@pfizer.com.

###Paranoia
The Paranoia module attempts to identify all the places that a user can evaluate PHP via Drupal's web interface and then block those. It reduces the potential impact of an attacker gaining elevated permission on a Drupal site. This module will be enforced at build time in the near feature and phpfilter will not work in Pfizer's platform.

###Secure Pages
This module is turned on and configured at build time. Site-specific configurations will be appended to Pfizer's standard configurations and SSL will be enforced for a pre-defined set of URLs.

###Security Kit
This module sets sensible defaults for http headers related to security.  You can customize this to enable sites to be loaded in an iframe for instance at the settings page located at /admin/config/system/seckit.

###Session Limit
This module prevents simultaneous logins with the same username.  The oldest session is logged off automatically if the user logs in from another location.

##Additional Features

* Adds the ability to [deny access](#deny-login-configuration) to all user paths preventing logins except for Pfizer VPN or GRV users.
* It provides some logic to render a not found page when a user hits node/123/whatever-you-type.something
* Checks file uploads for consistency between name-based MIME type and content-based MIME type.
* The menu item 'taxonomy/autocomplete' is unset unless you set the pfizer_security_taxonomy_autocomplete variable to TRUE. Add your own access control to restrict access when overriding this as Drupal permits access if you have the "Access published content" permission which is generally permitted by the anonymous role.

### Deny Login Configuration

As of version 1.6.0, the deny login functionality has been enhanced. It now supports blocking Drupal user paths while still allowing GRV users to login and view their profile at `/user` and Pfizer VPN users to login to the site via the `/user` path.

To enable, add the following to the local.settings.php file:

```
$conf['deny_login'] = TRUE;
```

In the Jenkins configuration "Authentication Parameters" section, ensure that "Allow user login into Drupal site" is checked and run a build.
