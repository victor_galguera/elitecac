# Pfizer Deploy 4.x

This module includes everything you will need to use Deploy along with Deployment Tools (formerly WF Tools). In addition, it supports standard content deployment using self service content management tools and automatic Services endpoint creation.

As of 3.x, much of the functionality in Pfizer Deploy has moved to [Deploy Add-on](https://www.drupal.org/project/deploy_addon) which is included as part of the Pfizer Deploy package.

As of 4.x, significant UX enhancements were made as part of the Self Service 2.x initiative.

The Deploy Add-on Services module can be enabled to provide automatic service endpoint creation with support for all discovered entities. Alternatively, "HCP: Deploy" included with the HCP Portal sets up the services endpoint for HCP Portal content deployments from HCP 2.3.x+ versions.

For installation instructions and other documentation, refer to [Zendesk](https://help.digitalpfizer.com). Please ensure you read the encrypting password instructions below: this should be done on all Pfizer Deploy installations to protect user credentials.

## TL;DR Install

* Ensure that you are using the latest platform version.
* Ensure that all environments' codebase and databases have an initial sync; code is merged and databases are cloned upstream or downstream.
* Enable Deploy Add-on Content on content editing environments.
* Enable Deploy Add-on Services on endpoints. This module will automatically create and configure Services endpoints.
* Configure the Deploy Add-on Push to Next configuration using the example $conf settings below or the admin form \(admin/config/content/deploy_addon_next\).

## TL;DR Configuration on HCP Portal

* Ensure that you are using the latest platform (TBD) and HCP Portal 2.3+ sub-platform as it ships with the HCP: Deploy module.
* Ensure that all environments' codebase and databases have an initial sync; code is merged and databases are cloned upstream or downstream.
* Enable the Deploy Add-on Content module on all content editing environments.
* Enable the HCP: Deploy (or Deploy Add-on Services) on endpoints.
* Configure Pfizer Deploy credentials for testing purposes using the admin form \(admin/config/content/deploy_addon_next\). Once this has been tested, add appropriate $conf variables to local.settings.php with Acquia environments taken into account.

## Example $conf variables in local.settings.php

```
// Disable mixed mode sessions.
$conf['https'] = FALSE;

// Set push to next creds, this can be in switch below if different creds per environment.
$conf['deploy_addon_next_user'] = 'username';

// This should be entered manually on the Push to Next configuration page at admin/config/content/deploy_addon_next.
$conf['deploy_addon_next_pass'] = 'password';

// If you are entering the password in plain text above, make sure the encryption false flag is set.
$conf['deploy_addon_next_pass_encrypt'] = FALSE;

if (isset($_ENV['AH_SITE_ENVIRONMENT'])) {
  switch ($_ENV['AH_SITE_ENVIRONMENT']) {
    // Content editing environment.
    case 'dev':

      // Set destination services endpoint where we deploy entities.
      $conf['deploy_addon_next_url'] = 'https://subscriptionstg.prod.acquia-sites.com/wf';

      // Enable debug on dev.
      $conf['deploy_addon_debug'] = 1;

      break;
    // Content staging and preview environment.
    case 'test':

      // Set destination services endpoint where we deploy entities.
      $conf['deploy_addon_next_url'] = 'https://subscription.prod.acquia-sites.com/wf';

      // Set the Services endpoint path when using Deploy Add-on Services.
      $conf['deploy_addon_services_path'] = 'wf';

      // Block users belonging to roles that have to be deployed but we don't want active.
      $conf['deploy_addon_user_roles_block'] = array(
        'Content Editor',
        'Webinar Content Editor',
        'Presentation Editor',
      );

      break;
    // Live site.
    case 'prod':

      // Set the Services endpoint path when using Deploy Add-on Services.
      $conf['deploy_addon_services_path'] = 'wf';

      break;
  }
}
```

## Encrypting the Push to Next password

If you are using the Deploy Add-on Push to Next module, the password can be stored as a variable in the database. This now must be encrypted if you wish to store it in this fashion:

1. Ensure the Pfizer Encrypt module is enabled.
2. Go to the Push to Next configuration page at admin/config/content/deploy_addon_next.
3. Ensure all the configuration is entered and *re-type the password*.
4. Save your configuration and start deploying!

## Mixed Mode Sessions

Mixed mode sessions must be disabled or you will get an error when trying to deploy content similar to:

```
DeployServiceException: Service error: 401 Unauthorized : CSRF validation failed in DeployServiceRest->httpRequest()
```

To ensure mixed mode sessions are disabled, ensure the following is in the local.settings.php file:

```
$conf['https'] = FALSE;
```
