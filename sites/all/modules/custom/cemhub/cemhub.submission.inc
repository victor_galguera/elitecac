<?php

/**
 * @file
 * Responsible for generating, encrypting and sending the file to CEMHUB.
 */

/**
 * Initiate submission process to CEMHUB.
 *
 * @throws Exception
 */
function cemhub_process_submissions($manual = FALSE) {
  module_load_include('inc', 'cemhub', 'cemhub.mail');
  $file_name = cemhub_get_new_file_name();

  try {
    cemhub_check_required_environment_settings();
    $status = cemhub_submission_bootstrap($file_name, $manual);
  }
  catch (Exception $e) {
    $error_message = $e->getMessage();
    watchdog('cemhub', $error_message);

    $status = array(
      'synchronized' => FALSE,
      'message' => $error_message,
      'error' => TRUE,
    );

    if (!$manual) {
      cemhub_send_failure_job_email($error_message);
    }
  }

  // Deletes the flat files at the end of any operations.
  cemhub_delete_flat_files($file_name);

  return $status;
}

/**
 * Handler function to process the submissions.
 *
 * @param string $file_name
 *   The file name that will be saved and processed.
 * @param bool $manual
 *   Defines wether is manual process or a cron process.
 *
 * @throws Exception
 *
 * @return array
 *   An array describing the job status.
 */
function cemhub_submission_bootstrap($file_name, $manual = FALSE) {
  // Sets default Job status as a success.
  $job_status = array(
    'synchronized' => TRUE,
    'error' => FALSE,
    'message' => NULL,
  );

  // Gets integrated webforms to treat.
  $integrated_webforms = cemhub_get_integrated_activated_webforms_settings();
  if (empty($integrated_webforms)) {
    throw new Exception('The file was not generated because there is no form selected to be integrated with CEMHUB.');
  }

  cemhub_assign_values();

  module_load_include('inc', 'cemhub', 'cemhub.mail');
  $vendor_id = variable_get('cemhub_vendor_id');
  $record_count = 0;
  $total_lines = 0;
  $file_lines = '';
  $last_submissions_by_webform = array();

  foreach ($integrated_webforms as $webform) {
    $last_submissions_by_webform[$webform->nid]['latest'] = cemhub_get_last_webform_submission_id($webform->nid, $webform->last_submission_id);
    $last_submissions_by_webform[$webform->nid]['prev'] = $webform->last_submission_id;

    // WARNING: $record_count and $total_lines are passed by reference below.
    $file_lines .= cemhub_get_file_lines_from_webform_submissions($webform, $vendor_id, $total_lines, $record_count);
  }

  if (!empty($file_lines)) {
    $file_data = cemhub_get_file_contents_with_trailer_records($vendor_id, $total_lines, $record_count, $file_lines);
    $inbound_folder_path = cemhub_get_files_repository_path(TRUE) . '/';

    $saved_file = file_save_data($file_data, $inbound_folder_path . $file_name . '.txt', FILE_EXISTS_REPLACE);
    if ($saved_file) {
      watchdog('cemhub', 'The file "@filename.txt" was successfully generated in the folder: @folder', array('@filename' => $file_name, '@folder' => $inbound_folder_path));

      if (cemhub_send_file($file_name)) {
        cemhub_update_config_with_last_submission_ids($last_submissions_by_webform);
        cemhub_delete_submissions($last_submissions_by_webform);

        if (!$manual) {
          cemhub_send_success_job_email();
        }
      }
    }
    else {
      throw new Exception('The file "' . $file_name . '.txt" can not be generated, make sure the permissions and folders are set properly. Folder: ' . $inbound_folder_path);
    }
  }
  else {
    // It is not a error, just a warning.
    $message = 'The file was not generated and sent because does not have data to integrate.';
    watchdog('cemhub', $message);

    if (!$manual) {
      cemhub_send_no_submissions_job_email();
    }

    // Sets Job status in case of no data to integrate.
    $job_status = array(
      'synchronized' => FALSE,
      'error' => FALSE,
      'message' => $message,
    );
  }

  return $job_status;
}

/**
 * Returns the file contents.
 *
 * Given a string containing the records to send to CEMHUB, return it with the
 * trailer records (first and last lines) expected by the CEMHUB parser.
 *
 * @param string $vendor_id
 *   The CEMHUB vendor id.
 * @param int $total_lines
 *   The number of lines that the file have.
 * @param int $record_count
 *   The number of records that the integration have.
 * @param string $file_lines
 *   ALL the lines content of the file.
 *
 * @return string
 *   Returns the final file content.
 */
function cemhub_get_file_contents_with_trailer_records($vendor_id, $total_lines, $record_count, $file_lines) {
  $trailer_record = cemhub_get_trailer_file_record($vendor_id, $total_lines, $record_count);
  $file_content = $trailer_record . $file_lines . $trailer_record;

  return $file_content;
}

/**
 * Builds the trailer record used to wrap the registries sent to CEMHUB.
 *
 * @param string $vendor_id
 *   The CEMHUB vendor id.
 * @param int $total_lines
 *   The number of lines that the file have.
 * @param int $record_count
 *   The number of records that the integration have.
 *
 * @return string
 *   The file trailer records.
 */
function cemhub_get_trailer_file_record($vendor_id, $total_lines, $record_count) {
  $trailer_record = $vendor_id . "|" . $total_lines . "|" . $record_count . "|" . date('Ymd') . "|" . date('His') . "|\r\n";

  return $trailer_record;
}

/**
 * Returns the file lines related to a submission.
 *
 * @param object $webform
 *   Webform object.
 * @param string $vendor_id
 *   The CEMHUB vendor id.
 * @param int $total_lines
 *   The number of lines that the file have.
 * @param int $record_count
 *   The number of records that the integration have.
 */
function cemhub_get_file_lines_from_webform_submissions($webform, $vendor_id, &$total_lines, &$record_count) {
  $root_uii = variable_get('cemhub_organization_code');
  $file_lines = '';

  $webform_submissions = cemhub_get_webform_submissions_since_last_submission_id($webform->nid, $webform->last_submission_id);
  if (empty($webform_submissions)) {
    return $file_lines;
  }

  // Check if we are to remove the duplicate data.
  $remove_duplicate = isset($webform->remove_duplicate) ? $webform->remove_duplicate : 0;

  foreach ($webform_submissions as $webform_submission) {
    if ($remove_duplicate && $webform_submission->cemhub_status == 1) {
      continue;
    }

    $submission_fields = cemhub_get_submission_fields($webform->nid, $webform_submission->sid);
    if ($submission_fields) {
      // Updated by reference. Used outside.
      $record_count++;

      $submission_fields['uii'] = cemhub_get_uii($root_uii, $record_count);

      // Call hook_cemhub_data_alter.
      foreach (module_implements('cemhub_data_alter') as $module) {
        call_user_func_array($module . '_' . 'cemhub_data_alter', array(
          &$submission_fields,
          $webform->nid, $webform_submission->sid,
        ));
      }

      foreach ($submission_fields['components'] as $fields) {
        foreach ($fields as $row) {
          // Can not be sent to an empty answer.
          if (empty($row['data'])) {
            continue;
          }

          $campaign_src = (!empty($row['campaign'])) ? $row['campaign'] : $webform->campaign_source;

          // Updated by reference. Used outside.
          $total_lines++;

          $file_lines .= cemhub_build_file_line(
            $vendor_id,
            date("Ymd", $webform_submission->submitted),
            trim($campaign_src),
            $submission_fields['uii'],
            $webform->survey_id,
            $row['question_code'],
            $row['answer_code'],
            str_replace(array("\r\n", "\n", "\r"), ' ', $row['data'])
          );
        }
      }
    }
  }

  return $file_lines;
}

/**
 * Update webform submission table with record status on cemhub submision.
 *
 * @param int $webform_nid
 *   Webform node ID.
 * @param int $last_submission_id
 *   Webform submission ID.
 */
function cemhub_update_submission_status($webform_nid, $last_submission_id) {
  if ($webform_nid && $last_submission_id['latest'] && $last_submission_id['prev']) {
    db_update('webform_submissions')
      ->fields(array('cemhub_status' => '1'))
      ->condition('sid', array($last_submission_id['prev'], $last_submission_id['latest']), 'BETWEEN')
      ->condition('nid', $webform_nid)
      ->execute();
  }
}

/**
 * Returns the submission fields from a given webform and a submission.
 *
 * @param int $webform_nid
 *   Webform node ID.
 * @param int $webform_submission_id
 *   Webform submission ID.
 *
 * @return array
 *   An array containing the submission fields.
 */
function cemhub_get_submission_fields($webform_nid, $webform_submission_id) {
  $rows = array();

  $submitted_fields_result_set = cemhub_get_webform_submission_fields_as_result_set($webform_nid, $webform_submission_id);
  while ($submitted_field = $submitted_fields_result_set->fetchAssoc()) {
    // Decrypts possible encrypted data.
    if (isset($submitted_field['extra']) && cemhub_is_data_encrypted($submitted_field['data'])) {
      $submitted_field['data'] = decrypt($submitted_field['data'], array('base64' => TRUE));
    }

    $answer_details = cemhub_get_answer_details($submitted_field);
    $submitted_field['answer_code'] = $answer_details['code'];
    $submitted_field['data'] = $answer_details['value'];

    $field_key = $submitted_field['form_key'];
    unset($submitted_field['form_key']);
    $rows['components'][$field_key][] = $submitted_field;
  }

  return $rows;
}

/**
 * Returns the details of a answer.
 *
 * @param array $submitted_field
 *   The given submitted field array.
 *
 * @return array
 *   An array containing the answer code and value.
 */
function cemhub_get_answer_details($submitted_field) {
  $answer_value = '';
  $answer_code = '';

  $answer_map = list_extract_allowed_values($submitted_field['answer_code'], 'list_text', FALSE);

  $is_select_field = count($answer_map) > 1;
  if ($is_select_field) {
    if (empty($submitted_field['data'])) {
      // Defines default value if no data was submitted.
      $answer_value = array_keys($answer_map);
      $answer_value = $answer_value[0];
      $answer_code = $answer_map[$answer_value];
    }
    else {
      // Sets the value selected according to the webform options.
      $answer_value = $submitted_field['data'];
      $answer_code = $answer_map[$submitted_field['data']];
    }
  }
  else {
    $answer_value = $submitted_field['data'];
    $answer_code = array_shift($answer_map);
  }

  $answer_details = array(
    'code' => $answer_code,
    'value' => str_replace('|', '', $answer_value),
  );

  return $answer_details;
}

/**
 * Returns the last submission id for the given webform nid.
 *
 * @param int $nid
 *   Webform node id.
 * @param int $latest_submission
 *   The latest webform submission id.
 *
 * @return int
 *   The latest submission id.
 */
function cemhub_get_last_webform_submission_id($nid, $latest_submission) {
  $limit = cemhub_get_limit_submissions_per_day($nid);

  $query = <<<QUERY
    SELECT COALESCE(MAX(sid), -1) AS last_sid
      FROM (
        SELECT sid
          FROM {webform_submissions}
         WHERE nid = :nid
           AND sid > :sid
         LIMIT $limit
      ) AS total
QUERY;

  $args = array(
    ':nid' => $nid,
    ':sid' => $latest_submission,
  );

  $last_submission = db_query($query, $args)->fetchField();

  if ($last_submission == '-1') {
    $last_submission = $latest_submission;
  }

  return $last_submission;
}

/**
 * Returns the file name in the format expected by cemhub.
 */
function cemhub_get_new_file_name() {
  $file_sequence = variable_get('cemhub_file_sequence');
  $file_vendor_code = variable_get('cemhub_vendor_id');

  $file_name = CEMHUB_FILE_CLIENT_CODE . '_' . $file_vendor_code . '_' . CEMHUB_FILE_INDICATOR . '_' . date('Ymd') . '_' . $file_sequence;

  return $file_name;
}

/**
 * Returns a formatted file line expected by cemhub.
 *
 * @param string $vendor_id
 *   The CEMHUB vendor id.
 * @param int $submission_timestamp
 *   Submission time. Must have the format Ymd.
 * @param string $campaign_source
 *   The CEMHUB campaign source code.
 * @param int $uii
 *   The generated CEMHUB UII.
 * @param string $survey_id
 *   The CEMHUB survey id.
 * @param string $question_code
 *   The question code of the field.
 * @param string $answer_code
 *   The answer code of the field.
 * @param string $value
 *   The value of the field.
 *
 * @return string
 *   The file line itself.
 */
function cemhub_build_file_line($vendor_id, $submission_timestamp, $campaign_source, $uii, $survey_id, $question_code, $answer_code, $value) {
  $line = $vendor_id . '|' .
          $submission_timestamp . '|' .
          trim($campaign_source) . '|' .
          $uii . '|' .
          $survey_id . '|' .
          $question_code . '|' .
          $answer_code . '|' .
          $value . "|\r\n";

  return $line;
}

/**
 * Generates a new CEMHUB UII ID.
 *
 * @param string $root_uii
 *   The CEMHUB root UII.
 * @param int $record_count
 *   The current record count.
 *
 * @return string
 *   The generated CEMHUB UII ID.
 */
function cemhub_get_uii($root_uii, $record_count) {
  $uii = $root_uii . date('mdYHis') . str_pad($record_count, 4, '0', STR_PAD_LEFT);

  return $uii;
}

/**
 * Handler function to encrypts the generated file and send to CEMHUB server.
 *
 * @param string $file_name
 *   The name of the file that will be sent.
 *
 * @return bool
 *   TRUE in case of success, FALSE otherwise.
 */
function cemhub_send_file($file_name) {
  $is_successful = FALSE;

  $encrypted_file = cemhub_encrypt_file($file_name . '.txt');
  if ($encrypted_file) {
    if (cemhub_sftp_send_file($encrypted_file->filename)) {
      $is_successful = TRUE;
      watchdog('cemhub', 'The encrypted file was successfully sent to CEMHUB: @filename', array('@filename' => $encrypted_file->filename));

    }
    else {
      throw new Exception('Failed to connect to CEMHUB SFTP.');
    }
  }

  return $is_successful;
}

/**
 * Sends the file generated file to CEMHUB using a SFTP connection.
 *
 * @param string $file_name
 *   The file name that will be sent.
 *
 * @return bool
 *   TRUE in case of success, FALSE otherwise.
 */
function cemhub_sftp_send_file($file_name) {
  $is_successful = FALSE;

  $resource = cemhub_ssh2_create_connection();
  if ($resource) {
    $sftp_stream = cemhub_open_sftp_stream($resource, $file_name);

    try {
      cemhub_sftp_write_file($sftp_stream, $file_name);
      $is_successful = TRUE;
    }
    catch (Exception $e) {
      watchdog('cemhub', 'Exception: @msg ', array("@msg" => $e->getMessage()));
    }

    cemhub_close_sftp_stream($sftp_stream);
  }

  return $is_successful;
}

/**
 * Write the file in remote sftp server.
 *
 * @param resource $sftp_stream
 *   A sftp stream session.
 * @param string $file_name
 *   The generated file name.
 *
 * @throws Exception
 */
function cemhub_sftp_write_file($sftp_stream, $file_name) {
  $source_file_path = cemhub_get_files_repository_path(TRUE) . '/' . $file_name;

  if (!$sftp_stream) {
    throw new Exception('Could not open remote file.');
  }

  $data_to_send = @file_get_contents($source_file_path);
  if ($data_to_send === FALSE) {
    throw new Exception("Could not open local file: $source_file_path.");
  }

  if (@fwrite($sftp_stream, $data_to_send) === FALSE) {
    throw new Exception("Could not send data from file: $source_file_path.");
  }
}

/**
 * Closes the opened SFTP file pointer.
 *
 * @param resource $stream
 *   A sftp stream session.
 */
function cemhub_close_sftp_stream($stream) {
  @fclose($stream);
}

/**
 * Establish a connection to a remote SSH server.
 *
 * @return bool
 *   TRUE in case of success, FALSE otherwise.
 */
function cemhub_ssh2_create_connection() {
  $sftp_settings = cemhub_get_sftp_settings();
  $resource = @ssh2_connect($sftp_settings['address'], 22);

  if ($resource && !cemhub_ssh2_auth_pubkey_file($resource)) {
    $resource = FALSE;
  }

  return $resource;
}

/**
 * Authenticates using a public key.
 */
function cemhub_ssh2_auth_pubkey_file($resource) {
  $sftp_settings = cemhub_get_sftp_settings();
  $private_path = cemhub_get_private_file_system_path(TRUE);

  return @ssh2_auth_pubkey_file(
    $resource,
    $sftp_settings['user_name'],
    $private_path . '/' . $sftp_settings['public_key'],
    $private_path . '/' . $sftp_settings['private_key'],
    $sftp_settings['passphrase']
  );
}

/**
 * Request the SFTP subsystem from an already connected SSH2 server.
 *
 * @param resource $resource
 *   An SSH connection link identifier, obtained from a call to ssh2_connect().
 *
 * @see cemhub_ssh2_create_connection
 */
function cemhub_initialize_sftp_subsystem($resource) {
  return @ssh2_sftp($resource);
}

/**
 * Binds a named resource, specified by filename, to a stream.
 */
function cemhub_open_sftp_stream($resource, $file_name) {
  $sftp_settings = cemhub_get_sftp_settings();
  $sftp = cemhub_initialize_sftp_subsystem($resource);

  return @fopen('ssh2.sftp://' . intval($sftp) . '/' . $sftp_settings['folder_destination'] . '/' . $file_name, 'w');
}

/**
 * Saves the latest webform submissions to CEMHUB integration.
 *
 * @param array $last_submissions_by_webform
 *   An array containing the lastest webform submission ids.
 */
function cemhub_update_config_with_last_submission_ids($last_submissions_by_webform) {
  foreach ($last_submissions_by_webform as $nid => $last_submission_id) {
    $config = array(
      'nid' => $nid,
      'last_submission_id' => $last_submission_id['latest'],
    );
    cemhub_update_webform_config($config);

    // Update cemhub submission status.
    cemhub_update_submission_status($nid, $last_submission_id);
  }
}

/**
 * Deletes webform submissions.
 */
function cemhub_delete_submissions($submissions) {
  if (empty($submissions) || !variable_get('cemhub_delete_entries', FALSE)) {
    watchdog('cemhub', 'Submissions were not deleted. Please verify admin interface settings for CEM HUB.');
    return FALSE;
  }

  $tables = array(
    'webform_submitted_data',
    'webform_submissions',
    'cemhub_campaign_source',
  );
  foreach ($submissions as $nid => $last_submission) {
    foreach ($tables as $table) {
      db_delete($table)->condition('sid', $last_submission['latest'], '<=')->condition('nid', $nid)->execute();
    }
  }

  watchdog('cemhub', 'The submissions were successfully excluded');
}

/**
 * Deletes flat files with extensions .txt and .txt.pgp.
 *
 * @param string $file_name
 *   The file will be removed without extension.
 */
function cemhub_delete_flat_files($file_name) {
  if (cemhub_is_to_delete_flat_file()) {
    $extensions = array(
      '.txt',
      '.txt.pgp',
    );

    foreach ($extensions as $extension) {
      cemhub_delete_file($file_name . $extension);
    }
  }
  else {
    watchdog('cemhub', 'The flat files were not excluded. Please verify admin interface settings for CEM HUB.');
  }
}

/**
 * Encrypts the given file using PGP.
 *
 * @param string $file_name
 *   The generated file name to be encrypted.
 *
 * @return bool
 *   The file details in case of success, FALSE otherwise.
 */
function cemhub_encrypt_file($file_name) {
  $file_path = cemhub_get_files_repository_path(TRUE) . '/' . $file_name;

  $encrypted_content = NULL;
  $gnupg = cemhub_gnupg_create_new_instance();
  if ($gnupg) {
    $encrypted_content = $gnupg->encrypt(file_get_contents($file_path));
  }

  $encrypted_file = NULL;
  if ($encrypted_content) {
    $encrypted_file = file_save_data($encrypted_content, $file_path . '.pgp', FILE_EXISTS_REPLACE);
  }

  if ($encrypted_file) {
    watchdog('cemhub', 'The encrypted file was successfully generated.');
  }
  else {
    throw new Exception('Unable to encrypt the file.');
  }

  return $encrypted_file;
}

/**
 * Returns a new pgp instantiated object.
 */
function cemhub_gnupg_create_new_instance() {
  $gnupg = NULL;

  if (extension_loaded(CEMHUB_EXTENSION_GNUPG_NAME)) {
    // Fix temporary files creation inside module dir.
    putenv('GNUPGHOME=' . variable_get('file_temporary_path', '/tmp'));

    $gnupg = new gnupg();

    // Set errormode to PHP WARNING rather than raise an exception.
    $gnupg->seterrormode(GNUPG_ERROR_WARNING);

    // Import key at every encryption to not rely on GPG.
    $pubkey_info = $gnupg->import(cemhub_get_pgp_public_key_content());
    $gnupg->addencryptkey($pubkey_info['fingerprint']);

    // setarmor=1 will include header information at the content.
    $gnupg->setarmor(0);
  }

  return $gnupg;
}

/**
 * Returns the pgp public key content.
 */
function cemhub_get_pgp_public_key_content() {
  $public_key = cemhub_get_private_file_system_path() . '/' . variable_get('cemhub_pgp_key_name');

  return file_get_contents($public_key);
}

/**
 * Returns all the webform submissions since the last submission.
 *
 * @param int $webform_nid
 *   Webform node id.
 * @param int $last_submission_id
 *   The lastest submission ID.
 *
 * @return array
 *   An array containing the webform submissions.
 */
function cemhub_get_webform_submissions_since_last_submission_id($webform_nid, $last_submission_id) {
  $limit_per_day = cemhub_get_limit_submissions_per_day($webform_nid);

  $webform_submissions = db_select('webform_submissions', 's')
    ->fields('s', array('nid', 'sid', 'submitted', 'cemhub_status'))
    ->condition('s.sid', $last_submission_id, '>')
    ->condition('s.nid', $webform_nid, '=')
    ->range(0, $limit_per_day)
    ->execute()
    ->fetchAll();

  return $webform_submissions;
}

/**
 * Returns all the data of a webform submission.
 *
 * @param int $webform_nid
 *   The webform node id.
 * @param int $submission_id
 *   The webform submission id.
 *
 * @return array
 *   An array containig the webform submissions data.
 */
function cemhub_get_webform_submission_fields_as_result_set($webform_nid, $submission_id) {
  $query = db_select('webform_submitted_data', 'w');
  $query->leftJoin('cemhub_campaign_source', 's', 's.sid = w.sid');
  $query->join('cemhub_fields', 'f', 'f.cid = w.cid');
  $query->join('webform_component', 'c', 'c.cid = w.cid AND c.nid = f.nid');
  $query->fields('w', array('data'))
    ->fields('s', array('campaign'))
    ->fields('f', array('question_code', 'answer_code'))
    ->fields('c', array('form_key'))
    ->fields('c', array('extra'))
    ->condition('f.nid', $webform_nid, '=')
    ->condition('f.active', 1, '=')
    ->condition('w.sid', $submission_id, '=');

  $result_set = $query->execute();

  return $result_set;
}
