<?php

/**
 * @file
 * Contains pfizer-uuid-duplicate command implementation.
 */

/**
 * Implements hook_drush_command().
 */
function pfizer_encrypt_drush_command() {

  $items['pfizer-encrypt-webform-check'] = array(
    'description' => 'Checks for webform components that should be encrypted.',
    'aliases' => array('pewc'),
    'outputformat' => array(
      'default' => 'key-value',
      'pipe-format' => 'json',
      'label' => 'Webform Encryption Check',
      'output-data-type' => 'format-single',
    ),
  );

  return $items;
}

/**
 * Callback for the pfizer-encrypt-webform-check command.
 */
function drush_pfizer_encrypt_webform_check() {
  $status = FALSE;

  // Exit check and return default status if Webform module is not enabled.
  if (!module_exists('webform')) {
    return $status;
  }

  $excluded_types = array('fieldset', 'file', 'markup', 'pagebreak');
  $fields = array('name', 'type', 'extra');

  $query = db_select('webform_component', 'wc')
    ->fields('wc', $fields)
    ->condition('type', $excluded_types, 'NOT IN');
  $results = $query->execute();

  foreach ($results as $result) {
    $extra = @unserialize($result->extra);
    if (empty($extra['encrypt'])) {
      $status = TRUE;
      break;
    }
  }

  return $status;
}
