<?php
/**
 * @file
 * Include file to handle data access - moved into own ic so can be portable.
 */

/**
 * Function for use with usort to sort by weight.
 *
 * @param object $a
 *   ctools crud object
 * @param object $b
 *   ctools crud object
 *
 * @return int
 *   usort compare value
 */
function _pfizer_webstandards_data_access_table_compare($a, $b) {
  return $a->weight > $b->weight;
}

/**
 * Return a list of custom links configured in the system.
 *
 * @return array
 *   The custom links table data
 */
function pfizer_webstandards_data_access_customlinks_get_data() {
  ctools_include('export');

  // Note - $reset is set to true to force a reload of data.
  $values = ctools_export_crud_load_all('pfizer_webstandards_customlink', TRUE);

  // Sort by weight:
  usort($values, '_pfizer_webstandards_data_access_table_compare');

  return $values;
}

/**
 * Return a list of custom links configured in the system.
 *
 * @return array
 *   The custom links table data
 */
function pfizer_webstandards_data_access_url_get_data() {
  ctools_include('export');

  $values = ctools_export_crud_load_all('pfizer_webstandards_url', TRUE);

  // Sort by weight:
  usort($values, '_pfizer_webstandards_data_access_table_compare');

  return $values;
}
