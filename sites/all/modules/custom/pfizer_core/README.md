# Pfizer Core

Pfizer Core is a mandatory module on the Pfizer Platform as of 2.5.6 and 2.4.28. It is included a dependency of [Pfizer Profile](https://github.com/pfizer/pfizer_profile), Pfizer's installation profile.

On the 7.x platform, all this module does is report the platform version and allow platform specific functionality such as hook_update_N implementations.
