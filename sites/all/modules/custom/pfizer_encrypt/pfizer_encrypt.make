; Drush make file for pfizer_encrypt modules.

api = 2
core = "7.x"

projects[] = drupal

projects[encrypt][subdir] = "contrib"
projects[encrypt][version] = "2.3"
; Legacy Pfizer Encrypt support.
projects[encrypt][patch][i1727930][url] = "https://s3-us-west-2.amazonaws.com/drupal-platform/patches/encrypt-bc-encrypt-metadata-key-name-support.patch"
projects[encrypt][patch][i1727930][md5] = "ecd44a832f999d946a6fdf8000b8bbf3"

projects[field_encrypt][subdir] = "contrib"
projects[field_encrypt][version] = "1.0-beta2"

projects[real_aes][subdir] = "contrib"
projects[real_aes][version] = "1.1"

projects[webform_encrypt][subdir] = "contrib"
projects[webform_encrypt][version] = "1.1"

; Webform 4.x support - https://www.drupal.org/node/2325353#comment-10686874
projects[webform_encrypt][patch][i2325353][url] = "https://www.drupal.org/files/issues/webform_encrypt-2325353-reroll.patch"
projects[webform_encrypt][patch][i2325353][md5] = "6733c5a3a364482d38c9469d22164231"

; Secure PHP wrapper library for Real AES supporting OpenSSL based symmetric-key encryption.
libraries[php-encryption][download][type] = "git"
libraries[php-encryption][download][url] = "https://github.com/defuse/php-encryption.git"
libraries[php-encryption][download][branch] = "master"
; Locked to most recent commit from August 4, 2015.
libraries[php-encryption][download][revision] = "399fc4c4798b301681888c2608b0ddd060d477da"
