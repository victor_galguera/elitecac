if (typeof(Storage) !== 'undefined') {
  // get the existing session storage data
  var data = sessionStorage.getItem('pfizer wsd');

  if (data == null) {
    data = {};
  }
  else {
    data = JSON.parse(data);
  }

  // track unique page hits
  var host = window.location.hostname;
  var path = window.location.pathname;

  if (typeof data['unique page hits'] == 'undefined') {
    data['unique page hits'] = {};
  }

  var unique_page_id = host + path;
  data['unique page hits'][unique_page_id] = unique_page_id;

  // track all page hits
  if (typeof data['page hits'] == 'undefined') {
    data['page hits'] = [];
  }

  var date = new Date();
  var time = "/" + date.getDate()+ (date.getMonth() + 1) + date.getFullYear() + date.getHours()
               + date.getMinutes() + date.getSeconds() + date.getMilliseconds();

  data['page hits'].push(unique_page_id + time);

  // save session storage data
  sessionStorage.setItem('pfizer wsd', JSON.stringify(data));

  // now update the fields data with new values
  if (typeof Drupal.settings.pfizer_webstandards_detail !== 'undefined') {
    // total number of pages viewed
    var total_pages = 0;
    if (!wsd_contains('prop46')) {
      total_pages = jQuery(data['page hits']).size();

      Drupal.settings.pfizer_webstandards_detail.push({
        name: 'prop46', // EDETAIL_NUMBER_PAGES_VIEWED
        value: total_pages
      });
    }

    // all three items contain number of unique pages viewed
    var unique_pages = 0;
    for (var key in data['unique page hits']) {
      if (data['unique page hits'].hasOwnProperty(key)) {
        unique_pages++;
      }
    }

    if (!wsd_contains('prop47') || !wsd_contains('prop49') || !wsd_contains('event34')) {
      Drupal.settings.pfizer_webstandards_detail.push({
        name: 'prop47', // EDETAIL_NUMBER_UNIQUE_PAGES_VIEWED
        value: unique_pages
      });

      Drupal.settings.pfizer_webstandards_detail.push({
        name: 'prop49', // WEB_NUMBER_UNIQUE_PAGES_VIEWED
        value: unique_pages
      });

      Drupal.settings.pfizer_webstandards_detail.push({
        name: 'event34', // WEB_NUMBER_UNIQUE_PAGES_VIEWED
        value: unique_pages
      });
    }
  }
}