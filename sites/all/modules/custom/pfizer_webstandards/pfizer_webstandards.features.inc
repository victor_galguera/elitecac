<?php
/**
 * @file
 * pfizer_webstandards.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pfizer_webstandards_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "pfizer_webstandards" && $api == "default_pfizer_webstandards_customlinks") {
    return array("version" => "1");
  }
  if ($module == "pfizer_webstandards" && $api == "default_pfizer_webstandards_urls") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
