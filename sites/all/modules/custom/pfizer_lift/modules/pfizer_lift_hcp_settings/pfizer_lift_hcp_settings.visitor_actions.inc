<?php
/**
 * @file
 * pfizer_lift_hcp_settings.visitor_actions.inc
 */

/**
 * Implements hook_visitor_actions_default_actions().
 */
function pfizer_lift_hcp_settings_visitor_actions_default_actions() {
  $export = array();

  $visitor_action = new stdClass();
  $visitor_action->disabled = FALSE; /* Edit this to true to make a default visitor_action disabled initially */
  $visitor_action->api_version = 1;
  $visitor_action->machine_name = 'marketo_form_submission';
  $visitor_action->label = 'Marketo Form Submission';
  $visitor_action->plugin = 'form';
  $visitor_action->event = 'submit_client';
  $visitor_action->identifier = 'marketo*';
  $visitor_action->pages = '';
  $visitor_action->client_side = 1;
  $visitor_action->module = 'visitor_actions';
  $visitor_action->data = array();
  $visitor_action->limited_use = FALSE;
  $export['marketo_form_submission'] = $visitor_action;

  return $export;
}
