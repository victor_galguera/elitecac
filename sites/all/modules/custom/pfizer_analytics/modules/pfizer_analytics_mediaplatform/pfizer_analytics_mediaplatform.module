<?php

/*----------------------------------------------------------------------------
      SETUP & CONFIG
----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
      DRUPAL HOOKS
----------------------------------------------------------------------------*/

/**
 * Implements hook_entity_view_alter();
 */
function pfizer_analytics_mediaplatform_entity_view_alter(&$build, $type) {
  // we're only interested in nodes ...
  if ($type !== 'node') {
    return;
  }

  // ... of live | onDemand types ...
  $entity = $build['#node'];
  if (!in_array($entity->type, pfizer_hcp2_webinar_webinar_types())) {
    return;
  }

  // ... and viewing actual node, not a teaser
  if (!in_array($build['#view_mode'], array(
      'pre_event', 'live', 'paused', 'post_event', 'cancelled', // old Portal
      'full' // new Portal
    ))) {
    return;
  }

  $have_mediaplatform = FALSE;

  // pre-2.7 Portal
  if ($field = field_get_items('node', $entity, 'hcp_webinar')) {
    if (isset($field[0]['config']['mediaplatform_alias'])) {
      $have_mediaplatform = (bool) ($field[0]['config']['mediaplatform_alias']);
    }
  }
  // 2.7+ Portal
  elseif ($field = field_get_items('node', $entity, 'hcp_ref_webinar')) {
    if (isset($field['0']['target_id'])) {
      $row = db_select('webinar', 'w')
              ->fields('w', array('type', 'data'))
              ->condition('wid', $field['0']['target_id'])
              ->execute()
              ->fetchObject();

      $webinar_type = $row->type;
      $webinar_data = unserialize(unserialize($row->data));

      $types = webinar_mediaplatform_webinar_plugin_info();
      $types = array_keys($types);

      if (in_array($webinar_type, $types)) {
        $have_mediaplatform = TRUE;

        // pull in cached webinar properties, or build the cache values
        $cache_key = 'webinar_mediaplatform_' . $entity->nid;
        $webinar_cache = cache_get($cache_key);

        if (!$webinar_cache) {
          $webinar = new \Drupal\webinar\Entity\Webinar(array(
            'type' => $webinar_type,
          ));

          $plugin = $webinar->getPlugin();
          $api = $plugin->getMediaPlatformService();
          if ($api) {
            $project = $api->getProject($webinar_data['mediaplatform_guid']);

            // webinar guid
            $guid = $webinar_data['mediaplatform_guid'];

            // webinar duration value (in seconds)
            $duration = $project->project->event_end_ts - $project->project->event_start_ts;

            $webinar_cache['guid'] = $guid;
            $webinar_cache['duration'] = $duration;

            cache_set($cache_key, $webinar_cache, 'cache', CACHE_TEMPORARY);

            $webinar_cache = cache_get($cache_key);
          }
        }

        if ($webinar_cache->data['guid']) {
          drupal_add_js(array(
            'pfizer_analytics_mediaplatform' => array(
              'webinar_guid' => $webinar_cache->data['guid'],
            ),
          ), 'setting');
        }

        if ($webinar_cache->data['duration']) {
          drupal_add_js(array(
            'pfizer_analytics_mediaplatform' => array(
              'webinar_duration' => $webinar_cache->data['duration'],
            ),
          ), 'setting');
        }
      }
    }
  }

  // still haven't found mediaplatform data? 
  if (!$have_mediaplatform) {
    return;
  }

  // add PFA
  $is_https = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on';
  $protocol = $is_https ? 'https' : 'http';

  drupal_add_js($protocol . '://s3.amazonaws.com/pfanalytics/pfaLoader.js?scope=pfizer_analytics.mediaplatform', 'external');

  // add MediaPlatform-specific JS; we put this in the footer so it runs after pfizer_lift
  drupal_add_js(drupal_get_path('module', 'pfizer_analytics_mediaplatform') . '/pfizer_analytics_mediaplatform.js',
    array(
      'scope' => 'footer',
      'weight' => 1,
    )
  );

  // event launch date
  $date = FALSE;
  if (isset($entity->hcp22_event_date_time['und'][0]['value'])) {
    $date = $entity->hcp22_event_date_time['und'][0]['value'];
  }

  // expose date as a JS setting for use by the same JS that will append base metrics
  drupal_add_js(array(
    'pfizer_analytics_mediaplatform' => array(
      'webinar_date' => $date,
      'webinar_id' => $entity->nid,
    ),
  ), 'setting');

  // pre-2.7 Portal
  if (isset($build['hcp_webinar'])) {
    foreach (array_keys($build['hcp_webinar']['#items']) as $key) {
      $build['hcp_webinar'][$key]['#options']['attributes']['class'][] = 'pfizer_analytics_mediaplatform_lobby_link';
    }
  }
}

/**
 * Implements hook_drupal_goto_alter();
 */
function pfizer_analytics_mediaplatform_drupal_goto_alter(&$path, &$options, &$http_response_code) {
  // catch redirects to mediaplatform and append the query string data
  if (preg_match('#mediaplatform\.com/.+token#', $path)) {
    if (isset($_SERVER['QUERY_STRING'])) {
      $path .= '&' . $_SERVER['QUERY_STRING'];
    }
  }
}

/*----------------------------------------------------------------------------
      CALLBACKS
----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
      INTERNAL / HELPERS
----------------------------------------------------------------------------*/

/**
 * Returns an array of node types applicable to webinar content.
 */
function pfizer_analytics_mediaplatform_webinar_types() {
  return array(
    'online_webinar',
    'on_demand_webinar',
  );
}