<?php
/**
 * @file
 * Pfizer Web Standards GRV Extension Module
 */

/*----------------------------------------------------------------------------
      SETUP & CONFIG
----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
      DRUPAL HOOKS
----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
      MODULE HOOKS
----------------------------------------------------------------------------*/

/**
 * Implements hook_janrain_capture_user_authenticated();
 */
function pfizer_webstandards_grv_janrain_capture_user_authenticated($capture_profile, $account, $new_user) {
  pfizer_webstandards_save_profile($capture_profile);
}

/**
 * Implements hook_janrain_capture_profile_sync();
 */
function pfizer_webstandards_grv_janrain_capture_profile_sync($account, $capture_profile) {
  pfizer_webstandards_save_profile($capture_profile);
}

/**
 * Implements hook_pfizer_grv_user_authenticated();
 */
function pfizer_webstandards_grv_pfizer_grv_user_authenticated($capture_profile, $account, $new_user) {
  pfizer_webstandards_save_profile($capture_profile);
}

/**
 * Implements hook_pfizer_grv_profile_sync();
 */
function pfizer_webstandards_grv_pfizer_grv_profile_sync($account, $capture_profile) {
  pfizer_webstandards_save_profile($capture_profile);
}

/**
 * Implements hook_sitecatalyst_variables().
 *
 * Return the array of variables for GRV. This is for Web Standard
 * implementations that use modules on live sites only - use the JS file for
 * gardens implementations.
 * @todo gardens js file.
 */
function pfizer_webstandards_grv_sitecatalyst_variables() {
  // Initialize a variables array to be returned by this hook.
  $variables = array();

  // janrain.capture.ui.start()
  // registration access code: pfizer123

  $variables['s.eVar10'] = pfizer_webstandards_grv_get('profession');
  $variables['s.eVar11'] = pfizer_webstandards_grv_get('specialty');
  $variables['s.eVar21'] = user_is_logged_in() ? 'logged in' : 'anonymous';
  $variables['s.prop21'] = $variables['s.eVar21'];

  $variables['s.eVar45'] = pfizer_webstandards_grv_get('hcp_id');
  // $variables['s.prop58'] = pfizer_webstandards_grv_get('first_name');
  // $variables['s.prop59'] = pfizer_webstandards_grv_get('last_name');

  $variables['s.prop60'] = pfizer_webstandards_grv_get('guid');

  // Note: eVar 25 appears to be removed from D6 of the Web Standards. Kept
  // in-case it is replaced in a later build:
  // $variables['s.eVar25'] = $variables['s.prop26'] =
  // $GLOBALS['language']->language;
  return array('variables' => $variables);
}

/**
 * Implements hook_preprocess on theme_pfizer_pitcher_field_default().
 *
 * Adds GUID query string argument for use in Pitcher's eDetail player.
 */
function pfizer_webstandards_grv_preprocess_pfizer_pitcher_field_default(&$variables) {
  if (user_is_anonymous()) {
    return;
  }
  
  // bail out if we're not on a node page (this node could be getting loaded in teaser form
  // elsewhere)
  $entity = menu_get_object();
  if (!is_object($entity) || $entity->type !== 'hcp_edetail') {
    return;
  }

  if (!isset($variables['path']['options'])) {
    $variables['path']['options'] = array();
  }

  if (!isset($variables['path']['options']['query'])) {
    $variables['path']['options']['query'] = array();
  }

  $variables['path']['options']['query']['guid'] = pfizer_webstandards_grv_get('guid');
  $variables['path']['options']['query']['hcp_id'] = pfizer_webstandards_grv_get('hcp_id');
}

/*----------------------------------------------------------------------------
      INTERNAL / HELPERS
----------------------------------------------------------------------------*/

/**
 * Returns an array of allowed keys.
 * 
 * @return [type] [description]
 */
function pfizer_webstandards_allowed_keys() {
  $allowed_keys = array(
    'profession',
    'specialty',
    // 'first_name',
    // 'last_name',
    'hcp_id',
    'guid',
  );

  return $allowed_keys;
}

/**
 * Saves the key:value pair in the session.
 * 
 * @param  [type] $key   [description]
 * @param  [type] $value [description]
 * @return [type]        [description]
 */
function pfizer_webstandards_grv_set($key, $value) {
  // make sure user is logged in and key is allowed
  if (!user_is_logged_in() || !in_array($key, pfizer_webstandards_allowed_keys())) {
    return FALSE;
  }

  // save value
  $_SESSION['pfizer_webstandards_grv'][$key] = $value;

  return TRUE;
}

/**
 * Returns the saved value for given key.
 * @param  [type] $key [description]
 * @return [type]      [description]
 */
function pfizer_webstandards_grv_get($key) {
  // make sure key is allowed
  if (!in_array($key, pfizer_webstandards_allowed_keys())) {
    return FALSE;
  }

  // return NULL if not set
  if (!isset($_SESSION['pfizer_webstandards_grv'][$key])) {
    return NULL;
  }

  // return value
  return $_SESSION['pfizer_webstandards_grv'][$key];
}

/**
 * Checks for required profile data and saves it in session.
 * @param  [type] $capture_profile [description]
 * @return [type]                  [description]
 */
function pfizer_webstandards_save_profile($capture_profile) {
  // drupal_set_message('<pre>'.print_r($capture_profile, 1).'</pre>');

  // save profession
  if (isset($capture_profile['designation']['name'])) {
    pfizer_webstandards_grv_set('profession', $capture_profile['designation']['name']);
  }

  // save specialty
  if (isset($capture_profile['designation']['specialty'])) {
    pfizer_webstandards_grv_set('specialty', $capture_profile['designation']['specialty']);
  }

  // save first name
  // if (isset($capture_profile['givenName'])) {
  //   pfizer_webstandards_grv_set('first_name', $capture_profile['givenName']);
  // }

  // save last name
  // if (isset($capture_profile['familyName'])) {
  //   pfizer_webstandards_grv_set('last_name', $capture_profile['familyName']);
  // }

  // save HCP ID; use hcpId when it's available and not empty, otherwise default
  // to uuid;
  if (isset($capture_profile['hcpId']) || isset($capture_profile['uuid'])) {
    if (isset($capture_profile['hcpId']) && !empty($capture_profile['hcpId'])) {
      pfizer_webstandards_grv_set('hcp_id', $capture_profile['hcpId']);
    }
    else {
      pfizer_webstandards_grv_set('hcp_id', $capture_profile['uuid']);
    }
  }

  // save GUID
  if (isset($capture_profile['guid'])) {
    pfizer_webstandards_grv_set('guid', $capture_profile['guid']);
  }
}

