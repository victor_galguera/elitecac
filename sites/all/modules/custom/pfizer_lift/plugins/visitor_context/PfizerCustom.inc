<?php
/**
 * @file
 * Provides a visitor context plugin for Custom Pfizer data.
 */

class PfizerCustom extends PersonalizeContextBase {
  /**
   * Implements PersonalizeContextInterface::create().
   */
  public static function create(PersonalizeAgentInterface $agent = NULL, $selected_context = array()) {
    return new self($agent, $selected_context);
  }

  /**
   * Implements PersonalizeContextInterface::getOptions().
   */
  public static function getOptions() {
    $options = array();

    $options['auth_type']           = array('name' => t('Authentication Type'),);
    $options['brand_name']          = array('name' => t('Brand name'),);
    $options['brand_id']            = array('name' => t('Brand ID'),);
    $options['build_number']        = array('name' => t('Build number'),);
    $options['campaign_country']    = array('name' => t('Campaign Country'));
    $options['content_source']      = array('name' => t('Content Source'));
    $options['customer_id']         = array('name' => t('Customer ID'),);
    $options['epermission']         = array('name' => t('Epermission'),);
    $options['featured']            = array('name' => t('Featured content'),);
    $options['guid']                = array('name' => t('GUID'),);
    $options['logged_in']           = array('name' => t('Logged in or not'),);
    $options['language']            = array('name' => t('Language'),);
    $options['medical_conditions']  = array('name' => t('Medical Conditions'),);
    $options['platform']            = array('name' => t('Platform'));
    $options['profession']          = array('name' => t('Profession'));
    $options['specialty']           = array('name' => t('Specialty'));

    foreach ($options as &$option) {
      $option['group'] = t('Pfizer');
    }

    return $options;
  }
}
