<?php
/**
 * @file
 * pfizer_webstandards_hcp.default_pfizer_webstandards_urls.inc
 */

/**
 * Implements hook_default_pfizer_webstandards_url().
 */
function pfizer_webstandards_hcp_default_pfizer_webstandards_url() {
  $export = array();

  $url = new stdClass();
  $url->disabled = FALSE; /* Edit this to true to make a default url disabled initially */
  $url->api_version = 1;
  $url->machine_name = 'brand_name_toviaz';
  $url->weight = -46;
  $url->url = '*/product/toviaz/*
*/product/toviaz';
  $url->variables = 'a:1:{s:6:"prop42";s:23:"1-230522026:1-230522037";}';
  $export['brand_name_toviaz'] = $url;

  $url = new stdClass();
  $url->disabled = FALSE; /* Edit this to true to make a default url disabled initially */
  $url->api_version = 1;
  $url->machine_name = 'brand_name_viagra';
  $url->weight = -45;
  $url->url = '*/product/viagra/*
*/product/viagra';
  $url->variables = 'a:1:{s:6:"prop42";s:23:"1-230522026:1-230522046";}';
  $export['brand_name_viagra'] = $url;

  $url = new stdClass();
  $url->disabled = FALSE; /* Edit this to true to make a default url disabled initially */
  $url->api_version = 1;
  $url->machine_name = 'hcp_fallback';
  $url->weight = -50;
  $url->url = '*';
  $url->variables = 'a:9:{s:6:"eVar27";s:7:"1038774";s:6:"eVar28";s:7:"WEBSITE";s:6:"eVar54";s:8:"PBTTest9";s:6:"prop28";s:7:"1038774";s:6:"prop29";s:7:"WEBSITE";s:6:"prop41";s:3:"PBT";s:6:"prop42";s:23:"1-230522026:1-230522109";s:6:"prop43";s:5:"VISIT";s:6:"prop54";s:8:"PBTTest9";}';
  $export['hcp_fallback'] = $url;

  return $export;
}
